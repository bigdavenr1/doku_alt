<?php
namespace inc;
$start = microtime(true);
// Config einbinden
include_once("inc/config.php");
// $_GET $_POST $_COOKIE $_FILES zusammenfasen
$request = array_merge($_GET, $_POST, $_COOKIE, $_FILES);
// Controller erstellen
$controller = new \inc\Controller($request);
//$end = microtime(true);
// Inhalt der Webanwendung ausgeben.
echo $controller->display();


//echo 'Dauer: ' . round(($end-$start)*1000).'ms';
