<?php #############################
# nbCMS Kernel V1.0               #
# erstellt 01/2011 von Tino Stief #
# copyright nb-cooperation        #
#                                 #
# Basis Konfigurationmsdatei      #
# für das Framework               #
#                                 #
# zugehörige Dateien              #
# - inc/stdlink.class.php.php     #
# - inc/std/basicdb.class.php     #
# - inc/std/dbconnect.class.php   #
###################################
ini_set('date.timezone', 'Europe/Berlin');
date_default_timezone_set('Europe/Berlin');
header("Content-Type: text/html; charset=utf-8"); // Gesamte seiten auf UTF8 setzen
error_reporting(0);
$root = "/";
define('WEBDIR', $root);
define('LOCALDIR', $_SERVER['DOCUMENT_ROOT'].$root);
define('ADMINDIR', $_SERVER['DOCUMENT_ROOT'].$root."admin/");
define('INCLUDEDIR', $_SERVER['DOCUMENT_ROOT'].$root."/inc/");
define('SALT',sha1('wInegPww.A8mfgedi3PWh!'));
include(INCLUDEDIR."std/func.class.php");

$func = new \inc\Func();
if($func->isInUrl("404"))
   header("HTTP/1.0 404 Not Found");
function shutdown()
{
    global $func;
    $noerror=array(2,8,128,512,1024,2048,8192,16384);
    $a=error_get_last();

    if ($a['message']!="Directive 'magic_quotes_gpc' is deprecated in PHP 5.4 and greater")
        if($a!==null):
            if(in_array($a['type'],$noerror)):
                $func->savePHPException($a['type'].":".$a['message'],1,$a['file']." : ".$a['line']);
            else:
                $func->savePHPException($a['type'].":".$a['message'],1,$a['file']." : ".$a['line'],WEBDIR.'error.html');
            endif;
        endif;

}
function exception_error_handler($errno, $errstr, $errfile, $errline) {
    global $func;
    if ($errno!=8)
        $func->savePHPException($errstr,$errno,$errfile." : ".$errline);
}
register_shutdown_function('shutdown');
set_error_handler("exception_error_handler");
mb_internal_encoding('UTF-8'); // Alle mb_* Funktionen sollen UTF-8 verwenden
ini_set('display_errors', 1);    // Fehler anzeigen
ini_set("memory_limit",'264M');  // Maximales Speicherlimit zu Bildbearbeitung raufsetzen
ini_set("post_max_size",'264M'); // Maximale Größe von POST-Werten heraufsetzen
setlocale(LC_TIME, "de_DE@euro", "de_DE", "deu_deu");
setlocale(LC_ALL,"de_DE.UTF8"); // Datumsangaben in PHP auf Deutsch-UTF8 setzen
ini_set('session.use_cookies'     ,1); // sicherlich!!
ini_set('session.use_only_cookies',1); // JA! Ohne Cookies geht hier nix!!
ini_set('session.use_trans_sid'   ,0); // bloss nicht auf 1 setzen
ini_set('session.cache_expire',3600); // Sessioncache auf 1 Stunde erhöhen
ini_set('session.cookie_lifetime',3600); // Lebenszeit des Cookies
ini_set('session.cookie_httponly',true);
ini_set('session.gc_probability', 1); //Mit der Wahrscheinlichkeit 1 (100%) für GarbageCollector (gc)
ini_set('session.gc_divisor', 100); //Die Wahrscheinlichkeit errechnet sich aus gc_probability/gc_divisor. 1/100 bedeutet z.B., dass die GC-Routine nach der Zeit x Maxtime bei jeder Anfrage mit einer Wahrscheinlichkeit von 1% gestartet wird
ini_set('session.gc_maxlifetime', 3600); // DAuer bis Garbagecolector alles aufräumt
// D I R S
// WICHTIG: Framework braucht $root
////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* activate reporting */

// Maximale Bildgröße Definieren
define('DEBUG',TRUE);


define('PICTURE_MAX_X', '480');
define('PICTURE_MAX_Y', '600');
define('PICTURE_MAX_THUMB_BIG_X','240');
define('PICTURE_MAX_THUMB_BIG_Y','300');
define('PICTURE_MAX_CROP_X','360');
define('PICTURE_MAX_CROP_Y','450');
define('PICTURE_MAX_THUMB_X','120');
define('PICTURE_MAX_THUMB_Y','150');
define('PICTURE_ASPECT','scaleToRatioOverflowHidden');
define('PICTURE_CROP_ASPECT','maxWidthOrHeightNoCrop');
define('PICTURE_THUMB_BIG_ASPECT','scaleToRatioOverflowHidden');
define('PICTURE_THUMB_ASPECT','scaleToRatioOverflowHidden');

define('SLIDER_MAX_X', '1920');
define('SLIDER_MAX_Y', '502');
define('SLIDER_MAX_THUMB_BIG_X','480');
define('SLIDER_MAX_THUMB_BIG_Y','126');
define('SLIDER_MAX_CROP_X','640');
define('SLIDER_MAX_CROP_Y','640');
define('SLIDER_MAX_THUMB_X','240');
define('SLIDER_MAX_THUMB_Y','63');
define('SLIDER_ASPECT','scaleToRatioOverflowHidden');
define('SLIDER_CROP_ASPECT','maxWidthOrHeightNoCrop');
define('SLIDER_THUMB_BIG_ASPECT','scaleToRatioOverflowHidden');
define('SLIDER_THUMB_ASPECT','scaleToRatioOverflowHidden');

define ('maxbild',"1024");
define ('maxbildthumbbig','980');
define ('maxbildthumb','42');
define ('MAXMENUDEEP','1');

// Formularvariablen
$sel='selected="selected"';
$chk='checked="checked"';
$pre = "&raquo;&nbsp;";

$widget = strstr($_SERVER['PHP_SELF'], '/widgets/');
$widget = strstr($_SERVER['PHP_SELF'], '/pdw_file_browser/');

$showsubmenu[]="footermenu";

// Icons
////////////////////////////////////////////////////////////////////////////////////////////////////////////
$icon_refresh = '<img src="'.WEBDIR.'images/icons/action_refresh.gif" alt="Status ändern" title="Status ändern" />';
$icon_accept = '<img src="'.WEBDIR.'images/icons/accept.png" alt="Eintrag akzeptieren" title="Eintrag akzeptieren" />';
$icon_edit_small = '<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />';
$icon_details_small = '<img src="'.WEBDIR.'images/icons/page_find.gif" alt="Bearbeiten" title="Details" />';
$icon_delete_small = '<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />';
$icon_neu_small = '<img src="'.WEBDIR.'images/icons/note_new.gif" alt="Neuen Datensatz anlegen" title="Neuen Datensatz anlegen" />';
$icon_up_small = '<img src="'.WEBDIR.'images/icons/arrow_up_small.gif" alt="Sortierung aufwärts" title="Sortierung aufwärts" />';
$icon_down_small = '<img src="'.WEBDIR.'images/icons/arrow_down_small.gif" alt="Sortierung abwärts" title="Sortierung abwärts" />';
$icon_up_small_red = '<img src="'.WEBDIR.'images/icons/arrow_up_small_red.gif" alt="aufwärts sortiert" title="aufwärts sortiert" />';
$icon_down_small_red = '<img src="'.WEBDIR.'images/icons/arrow_down_small_red.gif" alt="abwärts sortiert" title="abwärts sortiert" />';
$icon_changelog_small = '<img src="'.WEBDIR.'images/icons/bookmark.gif" alt="Lieferstellen ansehen und bearbeiten" title="Lieferstellen ansehen und bearbeiten" />';
$icon_up_red = '<img src="'.WEBDIR.'images/icons/s_asc.gif" alt="hochschieben" title="hochschieben" />';
$icon_down_red = '<img src="'.WEBDIR.'images/icons/s_desc.gif" alt="runterschieben" title="runterschieben" />';

// D A T E N B A N K
////////////////////////////////////////////////////////////////////////////////////////////////////////////

//$sql["host"] = "dd14200.kasserver.com";
//$sql["db"]   = "d017f15b";
//$sql["user"] = "d017f15b";
//$sql["pass"] = "bolygo";


/*$sql["host"] = "localhost";
$sql["db"]   = "usr_web1581_2";
$sql["user"] = "web1581";
$sql["pass"] = "H3NWmGM1";*/

$sql["host"] = "dd14200.kasserver.com";
$sql["db"]   = "d02695bb";
$sql["user"] = "d02695bb";
$sql["pass"] = "N3IeTv!0F.";


$sql["table_sprache"] = "kta_languages";
$sql["table_benutzer"] = "kta_users";

$con=null;
// K O N S T A N T E N
////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Title - Meta-Keywords - Meta-Description
////////////////////////////////////////////////////////////////////////////////////////////////////////////

$CONST_META[0]["url"] = "default";
$CONST_META[0]["title"] = "Dokumentation - natürliches Erleben UG";
$CONST_META[0]["keywords"] = "";
$CONST_META[0]["description"] = "";

// Email
////////////////////////////////////////////////////////////////////////////////////////////////////////////

$CONST_MAIL['from'] = "info@nb-cooperation.de";
$CONST_MAIL['an']   = "info@nb-cooperation.de";
$CONST_MAIL['cc']   = "";
$CONST_MAIL['bcc']  = "";

// Sonstige Variablen für das Framework
///////////////////////////////////////////////////////////////////////////

// S E S S I O N - I N I T I A L I S I E R U N G
// Muss vor den Klassen initialisiert werden
////////////////////////////////////////////////////////////////////////////////////////////////////////////

session_start();
// Prüfen ob Session gestohlen
if (($_SESSION['user']->SESSION_SANITY_KEY!=md5(substr($_SERVER['REMOTE_ADDR'],0,7).$_SERVER['DOCUMENT_ROOT'].$_SERVER['HTTP_USER_AGENT']) && empty($widget)) && (!empty($_SESSION['user']))):
    session_destroy();
    session_start();
endif;

if (empty($widget)):
    session_regenerate_id();
endif;

// S T A N D A R D - I N C L U D E S
////////////////////////////////////////////////////////////////////////////////////////////////////////////

include(INCLUDEDIR."std/basicdb.class.php");
// include(INCLUDEDIR."std/sprache.class.php");
include(INCLUDEDIR."std/link.class.php");
include(INCLUDEDIR."std/meta.class.php");
include(INCLUDEDIR.'std/view.php');
include(INCLUDEDIR.'std/controller.php');
include(INCLUDEDIR."std/menu/menucontroller.class.php");
// include(INCLUDEDIR."std/textparser.class.php");
include(INCLUDEDIR."std/modul.class.php");
// include(INCLUDEDIR."std/tpl.class.php");
include(INCLUDEDIR."std/email/controller.php");

// I N I T I A L I S I E R T E   O B J E K T E
////////////////////////////////////////////////////////////////////////////////////////////////////////////


$l=new \SessionLink();

// U S E R - V E R W A L T U N G
// R E C H T E - V E R W A L T U N G
////////////////////////////////////////////////////////////////////////////////////////////////////////////

// entscheiden ob User ihre Daten wie name ändern dürfen
$userCanChange=TRUE;

// ADMIN + Eingeschränkte Rechte für User
if ($func->isInUrl("/admin/") && $_SESSION["user"]->usertype != 1 && $_SESSION["user"]->usertype != 2 ):
    session_destroy();
    header("Location: ".WEBDIR."login.htm");
    exit;
elseif ($func->isInUrl("/user/") && !$_SESSION["user"] ):
    session_destroy();
    header("Location: ".WEBDIR."login.htm");
    exit;
elseif(($_GET['modul']=='startseite' ) &&  ($_SESSION["user"]->usertype == 1 || $_SESSION["user"]->usertype == 2) ):
    header("Location: ".WEBDIR."admin.htm");
    exit;
elseif(($_GET['modul']=='startseite') && $_SESSION["user"]):
    header("Location: ".WEBDIR."user.htm");
    exit;
elseif(!$_SESSION["user"] && !$func->isInUrl('login')  && !$func->isInUrl('pwreminder') && $_GET['modul']!="cronjob"):
	session_destroy();
    header("Location: ".WEBDIR."login.htm");
    exit;
endif;

// Sprachenauswahl
/*if (!empty($_GET['lang']))
{
    $langfac =new Sprache();
    $langfac->getLangByCode($_GET['lang']);
    $lang=$langfac->getElement();
    $_SESSION['lang']=$lang->id;
}
if (!$_SESSION['lang'])
{
    $_SESSION['lang']=2;
}*/
?>