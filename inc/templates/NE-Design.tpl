<!DOCTYPE html>
<html dir="ltr" lang="de">
	<head>
		<title><?php echo $this->_['pagetitle']?></title>
		<meta charset="UTF-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<link href="<?php echo WEBDIR?>images/favicon.png" rel="icon" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- CSS Part Start-->
		<link rel="stylesheet" type="text/css" href="<?php echo WEBDIR?>style/style.css" />


		<script type="text/javascript" >
		    var err='<?php echo $this->_['err']?>';
		    var msg='<?php echo $this->_['msg']?>';
		    var basedir='/';

		</script>

		<!--slider-->
		<link href="<?php echo WEBDIR?>style/slider.css" rel="stylesheet" type="text/css" media="all"/>
		<link href="<?php echo WEBDIR?>style/helper.css" rel="stylesheet" type="text/css" media="all"/>

		<!-- CSS Part End-->
		<!-- JS Part Start-->
		<script type="text/javascript" src="<?php echo WEBDIR?>script/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="<?php echo WEBDIR?>script/jquery-migrate-1.2.1.js"></script>
		<script type="text/javascript" src="<?php echo WEBDIR?>script/helper.min.js"></script>
		<script type="text/javascript" src="<?php echo WEBDIR?>script/placeholder.min.js"></script>
		<script type="text/javascript" src="<?php echo WEBDIR?>script/html5.js"></script>
		<script type="text/javascript" src="<?php echo WEBDIR?>script/jquery.nivo.slider.js"></script>
		<script type="text/javascript">
		    $(window).load(function() {
		        $('#slider').nivoSlider();
		    });
	    </script>

		<!-- JS Part End-->
	</head>
	<body>
		<div class="header">
	  		<div class="wrap">
	    		<div class="header-top">
	         		<div class="logo">
	             		<h1><a href="<?php echo WEBDIR?>">Dokumentation - NE UG</a></h1>
	         		</div>
	        		<div id="text-6" class="visible-all-devices header-text ">
	            		<div class="textwidget">NUR FÜR DEN INTERENEN GEBRAUCH<br>
			                <br>
			                <br>
			                Bei Fragen wenden Sie sich bitte an: <br>
			                <h5 class="phone"> 0351 / 4 84 84 510</h5>
			                <h5> <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#100;&#101;&#110;&#105;&#115;&#101;&#46;&#99;&#115;&#105;&#122;&#109;&#97;&#100;&#105;&#97;&#64;&#110;&#97;&#116;&#117;&#101;&#114;&#108;&#105;&#99;&#104;&#101;&#115;&#45;&#101;&#114;&#108;&#101;&#98;&#101;&#110;&#46;&#100;&#101;">&#100;&#101;&#110;&#105;&#115;&#101;&#46;&#99;&#115;&#105;&#122;&#109;&#97;&#100;&#105;&#97;&#64;&#110;&#97;&#116;&#117;&#101;&#114;&#108;&#105;&#99;&#104;&#101;&#115;&#45;&#101;&#114;&#108;&#101;&#98;&#101;&#110;&#46;&#100;&#101;</a></h5>
	                	</div>
	       		 	</div>
	         		<div class="clear"></div>
		       	</div>
			</div>
		</div>
	      <!------ Slider ------------>
	   	<div class="banner">
	      	<div class="wrap">
	           	<div class="cssmenu">
	           	<?php echo $this->_['publicmenu']?>
	           	   <?php echo $this->_['usermenu']?>
	           	   <?php echo $this->_['admin_menu']?>


	                <?php /*<ul>
	                   <li class="active"><a href="index.html"><span>Home</span></a></li>
	                   <li><a href="about.html"><span>About us</span></a></li>
	                    <li><a href="products.html"><span>Products</span></a></li>
	                   <li class="has-sub"><a href="services.html"><span>Services</span></a></li>
	                   <li class="last"><a href="contact.html"><span>Contact</span></a></li>
	                    <div class="clear"></div>
	                 </ul>*/ ?>
	                 <br class="clr"/>
	        	</div>
	    	</div>
	   	</div>
	        <!------End Slider ------------>
	  	<div class="main">
	     	<div class="wrap">

		        	<?php echo $this->_['content']?>



		    </div>
	    </div>

	    <div class="footer">
	        <div class="wrap">
	            <div class="footer-text">
	                <h2>HINWEIS</h2>
	                <p>Diese Dokumentation ist nur zur internen Verwendung freigegeben. Jegliche Veröffentlichung oder Weitergabe von Zugangsdaten ist verboten! </p>
	                <br/><br/>
	                <div class="copy">
	                    <p> © 2014 NE natürliches-erleben UG</p>
	                </div>
	            </div>
	        </div>
	    </div>
	</body>
</html>