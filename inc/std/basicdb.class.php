<?php
namespace inc;
/**
* nbCMS Kernel V1.0
* erstellt 01/2011 von Tino Stief
* copyright nb-cooperation
*
* Basis-Datenbankabfrageklasse
* für verschiedene Aktionen
*
* zugehörige Dateien
* - inc/config.php
* - inc/std/dbconnect.class.php
* - inc/std/link.class.php
 *
 * Grundlage Array $queryvars
 * folgende Optionen sind möglich
 * @param Array $queryvars->fields = Array ($key=>$value) -> Felder die abgefragt werden sollen. Key ist Feldname, Value ist Wert.
 * Wert kann ebenfalls ein Array sein. array('plainSQL'=>$value). Damit wird der Wert nicht im prepared Statement als Platzhalter sondern direkt
 * als Wert interpretiert.
 * @param String $queryVars->clause -> Einschränkung der Suche
 * @param string $queryVars->table -> Tabelle die abgefragt werden soll
 * @param array $queryVars->join = array  -> Join Variablen
*/

require_once("dbconnect.class.php");
class Basicdb extends \Dbconnect
{
     var $pointer = 0;
     var $liste = Array();
    /*var $tempanz=0;
    var $func;
    // Tabelle und Query

    var $result;
    var $id;
    var $field = Array();
    var $felder = false;    // Variable für JOINS
    // Listing


    // Navigator
    var $anzahl;
    var $offsetname="offset";
    var $offset=0;
    var $offsetaffix;
    var $seiten;
    var $countvar=false;
    var $lastid;
    var $debug=false;*/
    /**
     * Konstruktor
     */
    protected function __construct()
    {
        $tempanz;
        global $root;
        global $con;
        global $func;
        parent::__construct();
        $this->con=$con;
        $this->func=$func;
    }
	/**
	 * Methode zum Sperren von Tabellen
	 * @param array $param Array mit den zu sperrenden TAbellen und deren Sperrmnethode
	 */
	 protected function lockTables(array $param)
	 {
	 	$sql='LOCK TABLES kta_log WRITE,';
		foreach($param AS $table => $method):
			$sql.=$table.' '.$method.",";
		endforeach;
		$sql = substr($sql,0,-1);
		try
		{
			// Statement preparen
            $query = $this->con->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
			// Statement ausführen
            $this->result = $query->execute(array());
		}
		// Wenn PDO-Fehler
        catch(\PDOException $e )
        {
           $this->func->savePDOException($e->getMessage(),$e->getCode(),nl2br($sql).'<br/><br/>'. nl2br($e->getTraceAsString()),WEBDIR."status_msg/db_error");
        }
	 }

     private function checkTable($table)
     {

        if (is_array($table)):
            foreach($table AS $tbl):
                $check=$this->checkTable($tbl->name);
                if (empty($check)):
                    return false;
                    exit;
                endif;
            endforeach;
            return true;
        else:
            $sql="show tables like :TABLE";
            try
            {
                // Statement preparen
                $query = $this->con->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
                // Statement ausführen
                $result = $query->execute(array(':TABLE'=>$table));

                $erg = $query->fetch(\PDO::FETCH_ASSOC);
                return !empty($erg);

            }
            // Wenn PDO-Fehler
            catch(\PDOException $e )
            {
               $this->func->savePDOException($e->getMessage(),$e->getCode(),nl2br($sql).'<br/><br/>'. nl2br($e->getTraceAsString()),WEBDIR."status_msg/db_error");
            }
        endif;
     }

	 /**
	  * Methode zum Löschen eines LOCKS auf TAbellen
	  */
	  protected function unlockTables()
	  {
		 try
		 {
			// Statement preparen
            $query = $this->con->prepare('UNLOCK TABLES', array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
			// Statement ausführen
            $this->result = $query->execute(array());

		 }
		 // Wenn PDO-Fehler
         catch(\PDOException $e )
         {
           $this->func->savePDOException($e->getMessage(),$e->getCode(),nl2br($sql).'<br/><br/>'. nl2br($e->getTraceAsString()),WEBDIR."status_msg/db_error");
         }
	  }
     /**
     * Funktion zum Erstellen von Prepared statements und zum Datensatz zählen
     * @param object $queryVArs -> Object mit allen notwendigen Informationen
     * @param $countQuery -> Gesamtanzahl gefundener Datensätze (Wenn erst gezählt werden soll, dann PArameter false)
     * @return array $this->liste => Ergebnis Array mit Liste von Objekten
     */
    protected function createQuery($queryVars, $countQuery=false,$debug=false)
    {
        $this->cps=$queryVars->cps;
        $this->pointer = 0;
        // Initialisierung von Queryelementen
        $fieldQuery='';
        $joinQuery='';

        // Felder zusammenbauen
        if ( !empty( $queryVars->fields ) ):
            // Felder durchlaufen und an Statement anhängen
            foreach( $queryVars->fields as $value )
                $fieldQuery .= $value.", ";
            // letztes Komma mit Leerzeichen entfernen
            $fieldQuery = substr( $fieldQuery, 0, -2 );
        // Wenn keine Felder übergeben wurden, dann "*"
        else:
            $fieldQuery=" * ";
        endif;

        // Joins Zusammenbauen
        if ( !empty( $queryVars->joins ) ):
            // Felder durchlaufen und an Statement anhängen
            foreach( $queryVars->joins as $value ):
                $joinQuery .= $value->type . " JOIN " . $value->table . " ON (" . $value->clause . ") ";
            endforeach;
        endif;

        // Wenn sortiert werden soll, dann sortieren
        if (!empty($queryVars->order->field))
            $order = 'ORDER BY '. $queryVars->order->field." ".$queryVars->order->direction;

        // Offset herausfiltern Wenn keisn angegeben, dann Seite0
        $this->offsetname = $queryVars->offsetname;
        $this->offset = empty( $_REQUEST[ $queryVars->offsetname ] ) ? 0 : $_REQUEST[ $queryVars->offsetname ];

        // Ausführung des Statements
        try
        {
            // Wenn noch nicht gesamtanzahl gezählt wurde -> zählen
            if ($countQuery===false):
                $check=$this->checkTable($queryVars->table);
                if(empty($check)):
                    $this->func->savePDOException('Tabelle nicht vorhanden','666',json_encode($queryVars),WEBDIR."status_msg/db_error");
                    return false;
                else:
                     if(is_array($queryVars->table)):
                        foreach($queryVars->table AS $tbl)
                            $tbl_string.=$tbl->name." ".$tbl->alias.",";
                        $queryVars->table=substr($tbl_string,0,-1);
                    endif;
                    if (!empty($queryVars->countVar)):
                        //var_dump($countQuery);
                        //$countQuery->execute($queryVars->preparedVars);
                        // Query zusammenbasteln
                        $sql = "SELECT COUNT( DISTINCT ".$queryVars->countVar. ") rows FROM ".$queryVars->table." ".$joinQuery." ".(!empty($queryVars->clause)?" WHERE ".$queryVars->clause:'');

                        // Statement preparen
                        $countQuery = $this->con->prepare($sql,array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
                        // Statement ausführen
                        $countQuery->execute($queryVars->preparedVars);
                        // Anzahl zurückgeben
                        $this->count = $countQuery->fetch(\PDO::FETCH_OBJ);
                    else:
                        $sql = "SELECT SQL_CALC_FOUND_ROWS ".$fieldQuery." FROM ".$queryVars->table." ".$joinQuery." ".(!empty($queryVars->clause)?" WHERE ".$queryVars->clause:'')." ".(!empty($queryVars->groupBy)?" GROUP BY ".$queryVars->groupBy:'')." ".$order;
                        $query = $this->con->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
                        // Statement ausführen
                        $this->result = $query->execute($queryVars->preparedVars);
                        $sql="SELECT FOUND_ROWS() rows";
                        // Statement preparen
                        $countQuery = $this->con->prepare($sql,array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
                        // Statement ausführen
                        $countQuery->execute(array());
                        // Anzahl zurückgeben
                        $this->count = $countQuery->fetch(\PDO::FETCH_OBJ);

                    endif;

                    // Global zur Verfügung stellen
                    $this->rows = $this->count->rows;
                    // count per Sites => DAtensatzanzahl
                    $cps = ($queryVars->cps==false) ? $this->rows : $queryVars->cps;
                    // Anzahl der Seiten ermitteln
                    if (!empty($this->rows))
                        $this->sites = ceil($this->rows/$cps);
                    else
                        $this->sites=0;
                    // Rekursives Aufrufen mit Übergabe der ermittelten Datensätze
                    $this->createQuery($queryVars, $this->rows,$debug);
                endif;
            // Eigentliche Daten wählen
            else:
                // Counts per Sites
                $cps = ($queryVars->cps==false) ? $countQuery : $queryVars->cps;
                // Seitenanzahl ermitteln
                if (!empty($this->rows))
                    $this->sites = ceil($countQuery/$cps);
                else
                    $this->sites=0;

                // Count setzen (Gesamtanzahl, vorher ermittelt)
                $this->count = $countQuery;
                // sql-Statement beginnen
                $sql = "SELECT SQL_CALC_FOUND_ROWS ".$fieldQuery." FROM ".$queryVars->table." ".$joinQuery." ".(!empty($queryVars->clause)?" WHERE ".$queryVars->clause:'')." ".(!empty($queryVars->groupBy)?" GROUP BY ".$queryVars->groupBy:'')." ".$order." LIMIT ".($this->offset*$queryVars->cps).",".$cps;
                // Statement preparen
                $query = $this->con->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
                // Statement ausführen
                $this->result = $query->execute($queryVars->preparedVars);

                $this->pointer = 0;
                // Liste zum zurückgeben speichern
                $this->liste = $query->fetchAll(\PDO::FETCH_OBJ);
            endif;
       }
       // Wenn PDO-Fehler
       catch(\PDOException $e )
       {
           $this->func->savePDOException($e->getMessage(),$e->getCode(),nl2br($sql).'<br/><br/>'. nl2br($e->getTraceAsString()),WEBDIR."status_msg/db_error");
       }
       if($debug==true &&  is_numeric($countQuery)):
           echo "<pre>";
           print_r( $query);
           echo "</pre>";
       endif;
       return $this->liste;
    }

    ##########################################
    # Funktion zum Schreiben von Datensätzen #
    ##########################################
    public function write( $queryVars )
    {
        $keys='';
        $values='';
        if (!empty($queryVars->data) && is_array($queryVars->data)):
            foreach($queryVars->data AS $key => $value):
                $keys.=(!empty($keys)?', ':'')."`". $key . "`";
                !is_array($value) ? $pvalues[':' . $key] =  $value:'';
                $values .= ( !empty($values) ? ', ' : '').
                           ( is_array($value) ? $value['plainSQL'] : ':' . $key );
                if($queryVars->duplicateUpdate==true)
                   $duplicateval.=(!empty($duplicateval)?', ':'')."`". $key."`= ". (is_array($value)?$value['plainSQL']:":" . $key);
            endforeach;
            try
            {
                // sql-Statement beginnen
                $sql = "INSERT INTO " . $queryVars->table . '(' . $keys . ') VALUES ( ' . $values . ' )' ;
                if($queryVars->duplicateUpdate==true)
                    $sql.=' ON DUPLICATE KEY UPDATE '.(empty($queryVars->noCallbackLastId)?'id = LAST_INSERT_ID(id), ':'')." ".$duplicateval;

                // Statement preparen
                $query = $this->con->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
                if (isset( $queryVars->preparedVars ) && isset( $pvalues ) ):
                    $pvalues=array_merge($pvalues,(array) $queryVars->preparedVars);
                endif;
                // Statement ausführen
                $this->result = $query->execute($pvalues);

                if($this->result !== FALSE):// letzte Id heraussuchen
                    $this->lastid=$this->con->lastInsertId();
                    $this->writeLog( "new", $this->lastid, empty($_SESSION['user'])? 1 : $_SESSION['user']->id , $queryVars->table);
                endif;


            }
            catch( \PDOException $e )
            {
                if($e->getCode()=="23000")
                    $err=array("error"=>1,
                               "code"=>"23000",
                               "message"=>"Doppelter Eintrag. Eintrag schon vorhanden!",
                               "result"=>$this->result,
							   "query"=>$query,
							   "con"=>$this->con,
							   "values"=>$pvalues,
							   "code"=>$e->getCode(),
							   "msg"=>$e->getMessage(),
                               "sql"=>$sql);
                else
                    $this->func->savePDOException($e->getMessage(),$e->getCode(),nl2br($sql).'<br/><br/>'. nl2br($e->getTraceAsString()),WEBDIR."status_msg/db_error");

            }
        endif;
        if(!empty($err))
            return $err;
        else
            return $query;
    }
    ########################################
    # Funktion zum updaten von Datensätzen #
    ########################################
    public function update($queryVars)
    {
        $keys='';
        $values='';
        $pvalues=array();
        if (!empty($queryVars->data) && is_array($queryVars->data)):
            foreach($queryVars->data AS $key => $value):
                !is_array($value) ? $pvalues[':'.$key] = $value : '';
                $values .= (!empty($values)?', ':'').
                          (is_array($value)?'`'.$key.'`='.$value['plainSQL']:'`'.$key.'`= :' . $key);
            endforeach;
            try
            {
                // sql-Statement beginnen
                $sql = "UPDATE " . $queryVars->table . ' SET ' . $values .' WHERE ' . $queryVars->clause;
                // Statement preparen
                $query = $this->con->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
                // Statement ausführen
                if (isset( $queryVars->preparedVars ) && isset( $pvalues ) ):
                    $pvalues=array_merge($pvalues,(array) $queryVars->preparedVars);
                endif;
                $this->result = $query->execute($pvalues);

                if($this->result !== FALSE):
                    // letzte Id heraussuchen
                    $this->lastid=$this->con->lastInsertId();
                    $this->writeLog( "update", $queryVars->clause, empty($_SESSION['user'])? 1 : $_SESSION['user']->id, $queryVars->table );
                endif;

                $query->rowcount= $this->result->rowCount();

            }
            catch( \PDOException $e )
            {
                if($e->getCode()=="23000")
                    $err=array("error"=>1,
                               "code"=>"23000",
                               "message"=>"Doppelter Eintrag. Eintrag schon vorhanden!");

                else
                $this->func->savePDOException($e->getMessage(),$e->getCode(),nl2br($sql).'<br/><br/>'. nl2br($e->getTraceAsString()),WEBDIR."status_msg/db_error");
            }
        endif;
        if(!empty($err))
            return $err;
        else
            return $query;
    }

    ##########################################
    # Funktion zum Löschen eines Datensatzes #
    ##########################################
    public function deleteElement($queryVars)
    {

        // Nur Ausführen wenn eine Bedingung mitgegeben wurde... Sonst wirtd komplette TAbelle gelöscht
        if (!empty($queryVars->clause)):

            try
            {
                // sql-Statement beginnen
                $sql = "DELETE FROM " . $queryVars->table  . ' WHERE ' .$queryVars->clause;
                // Statement preparen
                $query = $this->con->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
                // Statement ausführen
                $this->result = $query->execute($queryVars->preparedVars);
                if($this->result !== FALSE):
                    $this->writeLog( "delete", $queryVars->clause, empty($_SESSION['user'])? 1 : $_SESSION['user']->id, $queryVars->table );
                endif;


            }
            catch( \PDOException $e )
            {

                    $this->func->savePDOException($e->getMessage(),$e->getCode(),nl2br($sql).'<br/><br/>'. nl2br($e->getTraceAsString()),WEBDIR."status_msg/db_error");
            }
        endif;
        if(!empty($err))
            return $err;
        else
            return $this->query;
    }
    #########################################
    # Funktion zur Ausgabe aller Datensätze #
    #########################################
    public function getAll($table, $clause=false,$anzahl=false,$orderfield=false,$direction=false,$offsetname=false)
    {
        $queryVars=(object) array('table'=>$table,
                                  'preparedVars'=>array(),
                                  'offsetname'=>(empty($offsetname)?'offset':$offsetname),
                                  'clause'=>$clause,
                                  'cps'=>$anzahl,
                                  'order'=>(object) array('field'=>$orderfield,
                                                          'direction'=>$direction));

        return $this->createQuery($queryVars);
    }
    #########################################################################################
    # Funktion zum holen eines DAtensatzes welches einid-Feld haben (Feld muss "id" lauten) #
    #########################################################################################
    public function getById($id, $table)
    {
        $queryVars=(object) array('table'=>$table,
                                  'clause'=>' id = :id ',
                                  );
        $queryVars->preparedVars=array(':id'=>intval($id));
        $this->createQuery($queryVars);
        return $this->getElement();
    }

    ###########################################
    # Funktion zur Rückgabe eines Datensatzes #
    ###########################################
    public function getElement($index = false)
    {
        $this->pointer= (($index === false) ? $this->pointer : $index);
        // auktuellen Datensatz zurückgeben
        !empty($this->liste[$this->pointer]) ? $datensatz = $this->liste[$this->pointer] : '';
        $this->pointer++;

        return !empty($datensatz)?$datensatz:'';
    }
    ###########################################################
    # Funktion um letztes Element aus der Liste zurückzugeben #
    ###########################################################
    public function getLastElement()
    {
        // letzten Datensatz zurückgeben
        $datensatz = $this->liste[($this->pointer-1 >= 0 ? $this->pointer-1 : 0)];
        return $datensatz;
    }

    public function getHtmlNavi($version = "std", $para = false)
    {
        // Wenn kein Parameter übergeben wurde->Querystring als Parameter
        if ($para===false)$para=htmlentities($_SERVER['QUERY_STRING']);
        // Altes offset aus Link löschen
        $para=str_replace("".$this->offsetname."=".$_REQUEST[$this->offsetname],"",str_replace("".$this->offsetname."=".$_REQUEST[''.$this->offsetname]."&amp;","",str_replace("&amp;".$this->offsetname."=".$_GET[''.$this->offsetname],"",$para)));
		$para=preg_replace('~(\&){2,}~','&',$para);
		if (substr($para, strlen($para)-1)==="&")
		    $para= substr($para,0,-1);
        // Parameterstring mit führendem und versehen
        $para=(($para!="")?"&amp;":"").$para;
        // neues Linkobjekt erzeugen
        $navilink = new \sessionLink();
        // Alte HTML-Variable löschen
        unset ($html);
        // Wenn keine Datensätz gefunden
        if ($this->rows<=0):
            $html="Keine Eintr&auml;ge vorhanden!";
        // wenn Datensätze gefunden
        else:
            // Schauen ob eine Seitenlimitierung existiert
            if ($this->anzahl OR !empty($this->cps)):
                // Wenn Offset größer als 0
                if ($this->offset > 0){$html .= $navilink->makeLink("&laquo;&laquo;&laquo;&nbsp;", $_SERVER['SCRIPT_URI']."?".$this->offsetname."=".($this->offset-1).$para);}
                // Wenn Offset =0
                else {$html .= '<span class="browsersmooth">&laquo;&laquo;&laquo;&nbsp;</span>';};
                // Seitenzahl durchehen und Links schreiben
                for ($i = 0; $i < $this->sites; $i++):
                    // Wenn weniger als 5 Seiten
                    if ($this->sites<=5):
                        $linknr = "";
                        $i==$this->offset?$linknr .= "<b><big>":"";
                        $linknr .= "[".(1+$i)."]";
                        $i==$this->offset?$linknr .= "</big></b>":"";
                        // Link schreiben
                        $html .= $navilink->makeLink($linknr, $_SERVER['SCRIPT_URI']."?".$this->offsetname."=".$i.$para);
                    // Wenn mehr als 5 Seiten
                    else:
                        // Ersten und letzten Seitenlink dauerhaft schreiben
                        if ($i==0 || $i == ($this->sites-1)):
                            $linknr = "";
                            $i==$this->offset?$linknr .= "<b><big>":"";
                            $linknr .= "[".(1+$i)."]";
                            $i==$this->offset?$linknr .= "</big></b>":"";
                            // Link schreiben
                            $html .= $navilink->makeLink($linknr, $_SERVER['SCRIPT_URI']."?".$this->offsetname."=".$i.$para);
                        endif;
                        // Einen Link vor und einen nach aktueller Seite schreiben
                        if ((($i+1) == $this->offset && $i !=0) || ($i == $this->offset  && ($i !=0 && $i != ($this->sites-1)) )|| (($i-1) == $this->offset && $i != ($this->sites-1))):
                            $linknr = "";
                            // Punkte schreiben wenn Abstand zu erstem Link größer als 2
                            if (($i+1) == $this->offset && $i !=0 && $i-1!=0) $linknr.=" ... ";
                            $i==$this->offset?$linknr .= "<b><big>":"";
                            $linknr .= "[".(1+$i)."]";
                            $i==$this->offset?$linknr .= "</big></b>":"";
                            // Punkte schreiben wenn Abstand zu letztemLink größer als 2
                            if (($i-1) == $this->offset && $i !=0 && $i+1!=0) $linknr.=" ... ";
                            // Link schreiben
                            $html .= $navilink->makeLink($linknr, $_SERVER['SCRIPT_URI']."?".$this->offsetname."=".$i.$para);
                        endif;
                    endif;
                endfor;
                // Wenn Offset kleiner als letzte Seite
                if ($this->offset < $this->sites - 1) $html .= $navilink->makeLink("&nbsp;&raquo;&raquo;&raquo;", $_SERVER['SCRIPT_URI']."?".$this->offsetname."=".($this->offset+1).$para);
                // Wenn Offset gleich letzte Seite
                else $html .= '<span class="browsersmooth">&nbsp;&raquo;&raquo;&raquo;</span>';
            endif;
        endif;
        // Rückgabe des Browsers
        return $html;
    }
    ###############################################
    # Funktion zum schreiben eines logs in die DB #
    ###############################################
    private function writeLog($aktion,$aid=false, $uid=false,$table=false)
    {
        $query="INSERT INTO kta_log
                (uid, aid, tabletyp, date, aktion)
                VALUES(".$this->con->quote((empty($uid)?$_SESSION['user']->id:$uid)).", ".$this->con->quote((($aktion=='new')?$this->con->insert_id:$aid)).", '".$table."' , NOW(), '".$aktion."')";
        try
        {
             $this->result = $this->con->query($query);
        } catch( \PDOException $e)
        {
            $this->func->savePDOException($e->getMessage(),$e->getCode(),nl2br($query).'<br/><br/>'. nl2br($e->getTraceAsString()),WEBDIR."status_msg/db_error");
        }
    }
    /**
     * Funktion zum Aufruf einer gespeicherten Prozedure
     * @param string $procedure der NAme der Prozedur
     * @param array $params Array mit parametern für die Prozedure
     * @param int $aid Eine ID des entsprechenden DAtensatzes für den EIntrag des Logs
     */
    public function callProcedure($procedure, array $params, $aid = false)
    {
        // Prozedure ausfüren
        $this->con->query('CALL '.$procedure."('".implode("', '", $params)."')");
        // Log schreiben
        $this->writeLog($procedure, $aid, false, false);
    }
    public function refValues($arr){
        if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
        {
            $refs = array();
            foreach($arr as $key => $value)
                $refs[$key] = &$arr[$key];
            return $refs;
        }
        return $arr;
    }
}

?>