<?php
namespace inc;
/**
 * Kümmert sich um die modulspezifischen Views. Speichert Variablen zwischen und läd per loadTemplate() das gesetzte Template
 */
class View
{
    /**
     * Name des Templates
     * @var string
     */
    private $template = 'default';

    /**
     * Pfad zum Template, endet auf /
     * @var string
     */
    private $templatePath = '';

    /**
     * enthält die Variablen, die in das Template eingebettet werden sollen
     * @var array
     */
    private $_ = array();

    /**
     * Hilfsklasse, die häufig gebrauchte Funktionen zur Verfügung stellt
     * @var Func
     */
    private $func;

    /**
     * Hilfsklasse, die Links erstellt
     * @var sessionLink
     */
    private $link;

    /**
     * Initialisiert die Klasse
     * @param Func $func Hilfsklasse, die spezielle Funktionen zur Verfügung stellt
     */
    public function __construct()
    {
        global $func;
        $this->func = $func;
        global $l;
        $this->link = $l;
    }

    /**
     * Setzt den Eintrag des Template-Daten-Arrays
     * @param mixed $key Schlüssel ("Variablenname")
     * @param mixed $value Wert ("Variablenwert")
     */
    public function assign($key, $value)
    {
        $this->_[$key] = $value;
    }

    /**
     * Setzt den Namen des Templates, dass geladen werden soll
     * @param string $template Name des zu ladenden Templates
     * @param string $path (absoluter) Pfad des Templates, NICHT auf / endend!
     * @param string $language
     */
    public function setTemplate($template, $path)
    {

        $this->templatePath = $path . '/';
        if (file_exists($this->templatePath . $template. '.tpl')):
            // das View-Skript existiert in der Sprache, die der User beim Registrieren ausgewählt hat
            $this->template = $this->templatePath .  '' .  $template. '.tpl';
            $this->mastermo = $this->templatePath . '.' . $template  . '.xml';
            $this->langmo = $this->templatePath . '.' . $_SESSION['lang']->code. '.' . $template  . '.xml';
            return true;
        endif;

        // @ TODO ALT-> Muss entfernt werden!!
        if (file_exists($this->templatePath . $template . '_' . $_SESSION['lang']->code . '.tpl')):
            // das View-Skript existiert in der Sprache, die der User beim Registrieren ausgewählt hat
            $this->template = $template . '_' . $_SESSION['lang']->code;
            return true;
        endif;
        // @ TODO ALT-> Muss entfernt werden!!
        if (file_exists($this->templatePath . $template . '.tpl')):
            // das View-Skript existiert unübersetzt
            $this->template = $template;
            return true;
        endif;

        header('Location: '.WEBDIR.'status_msg/server_error'); //TODO Fehlerumleitung Im Falle, dass das View-Skript nicht gefunden wurde
        exit;
    }

    /**
     * Läd das Template, übergibt alle Variablen und speichert dessen Ausgabe in einem Puffer zwischen
     * @return string fertiges View-Skript mit allen eingebetteten Daten
     */
    public function loadTemplate()
    {
        ob_start();
        // TODO Prüfung muss nach kompletter Umstellung entfernt werden!
        if (file_exists($this->mastermo)):
            // Inhalt des Templates auslesen
            $output=file_get_contents($this->template);
            // Variablenarray initialisieren
            $tplvars=array();
            // XML-Reader instanziieren
            $xml = new XMLReader();
            // Prüfen ob Master-Datei existiert
            if(@!$xml->open($this->mastermo)):
                // Wenn TPL-Datei nicht vorhanden, dann weiterleiten
                header('Location: '.WEBDIR.'status_msg/server_error'); //TODO Fehlerumleitung Im Falle, dass das View-Skript nicht gefunden wurde
                exit;
            else:
                // Master- Mo-Datei einlesen und VAriablen speichern
                while($xml->read()):
                    if($xml->name=='var'):
                        $value=$xml->readInnerXML();
                        if (!empty($value))
                            $tplvars[$xml->getAttribute('name')]= (object) array('typ'=>$xml->getAttribute('typ'),'value'=>html_entity_decode(trim($value)));
                    endif;
                endwhile;
            endif;
            // Sprach-Mo-Datei einlesen (soweit vorhanden)
            if (@$xml->open($this->langmo)):
                 while($xml->read()):
                    if($xml->name=='var'):
                        $value=$xml->readInnerXML();
                        if (!empty($value))
                            $tplvars[$xml->getAttribute('name')]=(object) array('typ'=>$xml->getAttribute('typ'),'value'=>html_entity_decode(trim($value)));
                    endif;
                endwhile;
            endif;
            // VAriablenteile ersetzen
            $output = preg_replace('~\<\?tpl( ){0,1}' . $replace['pattern'] . '( ){0,1}\?\>~u', $replace[$toPublic], $output);

             // Alles andre in tpl-Tags wird erlaubt
            $output = str_replace('<?tpl', '<?php', $output);
             // Übersetzungsvariablen ersetzen
            foreach ($tplvars as $name =>$var):
                // Volltext anders behandeln
                if ($var->typ=="TT_FULL")
                    $output = str_replace('{' . $name . '}', stripslashes($var->value), $output);
                else
                    $output = str_replace('{' . $name . '}', htmlspecialchars(stripslashes(nl2br($var->value)), ENT_QUOTES, 'UTF-8'), $output);
            endforeach;
            // Alle Klassen entfernen (muss extra gemacht werden wgen Namensüberschneidungen)
            foreach($tplvars as $name=>$val):
                $output = str_replace($name, '', $output);
            endforeach;
            // rauslöschen aller leeren class-Attribute, zB class=""
            $output = str_replace(array(' class=""', " class=''"), '', $output);
            $output = str_replace(array('class=""', "class=''"), '', $output);
            // Temporäre Datei mit Templatedaten füllen und includen
            $tmpfname = tempnam("/tmp", "fakeEval");
            $handle = fopen($tmpfname, "w+");
            fwrite($handle, $output);
            fclose($handle);
            include $tmpfname;
            unlink($tmpfname);
            // Rückgabe speichern
            $out2 = ob_get_contents();
            ob_end_clean();
            // Ausgabe des berechneten Ergebnisses
            return $out2;
        else:
            require_once ($this->template);
            $output = ob_get_contents();
            ob_end_clean();
            return $output;
        endif;
    }
}
