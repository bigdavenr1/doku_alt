<?php
namespace inc;
//////////////////////////////////////////////////////////////////////////////////////////////////
// Email-Klasse - Verschickt Mails an mehrere Empf�nger (TO, CC, BCC)
//              - HTML-Content
//////////////////////////////////////////////////////////////////////////////////////////////////
class Email extends Basicdb
{
    private $from;
    private $to;
    private $cc = Array();
    private $bcc = Array();
    private $replayto;
    private $subject;
    private $content;
    private $typ;

    /**
     * Konstruktor
     * @param string|false $typ (optional, default false) Wenn $typ = 'html' dann wird die Email als HTML abgesendet, sonst als normaler Text
     */
    public function __construct($typ = false, array $anhang=array())
    {
        $this->mime_boundary="-----=" . md5(uniqid(mt_rand(), 1));
        if ($typ == 'html')
            $this->typ = 'text/html';
        elseif($typ=='multipart')
            $this->typ='multipart/mixed';
        else
            $this->typ = 'text/plain';

        $this->table = 'kta_email e';
        parent::__construct();

        global $func;
        $this->func = $func;

        $this->anhang=$anhang;
    }

    /* === GRUNDLEGENDE EMAIL-FUNKTIONEN === */

    /**
     * Setzt den Absender
     * @param string $from Absender
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * Setzt den Empfänger
     * @param string $to Empfänger
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * Setzt wer alles im CC sein soll
     * Carbon Copy (direkte Kopie)
     * @param array $cc
     */
    public function setCC(array $cc)
    {
        $this->cc = $cc;
    }

    /**
     * Setzt wer alles im BCC sein soll
     * Blind Carbon Copy (direkte Kopie, taucht aber bei den Empfängern nicht direkt auf)
     * @param array $bcc
     */
    public function setBCC(array $bcc)
    {
        $this->bcc = $bcc;
    }

    /**
     * Setzt an wen die Rückantwort geschickt werden soll
     * @param string $replayto Rückantwort-Email-Adresse
     */
    public function setReplayto($replayto)
    {
        $this->replayto = $replayto;
    }

    /**
     * Setzt den Betreff
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * Setzt den Inhalt
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }



    /**
     * Schickt die Mail ab
     */
    public function sendMail()
    {
        $ccs = '';
        foreach ($this->cc as $cc)
            $ccs .= ','.$cc;
        $ccs = substr($ccs, 1);

        $bccs = '';
        foreach ($this->bcc as $bcc)
            $bccs .= ','.$bcc;
        $bccs = substr($bccs, 1);

        // Header zusammenbauen
        $header = 'From: '.$this->from.chr(10);

        if ($this->cc)
            $header .= 'CC: '.$ccs.chr(10);
        if ($this->bcc)
            $header .= 'BCC: '.$bccs.chr(10);
        if ($this->replayto)
            $header .= 'Reply-To: '.$this->replayto.chr(10);

        $header .= 'X-Mailer: PHP/'.phpversion().chr(10)
                 . 'X-Sender-IP: '.$_SERVER['REMOTE_ADDR'].chr(10)
                 . 'MIME-Version: 1.0'.chr(10);


         if ($this->typ=='multipart/mixed'):
             $header.= 'Content-Type: '.$this->typ.';  boundary='.$this->mime_boundary.chr(10);
             $header.= "This is a multi-part message in MIME format --
             Dies ist eine mehrteilige Nachricht im MIME-Format.\n";
             $header .= "--".$this->mime_boundary."\n";
            $header .= "Content-Type: text/html; charset=utf-8\n";
            $header .= "Content-Transfer-Encoding: 8bit\n\n";
            $header .= $this->content."\n";
            foreach($this->anhang As $value):

                $datei = $value[0];
                $typ = $value[1];

                $anhang1 = fread(fopen($datei, "r"), filesize($datei));
                $anhang1 = chunk_split(base64_encode($anhang1));
                $header .= "--".$this->mime_boundary."\n";
                $header .= "Content-Type: ".$typ."; name=\"".basename($datei)."\"\n";
                $header .= "Content-Transfer-Encoding: base64\n";
                $header .= "Content-Disposition: attachment; filename=\"".basename($datei)."\"\n\n";
                $header .= $anhang1."\n";
                $header .= "--".$this->mime_boundary."--\n";

             endforeach;
         else:
             $header.='Content-Type: '.$this->typ.';  charset=utf-8'.chr(10);
         endif;


        mail($this->to, $this->subject, $this->content, $header);
    }

    /* === INTEGRATION MIT KTA_EMAIL === */

    /**
     * Holt die Daten eines Email-Templates aus der Datenbank
     * @param string $name Name des Templates
     * @param int $language ID der Sprache
     * @param array (optional, default leeres Array) Weitere Parameter die im Template-Text ersetzt werden sollen
     * @return string Inhalt der Mail
     */
    private function getEmailTemplate($name, $language, array $params = array())
    {
        $this->_ = $params;
        ob_start();
        if (file_exists(dirname(__FILE__) . '/' . $name.'_' . $this->func->languages[$language] . '.tpl'))
            require dirname(__FILE__) . '/' . $name.'_' . $this->func->languages[$language] . '.tpl';
        elseif (file_exists(dirname(__FILE__) . '/' . $name.'_en.tpl'))
            require dirname(__FILE__) . '/' . $name.'_en.tpl';
        else // Mail weder in Zielsprache noch in englisch gefunden
            die();
        $text = ob_get_clean();

        return $text;
    }

    /* === SPEZIELLE EMAILS === */

    /**
     * Sendet die Aktivierungs-Email an einen Nutzer
     * @param string $emailAdr Email-Adresse
     * @param int $userId ID des Nutzers
     * @param string $actCode Aktivierungscode
     * @param int $langId ID der Sprache
     */
    public function sendActivationEmail($emailAdr, $userId, $actCode, $langId = 1)
    {
        $this->typ = 'text/html';
        $template = $this->getEmailTemplate('acc_act', $langId, array('userId' => $userId, 'actCode' => $actCode) );

        $this->setFrom('no-reply@vivamallorca.com');
        $this->setTo($emailAdr);

        $matches = array();
        preg_match('~\<title\s*\>(.*)\<\/title\>~u', $template, $matches);
        $this->setSubject($matches[1]);

        $this->setContent($template);
        $this->sendMail();
    }

    /**
     * Sendet eine Email an den Nutzer, dass für einen seiner / ihrer Suchaufträge neue Immobilien gefunden wurden
     * @param object $user Daten des Nutzers (Name, Vorname, Email)
     * @param int $realestateCount gesamte Anzahl neuer Immobilien
     * @param string $realestateText Text der Vorabauswahl der gefundenen Immobilien
     * @param string $url URL für die Ansicht der gefundenen Immobilien
     */
    public function sendSearchRequestFindingsEmail($username, $emailAdr, array $realestates, $realestateCount, $url)
    {
        $this->typ = 'text/html';
        $template = $this->getEmailTemplate('search_request', $langId,
            array(
                'username' => $username,
                'realestates' => $realestates,
                'realestateCount' => $realestateCount,
                'url' => $url,
            )
        );

        $this->setFrom('no-reply@vivamallorca.com');
        $this->setTo($emailAdr);

        $matches = array();
        preg_match('~\<title\s*\>(.*)\<\/title\>~u', $template, $matches);
        $this->setSubject($matches[1]);

        $this->setContent($template);
        $this->sendMail();
    }

    public function sendEmailToFriend($name, $emailAdr, $friendName, $friendMail, $message, $url, $subject)
    {
        $this->typ = 'text/html';
        $template = $this->getEmailTemplate('email_a_friend', $_SESSION['lang']->id, array(
            'name' => $name,
            'friendName' => $friendName,
            'message' => $message,
            'url' => $url,
        ));

        $this->setFrom($emailAdr);
        $this->setTo($friendMail);

        $this->setSubject($subject);
        $this->setContent($template);
        $this->sendMail();
    }
}
