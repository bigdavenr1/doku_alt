<?php
/*
* nbCMS Kernel V1.0
* erstellt 01/2011 von Tino Stief
* copyright nb-cooperation
*
* Basis-DB-Connectklasse
* für Kontakt zur Datenbank
*
* zugehörige Dateien
* - inc/config.php
* - inc/std/basicdb.class.php
*/
if (!class_exists("Dbconnect"))
{
    class Dbconnect
    {
        private $codierung;
        private $timenames;
        protected $con;
        private $sql;
        private $func;
        /*
        * Konstruktor
        */
        protected function __construct($codierung='UTF8',$timenames='de_DE')
        {
            $this->codierung = $codierung;
            $this->timenames = $timenames;
            // Zugangsdaten holen und global bereitstellen
            global $sql;
            global $con;
            global $func;
            $this->func=$func;

            // Datenbankconnection aufbauen
            if (empty($con)):
                try
                {
                    $con=new PDO('mysql:host='.$sql["host"].';dbname='.$sql["db"].';charset='.$this->codierung, $sql["user"], $sql["pass"],array(PDO::ATTR_PERSISTENT => true ));
                    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                }
                catch(PDOException $e)
                {
                    $func->savePDOException($e->getMessage(),$e->getCode(),'Database-Connection'.'<br/><br/>'. nl2br($e->getTraceAsString()),WEBDIR.'dberror.html');
                }
                $this->setLC($con);
            endif;
            // Global zur Verfügungstellen
            $this->con = $con;
        }
        /*
        * Funktion für die Einstellung der Zeit und Datumsnamen der Verbindung
        *
        */
        private function setLC($con)
        {
            if (!empty($con)):
                // Deutsche Zeit und DAtumsnamen
                try
                {
                    $con->query("SET lc_time_names = '".$this->timenames."'");
                }
                catch( PDOException $e )
                {
                    $this->func->savePDOException($e->getMessage(),$e->getCode(),'SET lc_time_names = '.$this->timenames.'<br/><br/>'. nl2br($e->getTraceAsString()));
                }
            endif;
        }
    }
}
?>