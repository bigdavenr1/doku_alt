<?php
###################################
# nbCMS Kernel V1.0               #
# erstellt 01/2011 von Tino Stief #
# copyright nb-cooperation        #
#                                 #   
# Basis-Linkklasse zu Kontrolle   #
# der SID-Übergabe                #
#                                 #
# session_start() muss ausgeführt #
# sein                            #
################################### 
if (!class_exists("sessionLink"))
{
    class sessionLink extends \inc\Basicdb
    {
        var $sesslink;
        var $sid;
        ###############
        # Konstruktor #
        ###############
        public function __construct()
        {
            // Sesslink zurücksetzen
            $this->sesslink = false;
            // SID zurücksetzen
            $this->sid = strip_tags(SID);
            // Prüfen ob Cookies erlaubt sind-> Sesslink auf true setzen
            if (isset($_SESSION) && !$_COOKIE[session_name()])
                $this->sesslink = true;  
        }
        #########################################
        # Funktion zum Zurückgeben eines Linkes #
        #########################################
        public function makeLink($name, $href, $class = false, $target = false, $extra = false, $ankor = false)
        {
            // HTML-Variable zurücksetzen
            $html="";
            // Wenn keine Cookies erlaubt, dann SID anfügen
            if ($this->sesslink):
                // Prüfen ob Link bereits Übergabewerte enthält
                if (ereg("[?]", $href)) {$href .= '&amp;'.$this->sid;}
                else {$href .= '?'.$this->sid;};
            endif;
            // Wenn Akner mitgegeben wurde
            if ($ankor) $href .='#'.$ankor;
            $html .= '<a href="'.$href.'"';
            // Wenn ein target mitgegeben wurde
            if ($target) $html .= ' target="'.$target.'"';
            // wenn eine CSS_Klasse mitgegeben wurde
            if ($class) $html .= ' class="'.$class.'"';
            // wenn irgendwelche Extra-Eigenschaften mitgegeben wurde
            if ($extra) $html .= ' '.$extra;
            $html .= '>'.$name.'</a>';
            // link zurückgeben 
            return $html; 
        }
        ##############################################
        # Funktion zum Zurückgeben einer FormularURL #
        ##############################################
        public function makeUrl($href)
        {
            // Wenn keine Cookies erlaubt, dann SID anfügen
            if ($this->sesslink):
                // Prüfen ob Link bereits Übergabewerte enthält
                if (ereg("[?]", $href)) {$href .= '&amp;'.$this->sid;}
                else {$href .= '?'.$this->sid;};  
            endif;
            // Rückgabe der URL
            return $href; 
        }
        ##############################################
        # Funktion zum Zurückgeben einer FormularURL #
        ##############################################
        public function makeFormLink($href)
        {
            // Alte Funktion für ältere Versionen des Frameworks
            return $this->makeURL($href);            
        }
    }
}

?>