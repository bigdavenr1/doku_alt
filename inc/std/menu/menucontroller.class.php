<?php
namespace inc\std\menu;
include('menumodel.class.php');
include('menuview.class.php');
class Controller extends View
{
    /**
     * Funktion zum holen der Daten
     * @param string $menutyp gibt den Menutyp an (headmenu usw)
     * @param integer $lang gibt die Sprache an
     * @param string $method gibt die Model-Methode an (dynamisch aus ersten Aufruf)
     */
   public function __construct($menu='public')
   {
        // Modultemplate auslesen
        $this->menutemplate = $menu;
        // Modulmodel auslesen
        $this->menumodel =new Model();
        switch ($menu):
            case 'dropdown':
                 $this->getMenuData('getMenuDataAdmin');
                 // Aufruf der Methode zum Bauen eines MenuBaumes
                $this->buildNested();
            break;
            case 'menuadmin':
                $this->getMenuData('getSubmenus');
                // Template setzen
                $this->assign('submenus',$this->menudata);
                $this->getMenuData('getMenuDataAdmin');
                // Aufruf der Methode zum Bauen eines MenuBaumes
                $this->buildNested();
            break;
            case 'admin':
                $this->menuarray=array();
            break;
            case 'user':
                $this->menuarray=array();
            break;
            case 'public':
            default:
                // Aufruf der Funktion zum Daten holen
                $this->getMenuData("getPublicMenuData");
                // Aufruf der Methode zum Bauen eines MenuBaumes
                $this->buildNested();
           default:
       endswitch;

   }


    private function getMenuData($method)
    {
        // $menudataarray leeren
        $this->menudata=array();
        // Pointer auf 0 setzen
        $this->pointer=0;
        // geerbte Methode zum Datenholen aufrufen (je anchdem für welchen Zweck)
        $this->menumodel->$method($menutyp);
        // Datenobject zwischenspeichern
        $this->menudata=$this->menumodel->liste;
    }

    /**
     * Funktion zum Aufbau des nestet-Sets als mehrdimensionales verschachteltes Array
     *
     */
    private function buildNested()
    {
        // menudatenarray durchgehen
        foreach($this->menudata as $item):
            // prüfen ob Menütype schon existiert => Wenn nicht, neuen Knoten erzeugen
            if(!isset($this->menuarray[$item->menutypename])):
                $array =& $this->menuarray[$item->menutypename][$item->title];
            // prüfen ob eventuell Gleiche Ebene wie vorher oder Ebene Drunter
            elseif( ($item->rgt > $lastright && $item->rgt < $rgt) || $item->rgt > $rgt):
                // Wenn nicht level 0 dann Zwischenspeicher solange durchgehen bis gewünschtes Level erreicht
                // Und Werte entsprechend setzen
                if ($item->level!=0):
                     $array=&$level[$item->level]['array'];
                     $lastright=$level[$item->level]['rgt'];
                     $rgt=$level[$item->level]['srgt'];
                // Wenn wechsel auf Ebene 0, die entsprechenden Werte setzen
                else:
                    $array =& $this->menuarray[$item->menutypename][$item->title];
                endif;
            // Wenn Ebene Drüber, Zeiger auf Kindknoten setzen
            else:
                $array=&$array[$lastright]['children'];
            endif;
            // Arraydatensatz füllen
            $array[$item->rgt]=array(
            'id'       => $item->id,
            'name'     => $item->name,
            'modulcheck' => $item->modulcheck,
            'link'      => $item->link,
            'maxdeep'  => $item->maxdeep,
            'left'     => $item->lft,
            'right'    => $item->rgt,
            'level'    => $item->level,
            'submenu_left'     => $item->subm_lft,
            'submenu_right'     => $item->subm_rgt,
            'menutype' => $item->menutype,
            'menutypename' => $item->menutypename,
            'menutypetitle' => $item->title,
             //'menutype' => !empty($item->menutitle)?$item->menutype:'',
            'alias'    => strtolower($item->alias), //name des links
             //'alias'    => !empty($item->alias)?$item->alias:'artikel',
            'menuid'   => $item->menutype,
            'children' => array());
            // Zwischenspeichervariablen setzen
            $level[$item->level]['rgt']=$item->rgt;
            $level[$item->level]['srgt']=$item->subm_rgt;
            $level[$item->level]['array']=&$array;
            $lastright=$item->rgt;
            $lft=$item->subm_lft;
            $rgt=$item->subm_rgt;
        endforeach;
    }
    public function getRequestetMenu($menu=false)
    {
        $this->assign("menutype",$menu);
        if(!empty($menu))
            $this->assign("menu",$this->menuarray[$menu]);
        else
            $this->assign("menu",$test=$this->menuarray);
        return $this->printMenu();
    }

}

?>