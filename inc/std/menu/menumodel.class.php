<?php
namespace inc\std\menu;
class Model extends \inc\Basicdb
{
    ###############################################################
    # Konstruktor (Parentkonstruktor übernehmen), DB-Daten setzen #
    ###############################################################
    public function __construct()
    {
        global $sql;
        $this->table = "kta_menupoints";
        $this->queryVars=(object) array('table' => $this->table, 'countVar'=>$this->countvar, 'offsetname'=>false);
        parent::__construct();
    }
    ####################################################
    # Funktion zum Holen der Daten für ein Public menu #
    ####################################################
    public function getPublicMenuData ($sprachid=false)
    {
        $queryVars=$this->queryVars;
        $queryVars->table=array((object) array("name"=>"kta_menupoints","alias"=>"n"),(object) array('name'=>'kta_menupoints',"alias"=>"p"));
        unset($queryVars->countVar);
        $queryVars->joins=array(
            (object)
            array('type'=>'LEFT',
                  'table'=>'kta_submenus l',
                  'clause'=>'l.id=p.menutype'
            ),
            (object)
            array('type'=>'LEFT',
                  'table'=>'kta_menus k',
                  'clause'=>'k.id=l.menutype'
            ),
            (object)
            array('type'=>'LEFT',
                  'table'=>'kta_content a',
                  'clause'=>'p.link=a.id'
            ),
            (object)
            array('type'=>'LEFT',
                  'table'=>'kta_modules b',
                  'clause'=>'p.link=b.id'
            )
        );
        $queryVars->fields=array('k.name menutypename',
                                 'k.id menutypeid',
                                 'k.maxdeep',
                                 'l.id menuid',
                                 'l.title',
                                 'l.lft subm_lft',
                                 'l.rgt subm_rgt',
                                 'n.*',
                                 '(SELECT id FROM kta_menupoints p WHERE p.lft < n.lft AND p.rgt > n.rgt ORDER BY p.lft DESC LIMIT 1) AS parent',
                                 'COUNT(*)-1 AS level',
                                 'if (n.modulcheck=00,(SELECT alias from kta_modules WHERE n.link=id LIMIT 0,1 ),(SELECT alias from kta_content WHERE n.link=id LIMIT 0,1 )) alias',
                                 'BIN (n.modulcheck) modulcheck',
                                 'ROUND ((n.rgt - n.lft - 1) / 2) AS offspring'
                                 );

        $queryVars->clause = 'n.lft BETWEEN p.lft AND p.rgt GROUP BY n.lft';
        $queryVars->order=(object) array('field'=>'n.lft');
        $queryVars->preparedVars = array();
        unset($queryVars->cps);
        unset($queryVars->offsetname);
        parent::createQuery($queryVars);

    }
    ####################################################
    # Funktionen zum Holen der Daten für den Menuadmin #
    ####################################################
    public function getMenuDataAdmin()
    {
        $queryVars=$this->queryVars;
        $queryVars->table=array((object) array("name"=>"kta_menupoints","alias"=>"n"),(object) array('name'=>'kta_menupoints',"alias"=>"p"));
        unset($queryVars->countVar);
        unset($queryVars->cps);
        unset($queryVars->offsetname);
        $queryVars->joins=array(
            (object)
            array('type'=>'LEFT',
                  'table'=>'kta_submenus',
                  'clause'=>'kta_submenus.id=p.menutype'
            ),
            (object)
            array('type'=>'LEFT',
                  'table'=>'kta_menus k',
                  'clause'=>'k.id=kta_submenus.menutype'
            )
        );
        $queryVars->fields=array('k.name menutypename',
                                 'CASE WHEN kta_submenus.title IS NULL THEN kta_submenus.description ELSE kta_submenus.title END menutitle',
                                 'kta_submenus.id menuid',
                                 'kta_submenus.lft subm_lft',
                                 'kta_submenus.rgt subm_rgt',
                                 'k.maxdeep',
                                 'kta_submenus.title',
                                 'n.*',
                                 'COUNT(*)-1  level' ,
                                 'ROUND ((CAST(n.rgt AS SIGNED) - CAST(n.lft AS SIGNED) - 1) / 2)  kinder' );

        $queryVars->clause = 'n.lft BETWEEN p.lft AND p.rgt GROUP BY n.lft';
        $queryVars->order=(object) array('field'=>'kta_submenus.id, n.lft');
        $queryVars->preparedVars = array();
        parent::createQuery($queryVars);
    }
    ##########################################################
    # Funktionen zum holen der Daten für die Menuübersetzung #
    ##########################################################
    protected function getMenuDataAdminWithUebersetzung($menutype=false,$langid=false)
    {
        $this->felder="( SELECT text from uebersetzung WHERE typ='menu' AND artid=n.id AND sprache='".intval(mysqli_quote($this->con,$langid))."') uebersetzung,
        CASE WHEN kta_submenus.title IS NULL THEN kta_submenus.description ELSE kta_submenus.title END menutitle, kta_submenus.id menuid, kta_submenus.title, n.*,  COUNT(*)-1  level ,ROUND ((CAST(n.rgt AS SIGNED) - CAST(n.lft AS SIGNED) - 1) / 2)  kinder";
        $this->table=" kta_menupoints n, kta_menupoints p";
        $query=" LEFT JOIN kta_submenus ON (kta_submenus.id=p.menutype) ";
        if ($langid != false):
            $this->felder.=", c.text menutypubersetzung";
            $query.="LEFT JOIN uebersetzung c ON(c.typ='menutyp' && c.artid=kta_submenus.id  && c.sprache='".intval(mysqli_quote($this->con,$langid))."')";
        endif;
        parent::createQuery($query." WHERE n.lft BETWEEN p.lft AND p.rgt GROUP BY n.lft ORDER BY kta_submenus.id, n.lft");

    }
    ########################################################
    # Funktion zum Holen der Daten für die Menuaktivierung #
    ########################################################
    protected function getAllWithActivationsByLang()
    {
        $this->felder="kta_menupoints.lft, kta_menupoints.name, kta_menupoints.id, b.id pointlang, b.ISOCode langnamepoint";
        $query="LEFT JOIN kta_menupointspoint_lang a ON(a.menupoint=kta_menupoints.id)";
        $query.="LEFT JOIN jos_sprachen b ON(b.id=a.lang)";
        $query.="UNION SELECT kta_menupoints.lft, kta_menupoints.name, kta_menupoints.id, b.id pointlang, b.ISOCode langnamepoint FROM kta_menupoints RIGHT JOIN kta_menupointspoint_lang a ON(a.menupoint=kta_menupoints.id) RIGHT JOIN jos_sprachen b ON(b.id=a.lang)";
        parent::createQuery($query." ORDER BY pointlang, lft "," "," ");
    }
    #########################################################################################
    # Funktion zum  Holen der MenuTypes die noch keinen Menupunkt zugeordnet bekommen haben #
    #########################################################################################
    public function getSubmenus()
    {
        $queryVars=clone($this->queryVars);
        $queryVars->table="kta_submenus";
        unset($queryVars->countVar);
        $queryVars->fields=array('kta_submenus.*',
                                 'a.name menutypename');
        $queryVars->joins=array(
            (object)
            array('type'=>'LEFT',
                  'table'=>'kta_menus a',
                  'clause'=>'a.id=kta_submenus.menutype'
            )
        );
        parent::createQuery($queryVars);
        return $this->liste;
    }

    protected function getMenuTypes()
    {
        $this->felder="*";
        $this->table="kta_menus";
        parent::createQuery(" "," "," ");
        return $this->liste;
    }
}