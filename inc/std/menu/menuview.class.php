<?php
namespace inc\std\menu;
/**
 * Klasse MenuView im MVC-Pattern die Ausgabeeinheit. printmenu startet das entsprechende Menu
 * einzelne Methoden je Menu
 */
class View
{
    /**
     * Methode zum registrieren von Variablen
     * @param string $key gibt den Arrayschlüssel an
     * @param $value gibt den zu registrierenden value an -> kann jeder Datentyp sein
     *
     */

    public function assign($key, $value)
    {
        $this->_[$key] = $value;
        global $func;
        $this->func=$func;
    }
    /**
     * Methode zum Start der Ausgabe des Menus. Abhängig von global gesetzter $this->menutemplate - Variable
     * @param string $this->menutemplate !!!(GLOBAL!!!) gibt den auszulesenden Menutype an. ist dieser nicht gesetzt wird Public verwendet
     * @return string $string Gibt Menu als String zurück
     */
    protected function printMenu()
    {
        // Maximale Tiefe auf 0 (1. Stufe) setzen
        $maxdeep=0;
        // Prüft ob Menudaten vorhanden sind
        if (is_array($this->_['menu']))
        {

            // Wählt anhand des angegebenen Menutemplates die Anzeige Methode aus und speichert in String
            switch ($this->menutemplate):
                case 'admin':
                    $string.=$this->showAdminMenu();
                    break;
                case 'user':
                    $string.=$this->showUserMenu();
                    break;
                case 'dropdown':
                    $string.='<select name="oberpunkt">';
                    $string.=$this->showAsDropDown($this->_['menu']);
                    $string.='</select>';
                    break;
                case 'menuadmin':
                    $string .= $this->ShowMenuAdmin($this->_['menu']);
                    break;
                case 'public':
                default:

                    $string.=$this->_['menutype']!='footermenu' && $this->_['menutype']!='footer_right'?'<ul id="menu" class="'.$this->_['menutype'].'">'.chr(10):'';
                        $string.=$this->showPublicMenu($this->_['menu'],0,($this->_['menutype']=='footermenu'  || $this->_['menutype']=='footer_right')?TRUE:FALSE);
                    $string.= $this->_['menutype']!='footermenu' && $this->_['menutype']!='footer_right'?'</ul>'.chr(10):'';
            endswitch;
        }
        // gibt string zurück
        return $string;
    }

     /**
     * Methode (Rekursiv) Zur Rückgabe der Menupunkte als Liste
     * @param array Array mit menudaten
     * @param int $level Gibt die gerade verwendete Stufe an. Wichtig für Rekursivität
     * @param boolean $showsubmenus legt fest ob die Titel der Submenus mit angezeigt werden sollen
     * @return string $string gibt das Menu als String zurück
     */
    private function showPublicMenu($array,$level=0, $showsubmenus=false)
    {
        // holt sich die maxtiefe global
        global $maxdeep;
        global $lastlevel;
        // Wenn unterste Menustufe
        if ($level===0):
            // Array durchgehen
            foreach($array AS $key => $value):
                $x++;
                // Wenn submenunamen angezeigt werden sollen diese entsprechend in String speichern
                if (!empty($showsubmenus))
                    $string.= ($this->_['menutype']!='footermenu' && $this->_['menutype']!='footer_right'?'<li>':'').'<ul class="submenu menu'.$x.'"><li>'.$key.'</li>'.chr(10);
                // einzelne Menüpunkte der Stufe 1 durchgehen
                foreach($value AS $key1 => $value1):
                   // Maxtiefe je nach Angabe aus DB setzen
                   $maxdeep=$value1['maxdeep'];
                   // menüpunkte schreiben
                   $string.= '<li><a href="';
                   if($value1['modulcheck']==00 || $value1['modulcheck']== 01):
                            $string.=WEBDIR.$value1['alias'].'.htm';
                   elseif($value1['modulcheck']==10): //ExternLink
                       if(preg_match('~http://~', $value1['link']) == 00) //Prüfung auf 'http://'
                                $value1['link']="http://".$value1['link'];
                           $string.= $value1['link'];
                   else:
                       $string.= 'javascript:void(0)';
                   endif;
                   $string.='">'.$value1['name']
                   .'</a>';
                   // Wenn Kindelemente Vorhanden
                   if (!empty($value1['children'])):
                       // Rekursiv Funktion aufrufen
                       $string.=$this->showPublicMenu($value1['children'],($value1['level']+1));
                   endif;
                   $string.='</li>'.chr(10);
                endforeach;
                // Wenn Submenus augegeben werden sollen dann entsprechend beenden
                if (!empty($showsubmenus))
                    $string.= "</ul>".($this->_['menutype']!='footermenu' && $this->_['menutype']!='footer_right'?'<li>':'').chr(10);
            endforeach;
            $lastlevel='';
        else:
            // Wenn die Max-Teife <= der gespeicherten (erlaubten) Maxtiefe ist
            if ($level <= $maxdeep):
                // Wenn neues Level erzeugt wird;
                if ($lastlevel!=$level)
                    // Neue UnterUL für diese Stufe erzeugen
                    $string.= '<ul class="level-'.$level.'">'.chr(10);
                // Array mit menupunkten durchgehen
                foreach($array AS $key1 => $value1):
                   //  Ausgabe an String anhängen
                   $string.= '<li><a href="';
                   if($value1['modulcheck']==00 || $value1['modulcheck']== 01):
                            $string.=WEBDIR.$value1['alias'].'.htm';
                   elseif($value1['modulcheck']==10): //ExternLink
                       if(preg_match('~http://~', $value1['link']) == 00) //Prüfung auf 'http://'
                                $value1['link']="http://".$value1['link'];
                           $string.= $value1['link'];
                   else:
                       $string.= 'javascript:void(0)';
                   endif;
                   $string.='">'.$value1['name'].'</a>';
                   // prüfen ob Unterpunkte bestehen
                   if (!empty($value1['children'])):
                       // Rekursiver Aufruf für die Unterpunkte
                       $string.=$this->showPublicMenu($value1['children'],($value1['level']+1));
                   endif;
                   $string.='</li>'.chr(10);
                endforeach;
                // Wenn neues Level erzeugt wurde;
                if ($lastlevel!=$level)
                    $string.= '</ul>'.chr(10);
            endif;
            // Zwischenspeicher des aktuellen Levels
            $lastlevel=$level;
        endif;
        // Rückgabe des Menus als String
        return $string;
    }

     /**
     * Methode (Rekursiv) Zur Rückgabe der Adminoberfläche
     * @param array Array mit menudaten
     * @param int $level Gibt die gerade verwendete Stufe an. Wichtig für Rekursivität
     * @return string $string gibt das Menu als String zurück
     */
    private function showMenuAdmin($array,$level=0)
    {

        $menutype='';
        // Sessionlinkinstanz initialisieren
        $l=new \SessionLink();
        // Icons setzen
        $icon_delete_small = '<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />';
        $icon_edit_small = '<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />';
        $icon_up_red = '<img src="'.WEBDIR.'images/icons/s_asc.gif" alt="hochschieben" title="hochschieben" />';
        $icon_down_red = '<img src="'.WEBDIR.'images/icons/s_desc.gif" alt="runterschieben" title="runterschieben" />';
        $icon_neu_small = '<img src="'.WEBDIR.'images/icons/note_new.gif" alt="Neuen Datensatz anlegen" title="Neuen Datensatz anlegen" />';
        $icon_show_small = '<img src="'.WEBDIR.'images/icons/page_find.gif" alt="Datensatz anzeigen" title="Datensatz anzeigen" />';
        //fehlt noch icon von Anzeigen************************************************
        $x=-1;
        // holt sich die maxtiefe global
        global $lastlevel;
        global $submenu;
        // Wenn unterste Menustufe
        if ($level===0):
            // Array durchgehen
            foreach($array AS $key => $value):
                // Wenn submenunamen angezeigt werden sollen diese entsprechend in String speichern
                $string.= "<div class=\"menutype\"><h2>".($menutype=$key).'</h2><br class="clr"/>'.chr(10);
                $y=0;
                // einzelne Menüpunkte der Stufe 1 durchgehen
                foreach($value AS $key1 => $value1):
                   foreach($value1 AS $point)
                        $menuid=$point['menuid'];
                   if ($submenu!=$key1):
                       $y++;
                        $string.= '<div class="left" style="float:left;margin-right:20px;width:30%">'
                        .$l->makeLink($icon_neu_small." neuen Menupunkt anlegen",
                        WEBDIR."admin/menuadmin.htm?view=edit&amp;mode=newpoint&amp;menu=".$menuid)
                        ."<br />".chr(10).'<table class="menuadmin" style="width:100%">';
                        $string.='<tr><th>&nbsp;'.$key1.'</th></tr>'.chr(10).'<tr>';
                   endif;
                   foreach($value1 AS $point):
                        $string.='<tr><td class="td'.($x%2).'">';
                        // Normalen Menupunkt ausgeben
                        $string.= CHR(10).
                        $l->makeLink($icon_delete_small,WEBDIR.'admin/menuadmin.htm?view=delete&amp;menuid='.$point['id'],"right").' '.
                        $l->makeLink($icon_edit_small,WEBDIR.'admin/menuadmin.htm?view=edit&amp;menu='
                        .$point['menuid'].'&amp;mode=edit&amp;menuid='.$point['id']).' '.
                        $l->makeLink($icon_up_red,WEBDIR."admin/menuadmin.htm?mode=positionup&amp;menuid=".$point['id']).' '.
                        $l->makeLink($icon_down_red,WEBDIR."admin/menuadmin.htm?mode=positiondown&amp;menuid=".$point['id'])." ".
                        $point['name']."&nbsp;&nbsp; <br/></td></tr>".chr(10);
                       // Wenn Kindelemente Vorhanden
                       if (!empty($point['children'])):
                           // Rekursiv Funktion aufrufen
                           $string.=$this->showMenuAdmin($point['children'],($point['level']+1));
                       endif;
                   endforeach;
                   if ($submenu!=$key1):
                       $string.='</table></div>'.($y%3==0?'<br class="clr"/>':'');
                       $submenu=$key1;
                       $subarray[$key][]=$key1;
                   endif;
                endforeach;
                foreach($this->_['submenus'] AS $kl => $var)
                    if (!empty($subarray[$var->menutypename]))
                        if(in_array($var->title,$subarray[$var->menutypename]))
                            unset($this->_['submenus'][$kl]);
                foreach($this->_['submenus'] AS $value1)
                    if($value1->menutypename==$key):
                        $y++;
                        $string.= '<div class="left" style="margin-right:20px;width:30%">'
                        .$l->makeLink($icon_neu_small." neuen Menupunkt anlegen",
                        WEBDIR."admin/menuadmin.htm?view=edit&amp;mode=newpoint&amp;menu=".urlencode($value1->id))
                        ."<br />".chr(10).'<table class="menuadmin" style="width:100%">';
                        $string.='<tr><th>&nbsp;'.$value1->title.'</th></tr>'.chr(10).'<tr><tr><td>kein Punkt zugeordnet</td>';
                        $string.='</table></div>'.($y%3==0?'<br class="clr"/>':'');
                     endif;
                // Wenn Submenus augegeben werden sollen dann entsprechend beenden
                $string.= '</div><br class="clr"/><br/>'.chr(10);
            endforeach;
        else:
            // Array mit menupunkten durchgehen
            foreach($array AS $key1 => $point):
              $string.='<tr><td class="td'.($x%2).' level-'.$level.'">';
                    // Normalen Menupunkt ausgeben
                    $string.= CHR(10).
                    $l->makeLink($icon_delete_small,WEBDIR.'admin/menuadmin.htm?view=delete&amp;menuid='.$point['id'],"right").' '.
                    $l->makeLink($icon_edit_small,WEBDIR."admin/menuadmin.htm?view=edit&amp;mode=edit&amp;menuid=".$point['id']).' '.
                    $l->makeLink($icon_up_red,WEBDIR."admin/menuadmin.htm?mode=positionup&amp;menuid=".$point['id']).' '.
                    $l->makeLink($icon_down_red,WEBDIR."admin/menuadmin.htm?mode=positiondown&amp;menuid=".$point['id'])."<span> ".
                    $point['name']."</span>&nbsp;&nbsp; <br/></td></tr>".chr(10);
                    // Wenn Kindelemente Vorhanden
                    if (!empty($point['children'])):
                        // Rekursiv Funktion aufrufen
                        $string.=$this->showMenuAdmin($point['children'],($point['level']+1));
                    endif;
            endforeach;
        endif;
        return $string;
    }
    /**
     * Methode zur Ansicht als DropdownBox
     * @param array $array Menuarray
     * @param int $level aktuelles Level
     * @return string $string gibt das Menu als String zurück
     */
    private function showAsDropDown($array,$level=0)
    {
    	$maxdeep=$this->_['maxdeep'];
        if ($level<$maxdeep):
            // prüfen ob unterstes Level
            if ($level==0):
                // Menuarray durchgehen
                foreach($array AS $value):
                    foreach($value AS $value1):
                        foreach($value1 AS $value2):
                            // Wenn Menupunkt zu Menü gehörend
                            if (intval($value2['menutype'])===intval($this->_['menutypeid'])):
                                $x++;
                                // prüfen ob erster Eintrag
                                if ($tmp_menutypeid !=$value2['menuid']):
                                     // Menutitel zum bearbeiten ausgeben
                                    $menustring.= '<option value="'.$value2['menuid'].'---'.$value2['submenu_left'].'---'.$value2['submenu_right'].'">'
                                    .'Als Hauptpunkt</option> ';
                                    $tmp_menutypeid=$value2['menuid'];
                                endif;
                                // Option mit allen Werten hinzufügen
                                $menustring.= '<option value="'.$value2['menuid'].'---'.$value2['left'].'---'.$value2['right'].'---'.$value2['level'].'"  '
                                .(($value2['left']==$this->_['parentlft']&&$value2['right']==$this->_['parentrgt'])?'selected="selected"':'')
                                .'>&nbsp;&nbsp;&nbsp;'.$value2['name']."</option>";
                                // Wenn Kinder vorhanden Aufruf der Funktion rekursiv
                                if (!empty($value2[children]))
                                    $menustring.= $this->showAsDropDown($value2[children], $value2[level]+1);
                            endif;
                        endforeach;
                    endforeach;
                endforeach;
            // Wenn uneter Stufe
            else :
                // Array durchgehen (Childarray)
                foreach($array AS $value2):
                      // Und untermenu zurückgeben
                      $menustring.= '<option class="level-'.$value2[level].'"
                                    value="'.$value2['menuid'].'---'.$value2['left'].'---'.$value2['right'].'---'.$value2['level'].'"  '
                                .(($value2['left']<$this->_['parentlft']&&$value2['right']>$this->_['parentrgt'])?'selected="selected"':'')
                                .'>&nbsp;&nbsp;&nbsp;'.$value2['name']."</option>";
                    // Wenn Kinder vorhanden Aufruf der Funktion rekursiv
                    if (!empty($value2[children]))
                        $menustring.= $this->showAsDropDown($value2[children], $value2[level]+1);
                endforeach;
            endif;
        endif;
        // Wenn keine Daten vorhanden: Nur ausgabe des Punktes "als Hauptmenu"
        if (empty($menustring))
              $menustring.= '<option value="'.$this->_['submenu']->id.'---'.$this->_['submenu']->lft.
              '---'.$this->_['submenu']->rgt.'">Als Hauptpunkt</option> ';
        // Rückgabe des Strings
        return $menustring;
    }

    private function showAdminMenu()
    {
        // in diesem Fall statisches Menu -> eventuell auf dynamisches Menu umstellen -> Pluginverwaltung vorausgesetzt
        $menu='

          <ul id="adminmenu" style="list-style-type:none;">
          <li><b>allgemeine Einstellungen</b>
          <li>
                <a href="'.WEBDIR.'admin/adminstart.htm">zur Adminstartseite</a>
            </li>
            <li>
            <br/>
            </li>
            <li>
                <a href="'.WEBDIR.'logout.htm">LOGOUT</a>
            </li>
            <li>
            <br/>
            </li>
            <li>
            <b>Seitenaufbau</b>
            <li>
                <a href="'.WEBDIR.'admin/contentadmin.htm">Content pflegen</a>
            </li>
            <li>
                <a href="'.WEBDIR.'admin/menuadmin.htm">Menü pflegen</a>
            </li>
            <li>
                <a href="'.WEBDIR.'admin/startslider.htm">Sliderbilder pflegen</a>
            </li>
            <li>
            <br/>
            </li>
                <b>Nutzerdaten</b>
            <li>
                <a href="'.WEBDIR.'admin/useradmin.htm">User pflegen</a>
            <li>
            </li>
            <li>
            <br/>
            </li>
             <li>
            <b>Leistungskatalog</b>
            <li>
                <a href="'.WEBDIR.'admin/leistungsgruppenadmin.htm">Leistungsgruppen pflegen</a>
            </li>
            <li>
                <a href="'.WEBDIR.'admin/leistungsadmin.htm">Leistungen pflegen</a>
            </li>
            <li>
            <br/></li>
            <li><b>Merkmale</b>

             <li>
                <a href="'.WEBDIR.'admin/figuradmin.htm">Figurangaben pflegen</a>
            </li>
             <li>
                <a href="'.WEBDIR.'admin/geschlechtsadmin.htm">Geschlechter pflegen</a>
            </li>
             <li>
                <a href="'.WEBDIR.'admin/typenadmin.htm">Typen der Anbieterinnen pflegen </a>
            </li>
             <li>
                <a href="'.WEBDIR.'admin/koerbchenadmin.htm">Körbchengrößen pflegen</a>
            </li>
             <li>
                <a href="'.WEBDIR.'admin/kundentypadmin.htm">Kundentypen pflegen</a>
            </li>
            <li>
                <a href="'.WEBDIR.'admin/sexualadmin.htm">sexuelle Orientierungen pflegen</a>
            </li>
             <li>
                <a href="'.WEBDIR.'admin/treffpunkteadmin.htm">Treffpunkte pflegen</a>
            </li>

             <li>
                <a href="'.WEBDIR.'admin/raeumlichkeitenadmin.htm">Räumlichkeiten pflegen (Clubs)</a>
            </li>
             <li>
                <a href="'.WEBDIR.'admin/bizarradmin.htm">bizarre Ausstattungen pflegen (Clubs)</a>
            </li>
             <li>
                <a href="'.WEBDIR.'admin/einrichtungsadmin.htm">Einrichtungen pflegen (Clubs)</a>
            </li>
            <li>
                <a href="'.WEBDIR.'admin/servicesadmin.htm">Services pflegen (Clubs)</a>
            </li>




        </ul>
        ';
        return $menu;
    }

    private function showUserMenu()
    {
        // in diesem Fall statisches Menu -> eventuell auf dynamisches Menu umstellen -> Pluginverwaltung vorausgesetzt
        $menu='

          <ul id="usermenu" style="list-style-type:none;">
          <li>'.
             // '.($func->isInUrl('user/userdaten.htm')?'class="active"':'').'
            '<a href="'.WEBDIR.'user/userdaten.htm" id="userdaten" '.($this->func->isInUrl('user/userdaten.htm')?'class="active"':'').'>Mein Konto</a>
          </li>
          '.(($_SESSION['user']->usertype==2 || $_SESSION['user']->usertype==4 || $_SESSION['user']->usertype==5)?
          '<li>
            <a href="'.WEBDIR.'user/profildaten.htm" id="profildaten" '.($this->func->isInUrl('user/profildaten.htm')?'class="active"':'').'>Meine Profildaten</a>
          </li>
           ':'').'
           <li>
            <a href="'.WEBDIR.'user/buchungen.htm"  id="buchungen" '.($this->func->isInUrl('user/buchungen.htm')?'class="active"':'').'>meine Buchungen</a>
          </li>'
          /*'.(!empty($_SESSION['user']->vip)?'
          <li>
            <a href="javascript:void(0)" id="aktionen" '.($this->func->isInUrl('user/aktionen.htm')?'class="active"':'').'>meine Aktionen</a>
          </li>
          ':'').'
          <li>
            <a href="javascript:void(0)" id="bewertungen" '.($this->func->isInUrl('user/bewertungen.htm')?'class="active"':'').'>Bewertungen</a>
          </li>*/


          .(($_SESSION['user']->usertype==4 || $_SESSION['user']->usertype==5)?'
          <li>
              <a href="'.WEBDIR.'user/models.htm" id="models" '.($this->func->isInUrl('user/models.htm')?'class="active"':'').'>Meine Mitarbeiter</a>
          </li>'
          /*<li>
              <a href="javascript:void(0)" id="statistiken" '.($this->func->isInUrl('user/statistiken.htm')?'class="active"':'').'>Statistiken</a>
          </li>*/
          :'').

        /*<li>
            <a href="javascript:void(0)" id="bewertungen" '.($this->func->isInUrl('user/kalender.htm')?'class="active"':'').'>Mein Kalender</a>
          </li>*/
        '</ul>';
        /*
         * <ul id="usermenu" style="list-style-type:none;">
          <li>'.
             // '.($func->isInUrl('user/userdaten.htm')?'class="active"':'').'
            '<a href="'.WEBDIR.'user/userdaten.htm" id="userdaten" '.($this->func->isInUrl('user/userdaten.htm')?'class="active"':'').'>Mein Konto</a>
          </li>
          <li>
            <a href="'.WEBDIR.'user/profildaten.htm" id="profildaten" '.($this->func->isInUrl('user/profildaten.htm')?'class="active"':'').'>Meine Profildaten</a>
          </li>

          <li>
            <a href="'.WEBDIR.'user/buchungen.htm" id="buchungen" '.($this->func->isInUrl('user/buchungen.htm')?'class="active"':'').'>meine Aktionen</a>
          </li>

          <li>
            <a href="'.WEBDIR.'user/bewertungen.htm" id="bewertungen" '.($this->func->isInUrl('user/bewertungen.htm')?'class="active"':'').'>Bewertungen</a>
          </li>


          '.(($_SESSION['user']->usertype==4 || $_SESSION['user']->usertype==5)?'
          <li>
              <a href="'.WEBDIR.'user/models.htm" id="models" '.($this->func->isInUrl('user/models.htm')?'class="active"':'').'>Meine Mitarbeiter</a>
          </li>
          <li>
              <a href="'.WEBDIR.'user/statistiken.htm" id="statistiken" '.($this->func->isInUrl('user/statistiken.htm')?'class="active"':'').'>Statistiken</a>
          </li>
          ':'').
        '
        <li>
            <a href="'.WEBDIR.'user/kalender.htm" id="bewertungen" '.($this->func->isInUrl('user/kalender.htm')?'class="active"':'').'>Mein Kalender</a>
          </li>
        </ul>';
         */
        return $menu;
    }
}
