<?php
$maxdeep=0;
if (is_array($this->_['menu']))
{
    function showMenu($array,$level=0)
    {
        global $maxdeep;
        if ($level===0):
            foreach($array AS $key => $value):
                echo '<li><ul class="submenu"><li>'.$key.'</li>';
                foreach($value AS $key1 => $value1):
                   $maxdeep=$value1['maxdeep'];
                   echo '<li>'.$value1['name'].'</li>';
                   if (!empty($value1['children'])):
                       showMenu($value1['children'],$level=($value1['level']+1));
                   endif;
                endforeach;
                echo "</ul>";
            endforeach;
        else:
            if ($level<=$maxdeep):
            echo '<ul class="level-'.$level.'">';
            foreach($array AS $key1 => $value1):
               echo '<li>'.$value1['name'].'</li>';
               if (!empty($value1['children'])):
                   showMenu($value1['children'],$level=($value1['level']+1));
               endif;
            endforeach;
            echo '</ul>';
            endif;
        endif;
    }

    echo '<ul id="'.$this->_['menutype'].'">';
    showMenu($this->_['menu']);
    echo '</ul>';
}