<?php
namespace inc;
require_once("model.php");
class Controller
{
    // Private Variablen definieren ($request, $template, $view)
    private $request = null;
    private $template = 'default';
    private $view = null;
    public $func = null;
    /*
    * Konstruktor, erstellet den Controller
    *
    * @param Array $request Array aus $_GET & $_POST.
    */
    public function __construct($request =false)
    {
        global $func;
        global $l;
         // neue Funktionsinstanz
        $this->func = $func;
        // neue Linkinstanz
        $this->l = $l;
        // neue Viewinstanz (für äußeres Template
        $this->view = new View();
        // Für die Klasse die Requestarrays bereitstellen
        $this->request = $request;
        // schauen ob ein Virew angegeben wurde, ansonsten auf default setzen
        $this->template = isset($request['layout'])?$request['layout']:'default';
        //MainModel instanziieren
        $this->model=new Model();
    }
    ##########################################
    # Methode zum anzeigen des Contents.     #
    #                                        #
    # @return String Content der Applikation.#
    ##########################################
    public function display()
    {
        if($this->request['modul'] != 'notizen' && $this->request['modul'] != 'notizadmin')
            unset($_SESSION['nsearch']);
        // neuen Menücontroller für Public menüs
        $menufac = new \inc\std\menu\Controller('public');
        // Instanz der Metafunktion
        $metas = new \Meta();
        // Metadaten setzen
        $this->view->assign('description',$metas->getDescription());
        $this->view->assign('keywords',$metas->getKeywords());
        $this->view->assign('pagetitle',$metas->getTitle());
        // Footermenüs setzen
        if (!empty($_SESSION['user']))
            $this->view->assign('usermenu', $test=$menufac->getRequestetMenu('topmenu_user'));
            // Home /
            // Loggout /logout.htm
            // Profildaten ändern /user/profil.htm
            // Notizen ansehen /user/notizen.htm
            // Klienten ansehen /user/klienten.htm
        else
            $this->view->assign('publicmenu', $menufac->getRequestetMenu('topmenu_public'));
            // Home /
            // Login /login.htm

        if ($_SESSION['user']->usertype==1 || $_SESSION['user']->usertype==2)
            $this->view->assign('admin_menu', $menufac->getRequestetMenu('topmenu_admin'));
            // Userverwaltung /admin/useradmin.htm
            // ASD verwalten /admin/asdadmin.htm
            // Hilfearten verwalten /admin/hilfeartenadmin.htm
            // Klienten verwalten /admin/klientenadmin.htm
            // Notizen verwalten /admin/notizadmin.htm
            // Menü verwalten / admin/menuadmin.htm
            // Inhaltsseiten verwalten / admin/contentadmin.htm

        // Headermenü setzen
        $this->view->assign('header_navi', $menufac->getRequestetMenu('header_navi'));
        // Usermenüs einblenden
        if (!empty($_SESSION['user'])):
            $this->view->assign('header_navi_right_login', $menufac->getRequestetMenu('header_navi_right_login'));
        else :
            $this->view->assign('header_navi_right', $menufac->getRequestetMenu('header_navi_right'));
        endif;
        // Mainmodel initialisieren
        $this->model->getContentByAlias($this->request['modul']);
        $this->content=$this->model->getElement();

        // Adminweiterleitung
        if ($this->func->isInUrl("/user.htm") && $_SESSION['user']->usertype==1):
            header('Location:'.WEBDIR.'admin.htm');
            exit;
        endif;

        //$this->view->assign("news",$this->func->widgetLoader('getnews'));
        // Abfrage des aufgerufenen Moduls
        // Wenn user oder Adminmodul
        if ( !empty( $this->request['res'] ) &&
             !empty( $this->request['modul'] ) &&
             file_exists( LOCALDIR . $this->request['res'] . "/modules/" . $this->request['modul'] . "/controller.php" ) ):

           if( file_exists( LOCALDIR . $this->request['res'] . "/modules/" . $this->request['modul'] . "/config.php" ))
                require( LOCALDIR . $this->request['res'] . "/modules/" . $this->request['modul'] . "/config.php" );

           require( LOCALDIR . $this->request['res'] . "/modules/" . $this->request['modul'] . "/controller.php" );

           $controller = "\\" . $this->request['res'] . "\\modules\\" . $this->request['modul'] . "\\Controller";

        // Wenn normales Modul
        elseif ( !empty( $this->request['modul'] ) && file_exists( LOCALDIR . "modules/".$this->request['modul'] . "/controller.php" ) ):

           if( file_exists( LOCALDIR . "/modules/" . $this->request['modul'] . "/config.php" ))
                require( LOCALDIR . "/modules/" . $this->request['modul'] . "/config.php" );

           require( LOCALDIR . "modules/" . $this->request['modul'] . "/controller.php" );

           $controller = "\\modules\\" . $this->request['modul'] . "\\Controller";

        // wenn Artikel
        elseif( !empty( $this->content ) ):
           require( LOCALDIR . "modules/artikel/controller.php" );
           $controller = "\\modules\\artikel\\Controller";
        // Wenn Newsartikel
        elseif( !empty( $this->newsart ) ):
           include( LOCALDIR . "modules/news/controller.php" );
           $controller = "\\modules\\news\\Controller";
        // ansonsten bitte 404 Status-Seite ausgeben
        else:
           header("HTTP/1.0 404 Not Found");
           $this->request['code']="404";
           include( LOCALDIR . "modules/status_msg/controller.php" );
           $controller = "\\modules\\status_msg\\Controller";
        endif;
        // Modulcontroller instanziieren
        $modulcontroller = new $controller( $this->request );
        // Wenn eingeloggt dann entsprechendes Menü einbinden
        if( $_SESSION['user'] ):
            $menufac = new \inc\std\menu\Controller( 'user' );
            $this->usermenu = $menufac->getRequestetMenu();
            $modulcontroller->usermenu = $this->usermenu;
        endif;
        // Übergabe von Variablen an das Modul
        $modulcontroller->func = $this->func;
        $modulcontroller->l = $this->l;
        $modulcontroller->content = $this->content;
        $modulcontroller->newsart = $this->newsart;

        // Inhalt des Moduls (inkl. Modultemplate) als Contentvariable registrieren
        $this->view->assign('content', $modulcontroller->display());
        // Wenn noch Assign-Variablen vom Modul übergeben wurden, diese auch registrieren
        if ( is_array( $modulcontroller->assignvalues ) ):
            foreach( $modulcontroller->assignvalues AS $key => $value):
                $this->view->assign( $key, $value );
            endforeach;
        endif;
        $startslider = $this->model->fetchSliderImages();
        if ( is_array( $startslider ) ):
            foreach ( $startslider as $k => $slider):
                $img = $this->func->checkPictureAndGetInformations( 'images/sliderimages/', $slider->file );
                if ( $img->error == 0):
                    $slider->file = $img->picture;
                else:
                    unset( $startslider[$k] );
                endif;
            endforeach;
        endif;
        $this->view->assign( 'startslider', $startslider );

        if ($_SESSION['user']->usertype == '1'):
            $menufac = new \inc\std\menu\Controller( 'admin' );
            $this->view->assign( 'adminmenu', $menufac->getRequestetMenu() );
        endif;

        $this->view->assign( 'err' , $_SESSION['err'] );
        unset ( $_SESSION['err'] );
        $this->view->assign( 'msg', $_SESSION['msg'] );
        unset ( $_SESSION['msg'] );
        if ( !empty( $this->request['layout'] ) )
            $this->view->setTemplate( $this->request['layout'], 'inc/templates' );
        // Äußeres Template setzenund laden /->ausgeben
        else
            $this->view->setTemplate( 'NE-Design', 'inc/templates' );

        $this->model->registerClick();
		$this->model->updatetest();



        return $this->view->loadTemplate();



    }
}
?>