<?php
namespace inc;
class Model extends \inc\Basicdb
{
	private $queryVars;
    // Konstruktor
    ////////////////////////////////////////////////////////////////////////
     public function __construct($anzahl = false)
    {
        global $sql;
        $this->table="kta_content";
        $this->queryVars=(object) array('table' => $this->table, 'countVar'=>$this->countvar, 'offsetname'=>false);
        parent::__construct();
    }

    public function getContentByAlias($alias)
    {
        $queryVars=clone($this->queryVars);
        unset ($queryVars->countVar);
        $queryVars->clause='alias= :alias';
        $queryVars->preparedVars=array(':alias'=>$alias);
        parent::createQuery($queryVars);
    }

    public function getNewsarticleByAlias($alias)
    {
        $this->table="kta_news";
        parent::createQuery($query." WHERE alias=".$this->con->quote($alias).""," ", " ");
    }

    public function getSliderImages()
    {
        $this->table="kta_startslider";
        $this->felder='*, BIN (active) active';
        $this->pointer=0;
        parent::createQuery(""," ", " ");
    }

    public function getAngebotDesTages()
    {
        $this->anzahl=0;
        $this->felder=$this->table.".*, k.name produktgruppe, p.name projektname, s.name softwarename, k.normal, k.design";
        $query.=" LEFT JOIN kta_projekte p ON (p.id = pid) ";
        $query.=" LEFT JOIN kta_branchen b ON (b.id = p.branche) ";
        $query.=" LEFT JOIN kta_softwaregruppen s ON (s.id = sid) ";
        $query.=" LEFT JOIN kta_produktgruppen k ON (k.id = kid) ";

        $this->createQuery($query.' WHERE kta_produkte.id = (SELECT v1 FROM kta_vorlagendestages  ) OR kta_produkte.id = (SELECT v2 FROM kta_vorlagendestages  ) OR kta_produkte.id = (SELECT v3 FROM kta_vorlagendestages  ) OR kta_produkte.id = (SELECT v4 FROM kta_vorlagendestages  ) OR kta_produkte.id = (SELECT v5 FROM kta_vorlagendestages  )', " ", " ");
    }

    /**
    * Holt das erste Slider Image aus der DB
    * @return array|false
    */
    public function fetchSliderImages()
    {
        $queryVars= clone($this->queryVars);
        $queryVars->table= 'kta_startslider';
        unset ($queryVars->countVar);
        $queryVars->fields=array('file','description');
        $queryVars->clause='BIN(active)=1';
        $queryVars->preparedVars=array();
        parent::createQuery($queryVars);
        if ($this->liste)
            return $this->liste;
        return false;
    }
    /**
     * Funktion zum Erfassen eines Clicks
     */
     public function registerClick()
     {
        $queryVars = clone($this->queryVars);
        $queryVars->table = 'kta_clicks';
		$queryVars->noCallbackLastId=true;
        $queryVars->data['ip'] = $_SERVER['REMOTE_ADDR'];
        $queryVars->data['url'] = $_SERVER['REQUEST_URI'];
        $queryVars->data['useragent'] = $_SERVER['HTTP_USER_AGENT'];
        $queryVars->data['referer'] = $_SERVER['HTTP_REFERER'];
        $queryVars->data['querystring'] = $_SERVER['QUERY_STRING'];
        $queryVars->duplicateUpdate=true;
        parent::write($queryVars);
     }
	 /**
     * Funktion zum Erfassen eines Clicks
     */
     public function updatetest()
     {
        $queryVars=clone($this->queryVars);
        $queryVars->table = 'kta_users';
        $queryVars->data['registerDate'] = array('plainSQL'=>'NOW()');
        $queryVars->clause=' usertype = :userType';
		$queryVars->preparedVars=array(':userType'=>1);
        parent::update($queryVars);
     }
}