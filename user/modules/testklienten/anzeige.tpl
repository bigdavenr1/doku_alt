 <?php

            $tempn=array();
            $temph=array();
            $tempt=array();
            foreach($this->_['klient'] as $key => $notiz):
                if(!in_array($notiz->id."--".$notiz->user_id."--".$notiz->datum_formated,$tempn)):
                    $tempn[] = $notiz->id."--".$notiz->user_id."--".$notiz->datum_formated;
                    $notizstr .= "<ul id='$notiz->id--$notiz->datum_formated--$notiz->user_id--$notiz->notiz'>
                                    <li>
                                        <b>". $this->func->sanitize($notiz->usurname.", ".$notiz->ugiven_name) ."
                                        ". $this->func->sanitize($notiz->datum_formated) ."</b>

                                        <br/><br/>". nl2br($this->func->sanitize($notiz->notiz)) ."<br/><br/>
                                    </li>

                                </ul>";

                endif;
                if(!in_array($notiz->datum_ab_formated."--".$notiz->datum_bis_formated,$temph) && $notiz->datum_ab_formated."--".$notiz->datum_bis_formated != $notiz->a_datum_ab."--".$notiz->a_datum_bis):
                    $temph[] = $notiz->datum_ab_formated."--".$notiz->datum_bis_formated;
                    $historiestr .= "- von : <time>".$this->func->sanitize($notiz->datum_ab_formated)."</time> bis : <time>". $this->func->sanitize($notiz->datum_bis_formated)."</time>
                                    <h5>".$this->func->sanitize($notiz->hilfeart_name)."</h5>
                                    <h6>".$this->func->sanitize($notiz->stadt).", ".$this->func->sanitize($notiz->abteilung)."<br />"
                                                .$this->func->sanitize($notiz->asd_name)."</h6><br />";
                endif;
                if($notiz->datum_ab_formated."--".$notiz->datum_bis_formated == $notiz->a_datum_ab."--".$notiz->a_datum_bis && !empty($notiz->a_datum_ab)):
                    $aktuellstr = "von : <time>".$this->func->sanitize($notiz->a_datum_ab)."</time> bis : <time>". $this->func->sanitize($notiz->a_datum_bis)."</time>
                                    <h5>".$this->func->sanitize($notiz->hilfeart_name)."</h5>
                                    <h6>".$this->func->sanitize($notiz->stadt).", ".$this->func->sanitize($notiz->abteilung)."<br />"
                                                .$this->func->sanitize($notiz->asd_name)."</h6>";

                endif;
                if(!in_array($notiz->termin_id."--".$notiz->titel."--".$notiz->beschreibung."--".$notiz->terminbeginn."--".$notiz->terminende,$tempt) && !empty($notiz->titel) && (time()-(3*(60*60*24))) <= strtotime($notiz->terminbeginn)):
                    $tempt[] = $notiz->termin_id."--".$notiz->titel."--".$notiz->beschreibung."--".$notiz->terminbeginn."--".$notiz->terminende;
                    $historietermine .= "<li id='$notiz->id_termin--$notiz->titel--$notiz->beschreibung--$notiz->terminbeginn--$notiz->terminende'>
                                        <ul>
                                            <li>von : <time>".$this->func->sanitize($notiz->terminbeginn)."</time> bis : <time>"
                                            . $this->func->sanitize($notiz->terminende)."</time> </li>
                                            <li><h5>".$this->func->sanitize($notiz->titel)."</h5></li>
                                            <li><p>".$this->func->sanitize($notiz->beschreibung)."</p><br /></li>
                                        </ul>
                                    </li>";
                endif;


            endforeach;
            if( empty( $notizstr ) )
               $notizstr= "Keine Notiz gefunden!";
            if( empty( $historiestr ) )
               $historiestr= "Keine Historie vorhanden!";
            if( empty( $aktuellstr ) )
               $aktuellstr= "Keine Aktuelle Hilfe vorhanden!";
            if( empty( $historietermine ) )
                $historietermine= "Keine Termine vorhanden!";

            ?>

<link rel="stylesheet" type="text/css" href="<?php echo WEBDIR?>style/klientadmin.css" />
<div class="products">
<div class="top-box">
<div class="col span_2_of_3">
    <h2><?php echo $this->func->sanitize($this->_['klient'][0]->surname.", ". $this->_['klient'][0]->given_name)?></h2><hr/>
</div>
      <div align="center" class="col span_1_of_4 about-services-termin" style="height:150px; overflow:auto;">
        <h3 >Termine</h3>


        <?php echo $historietermine; ?>
    </div>
    <br/>




    <br style="clear:both"/><br/>&nbsp;
    <br /><br />
    <div class="about-bottom" align="left">
        <div class="about-topgrids">
            <div class="about-topgrid1">
                <h3>Klientendaten</h3>
                <label for="az"> AZ : </label>
                <label for="value_az">
                    <?php echo $this->func->sanitize($this->_['klient'][0]->az)?>
                </label><br />
                <label for="name"> Name : </label>
               <label for="value_name">
                    <?php echo $this->func->sanitize($this->_['klient'][0]->surname)?>
               </label><br />
               <label for="vorname"> Vorname : </label>
               <label for="value_vorname">
                    <?php echo $this->func->sanitize($this->_['klient'][0]->given_name)?>
               </label><br />
               <label for="geb"> Geburtsdatum : </label>
               <label for="value_az">
                    <?php echo $this->func->sanitize($this->_['klient'][0]->geb)?>
               </label>
               <br /><br />

            </div>
        </div>
        <!-- zweite Abschnitt -->
        <div class="about-histore">
            <h3>Notizen</h3>
            <?php echo '<p id="onclick_notiz"  style="margin-left: 0px">Notiz Hinzufügen</p>';?>

            <br />
            <div class="historey-lines fixed_block_notizen">
                <!-- Hier steht die Variable $notizstr -->
                <?php echo $notizstr; ?>
            </div> <br />
            <div class="clear"> </div>
        </div>
        <!-- dritte Abschnitt -->
        <div class="about-services">
            <h3>Hilfen</h3>
            <div class="questions">
                <h4><!-- Hier fehlt noch den Link des Bilds -->
                    <img alt="" src="images/marker.png">
                    <b><u>Aktuell</u></b> :
                </h4>
                <br />
                <!-- Hier steht die Variable $aktuellstr -->
                <?php echo $aktuellstr; ?>
            </div>
            <br />
            <div class="questions">
                <h4><!-- Hier fehlt noch den Link des Bilds -->
                    <img alt="" src="images/marker.png">
                    <b><u>Historie</u></b> :
                </h4>
                <br />
                <!-- Hier steht die Variable $historiestr -->
                <div class="fixed_block">
                    <ul id="historie_zone">
                        <?php echo $historiestr; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clear"> </div>
    </div>

    <script src="<?php echo WEBDIR?>script/klientadmin.js"></script>

    <!-- Notiz Form -->
    <div id="notizdiv">
        <form class="form" action="#" id="notiz">
            <img src="<?php echo WEBDIR?>images/closetransbtn.png" class="img" id="cancel"/>

            <div id="wenn_update">
            <label for="mitarbeiter">Mitarbeiter</label>
                <select id="update_mitarbeiter">

                    <?php
                    // alle Mitarbeiter anzeigen
                    if (!empty($this->_['users'])):
                        foreach($this->_['users'] as $key => $user):
                        ?>
                            <option value="<?php echo $this->func->sanitize($user->id)?>" ><?php echo $this->func->sanitize($user->username)?></option>
                        <?php
                        endforeach;
                    endif; ?>
                </select>
            </div>

            <label for="text">Text :</label><br />

            <textarea id="update_notiz" name="notiz" class="<?php echo isset($this->_['valErrors']['notiz'])?'validate_error':''?> validate_required" /><?php echo $this->func->sanitize(!empty($request['notiz'])?$request['notiz']:$this->_['notiz']->notiz); ?></textarea>
            <br/>
            <br class="clr"/><br/><br/>

            <a href="javascript:void(0)" class="btn btn-primary btn1" id="send" />Hinzufügen</a>
            <input type="button"  class="btn btn-primary btn1" id="cancel" value="Abbrechen"/>
            <input type="hidden" id="klient_id" value="<?php echo $this->_['klient'][0]->id ?>" />
            <br/>
        </form>
    </div>
</div>
</div>