<?php
namespace user\modules\testklienten;
require_once("model.php");
class Controller
{
    // private VAriablen setzen
    private $request = null;
    private $template = 'list';
    private $view = null;
    /**
	 * Konstruktur
	 * bereitet Template und übergebene Entscheidungsvariablen auf
	 * @param Array $request Array mit übergebenen Variablen (GET POST FILE)
	 */
    public function __construct($request)
    {
        // neuen viewer instanziieren
        $this->view = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // template auslesen
        $this->template =isset($this->request['view'])?$this->request['view']:'list';
		// Modelobject
		$this->model = new Model();
    }
	public function display()
    {
    	// prüfen und aufbereiten ob User ID übergeben wurde
    	$this->request['id']=empty($this->request['id'])?0:$this->request['id'];
		// Anhand übergebener ID entscheiden
		switch($this->request['id']):
			// Wenn keine USerid übergeben wurde  (list oder new)
			case '0':
				// Wenn neue Notiz hinzugefügt wird
				if($this->template=="notiz_add" ):
                   $this->model->insertNotiz($this->request);
                   if (!empty($_SESSION['err'])):
                       header("HTTP/1.0 400 Bad Request");
                       echo $_SESSION['err'];
                       unset($_SESSION['err']);
                       exit;
                   endif;
                   $this->view->assign("klient", $this->model->getClientById(intval($this->request['klient_id']),'notiz'));
                   $this->template="notiz_update";

				// Wenn Liste
				else:

					$this->view->assign("klienten",$this->model->getAllClientsMitAsdUndHilfearten($this->request));
                	$this->view->assign("htmlnavi",$this->model->getHtmlNavi('std',preg_replace('~(\&{0,1})res='.$this->request['res'].'~','',preg_replace("~(\&{0,1})modul=".$this->request['modul']."~","",$_SERVER['QUERY_STRING']))));

				endif;
			break;
			// Wenn ID übergeben wurde (edit oder delete)
            default:

                // Template auf edit, wenn nicht gelöscht werden soll
                $this->template =isset($this->request['view'])?$this->request['view']:'anzeige';
                // Benutzerdaten Global zu verfügung stellen
                $this->view->assign("klient",$this->klient = $this->model->getClientById(intval($this->request['id'])));

            break;
		endswitch;


        $this->view->assign('hilfearten', $this->model->getAll('hilfeart',false,false));
        $this->view->assign('asds', $this->model->getAll('asd',false,false));
        $this->view->assign('users', $this->model->getAll('kta_users',false,false));
        $this->view->setTemplate($this->template,dirname(__FILE__));
        return $this->view->loadTemplate();
	}






}
?>