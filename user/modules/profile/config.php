<?php
namespace user\modules\profile;
trait ModulConfig
{
    /**
     * Name der SQL-Tabelle
     */
    protected static $table = "kta_users";
    
    /**
     * Feld id
     */
    protected static $id = 'id';
    /**
     * Feld given_name
     */
    protected static $given_name = 'given_name';
    /**
     * Feld surname
     */
    protected static $surname = 'surname';
    /**
     * Feld street1
     */
    protected static $street1 = 'street1';
    /**
     * Feld street2
     */
    protected static $street2 = 'street2';
    /**
     * Feld zip
     */
    protected static $zip = 'zip';
    /**
     * Feld city
     */
    protected static $city = 'city';
    /**
     * Feld state
     */
    protected static $state = 'state';
    /**
     * Feld country
     */
    protected static $country = 'country';
    /**
     * Feld tel
     */
    protected static $tel = 'tel';
    /**
     * Feld mobil
     */
    protected static $mobil = 'mobil';
    /**
     * Feld fax
     */
    protected static $fax = 'fax';
    /**
     * Feld company
     */
    protected static $company = 'company';
    /**
     * Feld trycount
     */
    protected static $trycount = 'trycount';
    /**
     * Feld username
     */
    protected static $username = 'username';
    /**
     * Feld email
     */
    protected static $email = 'email';
    /**
     * Feld pass
     */
    protected static $pass = 'pass';
    /**
     * Feld usertype
     */
    protected static $usertype = 'usertype';
    /**
     * Feld lastWrongTry
     */
    protected static $lastWrongTry = 'lastWrongTry';
    /**
     * Feld registerDate
     */
    protected static $registerDate = 'registerDate';
    /**
     * Feld lastvisitDate
     */
    protected static $lastvisitDate = 'lastvisitDate';
    /**
     * Feld activationCode
     */
    protected static $activationCode = 'activationCode';
    /**
     * Feld active
     */
    protected static $active = 'active';
    /**
     * Feld language
     */
    protected static $language = 'language';
    /**
     * Feld url
     */
    protected static $url = 'url';
    
    
    
}