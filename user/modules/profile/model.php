<?php
namespace user\modules\profile;
class Model extends \inc\Basicdb
{
    use ModulConfig;    
    // Konstruktor
    ////////////////////////////////////////////////////////////////////////

    function __construct($anzahl = false)
    {
        global $sql;
        $this->anzahl = $anzahl;
        $this->queryVars=(object) array('table' => self::$table, 'countVar'=>$this->countvar, 'cps'=>15, 'offsetname'=>'usite');

        parent::__construct();
    }
	function getUserById($id)
	{
	    $queryVars= clone $this->queryVars;
        

        $queryVars->fields=array(self::$table.".".self::$id,
                                 self::$table.".".self::$tel,
                                 self::$table.".".self::$street1,
                                 self::$table.".".self::$zip,
                                 self::$table.".".self::$city,
                                 self::$table.".".self::$given_name,
                                 self::$table.".".self::$surname,
                                 self::$table.".".self::$username,
                                 self::$table.".".self::$email
                                 );

        $queryVars->clause = "id= :userId";
        $queryVars->preparedVars=array(':userId'=>intval($id));
        unset($queryVars->cps);
        unset($queryVars->offsetname);

	    parent::createQuery($queryVars);
        
        
        return parent::getElement();
        
	}
    
    public function getPWBySessionId()
    {
        $queryVars=clone($this->queryVars); 

        $queryVars->fields=array(self::$table.".".self::$pass);

        $queryVars->clause = "id= :userId";
        $queryVars->preparedVars=array(':userId'=>intval($_SESSION['user']->id));
        unset($queryVars->cps);
        unset($queryVars->offsetname);

        parent::createQuery($queryVars);
        return $this->liste[0]->pass;
    }
    

    public function updateUser($request)
	{//print_r($request);exit;
		$queryVars = clone $this->queryVars;
		$queryVars->data = array("given_name"=>$request['given_name'],
	                          "surname"=>$request['surname'],
	                          "street1"=>$request['street1'],
	                              "zip"=>$request['zip'],
	                             "city"=>$request['city'],
	                             
	                              "tel"=>$request['tel']);
        
			$queryVars->clause="id=:ID";
			$queryVars->preparedVars=array(':ID'=>$_SESSION['user']->id);
			return parent::update($queryVars);
		
		
	}
    public function updatePassword($new_pass)
	{
		$queryVars = clone $this->queryVars;
		$queryVars->data = array("pass"=>$this->func->bcrypt_encode ( $_SESSION['user']->email, $new_pass));
        
		$queryVars->clause="id=:ID";
		$queryVars->preparedVars=array(':ID'=>$_SESSION['user']->id);
		return parent::update($queryVars);

		
	}
    

    public function insertIntoSession($benutzer=false)
    {
        // Alte Session löschen
        unset($_SESSION["user"]);
        // Benutzerdaten in Session schreiben wenn Benutzer übergeben wurde
        if ($benutzer):
            //unset($benutzer->pass);
            $_SESSION["user"]=$benutzer;
            
            // Sanity-Key in SESSION schreiben
            $_SESSION["user"]->SESSION_SANITY_KEY=md5(substr($_SERVER['REMOTE_ADDR'],0,7).$_SERVER['DOCUMENT_ROOT'].$_SERVER['HTTP_USER_AGENT']);
        endif;
        //session_regenerate_id(TRUE);
    }


}

?>