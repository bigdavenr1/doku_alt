<?php // view und Modulmodel einbinden
namespace user\modules\profile;
require_once("model.php");
class Controller
{
    // private VAriablen setzen
    private $request = null;
    private $template = 'edit';
    private $view = null;
    /**
	 * Konstruktur
	 * bereitet Template und übergebene Entscheidungsvariablen auf
	 * @param Array $request Array mit übergebenen Variablen (GET POST FILE)
	 */
    public function __construct($request)
    {
        // neuen viewer instanziieren
        $this->view = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // template auslesen
        $this->template =isset($this->request['view'])?$this->request['view']:'edit';
		// Modelobject
		$this->model = new Model();
    }
	public function display()
    {
    	
	    // prüfen wenn Passwort 2 Zahlen, 2 kleine und 2 groß Bochstaben und 2 Sonderzeichnen enthalt
		if($this->template=="password_verify" ):
            if($this->func->checkPassword($this->request['new_pass'])===true)
                echo 1;
            else
                echo 0;
            exit;                
        //Wenn Passwort geändert werden
        elseif($this->template=="password_insert"):

            if ( ! $this->func->bcrypt_check ( $_SESSION['user']->email,$this->request['old_pass'] , $this->model->getPWBySessionId() ) ):
                echo 0;
            else:
                echo 1;
                $this->model->updatePassword($this->request['new_pass']);
            endif;
            exit;  
        //Wenn personaldaten geändert werden    
        elseif($this->template=="personaldaten_update" ):
            $this->model->updateUser($this->request);
            $_SESSION['msg']='User erfolgreich geändert!';
            $this->model->insertIntoSession( $this->model->getUserById($_SESSION['user']->id));
            $this->template = "edit";
        endif;
                
			
		
        $this->view->setTemplate($this->template,dirname(__FILE__));
        return $this->view->loadTemplate();
	}

    
    
    
	
}
?>