
<?php  $_SERVER['QUERY_STRING']=str_replace("&&", '&', str_replace("res=".$_GET['res'], '', str_replace("modul=".$_GET['modul'], '', $_SERVER['QUERY_STRING'])))?>
<div class="col span_2_of_3">
<h3>Sie sind <?php echo $this->func->sanitize($_SESSION['user']->given_name).", ".$this->func->sanitize($_SESSION['user']->surname); ?> - Personaldaten ändern</h3>
<hr>
<p class="desc">Sie haben die Möglichkeit mit dem nachfolgenden Formularen die Userdaten zu ändern.<br/>* markierte Felder sind Pflichtfelder!</p>

<div class="contact-form">
<br/><br/>
<form action="?view=personaldaten_update" method="post" style="padding-right:0px;margin-right:0px;" id="form_personaldaten">
<fieldset>
    <label>Telefon</label>
    <input type="text" id="tel"  name="tel" value="<?php echo $this->func->sanitize(!empty($request['tel'])?$request['tel']:$_SESSION['user']->tel); ?>"/><br/>
    <label>Anschrift</label>
    <input type="text" id="street1" name="street1" class="<?php echo isset($this->_['valErrors']['street1'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['street1'])?$request['street1']:$_SESSION['user']->street1); ?>"/><br/>



    <br class="clr"/>
    <label>PLZ / Ort</label>
    <br class="clr"/>
    <input style="width:45px;margin-right:4px;float:left"  type="text" id="zip" name="zip" value="<?php echo $this->func->sanitize(!empty($this->request['zip'])?$this->request['zip']:$_SESSION['user']->zip); ?>"/>
    <input type="text" id="city" name="city" style="max-width:500px;margin-top:0px;" value="<?php echo $this->func->sanitize(!empty($this->request['city'])?$this->request['city']:$_SESSION['user']->city); ?>"/><br/>
    <br class="clr"/><br/><br/>
</fieldset>
<fieldset>
   <label>Vorname</label>

    <input type="text" id="given_name" name="given_name" class="<?php echo isset($this->_['valErrors']['given_name'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['given_name'])?$request['given_name']:$_SESSION['user']->given_name); ?>"/><br/>
	<label>Nachname</label>
    <input type="text" id="surname" name="surname" class="<?php echo isset($this->_['valErrors']['surname'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['surname'])?$request['surname']:$_SESSION['user']->surname); ?>"/><br/>
</fieldset>
<br class="clear" />
<br class="clr" style="clear:both;float:none;"/>
<input type="submit" id="send" class="btn btn-primary btn1" name="submit" value="Daten speichern"/>



</form>
</div>
</div>
<div class="col span_1_of_3">
    <h3 style="margin-bottom: 5px;">Passwort ändern</h3>
    Passwörter müssen aus mindestens 2 Großbuchstaben, mindestens 2 Kleinbuchstaben, mindestens 2 Zahlen und mindestens 2 Sonderzeichen bestehen!
    <span id="message_from_db"></span>

    <div class="contact-form">
        <form action="" method="post" style="padding-right:0px;margin-right:0px;" id="form_password">
            <fieldset>
                <label>Jetziges Passwort</label>
                <input type="password" id="old_pass" placeholder="" name="old_pass" class="<?php echo isset($this->_['valErrors']['pass'])?'validate_error':''?> validate_required" autocomplete="off"/><br/>
                <label class="err" id="feedback_current_password"></label>
                <label>Neues Passwort</label>
                <input type="password" id="pass" placeholder="" name="pass" class="<?php echo isset($this->_['valErrors']['pass'])?'validate_error':''?> validate_required" autocomplete="off"/><br/>
                <label class="err" id="feedback_new_password"></label>
                <label>Passwort wiederholen</label>
                <input type="password" placeholder="" id="pass_repeat" name="pass_repeat" class="validate_required" autocomplete="off"/>
                <label class="err" id="feedback_repeat_password"></label>
                <br/>
            </fieldset>
            <br class="clear" />
            <br class="clr" style="clear:both;float:none;"/>
            <!--<input type="submit" id="send" class="btn btn-primary btn1" name="submit" value="Neues Passwort speichern"/>-->
            <a href="javascript:void(0)" class="btn btn-primary btn1" id="send_password" />Neues Passwort speichern</a>
            <input name="email" id="email" type="hidden" value="<?php echo $this->func->sanitize($this->_['user'][0]->email); ?>" />
            <input name="saved_password" id="saved_password" type="hidden" value="<?php echo $this->func->sanitize($_SESSION['user']->pass); ?>" />
        </form>

    </div>
</div>
<div class="clr"/></div>
<script src="<?php echo WEBDIR?>script/password.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WEBDIR?>style/klientadmin.css" />
