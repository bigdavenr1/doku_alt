<?php
    if(!empty($this->_['klient'])):
        $tempn=array();
        $temph=array();
        foreach($this->_['klient'] as $key => $notiz):
            if(!in_array($notiz->id."--".$notiz->user_id."--".$notiz->datum_formated,$tempn)):
                $tempn[] = $notiz->id."--".$notiz->user_id."--".$notiz->datum_formated;
                $notizstr .=
                                "<ul id='$notiz->id--$notiz->datum_formated--$notiz->user_id--$notiz->notiz'>
                                    <li>
                                        <b>". $this->func->sanitize($notiz->usurname.", ".$notiz->ugiven_name) ."<br />
                                        ". $this->func->sanitize($notiz->datum_formated) ."</b>

                                        <br/><br/>". nl2br($this->func->sanitize($notiz->notiz)) ."<br/><br/>
                                    </li>
                                    <div class='clear'> </div>
                                </ul>"
                        ;
            endif;
        endforeach;
        if( empty( $notizstr ) )
           $notizstr= "Keine Notiz gefunden!";
   endif;
?>

<h3>Klients Notizen</h3>
<p id="onclick_notiz"  style="margin-left: 0px">Notiz Hinzufügen</p>
<br />
<div class="historey-lines fixed_block_notizen">
    <!-- Hier steht die Variable $notizstr -->
    <?php echo $notizstr; ?>
</div>
<br />
<div class="clear"> </div>