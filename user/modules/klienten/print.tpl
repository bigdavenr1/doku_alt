<!DOCTYPE html>
<html>
    <head>
    <title>Ausdruck Notizen <?php echo $this->_['notizen'][0]->surname?>, <?php echo $this->_['notizen'][0]->given_name?><?php

        if (!empty($_GET['start']))
            echo ' vom: '.$_GET['start'];
        if (!empty($_GET['ende']))
            echo ' bis: '.$_GET['ende'];
        ?></title>
    </head>
    <body>
        <a href="javascript:window.print()"><img src="<?php echo WEBDIR?>images/icons/print.gif"/></a>
<br/>
        <h1>Notizliste für <?php echo $this->_['notizen'][0]->surname?>, <?php echo $this->_['notizen'][0]->given_name?>
        <?php

        if (!empty($_GET['start']))
            echo ' vom: '.$_GET['start'];
        if (!empty($_GET['ende']))
            echo ' bis: '.$_GET['ende'];
        ?>

        </h1>

        <hr/>
        <?php
        if (empty($this->_['notizen'][0]->klient_id)):
            echo 'keine Notizen zu diesen Kriterien vorhanden!';
        else:
            foreach($this->_['notizen'] AS $notiz):
                echo '<div style="float:none;clear:both;">';
                    echo ''.$notiz->datum_formated."<br/><i>".$notiz->usurname.", ".$notiz->ugiven_name."</i><br/>-------------------<br/>";
                    echo ''.nl2br($notiz->notiz);
                echo '</div><br/><hr style="float:none;clear:both;"/><br/>';
            endforeach;
        endif;?>

    </body>
</html>