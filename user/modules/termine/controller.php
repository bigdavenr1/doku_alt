<?php // view und Modulmodel einbinden
namespace user\modules\termine;
require_once("model.php");
class Controller
{
    // private VAriablen setzen
    private $request = null;
    private $template = 'list';
    private $view = null;
    /**
	 * Konstruktur
	 * bereitet Template und übergebene Entscheidungsvariablen auf
	 * @param Array $request Array mit übergebenen Variablen (GET POST FILE)
	 */
    public function __construct($request)
    {
        // neuen viewer instanziieren
        $this->view = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // template auslesen
        $this->template =isset($this->request['view'])?$this->request['view']:'list';
		// Modelobject
		$this->model = new Model();
    }
	public function display()
    {
    	// prüfen und aufbereiten ob Asd ID übergeben wurde
    	$this->request['id']=empty($this->request['id'])?0:$this->request['id'];
		// Anhand übergebener ID entscheiden
		switch($this->request['id']):
			// Wenn keine Asd id übergeben wurde  (list oder new)
			case '0':
				// Wenn neuer DAtensatz
				if ($this->template=='edit'):
					$this->editOrDelete($id,'new');

		            // Wenn Liste
				else:
					$this->view->assign("termine",$this->model->getAllTermine($this->request));
                	$this->view->assign("htmlnavi",$this->model->getHtmlNavi());

				endif;
				break;
			// Wenn ID übergeben wurde (edit oder delete)
			default:
				// Template auf edit, wenn nicht gelöscht werden soll
				$this->template =isset($this->request['view'])?$this->request['view']:'edit';
                // Asd daten Global zu verfügung stellen
				$this->view->assign("termin",$this->asd = $this->model->getTerminById(intval($this->request['id'])));
				// Prüfung ob gelöscht oder bearbeitet werden soll.
				$this->editOrDelete($this->asd->id);
				break;
		endswitch;
        $this->view->setTemplate($this->template,dirname(__FILE__));
        return $this->view->loadTemplate();
	}

	private function changeData($modus)
	{
        return $this->model->updateOrNewAsd($this->request,$this->asd);
	}

	private function editOrDelete($id,$modus=false)
	{
		if ($this->template=='edit' && !empty($this->request['submit'])):
			$id = $this->changeData($modus);
			$_SESSION['msg']='Termin erfolgreich'.(empty($modus)?'geändert':'angelegt');
				header('Location:'.WEBDIR.'admin/termine.htm');

        elseif ($this->template=='delete' && !empty($this->request['confirmed'])):
        	$this->model->deleteById($id);
            if ($this->user->usertype!=1):
                $_SESSION['msg']='Termin erfolgreich gelöscht!';
			    header('Location:'.WEBDIR.'admin/termine.htm');
				exit;
            endif;
		endif;
	}
}
?>