<?php // Modulview und Modulmodel einbinden
namespace user\modules\userstart;
require_once("model.php");
class Controller
{
    // private VAriablen setzen
    private $request = null;
    private $template = 'userstart';
    private $modulview = null;
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->modulview = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->template ='userstart';
    }
    ############################################
    # Methode zum anzeigen des Moduöl-Contents.#
    #                                          #
    # @return String Content der Applikation.  #
    ############################################
    public function display()
    {

        $this->assignvalues['noslider']=true;

        // modultemplate setzen
        $this->modulview->setTemplate($this->template,dirname(__FILE__));
        // Modelklasse instanziieren
        $userstartfac=new Model();
        // methode zum Abruf des Artikel aufrufen
        $user=$userstartfac->getById($_SESSION['user']->id,'kta_users');
        // Videocodes auslesen
        if (!empty($user)):
            $this->modulview->assign('username',$user->vorname." ".$user->name);
            $this->modulview->assign('content',$userstartfac->getContent());
        else:
            // SESSIONdaten wieder löschen und umleiten
            unset ($_SESSION['user']);
            header("Location: ".WEBDIR."status_msg/Session_destroyed");
        endif;

         // Template laden, füllen und ausgeben
        return $this->modulview->loadTemplate();
    }
}
?>