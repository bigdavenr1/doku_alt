<?php
namespace user\modules\userstart;
class Model extends \inc\Basicdb
{
    use ModulConfig;
    
    // Konstruktor
    ////////////////////////////////////////////////////////////////////////

    function __construct($anzahl = false)
    {
        global $sql;
        $this->queryVars=(object) array('table' => self::$table, 'offsetname'=>false);
        parent::__construct();
    }
    
    public function getContent()
    {
        $queryVars= clone($this->queryVars);
        $queryVars->table="kta_content";
        $queryVars->fields=array('`fulltext`');
        $queryVars->clause="alias='usercontent'";
        parent::createQuery($queryVars);
        return parent::getElement();
    }
}
?>