<?php  $_SERVER['QUERY_STRING']=str_replace("&&", '&', str_replace("res=".$_GET['res'], '', str_replace("modul=".$_GET['modul'], '', $_SERVER['QUERY_STRING'])))?>
<div class="col span_2_of_3">
<h3>Notizen <?php echo empty($this->_['notiz'])?'neu anlegen':'ändern';?></h3>
<hr>
<p class="desc">Sie haben die Möglichkeit mit dem nachfolgenden Formularen die Notizen zu ändern.<br/>* markierte Felder sind Pflichtfelder!</p>

<div class="contact-form">
<br/><br/>
<form action="" method="post" style="padding-right:0px;margin-right:0px;">
<fieldset>
    <label for="klient">Klienten</label>
        <select name="klient_id">
            <option value="0">Alle</option>
    <?php

                    foreach($this->_['klienten'] AS $key => $value):
                        $klienten[$key]=(array) $value;
                    endforeach;

                    function array_sort($array, $on, $order=SORT_ASC)
                    {
                        $new_array = array();
                        $sortable_array = array();

                        if (count($array) > 0) {
                            foreach ($array as $k => $v) {
                                if (is_array($v)) {
                                    foreach ($v as $k2 => $v2) {
                                        if ($k2 == $on) {
                                            $sortable_array[$k] = $v2;
                                        }
                                    }
                                } else {
                                    $sortable_array[$k] = $v;
                                }
                            }

                            switch ($order) {
                                case SORT_ASC:
                                    asort($sortable_array);
                                break;
                                case SORT_DESC:
                                    arsort($sortable_array);
                                break;
                            }

                            foreach ($sortable_array as $k => $v) {
                                $new_array[$k] = $array[$k];
                            }
                        }

                        return $new_array;
                    }

                    $klienten=array_sort($klienten, 'surname', SORT_ASC);

    // alle Klienten zu auswählen
    foreach($klienten as $key => $klient):
        if($klient['id'] == $this->_['notiz']->klient_id):

    ?>
              <option value="<?php echo $this->func->sanitize($klient-['id'])?>" selected ><?php echo $this->func->sanitize($klient['surname']).", ".$this->func->sanitize($klient['given_name'])?></option>
    <?php
        else:
            echo "klient-id ist : ".$this->_['notiz']->klient_id;
    ?>
              <option value="<?php echo $this->func->sanitize($klient['id'])?>"><?php echo $this->func->sanitize($klient['surname']).", ".$this->func->sanitize($klient['given_name'])?></option>
    <?php endif; endforeach; ?>
    </select>
   <label>Text :</label>

    <textarea placeholder="Notiz" id="notiz" name="notiz" class="<?php echo isset($this->_['valErrors']['notiz'])?'validate_error':''?> validate_required" /><?php echo $this->func->sanitize(!empty($request['notiz'])?$request['notiz']:$this->_['notiz']->notiz); ?></textarea>
    <br/>
    <br class="clr"/><br/><br/>
</fieldset>
<?php if (!empty($this->request['id'])):?>
<fieldset>



</fieldset>
<?php endif;?>

<br class="clear" />
<br class="clr" style="clear:both;float:none;"/>

<input type="submit" class="btn btn-primary btn1" name="submit" value="Daten speichern"/>



</form>
</div>
</div>
<div class="col span_1_of_3">
<h3>Hinweis</h3>

Ein Hinweis zu schreiben später!
<br/><br/>

</div>
<div class="clr"/></div>
