<?php //print_r($this->_['notizen']); ?>
<div class="section group">
                <div class="col span_2_of_3">
                  <div class="contact-form">
                <h3>Notizen</h3>
                <hr>
                <p class="desc">Übersicht über alle Notizen. auf der rechten Seite können die Daten gefiltert werden</p>

               <br/>

                <?php echo  $this->link->makeLink($icon_neu_small." Neuen Notiz einpflegen",WEBDIR."user/notizen.htm?view=edit","adminlink")?>
                <br/>
                <table style="width:98%">
                <tr>
                    <?php
                      echo $this->func->tableHeadSort("AZ","az","","","width:7%");
                      echo $this->func->tableHeadSort("Name","surname","","","width:18%;");

                      echo $this->func->tableHead("Notiz","","","width:50%");
                      echo $this->func->tableHeadSort("Info","datum");
                    ?>
                </tr>
                <?php
                if (!empty($this->_['notizen'])):
                    // Studiodaten abrufen
                    foreach($this->_['notizen'] as $key => $notiz):
                    // Ausgabe
                        if(!empty($notiz->notiz)):
                        ?>
                        <tr class="td<?php echo $x%2?>">
                            <td>
                                <?php echo $this->func->sanitize($notiz->az)?>
                            </td>
                            <td>
                                <?php echo  $this->link->makeLink($this->func->sanitize($notiz->klient_surname.", ".$notiz->klient_given_name) , WEBDIR."user/klienten.htm?view=anzeige&amp;id=".$notiz->klient_id,"adminlink");?>
                            </td>
                            <td>
                                <div style="max-height:100px;overflow:auto;">
                                    <?php echo nl2br($this->func->sanitize($notiz->notiz))?>
                                </div>
                            </td>
                            <td>
                                <?php echo $this->func->sanitize($notiz->surname.", ".$notiz->given_name)?>
                                <br>
                                <?php echo $this->func->sanitize($notiz->datum_formated)?>
                            </td>

                        </tr>

                     <?php
                        endif;
                     endforeach;
                     ?></table><br/><?php
                     echo $this->_['htmlnavi'];
                 else:
                    ?> </table><br/>
                    <?php echo $this->_['htmlnavi'];

                 endif;
                 ?>

            <br/><br/>
            <?php echo  $this->link->makeLink($icon_neu_small." Neuen Notiz einpflegen",WEBDIR."user/notizen.htm?view=edit","adminlink")?>
 </div>
                </div>
                <div class="col span_1_of_3">
                    <div class="contact_info">

                      <h3>Filter</h3>
                      <br/><br/><br/><br/>
                     <form action="" method="post" style="padding-right:0px;margin-right:0px;">
                     <input type="hidden" name="submit1" value="1"/>
                    <label for="klient">Klienten</label>
                    <select name="klient" onchange="this.form.submit()">
                        <option value="0">Alle</option>
                    <?php
                    foreach($this->_['klienten'] AS $key => $value):
                        $klienten[$key]=(array) $value;
                    endforeach;

                    function array_sort($array, $on, $order=SORT_ASC)
                    {
                        $new_array = array();
                        $sortable_array = array();

                        if (count($array) > 0) {
                            foreach ($array as $k => $v) {
                                if (is_array($v)) {
                                    foreach ($v as $k2 => $v2) {
                                        if ($k2 == $on) {
                                            $sortable_array[$k] = $v2;
                                        }
                                    }
                                } else {
                                    $sortable_array[$k] = $v;
                                }
                            }

                            switch ($order) {
                                case SORT_ASC:
                                    asort($sortable_array);
                                break;
                                case SORT_DESC:
                                    arsort($sortable_array);
                                break;
                            }

                            foreach ($sortable_array as $k => $v) {
                                $new_array[$k] = $array[$k];
                            }
                        }

                        return $new_array;
                    }

                    $klienten=array_sort($klienten, 'surname', SORT_ASC);
                    // alle Klienten anzeigen
                    foreach($klienten as $key => $klient):  ?>
                        <option value="<?php echo $this->func->sanitize($klient['id']) ?>" <?php echo $klient['id']==$_SESSION[nsearch]['k']?'selected="selected"':''?>>
                            <?php echo $this->func->sanitize($klient['surname']).", ".$this->func->sanitize($klient['given_name'])?>
                        </option>
                    <?php endforeach; ?>
                    </select>

                    <label for="hilfeart">Hilfearten</label>
                    <select name="hilfeart" onchange="this.form.submit()" >
                        <option value="0">Alle</option>
                    <?php
                    // alle Hilfearten anzeigen
                    foreach($this->_['hilfearten'] as $key => $hilfeart):
                    ?>
                            <option value="<?php echo $this->func->sanitize($hilfeart->id)?>" <?php echo $hilfeart->id==$_SESSION[nsearch]['h']?'selected="selected"':''?>><?php echo $this->func->sanitize($hilfeart->name)?></option>
                    <?php endforeach; ?>
                    </select>
                    <label for="asd">ASD</label>
                    <select name="asd" onchange="this.form.submit()">
                        <option value="0">Alle</option>
                    <?php
                    // alle ASDs anzeigen
                    foreach($this->_['asds'] as $key => $asd):
                    ?>
                            <option value="<?php echo $this->func->sanitize($asd->id)?>" <?php echo $asd->id==$_SESSION[nsearch]['a']?'selected="selected"':''?>><?php echo $this->func->sanitize($asd->name)?></option>
                    <?php endforeach; ?>
                    </select>
                    <label for="user">Mitarbeiter</label>
                    <select name="user" onchange="this.form.submit()">
                        <option value="0">Alle</option>
                    <?php
                    // alle ASDs anzeigen
                    foreach($this->_['users'] as $key => $user):
                    ?>
                            <option value="<?php echo $this->func->sanitize($user->id)?>" <?php echo $user->id==$_SESSION[nsearch]['u']?'selected="selected"':''?>><?php echo $this->func->sanitize($user->surname).", ".$this->func->sanitize($user->given_name)?></option>
                    <?php endforeach; ?>
                    </select>
                </form>
                    </div>

                 </div>
                   <div class="clear"></div>
              </div>
