$(document).ready(function()
{
    var form_personaldaten=$("#form_personaldaten");
    var form_password=$("#form_password");

    var submit_form_personaldaten = $('#form_personaldaten #submit');
    var submit_form_password = $('#form_password #submit');

    var old_pass=$('#old_pass');
    var pass=$('#pass');
    var pass_repeat=$('#pass_repeat');

    var valid_new_password = false;
    var valid_new_repeat_password = false;

    //Nachrichten der Rückmeldungen verbergen
    $('#feedback_current_password').hide();
    $('#feedback_new_password').hide();
    $('#feedback_repeat_password').hide();

    //Passwort Formular schicken
    $('#form_password #send_password').click(function()
    {
        password_treatement();
    });

    //in dem Moment die Person schreibt, Passwort wird geprüft
    $('#pass').keyup(function()
    {
        var p=$(this);
        $.post("?view=password_verify&layout=empty"
                ,{
                    new_pass : $.trim(pass.val())
                 }
                , function(e){
                    //prüfen ob neues Passwort und wiederholtes Passwort gültig sind (2 Zahlen, 2 kleineBuchstaben, 2 großeBuchstaben und 2 sonderzeichnen)
                    if(e == 0 && p.val().length > 0)
                    {
                        p.css('border','1px solid #ff0000');
                        $('#pass_repeat').css('border','1px solid #ff0000');
                        $('#feedback_new_password').html('Ihr neues Passwort ist nicht korrekt!');
                        $('#feedback_new_password').css("width","89%");
                        $('#feedback_new_password').css("margin-top","5px");
                        $('#feedback_new_password').css("margin-bottom","10px");
                        $('#feedback_new_password').css('display','block');
                        p.parent('form').attr('action','javascript:void(0)');

                    }
                    else
                    {
                        p.css('border','1px solid #CFCFCF');
                        $('#feedback_new_password').hide();
                        p.parent('form').attr('action','');
                        valid_new_password = true;
                    }
                 });

    });

    //in dem Moment die Person schreibt, Passwort wird geprüft wenn gut wiederholt ist
    $('#pass_repeat').keyup(function()
    {
        if($(this).val() != $('#pass').val() && $(this).val().length >0 )
        {
            $(this).css('border','1px solid #ff0000');
            $('#feedback_repeat_password').html('Ihr wiederholtes Passwort ist nicht korrekt!');
            $('#feedback_repeat_password').css("width","89%");
            $('#feedback_repeat_password').css("margin-top","5px");
            $('#feedback_repeat_password').css("margin-bottom","10px");
            $('#feedback_repeat_password').css('display','block');
            $(this).parent('form').attr('action','javascript:void(0)');

        }
        else
        {
            $(this).css('border','1px solid #CFCFCF');
            $('#feedback_repeat_password').hide();
            $(this).parent('form').attr('action','');
            valid_new_repeat_password = true;
        }
    });

    //function ajax für das Passwort speichern nach send buton geklickt
    function password_treatement()
    {


        if($.trim(old_pass.val()) == "" ){
            old_pass.css('border','1px solid #ff0000');
            return false;
        }

        //prüfen ob jetziges Passwort gültig ist, neues Passwort und wiederholtes Passwort gültig auch sind
        if($.trim(old_pass.val()) != "" && valid_new_password == true && valid_new_repeat_password == true )
        {
            //neues Passwort wird geschpeichert wenn das richtig ist
            $.post("?view=password_insert&layout=empty",
                {
                    old_pass : $.trim(old_pass.val()),
                    new_pass : $.trim(pass.val()),
                    repeat_pass : $.trim(pass_repeat.val())
                },

                  function(e)
                  {
                        $('#message_from_db').html(e);
                        //prüfen ob Passwort richtig oder falsch ist
                        if(e==1)
                        {

                            $('#message_from_db').css("border","1px solid #006400");
                            $('#message_from_db').css("background-color","#F0ffF0");
                            $('#message_from_db').html('Passwort erfolgreich geändert!');
                            $('#feedback_current_password').hide();

                            //Rückmeldung für Zeitraffer 5 Sekunden
                            setTimeout(function() { $("#message_from_db").css("border","1px solid #ffffff"); }, 5000);
                            setTimeout(function() { $("#message_from_db").css("background-color","#ffffff"); }, 5000);
                            setTimeout(function() { $("#message_from_db").html(""); }, 5000);

                        }
                        else
                        {
                            $('#message_from_db').css("border","1px solid #ff0000");
                            $('#message_from_db').css("background-color","#fff5ee");
                            $('#message_from_db').html('Passwort leider nicht geändert!');
                            $('#feedback_current_password').html('Ihr jetziges Passwort ist nicht korrekt!');
                            $('#feedback_current_password').css("width","89%");
                            $('#feedback_current_password').css("margin-top","5px");
                            $('#feedback_current_password').css("margin-bottom","10px");
                            $('#feedback_current_password').css('display','block');
                        }
                  }

              );


        }

    }




    //entfernen rot border wenn man schreibt
    old_pass.keyup(function()
    {
        if($(this).val() != "")
        {
            $(this).css('border', '1px solid #cfcfcf');
        }
    });

    //entfernen rot border wenn man schreibt
    $('#form_personaldaten #tel').keyup(function()
    {
        if($(this).val() != "")
        {
            $(this).css('border', '1px solid #cfcfcf');
        }
    });

    //entfernen rot border wenn man schreibt
    $('#form_personaldaten #street1').keyup(function()
    {
        if($(this).val() != "")
        {
            $(this).css('border', '1px solid #cfcfcf');
        }
    });

    //entfernen rot border wenn man schreibt
    $('#form_personaldaten #city').keyup(function()
    {
        if($(this).val() != "")
        {
            $(this).css('border', '1px solid #cfcfcf');
        }
    });

    //entfernen rot border wenn man schreibt
    $('#form_personaldaten #zip').keyup(function()
    {
        if($(this).val() != "")
        {
            $(this).css('border', '1px solid #cfcfcf');
        }
    });

    //entfernen rot border wenn man schreibt
    $('#form_personaldaten #given_name').keyup(function()
    {
        if($(this).val() != "")
        {
            $(this).css('border', '1px solid #cfcfcf');
        }
    });

    //entfernen rot border wenn man schreibt
    $('#form_personaldaten #surname').keyup(function()
    {
        if($(this).val() != "")
        {
            $(this).css('border', '1px solid #cfcfcf');
        }
    });

    //Prüfung wenn Felder leer ist
    $('#form_personaldaten #send').click(function(){
        var a = $(this);

        if( $('#tel').val() == "")
        {
            $('#tel').css('border','1px solid #ff0000');
            $('#tel').focus();
            return false;
        }


        if( $('#street1').val() == "")
        {
            $('#street1').css('border','1px solid #ff0000');
            $('#street1').focus();
            return false;
        }


        if( $('#zip').val() == "")
        {
            $('#zip').css('border','1px solid #ff0000');
            $('#zip').focus();
            return false;
        }


        if( $('#city').val() == "")
        {
            $('#city').css('border','1px solid #ff0000');
            $('#city').focus();
            return false;
        }


        if( $('#given_name').val() == "")
        {
            $('#given_name').css('border','1px solid #ff0000');
            $('#given_name').focus();
            return false;
        }


        if( $('#surname').val() == "")
        {
            $('#surname').css('border','1px solid #ff0000');
            $('#surname').focus();
            return false;
        }


    });

});