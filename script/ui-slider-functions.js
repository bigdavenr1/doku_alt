$.each(sliderarray, function(k, v){
    var mantissas = [1, 1.2, 1.4, 1.6, 1.8, 2, 2.5, 3, 3.5, 4, 4.5, 5, 6, 7, 8, 9];
    var slider_values = [];
    /*
     * Function zum erstellen des Sliderarrays
     * @param max Maximum
     */
    function getSliderArray(field,min,max,type)
    {
        // slidervalues löschen und Null einfügen
        slider_values = [(min>0?min:0)];
        if (type=='linear')
        {
            for (var x=min;x<=max;x++)
            {
                slider_values.push(x);
            }
        }
        else
        {
            // Expotentiales auffüllen des Sliders
            for (var base = 1; base < max; base *= 10) {
                for (var i = 0; i < mantissas.length; i++)
                    if ((mantissas[i] * base)<max && (mantissas[i] * base)>min)
                        slider_values.push(mantissas[i] * base);
            }
            slider_values.splice(2, 1);
            slider_values.splice(2, 1);
            slider_values.splice(2, 1);
            slider_values.splice(2, 1);
            slider_values.splice(3, 1);
            slider_values.splice(4, 1);
            slider_values.splice(5, 1);
        }



        // Maximalwert als Sliderwert hinzufügen und Sliderlänge zurückgeben lassen
        slider_values.push(max);
        slider_values=$.unique(slider_values);
        slider_values.sort(function(a,b){return a-b;});
        return (slider_values.length-1);
    }
    //Zahlen in bestimmtes Format umwandeln
    function number_format (number)
    {
        var exponent = "";
        var numberstr = number.toString ();


        var temp = Math.pow (10, 0);
        number = Math.round (number * temp) / temp;
        var sign = number < 0 ? "-" : "";
        var integer = (number > 0 ?  Math.floor (number) : Math.abs (Math.ceil (number))).toString ();
        var fractional = number.toString ().substring (integer.length + sign.length);
        fractional = fractional.length > 1 ? ("." + fractional.substring (1)) : "";
        thousands_sep = fractional.length == 0 ?  "." : null;
        for (i = integer.length - 3; i > 0; i -= 3)
                  integer = integer.substring (0 , i) + thousands_sep + integer.substring (i);
        return sign + integer + fractional + exponent;
    }
    /*
     * Funktion für die Slidereinstellung Gilt sowohl für die ANfangswerte als auch die eingestellten Werte
     * @param String field gibt an welches Feld verändert werden soll
     * @param int val1 Minwert
     * @param int val2 Maxwert
     */
    function getSliderValue(field,val1,val2)
    {
        var end;
        switch(field)
        {
            case "preis":
                end=' €';
                break;
            case "groesse":
                end=" cm";
                break;
             case 'distancec':
             case "distancea":
                end=" km";
                break;
            case "age":
            default:
                end="";
        }
        // Setzen der neuen Werte und des Suffix
        if (values[field]!== undefined )
        {
            $( "#" + field + "Min" ).html('<span>'+values[field][val1-1]+'</span>' + end);
            $( "#" + field + "Max" ).html('<span>'+values[field][val2-1]+'</span>' + end);
        }
        else
        {
            $( "#" + field + "Min" ).html('<span>'+number_format(val1)+'</span>' + end);
            $( "#" + field + "Max" ).html('<span>'+number_format(val2)+'</span>' + end);
        }
    }
    // Function für die Slider
    $(function()
    {
        $( "#slider-" + v[0] ).slider(
        {
            range: true,
            // Sliderlänge zur Bestimmung der Abstände der einzelnen Bereiche des Sliders
            max: getSliderArray(v[0], v[1] , v[2],($( "#slider-" + v[0] ).hasClass('linear')?'linear':($( "#slider-" + v[0] ).hasClass('values')?'values':false))),

            //min: slider_values.indexOf(v[1]),
            min: jQuery.inArray( v[1], slider_values ),
            values: [ jQuery.inArray( v[3], slider_values ), jQuery.inArray( v[4], slider_values )],
            //Slide Funktion -> Werte ändern sich nur durch verstellen des Sliders
            slide: function( event, ui )
            {
                // Sliderwerte setzen
                getSliderValue(v[0],slider_values[ui.values[0]],slider_values[ui.values[1]]);

            }
        });
        $( "#slidernorange-" + v[0] ).slider(
        {
            range: 'min',
            // Sliderlänge zur Bestimmung der Abstände der einzelnen Bereiche des Sliders
            max: getSliderArray(v[0], v[1] , v[2],($( "#slider-" + v[0] ).hasClass('linear')?'linear':($( "#slider-" + v[0] ).hasClass('values')?'values':false))),
            min: jQuery.inArray( v[1], slider_values ),
            value: jQuery.inArray( v[4], slider_values ),
            //Slide Funktion -> Werte ändern sich nur durch verstellen des Sliders
            slide: function( event, ui )
            {
                // Sliderwerte setzen
                getSliderValue(v[0],slider_values[ui.value],slider_values[ui.value]);
                name="ind."+v[0];
                eval(name);
                name=ui.value;
                //console.debug(name);
            }
        });
        //gegebene Startwerte werden gesetzt
        if(slider_values[$( "#slider-" + v[0]).slider( "values", 1 )]==undefined)
            getSliderValue(v[0],v[3],slider_values[$( "#slidernorange-" + v[0]).slider( "value" )]);
        else
            getSliderValue(v[0],v[3],slider_values[$( "#slider-" + v[0]).slider( "values", 1 )]);
    });
});