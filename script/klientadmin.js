$(document).ready(function()
{
    var ha_div=$("#hilfeartdiv");
    var notiz_div=$("#notizdiv");
    var termin_div=$("#termindiv");

    var add_ha_obj=$('#select_hilfeart_add');
    var add_asd_obj=$('#select_asd_add');
    var add_da_obj=$('#datum_ab_add');
    var add_db_obj=$('#datum_bis_add');

    var add_titel_obj=$('#titel_add');
    var add_beschreibung_obj=$('#beschreibung_add');
    var add_terminbeginn_obj=$('#terminbeginn_add');
    var add_terminende_obj=$('#terminende_add');

    var update_user_obj=$('#update_mitarbeiter');
    var update_text_obj=$('#update_notiz');


    var all_infos=false;

    //Hilfeart und ASD hinzufügen
    $(document).on('click',"#onclick",function()
    {
        $('#select_hilfeart_add option:eq(0)').before('<option value="0" selected>Wählen Sie eine Hilfeart</option>');
        $('#select_asd_add option:eq(0)').before('<option value="0" selected>Wählen Sie eine asd</option>');
        $('#hilfeart #send').text('Hinzufügen');
        ha_div.css("display", "block");
    });

     //notiz hinzufügen
    $(document).on('click',"#onclick_notiz",function()
    {
        $('#notiz #wenn_update').css("display","none");
        $('#update_mitarbeiter option:eq(0)').before('<option value="0" selected>Wählen Sie einen User</option>');
        $('#notiz #send').text('Hinzufügen');
        notiz_div.css("display", "block");
    });

     //termin hinzufügen
    $(document).on('click',"#onclick_termin",function()
    {
        $('#hilfeart #send').text('Hinzufügen');
        termin_div.css("display", "block");
    });

    //Hilfeart updaten
    $(document).on('click','.onclick_update',function()
    {
        updateHilfeart($(this));
    });

    //Termin updaten
    $(document).on('click','.onclick_update_termin',function()
    {
        updateTermin($(this));
    });


    function updateHilfeart(e)
    {
        all_infos = e.parent().parent().parent().attr('id').split("--");

        //$("#hilfeart #select_asd_add option[value='"+all_infos[2]+"']").attr('selected', 'selected');
        //$("#hilfeart #select_hilfeart_add option[value='"+all_infos[1]+"']").attr('selected', 'selected');

        $("#hilfeart #select_hilfeart_add option[value='0']").remove();
        $("#hilfeart #select_asd_add option[value='0']").remove();

        $('#hilfeart #send').text('ändern');

        add_ha_obj.val(all_infos[1]);
        add_asd_obj.val(all_infos[2]);
        add_da_obj.val(all_infos[3]);
        add_db_obj.val(all_infos[4]);


        ha_div.children('form').prepend('<input type="hidden" name="update" id="update" value="1"/>');

        ha_div.css("display", "block");
    }

    function updateTermin(e)
    {
        all_infos = e.parent().parent().parent().attr('id').split("--");
        console.info(all_infos);

        $('#termin #send').text('ändern');

        add_titel_obj.val(all_infos[1]);
        add_beschreibung_obj.val(all_infos[2]);
        add_terminbeginn_obj.val(all_infos[3]);
        add_terminende_obj.val(all_infos[4]);

        termin_div.children('form').prepend('<input type="hidden" name="termin_id" id="termin_id" value="'+all_infos[0]+'"/>');

        termin_div.children('form').prepend('<input type="hidden" name="update_termin" id="update_termin" value="1"/>');

        termin_div.css("display", "block");
    }

    $(document).on('click','.onclick_update_notiz',function()
    {
        update_notiz($(this));
    });

    function update_notiz(e)
    {
        $('#notiz #wenn_update').css("display","block");
        all_infos = e.parent().parent().attr('id').split("--");
        $("#notizdiv #update_mitarbeiter option[value='0']").remove();

        $('#notiz #send').text('ändern');

        update_user_obj.val(all_infos[2]);
        update_text_obj.val(all_infos[3]);
        notiz_div.children('form').prepend('<input type="hidden" name="update_notiz_input" id="update_notiz_input" value="1"/>');
        notiz_div.css("display", "block");
    }

    //Cancel button HIlfeart
    $("#hilfeart #cancel").click(function()
    {
       clear_trace_hilfeart();
       ha_div.hide();
       $("#hilfeart #select_hilfeart_add option[value='0']").remove();
       $("#hilfeart #select_asd_add option[value='0']").remove();
       $('form #update').remove();

    });

    // Hilfeart form popup send-button click event.
    $("#hilfeart #send").click(function() {

        kha_insert_or_edit();

    });
    // Termin form popup send-button click event.
    $("#termin #send").click(function() {

        termin_insert_or_edit();

    });
    $("#notiz #send").click(function() {

        notiz_add_or_update();

    });

     $("#print #send").click(function() {
        var winURL = '/user/klienten.htm?layout=empty&view=print&id='+$('#klient_id').val()+'&start='+$('#print_start').val()+'&ende='+$('#print_ende').val();
        var winName = 'win1';
        var winSize = 'width=660,height=620,scrollbars=yes';
        var ref = window.open(winURL, winName, winSize);

        clear_trace_print();
        $(this).parent().parent().hide();
    });

    //function zum HIlfeart und ASD hinzufügen oder updaten
    function kha_insert_or_edit()
    {


        if(add_ha_obj.val() == 0 ){
            add_ha_obj.css('border','1px solid #ff0000');
        }

        if(add_asd_obj.val() == 0){
            add_asd_obj.css('border','1px solid #ff0000');
        }

        if(add_da_obj.val() == ""){
            add_da_obj.css('border','1px solid #ff0000');
        }

        if(add_db_obj.val() == ""){
            add_db_obj.css('border','1px solid #ff0000');
        }

        if(add_asd_obj.val() != 0 && add_ha_obj.val() != 0 )
        {
            if($('#update').val() == undefined )
                var view="kha_add";
            else
                var view="kha_update";
            $.post("?view="+view+"&layout=empty",
                    {
                      obj: all_infos,
                      klient_id  : $('#klient_id').val(),
                      hilfeart_id  : add_ha_obj.val(),
                      asd_id  : add_asd_obj.val(),
                      datum_ab  : add_da_obj.val(),
                      datum_bis : add_db_obj.val()
                    }
                  ).error(
                      function(e)
                      {
                         if($('form .err').length==0)
                            $('form#hilfeart').prepend('<div class="err">'+e.responseText+'</div>');
                         else
                            $('form .err').text(e.responseText);
                      }
                  ).success(
                      function(e)
                      {
                          $('.about-services').html(e);
                          clear_trace_hilfeart();
                          ha_div.hide();
                      }
                  );
        }

    }

    //function zum Termin hinzufügen oder updaten
    function termin_insert_or_edit()
    {

        if(add_titel_obj.val() == ""){
            add_titel_obj.css('border','1px solid #ff0000');
        }

        if(add_beschreibung_obj.val() == ""){
            add_beschreibung_obj.css('border','1px solid #ff0000');
        }

        if(add_terminbeginn_obj.val() == ""){
            add_terminbeginn_obj.css('border','1px solid #ff0000');
        }

        if(add_terminende_obj.val() == ""){
            add_terminende_obj.css('border','1px solid #ff0000');
        }

        if(add_titel_obj.val() != 0 && add_beschreibung_obj.val() && add_terminbeginn_obj.val() != 0 && add_terminende_obj.val() != 0 )
        {
            if($('#update_termin').val() == undefined )
                var view="termin_add";
            else
                var view="termin_update";
            $.post("?view="+view+"&layout=empty",
                    {
                      obj: all_infos,
                      termin_id  : $('#termin_id').val(),
                      titel  : add_titel_obj.val(),
                      beschreibung  : add_beschreibung_obj.val(),
                      terminbeginn  : add_terminbeginn_obj.val(),
                      terminende : add_terminende_obj.val(),
                      klient_id  : $('#klient_id').val()
                    }
                  ).error(
                      function(e)
                      {
                         if($('form .err').length==0)
                            $('form#termin').prepend('<div class="err">'+e.responseText+'</div>');
                         else
                            $('form .err').text(e.responseText);
                      }
                  ).success(
                      function(e)
                      {
                          $('.about-services-termin').html(e);
                          clear_trace_termin();
                          termin_div.hide();
                      }
                  );
        }

    }

    //function zum Notiz hinzufügen oder updaten
    function notiz_add_or_update()
    {

        if($.trim(update_text_obj.val()) == ""){
            update_text_obj.css('border','1px solid #ff0000');
        }

        if($.trim(update_text_obj.val()) != "" )
        {
            if($('#update_notiz_input').val() == undefined )
                var view="notiz_add";
            else
                var view="notiz_update";
            $.post("?view="+view+"&layout=empty",
                    {
                      obj        : all_infos,
                      klient_id  : $('#klient_id').val(),
                      user_id    : update_user_obj.val(),
                      notiz_text : update_text_obj.val()
                    }
                  ).error(
                      function(e)
                      {
                         if($('form#notiz .err').length==0)
                            $('form#notiz').prepend('<div class="err">'+e.responseText+'</div>');
                         else
                            $('form .err').text(e.responseText);
                      }
                  ).success(
                      function(e)
                      {
                          $('.about-histore').html(e);
                          clear_trace_notiz();
                          notiz_div.hide();
                      }
                  );
        }
        $('form #update').remove();
    }

    //border und update input entfernen
    function clear_trace_hilfeart()
    {
        add_ha_obj.val(0);
        add_asd_obj.val(0);
        add_da_obj.val("");
        add_db_obj.val("");
        add_ha_obj.css('border','');
        add_asd_obj.css('border','');
        add_da_obj.css('border','');
        add_db_obj.css('border','');
        $('form #update').remove();
        if($('#hilfeart .err').length > 0)
            $('#hilfeart .err').remove();
    }

    //border und update input entfernen
    function clear_trace_termin()
    {
        add_titel_obj.val("");
        add_beschreibung_obj.val("");
        add_terminbeginn_obj.val("");
        add_terminende_obj.val("");
        add_titel_obj.css('border','');
        add_beschreibung_obj.css('border','');
        add_terminbeginn_obj.css('border','');
        add_terminende_obj.css('border','');
        $('form #update_termin').remove();
        if($('#termin .err').length > 0)
            $('#termin .err').remove();
    }

    //border und update input entfernen
    function clear_trace_notiz()
    {
        update_text_obj.val("");
        update_text_obj.css('border','');
        $('form #update_notiz_input').remove();
        if($('#notiz .err').length > 0)
            $('#notiz .err').remove();
    }

     //border und update input entfernen
    function clear_trace_print()
    {
        $('#print_start').val('');
        $('#print_ende').val('');
    }

    //popup schließen
    $("#notiz #cancel").click(function()
    {
        clear_trace_notiz();
        $("#notiz #update_mitarbeiter option[value='0']").remove();
        $(this).parent().parent().hide();
    });

    //popup schließen
    $("#termin #cancel").click(function()
    {
        clear_trace_termin();
        $('#termin #send').text('Hinzufügen');
        $(this).parent().parent().hide();
    });

     //popup schließen
    $("#print #cancel").click(function()
    {
        clear_trace_print();
        $(this).parent().parent().hide();
    });

    $('#print_notiz').click(function(){
       $('#printdiv').css("display", "block");
    });

});