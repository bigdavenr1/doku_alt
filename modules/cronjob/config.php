<?php
namespace modules\cronjob;
trait ModulConfig
{
    /**
     * Name der SQL-Tabelle
     */
     protected static $table = ['t' => "termine",
                                'k' => "kta_klienten"
                               ];

    /**
     * Feld id
     */
    protected static $id = 'id';
    /**
     * Feld stadt
     */
    protected static $titel = 'titel';
    /**
     * Feld abteilung
     */
    protected static $beschreibung = 'beschreibung';
    /**
     * Feld name
     */
    protected static $beginn = 'terminbeginn';
    /**
     * Feld name
     */
    protected static $ende = 'terminende';
    /**
     * Feld name
     */
    protected static $klient = 'klient_id';
    /**
     * Feld name
     */
    protected static $klientVorname = 'given_name';
    /**
     * Feld name
     */
    protected static $klientName = 'surname';






}