<?php
namespace modules\cronjob;
class Model extends \inc\Basicdb
{
    use ModulConfig;
    // Konstruktor
    ////////////////////////////////////////////////////////////////////////

    function __construct($anzahl = false)
    {
        global $sql;
        $this->anzahl = $anzahl;

        $this->queryVars=(object) array('table' => self::$table['t']);

        parent::__construct();
    }

    public function getAllOpenTermine()
    {

        $hp=date('Y-m-d',mktime(0,0,0,date('m'),date('d')+15,date('Y')));
        $hpsend=date('Y-m-d',mktime(0,0,0,date('m'),date('d')+8,date('Y')));
        $queryVars=clone $this->queryVars;
        $queryVars->fields=array("IF ((terminbeginn >= '$hp 00:00:00' AND terminbeginn <= '$hp 23:59:59'),'hp',(IF ((terminbeginn >= '$hpsend 00:00:00' AND terminbeginn <= '$hpsend 23:59:59' ),'hpsend',NULL))) status",
                                  self::$table['t'].".".self::$id,
                                  self::$table['t'].".".self::$titel,
                                  self::$table['t'].".".self::$beschreibung,
                                  'HOUR('.self::$table['t'].".".self::$beginn.') hb',
                                  'MINUTE('.self::$table['t'].".".self::$beginn.') mb',
                                  'DAYNAME('.self::$table['t'].".".self::$beginn.') db',
                                  "DATE_FORMAT(".self::$table['t'].".".self::$beginn.",GET_FORMAT(DATE,'EUR')) beginn",
                                  'HOUR('.self::$table['t'].".".self::$ende.') he',
                                  'MINUTE('.self::$table['t'].".".self::$ende.') me',
                                  'DAYNAME('.self::$table['t'].".".self::$ende.') de',
                                  "DATE_FORMAT(".self::$table['t'].".".self::$ende.",GET_FORMAT(DATE,'EUR')) ende",
                                  self::$table['k'].".".self::$klientVorname,
                                  self::$table['k'].".".self::$klientName
                                 );
         $queryVars->joins=array(
                                (object)
                                array('type'   => 'LEFT',
                                      'table'  => self::$table['k'],
                                      'clause' => self::$table['t'].'.'.self::$klient.' = '.self::$table['k'].'.'.self::$id

                                ));

        $queryVars->order = (object) array('field' => self::$table['t'].".".self::$beginn, 'direction' => 'ASC');
        $queryVars->clause="(terminbeginn >= '$hp 00:00:00' AND terminbeginn <= '$hp 23:59:59') OR (terminbeginn >= '$hpsend 00:00:00' AND terminbeginn <= '$hpsend 23:59:59' )";
        parent::createQuery($queryVars);
        return($this->liste);
    }
}