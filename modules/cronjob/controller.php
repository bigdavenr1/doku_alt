<?php // Modulview und Modulmodel einbinden
namespace modules\cronjob;
require_once("model.php");
class Controller
{
    private $request = null;
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        $this->model=new Model();
    }

    public function display()
    {
        $termine = $this->model->getAllOpenTermine();
        if($termine[0]):
            $contenthp="";
            $contenthpsend="";
            foreach($termine AS $asd):

               if($asd->status=='hp'):

                   $contenthp.=
                   '-----------------------------<br/><br/><div style="background:rgb(210,210,210)"><i style="color:rgb(150,150,150)">'.$asd->beginn.'</i><br/>'.
                    $asd->db.'<br/>'.
                    ($asd->hb<10?'0':'').$asd->hb.':'.($asd->mb<10?'0':'').$asd->mb.' Uhr<br/><br/>bis:<br/><br/>
                    <i style="color:rgb(150,150,150)"><small>'.$asd->ende.'</small><br/></i>'.
                    $asd->de.'<br/>'.
                    ($asd->he<10?'0':'').$asd->he.':'.($asd->me<10?'0':'').$asd->me.' Uhr<br/><br/>
                    Klient:<br/>
                    '.$asd->surname.', '.$asd->given_name.
                    '<br/><br/>
                    <b>'.$asd->titel.'</b><br/>'.$asd->beschreibung.'<br/></div><br/>----------------------------<br/><br/>';

               elseif($asd->status=='hpsend'):

                   $contenthpsend.=
                   '-----------------------------<br/><br/><div style="background:rgb(210,210,210)"><i style="color:rgb(150,150,150)">'.$asd->beginn.'</i><br/>'.
                    $asd->db.'<br/>'.
                    ($asd->hb<10?'0':'').$asd->hb.':'.($asd->mb<10?'0':'').$asd->mb.' Uhr<br/><br/>bis:<br/><br/>
                    <i style="color:rgb(150,150,150)"><small>'.$asd->ende.'</small><br/></i>'.
                    $asd->de.'<br/>'.
                    ($asd->he<10?'0':'').$asd->he.':'.($asd->me<10?'0':'').$asd->me.' Uhr<br/><br/>
                    Klient:<br/>
                    '.$asd->surname.', '.$asd->given_name.
                    '<br/><br/>
                    <b>'.$asd->titel.'</b><br/>'.$asd->beschreibung.'<br/></div><br/>----------------------------<br/><br/>';

               endif;
            endforeach;
            if(!empty($contenthp)):
                $content.="<br/><b style=\"font-size:150%\">Termine für die der Hilfeplan vorbereitet werden muss:</b><br/>=======================================<br/>";
                $content.=$contenthp;
                $content.='<br/>========================================<br/>';
            endif;
            if(!empty($contenthpsend)):
                $content.="<br/><b style=\"font-size:150%\">Termine für die die Hilfeplanunterlagen abgegeben werden müssen:</b><br/>=======================================<br/>";
                $content.=$contenthpsend;
                $content.='<br/>========================================<br/>';
            endif;

            $email=new \inc\std\email\Controller('html');
            $email->setTo('kontakt@natuerliches-erleben.de');
            $email->setFrom('no-reply@natuerliches-erleben.de');
            $email->setSubject('Erinnerung zu Hilfeplanterminen');
            $email->setContent($content);
            $email->sendMail();
        endif;
        exit;
    }
}
