<?php
namespace modules\startseite;
class Model extends \inc\Basicdb
{
    // Konstruktor
    ////////////////////////////////////////////////////////////////////////

    function __construct($anzahl = false)
    {
        global $sql;
        $this->anzahl = $anzahl;
        $this->table="kta_content";
        $this->queryVars=(object) array('table' => $this->table, 'countVar'=>$this->countvar, 'offsetname'=>false);
        parent::__construct();
    }
	function getStartsiteContent()
	{
        $queryVars= clone($this->queryVars);
        unset ($queryVars->countVar);
        $queryVars->clause=" alias='startseite'";
        $queryVars->preparedVars=array();
        parent::createQuery($queryVars);
	}

}