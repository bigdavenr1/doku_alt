<!-- Nivo Slider Start -->
        <section class="slider-wrapper">
          <div id="slideshow" class="nivoSlider"> <a class="nivo-imageLink" href="#"><img src="<?php echo WEBDIR?>images/slider/slide-1.jpg" alt="slide-1" /></a><img src="<?php echo WEBDIR?>images/slider/slide-2.jpg" alt="slide-2" /></a></div>
        </section>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#slideshow').nivoSlider({pauseTime:7000, animSpeed: 1500});
            });</script>
        <!-- Nivo Slider End-->
        <!-- Welcom Text Start-->
        <div class="welcome">Ticketshop für alternative Musik, Konzerte und Partys!</div>
        <p><strong>TICKETWORLD Dresden</strong> Dein Ticketshop für viele alternative Events, deutschlandweit. Wir bieten Tickets zu fairen Preisen, und ohne Abzocke. Alle Tickets sind offiziell lizensiert. Aktuelle Highlights in unserem Shop sind: <a href="">CAMOUFLAGE</a>, <a href="">MESH</a>, <a href="">PROJECT PITCHFORK</a> und <a href="">VNV NATION</a>. Du findest aber noch viele weitere interessante Events aus verschiedenen Sparten in unserem Shop. Viel Spaß beim Stöbern. </p>
        <!-- Welcom Text End-->
        <!-- Featured Product Start-->
        <section class="box">
          <div class="box-heading">Featured</div>
          <div class="box-content">
            <div class="box-product">
              <div class="flexslider featured_carousel">
                <ul class="slides">
                  <li>
                    <div class="slide-inner">
                      <div class="image"><a href="product.html"><img src="<?php echo WEBDIR?>images/product/lotto-sports-shoes-white-210x210.jpg" alt="Lotto Sports Shoes" /></a></div>
                      <div class="name"><a href="https://localhost/polishop/index.php?route=product/product&amp;product_id=43">Lotto Sports Shoes</a></div>
                      <div class="price"> $589.50 </div>
                      <div class="cart">
                        <input type="button" value="Add to Cart" class="button" />
                      </div>
                      <div class="clear"></div>
                    </div>
                  </li>
                  <li>
                    <div class="slide-inner">
                      <div class="image"><a href="product.html"><img src="<?php echo WEBDIR?>images/product/iphone_1-210x210.jpg" alt="iPhone 4s" /></a></div>
                      <div class="name"><a href="product.html">iPhone 4s</a></div>
                      <div class="price"> $120.68 </div>
                      <div class="cart">
                        <input type="button" value="Add to Cart" class="button" />
                      </div>
                      <div class="rating"><img src="<?php echo WEBDIR?>images/stars-4.png" alt="Based on 1 reviews." /></div>
                      <div class="clear"></div>
                    </div>
                  </li>
                  <li>
                    <div class="slide-inner">
                      <div class="image"><a href="product.html"><img src="<?php echo WEBDIR?>images/product/apple_cinema_30-210x210.jpg" alt="Apple Cinema 30&quot;" /></a></div>
                      <div class="name"><a href="product.html">Apple Cinema 30&quot;</a></div>
                      <div class="price"> <span class="price-old">$119.50</span><span class="price-new">$107.75</span> </div>
                      <div class="cart">
                        <input type="button" value="Add to Cart" class="button" />
                      </div>
                      <div class="clear"></div>
                    </div>
                  </li>
                  <li>
                    <div class="slide-inner">
                      <div class="image"><a href="product.html"><img src="<?php echo WEBDIR?>images/product/sony_vaio_1-210x210.jpg" alt="Friendly Jewelry" /></a></div>
                      <div class="name"><a href="product.html">Friendly Jewelry</a></div>
                      <div class="price"> $1,177.00 </div>
                      <div class="cart">
                        <input type="button" value="Add to Cart" class="button" />
                      </div>
                      <div class="clear"></div>
                    </div>
                  </li>
                  <li>
                    <div class="slide-inner">
                      <div class="image"><a href="product.html"><img src="<?php echo WEBDIR?>images/product/Jeep-Casual-Shoes-210x210.jpg" alt="Jeep-Casual-Shoes" /></a></div>
                      <div class="name"><a href="product.html">Jeep-Casual-Shoes</a></div>
                      <div class="price"> $131.25 </div>
                      <div class="cart">
                        <input type="button" value="Add to Cart" class="button" />
                      </div>
                      <div class="clear"></div>
                    </div>
                  </li>
                  <li>
                    <div class="slide-inner">
                      <div class="image"><a href="product.html"><img src="<?php echo WEBDIR?>images/product/htc_touch_hd_1-210x210.jpg" alt="iPhone 5s" /></a></div>
                      <div class="name"><a href="product.html">iPhone 5s</a></div>
                      <div class="price"> $119.50 </div>
                      <div class="cart">
                        <input type="button" value="Add to Cart" class="button" />
                      </div>
                      <div class="clear"></div>
                    </div>
                  </li>
                  <li>
                    <div class="slide-inner">
                      <div class="image"><a href="product.html"><img src="<?php echo WEBDIR?>images/product/ipod_touch_1-210x210.jpg" alt="Sunglass" /></a></div>
                      <div class="name"><a href="product.html">Sunglass</a></div>
                      <div class="price"> $1,177.00 </div>
                      <div class="cart">
                        <input type="button" value="Add to Cart" class="button" />
                      </div>
                      <div class="clear"></div>
                    </div>
                  </li>
                  <li>
                    <div class="slide-inner">
                      <div class="image"><a href="product.html"><img src="<?php echo WEBDIR?>images/product/reebok-men-sports-shoes-210x210.jpg" alt="Reebok Men Sports Shoes" /></a></div>
                      <div class="name"><a href="product.html">Reebok Men Sports Shoes</a></div>
                      <div class="price"> $119.50 </div>
                      <div class="cart">
                        <input type="button" value="Add to Cart" class="button" />
                      </div>
                      <div class="clear"></div>
                    </div>
                  </li>
                  <li>
                    <div class="slide-inner">
                      <div class="image"><a href="product.html"><img src="<?php echo WEBDIR?>images/product/samsung_tab_1-210x210.jpg" alt="Eagle Print Top" /></a></div>
                      <div class="name"><a href="product.html">Eagle Print Top</a></div>
                      <div class="price"> $236.99 </div>
                      <div class="cart">
                        <input type="button" value="Add to Cart" class="button" />
                      </div>
                      <div class="clear"></div>
                    </div>
                  </li>
                  <li>
                    <div class="slide-inner">
                      <div class="image"><a href="product.html"><img src="<?php echo WEBDIR?>images/product/nikon_d300_1-210x210.jpg" alt="Nikon D300" /></a></div>
                      <div class="name"><a href="product.html">Nikon D300</a></div>
                      <div class="price"> $942.00 </div>
                      <div class="cart">
                        <input type="button" value="Add to Cart" class="button" />
                      </div>
                      <div class="clear"></div>
                    </div>
                  </li>
                  <li>
                    <div class="slide-inner">
                      <div class="image"><a href="product.html"><img src="<?php echo WEBDIR?>images/product/samsung_syncmaster_941bw-210x210.jpg" alt="Samsung SyncMaster 941BW" /></a></div>
                      <div class="name"><a href="product.html">Samsung SyncMaster 941BW</a></div>
                      <div class="price"> $237.00 </div>
                      <div class="cart">
                        <input type="button" value="Add to Cart" class="button" />
                      </div>
                      <div class="clear"></div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
