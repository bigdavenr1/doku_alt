<?php // Modulview und Modulmodel einbinden
namespace modules\startseite;
require_once("model.php");
class Controller
{
    // private VAriablen setzen
    private $request = null;
    private $modultemplate = 'startseite';
    private $modulview = null;
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->modulview = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->modultemplate =isset($request['view'])?$request['view']:'startseite';
    }
    ############################################
    # Methode zum anzeigen des Moduöl-Contents.#
    #                                          #
    # @return String Content der Applikation.  #
    ############################################
    public function display()
    {
        // modultemplate setzen
        $this->modulview->setTemplate($this->modultemplate,dirname(__FILE__));
        $this->assignvalues['slider']= true;
        // Modelklasse instanziieren
        $this->startseitefac=new Model();
        $this->startseitefac->getStartsiteContent();
        $this->assignvalues['title']="Sexradar.com - das größte Erotikportal für Sextreffen deutschlandweit";
		$this->assignvalues["keywords"] = "Erotik, Sex, Sextreffen, Sexdate, Huren, Nutten, Prostituierte, Escortservice, Clubanbieter, Swingerclub, Begleitservice, Bordelle, Erotikportal, Privatanbieter Erotikplattform, Sex buchen, bestellen, Berlin, Dresden, Brandenburg, Hamburg, deutschlandweit, Rotlicht, Modelle, Erotikmodelle";
        $this->assignvalues['description']="Sexradar.com - das größte Erotikportal für Sextreffen deutschlandweit";
		for($i=0;$i<sizeof($this->startuser);$i++)
		    if ($i>4)
		        $tmp[]=$this->startuser[$i];
		$this->modulview->assign('startuser',$tmp);
		// Codes übergeben
        //$this->modulview->assign("startslider",$this->func->widgetLoader('startslider'));
        $this->modulview->assign("startsitecontent",$this->startseitefac->getElement());
         // Template laden, füllen und ausgeben
        return $this->modulview->loadTemplate();
    }
}
?>