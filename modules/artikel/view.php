<?php
class Modulview
{
    // Name des Templates, in dem Fall das Standardtemplate.
    private $template = 'artikel';
    // Enthält die Variablen, die in das Template eingebetet werden sollen.
    private $_ = array();

    ############################################################
    # Ordnet einer Variable einem bestimmten Schl&uuml;ssel zu.#
    #                                                          #
    # @param String $key Schlüssel                             #
    # @param String $value Variable                            #
    ############################################################
    public function assign($key, $value)
    {
        $this->_[$key] = $value;
    }
    ##############################################
    # Setzt den Namen des Templates.             #
    #                                            #
    # @param String $template Name des Templates.#
    ##############################################
    protected function setTemplate($template = 'artikel')
    {
            $this->template = $template;
    }
    ################################################################
    # Das Template-File laden und zurückgeben                      #
    ################################################################
    protected function loadTemplate()
    {
        // Der Output des Scripts wird n einen Buffer gespeichert, d.h.
        // nicht gleich ausgegeben.
        ob_start();
            // Das Template-File wird eingebunden und dessen Ausgabe in
            // $output gespeichert.
            require_once ($this->template . '.tpl');
            $output = ob_get_contents();
        ob_end_clean();
        // Output zurückgeben.
        return $output;
    }
}
?>