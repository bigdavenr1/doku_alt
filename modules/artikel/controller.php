<?php // Modulview und Modulmodel einbinden
namespace modules\artikel;
include_once("model.php");
class Controller
{
    // private Variablen setzen
    private $request = null;
    private $modultemplate = 'artikel';
    private $modulview = null;
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->view = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->template =isset($request['view'])?$request['view']:'artikel';
    }
    ############################################
    # Methode zum anzeigen des Moduöl-Contents.#
    #                                          #
    # @return String Content der Applikation.  #
    ############################################
    public function display()
    {
        // Metas als Konstanten definieren
        global $CONST_META;
        // modultemplate setzen
        $this->view->setTemplate($this->template,dirname(__FILE__));

        // Modelklasse instanziieren
        $artfac=new Model();
        // methode zum Abruf des Artikel aufrufen
        // Wenn Artikel gefunden
        if (!empty($this->content)):
            // Title setzen
            $this->assignvalues['pagetitle'] = htmlspecialchars(stripslashes(strip_tags($this->content->title)))."";
            // keywords setzen
            !empty($this->content->metakey)?$this->assignvalues['keywords'] = stripslashes(strip_tags($this->content->metakey)):'';
            // Description setzen
            !empty($this->content->metadesc)?$this->assignvalues['description'] = stripslashes(strip_tags($this->content->metadesc)):'';
            // Artikelinhalt setzen
            $this->view->assign('content', stripslashes(!empty($this->content->text_uebersetzung)?$this->content->text_uebersetzung:$this->content->fulltext));
			$this->view->assign('title', htmlspecialchars(stripslashes(strip_tags($this->content->title))));
        // Wenn Artikel nicht gefunden wurde
        else:
            // Artikelinhalt mit entsprechender Fehlermeldung setzen
            header("Location: ".WEBDIR."status_msg/article_not_found");
        endif;
        // Template laden, füllen und ausgeben
        return $this->view->loadTemplate();
    }
}
?>