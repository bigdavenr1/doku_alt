<?php
namespace modules\andere_modul;

class Model extends \inc\Basicdb
{
    use ModulConfig;
    
    public function __construct($anzahl = 0)
    {
        global $request;            
        parent::__construct();
    }
    
    
     public function getOneContent()
    {
        $queryVars= clone((object) self::$queryVars);           
        $queryVars->clause= self::$alias."='andere'";
        $queryVars->preparedVars=array();
        parent::createQuery($queryVars);
        return \inc\BasicDb::getElement();
    }
    
    public function updateTime()
    {
        $queryVars= clone ((object) self::$queryVars);
        $queryVars->data = array(
                               self::$modified=>array(
                                                'plainSQL'=>'NOW()'
                                                ),
                               self::$created=>array(
                                                'plainSQL'=>'NOW()'
                                                ),
                               self::$publish_up=>'2015-01-05 10:12:00'
                                                
                               );
        $queryVars->clause= self::$alias." = 'andere'";
        $queryVars->preparedVars=array();
        return \inc\BasicDb::update($queryVars);
    }
    
    public function saveData($request=false)
    {
        
        
        if (!empty($request['artikel'])):
           $queryVars = clone ((object) self::$queryVars);
           $queryVars->data = $request['artikel'];
           $queryVars->duplicateUpdate=true;
           
           \inc\BasicDb::write($queryVars);
            
        endif;
        
    }
}
?>