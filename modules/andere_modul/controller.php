<?php // Modulview und Modulmodel einbinden
namespace modules\andere_modul;
include_once("model.php");
class Controller
{
    // private Variablen setzen
    private $request = null;
    private $modultemplate = 'andere';
    private $modulview = null;
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->view = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->template =isset($request['view'])?$request['view']:'andere';
        $this->model= new Model();
    }
    ############################################
    # Methode zum anzeigen des Moduöl-Contents.#
    #                                          #
    # @return String Content der Applikation.  #
    ############################################
    public function display()
    {
        // Metas als Konstanten definieren
        global $CONST_META;
        // modultemplate setzen
        $this->view->setTemplate($this->template,dirname(__FILE__));
        $this->model->saveData($this->request);
        // Modelklasse instanziieren
        $this->view->assign('content',$content = $this->model->getOneContent());
        $this->assignvalues['pagetitle']=$content->title;
        
        if( $this->request['action'] == 'updatetime'):
            $this->model->updateTime();
        endif;
        
        
        
        
        // Template laden, füllen und ausgeben
        return $this->view->loadTemplate();
    }
}
?>