<?php
namespace modules\andere_modul;
trait ModulConfig
{
    /**
     * Name der SQL Tabelle
     */
    protected static $table = "kta_content";
    /**
     * Feldname Titel
     */
    protected static $title = 'title';
    /**
     * Feldname Alias
     */
    protected static $alias = 'alias';
    /**
     * Feldname Titel-Alias
     */
    protected static $title_alias = 'title_alias';
    /**
     * Feldname Introtext
     */
    protected static $introtext = 'introtext';
    /**
     * Feldname Fultext
     */
    protected static $fulltext = 'fulltext';
    /**
     * Feldname Created
     */
    protected static $created = 'created';
    /**
     * Feldname Modified
     */
    protected static $modified = 'modified';
    /**
     * Feldname Plublished up
     */
    protected static $publish_up = 'publish_up';

    /**
     * Feldname Standard-Array für Objekt-Cloning in ModulModel-Klassen
     * dient als Prototype
     */
    protected static $queryVars =  array('table' => 'kta_content');


}