<?php // Modulview und Modulmodel einbinden
namespace modules\status_msg;
include_once("model.php");
class Controller
{
    // private Variablen setzen
    private $request = null;
    private $modultemplate = 'status_msg';
    private $modulview = null;
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->modulview = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->modultemplate =isset($request['view'])?$request['view']:'status_msg';
    }
    ############################################
    # Methode zum anzeigen des Moduöl-Contents.#
    #                                          #
    # @return String Content der Applikation.  #
    ############################################
    public function display()
    {
        $this->assignvalues['slider']= true;
        // Metas als Konstanten definieren
        global $CONST_META;
        // modultemplate setzen
        $this->modulview->setTemplate($this->modultemplate,dirname(__FILE__));
        // Modelklasse instanziieren
        $msgfac=new Model();
        // methode zum Abruf des Artikel aufrufen
        $msgfac->getMSGByCode($this->request['code'],$this->request['lang']);
        // Wenn Artikel gefunden
        if ($msg=$msgfac->getElement()):
            // Title setzen
            $this->assignvalues['pagetitle'] = htmlspecialchars(stripslashes(strip_tags($msg->title)))."";
            // Artikelinhalt setzen
            $this->modulview->assign('content', "<h1>".stripslashes(!empty($msg->titel_uebersetzung)?$msg->titel_uebersetzung:$msg->titel)."</h1>".stripslashes(!empty($msg->text_uebersetzung)?$msg->text_uebersetzung:$msg->text));
        // Wenn Artikel nicht gefunden wurde
        else:
			// methode zum Abruf des Artikel aufrufen
        	$msgfac->getMSGByCode("unknown",$this->request['lang']);
            // Wenn Artikel gefunden
        	$msg=$msgfac->getElement();
            // Title setzen
            $this->assignvalues['pagetitle'] = htmlspecialchars(stripslashes(strip_tags($msg->title)))."";
            // Artikelinhalt setzen
            $this->modulview->assign('header',stripslashes(!empty($msg->titel_uebersetzung)?$msg->titel_uebersetzung:$msg->titel));
            $this->modulview->assign('content', stripslashes(!empty($msg->text_uebersetzung)?$msg->text_uebersetzung:$msg->text));
        // Wenn Artikel nicht gefunden wurde
        endif;
        // Template laden, füllen und ausgeben
        return $this->modulview->loadTemplate();
    }
}
?>