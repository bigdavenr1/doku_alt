<?php
namespace modules\status_msg;
class Model extends \inc\Basicdb
    {
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////

         function __construct($anzahl = 0)
        {
            global $sql;
            $this->table = "kta_status_msg";
            $this->queryVars=(object) array('table' => $this->table, 'offsetname'=>false);
            parent::__construct();

        }

        // User Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////

        // �berladen
         function getMSGByCode($code,$langid=false)
        {
            $queryVars = clone($this->queryVars);
            $queryVars->clause='code= :Code';
            $queryVars->preparedVars=array(':Code'=>$code);
            parent::createQuery($queryVars);
		}
    }
?>