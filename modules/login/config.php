<?php
namespace modules\login;
trait ModulConfig
{
    /**
     * Name der SQL-Tabelle
     */
    protected static $table=['u' => "kta_users",
                                'c' => "kta_content"];
    
    /**
     * Feld id
     */
    protected static $id = 'id';
    
    // die Felder der Tabelle 'kta_users'------------------------------------------
    
    /**
     * Feld given_name
     */
    protected static $given_name = 'given_name';
    /**
     * Feld surname
     */
    protected static $surname = 'surname';
    /**
     * Feld street1
     */
    protected static $street1 = 'street1';
    /**
     * Feld street2
     */
    protected static $street2 = 'street2';
    /**
     * Feld zip
     */
    protected static $zip = 'zip';
    /**
     * Feld city
     */
    protected static $city = 'city';
    /**
     * Feld state
     */
    protected static $state = 'state';
    /**
     * Feld country
     */
    protected static $country = 'country';
    /**
     * Feld tel
     */
    protected static $tel = 'tel';
    /**
     * Feld mobil
     */
    protected static $mobil = 'mobil';
    /**
     * Feld fax
     */
    protected static $fax = 'fax';
    /**
     * Feld company
     */
    protected static $company = 'company';
    /**
     * Feld trycount
     */
    protected static $trycount = 'trycount';
    /**
     * Feld username
     */
    protected static $username = 'username';
    /**
     * Feld email
     */
    protected static $email = 'email';
    /**
     * Feld pass
     */
    protected static $pass = 'pass';
    /**
     * Feld usertype
     */
    protected static $usertype = 'usertype';
    /**
     * Feld lastWrongTry
     */
    protected static $lastWrongTry = 'lastWrongTry';
    /**
     * Feld registerDate
     */
    protected static $registerDate = 'registerDate';
    /**
     * Feld lastvisitDate
     */
    protected static $lastvisitDate = 'lastvisitDate';
    /**
     * Feld activationCode
     */
    protected static $activationCode = 'activationCode';
    /**
     * Feld active
     */
    protected static $active = 'active';
    /**
     * Feld language
     */
    protected static $language = 'language';
    /**
     * Feld url
     */
    protected static $url = 'url';
    
    // die Felder der Tabelle 'kta_content'------------------------------------------
    
    /**
     * Feld Titel
     */
    protected static $title = 'title';
    /**
     * Feld alias
     */
    protected static $alias = 'alias';
    /**
     * Feld title_alias
     */
    protected static $title_alias = 'title_alias';
    /**
     * Feld introtext
     */
    protected static $introtext = 'introtext';
    /**
     * Feld fulltext
     */
    protected static $fulltext = 'fulltext';
    /**
     * Feld created
     */
    protected static $created = 'created';
    /**
     * Feld modified
     */
    protected static $modified = 'modified';
    /**
     * Feld publish_up
     */
    protected static $publish_up = 'publish_up';
    /**
     * Feld publish_down
     */
    protected static $publish_down = 'publish_down';
    /**
     * Feld headerueberschrift
     */
    protected static $headerueberschrift = 'headerueberschrift';
    /**
     * Feld headertext
     */
    protected static $headertext = 'headertext';
    /**
     * Feld metakey
     */
    protected static $metakey = 'metakey';
    /**
     * Feld metadesc
     */
    protected static $metadesc = 'metadesc';
    /**
     * Feld access
     */
    protected static $access = 'access';
    /**
     * Feld hits
     */
    protected static $hits = 'hits';
    

   


}