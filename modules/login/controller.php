<?php // view und model einbinden
namespace modules\login;
include_once("model.php");
class Controller
{
    /**
     * Container-Array zum Speichern von GET- und POST-Daten
     * @var array
     */
    private $request = array();
    /**
     * Standard-Modul-Template
     * @var string
     */
    private $template = 'login';
    /**
     * Standard-Modul-View
     * @var string
     */
    private $view = null;
    /**
     * Rückleistungsseiten
     * @var array
     */
    private $url= array();
    /**
     * Messages
     * @var array
     */
    private $msg=array();
   /**
    * Konstruktor, erstellt den Controller
    * @param array $request -> Array aus $_GET & $_POST
    */
    public function __construct($request)
    {
        // Fetslegung der Weiterleitungsurls mit entsprechender Logik
        $this->url=(object) array('wrong'=>($_GET['action']=='booking'?'booking.htm':'login.htm'),
                                  'true'=>($_GET['action']=='booking'?'booking.htm':(($_SESSION['user']->usertype==1 || $_SESSION['user']->usertype==2)?'admin':'user').".htm"),
                                  'pwremind'=>'index.htm',
                                  'logout'=>'index.htm',
                                  'check'=>"login.htm?check=1&action=".$_GET['action']."&id=".$_GET['id']
                                 );
        $this->msg=(object) array('wrongdata'=>(object) array('typ'=>'err','text'=>'Ihre Logindaten waren falsch. Bitte versuchen Sie es noch einmal!'),
                                  'wrongemail'=>(object) array('typ'=>'err','text'=>'Diese E-Mail ist nicht in unserem Verzeichnis!'),
                                  'inactive'=>(object) array('typ'=>'err','text'=>'Ihr Account ist inaktiv. Bitte wenden Sie sich an den Administrator.'),
                                  'pwremind'=>(object) array('typ'=>'msg','text'=>'Eine E-Mail mit dem neuen Passwort wurde versendet!'),
                                  'nocookies'=>(object) array('typ'=>'err','text'=>'Aus Sicherheitsgründen werden Cookies benötigt. Bitte akzeptieren Sie unsere Cookies.'),
                                  'logout'=>(object) array('typ'=>'msg','text'=>'Sie wurden erfolgreich ausgeloggt.'),
                                  'locked'=>(object) array('typ'=>'err','text'=>'Ihr Zugang wurde aufgrund vieler Fehlversuche für {seconds} Sekunden gesperrt! Bitte warten Sie die angegebene Zeit und versuchen Sie es noch einmal!')
                                  );
        // neuen viewer instanziieren
        $this->view = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // template auslesen
        $this->template =$this->request['view']!='pwreminder'?'login':'pwreminder';
        $this->model =new Model();
    }
    /**
    * Methode zum Anzeigen des Modul-Contents.
    * @return String Content der Applikation.
    */
    public function display()
    {
        //print_r($this->request);
        // Metas als Konstanten definieren
        global $CONST_META;
        $this->assignvalues['keywords'] = '';
        $this->assignvalues['description'] = '';
        // template setzen
        $this->view->setTemplate($this->template,dirname(__FILE__));
        // Modelklasse instanziieren
        $benutzerfac=new Model();
        // Wenn Logout
        if ($_GET['action']=="logout"):
            // Alte DAten löschen, Session ungültig machen und weiterleiten
            session_unset();
            unset($_SESSION['user']);
            session_destroy();
            session_start();
            $_SESSION=array();
            session_regenerate_id(TRUE);
            $this->goToUrl('logout','logout');
            exit;
        // Wenn Passwortreminder
        elseif($this->template=='pwreminder' && !empty($_POST['email'])):
            // Schauen ob ein user existiert
            $this->user=$this->model->getByMail($_POST['email']);
            if (empty($this->user)):
                $this->goToUrl('pwremind','wrongmail');
                exit;
            // Wenn user existiert
            else:
                // neues PAsswort generieren
                $pwnew = $this->func->generateRandomKey(10);

                // neues PAsswort speichern
                $this->model->updatePw($pwnew = $this->func->generateRandomKey(10),$this->user );
                //$this->model->update(array('pass'=>$this->func->bcrypt_encode($this->user->email,$pwnew)),'id',$this->user->id);
                // Email versenden
                require_once LOCALDIR . '/inc/std/email.class.php';
                $email = new \inc\Email();
                $email->setFrom('no-reply@natuerliches-erleben.de');
                $email->setTo($this->user->email);
                $email->setSubject('SoziDoku - neues Passwort');
                $email->setContent('Es wurde ein neues Passwort für den Zugang für die SoziDoku erstellt. Dieses kann nach erfolgreichen Login geändert werden. Das neue Passwort lautet:'.CHR(10).CHR(10).$pwnew.CHR(10).CHR(10).'');
                $email->sendMail();
                $this->goToUrl('pwremind','pwremind');
                exit;
            endif;
        // Wenn Login
        else:
            // wenn bereits eine Session besteht, in den USerbereich weiterleiten
            if (isset($_SESSION['user']) && empty($_GET['check']) && empty($_GET['action'])):
                $this->goToUrl('true');
                exit;
            endif;
            // Wenn Emasil und Passwort übertragen wurde oder der Check durchgeführt wird
            if ((!empty($_POST['email']) && (!empty($_POST['pass']))) || !empty($_GET["check"])):
                // wenn Login noch nicht durchgeführt wurde
                if (!$_GET["check"]):
                    // alte User Session löschen
                    unset($_SESSION["user"]);
                    // Login ausführen
                    $seconds = $benutzerfac->getByLogin($_POST['email'], $_POST['pass']);
                    // Wenn Logindaten Korrekt und Session erstellt ist
                    if (!empty($_SESSION['user'])):
                        // Prüfen ob Benutzer aktiv ist
                        if ($_SESSION['user']->active== 01 && empty($_SESSION['user']->activationcode)):
                            // Passwort und Status löschen
                            unset($benutzer->pass);
                            unset($benutzer->active);
                            $this->goToUrl('check');
                            exit;
                        // Wenn Benutzer inaktiv
                        else:
                            // SESSIONdaten wieder löschen und umleiten
                            unset ($_SESSION['user']);
                            $this->goToUrl('wrong','inactive');
                            exit;
                        endif;
                    // wenn gesperrter Zugang
                    elseif(!empty($seconds)):
                        $this->goToUrl('wrong','locked',ceil($seconds));
                        exit;
                    // Wenn Einloggdaten falsch
                    else:
                        $this->goToUrl('wrong','wrongdata');
                        exit;
                    endif;
                 // Wenn Login erfolgreich->Cookiecheck (wg. SID)
                else:
                    // wenn Session Cookie vorhanden, Autiomatische Weiterleitung
                    if ($_COOKIE[session_name()]):
                        $this->goToUrl('true');
                        exit;
                    // wenn kein Session Cookie gesetzt ist
                    else:
                        $this->goToUrl('wrong','nocookies');
                        exit;
                    endif;
                endif;
            endif;
        endif;
        // Holt sich den Kontent aus der DAtenbank zur Anzeige
        $benutzerfac->getLoginContent();
        $this->view->assign("logincontent",$benutzerfac->getElement());
        // Template laden, füllen und ausgeben
        return $this->view->loadTemplate();
    }
    /**
     * Methode zum Weiterleiten und setzen einer Fehlermeldung
     */
    private function goToUrl($url, $msg=false, $var=false)
    {
        // Wenn Message nicht leer
        if (!empty($msg))
            $_SESSION[ $this->msg->$msg->typ ] = preg_replace('~{.*?}~',$var,$this->msg->$msg->text);
        header("Location: ".WEBDIR.$this->url->$url);
        exit;
    }
}