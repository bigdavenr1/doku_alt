
<div class="section group">
				<div class="col span_2_of_3">
				  <div class="contact-form">
				  	<h3>Login</h3>
				  	     <form action="login.htm" method="POST" id="register">
				  	        <label>E-Mail-Adresse</label>
					        <input name="email" type="text" placeholder="E-Mail" />
					        <br class="clr"/> <br/>
					        <label>Passwort</label>
					        <input name="pass" type="password" placeholder="Passwort"/>
					         <br class="clr"/> <br/>
					        <input type="submit" class="submit" name="submit" value="Einloggen&nbsp;&nbsp;&nbsp;>>>" style="margin-left:25px;">
					         <br class="clr"/><br/><br/>
					        <hr>
					        <br/><br/>
					        <a href="<?php echo WEBDIR?>pwreminder.htm">Sie haben Ihr Passwort vergessen?</a>
					    </form>
					    
					    
				  </div>
  				</div>
				<div class="col span_1_of_3">
					<div class="contact_info">
    	 				<h3>Loginbereich</h3>
					    <p>
					    Alle Bereiche dieses Dokumentationstools sind nur nach Login verwendbar. Für Fragen wenden Sie sich bitte an die Geschäftsleitung.
					    <br/><br/>
					    Dieses Tool wurde erstellt durch <a href="http://nb-cooperation.de">nb-cooperation UG (haftungsbeschränkt)</a>
					    </p>
      				</div>
      			   <div class="company_address">
				     	<h3>Kontaktinformationen :</h3>
						    	<p>NE natürliches-erleben UG (haftungsbeschränkt)</p>
						   		<p>Berliner Straße 50</p>
						   		<p> 01067 Dresden </p>
				   		<p>Tel: 0351/ 4 84 84 51 0</p>
				   		
				 	 	<p>Email: <span><a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#107;&#111;&#110;&#116;&#97;&#107;&#116;&#64;&#110;&#97;&#116;&#117;&#101;&#114;&#108;&#105;&#99;&#104;&#101;&#115;&#45;&#101;&#114;&#108;&#101;&#98;&#101;&#110;&#46;&#100;&#101;">&#107;&#111;&#110;&#116;&#97;&#107;&#116;&#64;&#110;&#97;&#116;&#117;&#101;&#114;&#108;&#105;&#99;&#104;&#101;&#115;&#45;&#101;&#114;&#108;&#101;&#98;&#101;&#110;&#46;&#100;&#101;</a></span></p>
				   		
				   </div>
				 </div>
				   <div class="clear"></div>
			  </div>
   
