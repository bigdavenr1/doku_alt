<?php
namespace modules\login;
class Model extends \inc\Basicdb
{
    use ModulConfig;
    // Konstruktor
    //////////////////////////////////////////////////////////////////////////////////////////////

     function __construct($anzahl = 0)
    {
       global $sql;
	    global $func;
        $this->func=$func;
       $this->queryVars=(object) array('table' => self::$table['u'], 'offsetname'=>false);
       parent::__construct();
    }

    // User Funktionen
    //////////////////////////////////////////////////////////////////////////////////////////////

    /**
    * Funktion für den Loginvorgang
    *
    * @param string $name -> Login-Name
    * @param string $pass -> Passwort im Klartext
    * @return null
    */
    public function getByLogin($name, $pass)
    {
       
        $queryVars = clone($this->queryVars);
        $queryVars->fields=array(self::$table['u'].".*",
                                'a.rights',
                                "BIN(".self::$table['u'].".active) active",
                                "((((trycount-9)*(trycount-9))*((trycount-9)*(trycount-9)))/10) -(UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(lastWrongTry)) seconds");
        $queryVars->joins=array(
                                  (object) array('type'=>'LEFT',
                                                 'table'=>'kta_usergroups a',
                                                 'clause'=>"a.id=".self::$table['u'].".usertype")
                                );
        $queryVars->clause='email= :eMail';
        $queryVars->preparedVars=array(':eMail'=>$name);
        parent::createQuery($queryVars);
        $user = parent::getElement();
        $check = $this->func->bcrypt_check($user->email, $pass, $user->pass);
        // Wenn Check in Ordnung und keine Sperre beim Login drin ist....
        if(!empty($check) && ($user->trycount<10 || ($user->trycount>=10 && $user->seconds<=0))):
            $this->insertIntoSession($user);
            return ;
        // Wenn login nicht in Ordnung und keine Sperre Drin ist.
        elseif( empty($check) &&  ( ($user->seconds<=0 && $user->trycount>10) || $user->trycount<=10) ):
            $this->incrementTryCount($user->id);
            return;
        // Wenn Sperre vorhanden und der USer aber vorhanden ist.
        elseif(!empty($user->id) && ($user->trycount>=10 && $user->seconds>0)):
            return $user->seconds;
        endif;
    }
    /**
    * Funktion zur Abfrage eines Datensatzes anhand der eMail-Adresse
    *
    * @param string $mail E-Mail-Adresse anhand derer der Datensatz gefunden werden soll
    * @return null
    *
    */
    public function getByMail($mail)
    {
    	$queryVars = clone($this->queryVars);
		$queryVars->clause="email = :EMAIL";
		$queryVars->preparedVars=array(':EMAIL'=>$mail);
        parent::createQuery($queryVars);
		return parent::getElement();
    }
    /**
    * Funktion zur Erzeugung der Session-Daten bei erfolgreichem Login und setzen eine SANITY-KEYS
    *
    * @param object $benutzer Benutzerobject, das aus der Datenbank ausgelesen wurde
    * @return null
    *
    */
    private function insertIntoSession($benutzer=false)
    {
        // Alte Session löschen
        unset($_SESSION["user"]);
        // Benutzerdaten in Session schreiben wenn Benutzer übergeben wurde
        if ($benutzer):
            unset($benutzer->pass);
            $_SESSION["user"]=$benutzer;
            // Sanity-Key in SESSION schreiben
            $_SESSION["user"]->SESSION_SANITY_KEY=md5(substr($_SERVER['REMOTE_ADDR'],0,7).$_SERVER['DOCUMENT_ROOT'].$_SERVER['HTTP_USER_AGENT']);
            $this->incrementTryCount($benutzer->id,true);
        endif;
        session_regenerate_id(TRUE);
    }

    public function getLoginContent()
    {
        global $func;
        $this->func=$func;
        $queryVars = clone($this->queryVars);
        $queryVars->table="kta_content";
        $queryVars->clause="alias= 'login'";
        parent::createQuery($queryVars);
    }

    private function incrementTryCount($id,$reset=false)
    {
        $queryVars = clone($this->queryVars);
        if (empty($reset)):
            $queryVars->data=array('trycount'=>array('plainSQL'=>'trycount+1'), 'lastWrongTry'=>array('plainSQL'=>'NOW()') );

        else:
            $queryVars->data=array("trycount"=>0, 'lastWrongTry'=>'0000-00-00 00:00:00');

        endif;
        $queryVars->clause="id=:ID";
        $queryVars->preparedVars=array(':ID'=>intval($id));
        parent::update($queryVars);

    }
	
	public function updatePw($pw, $user)
	{
		$queryVars = clone($this->queryVars);
		$queryVars->data=array("pass"=>$this->func->bcrypt_encode($user->email,$pw));
		$queryVars->clause='id= :ID';
		$queryVars->preparedVars=array(':ID'=>intval($user->id));
		parent::update($queryVars);
	}
}
