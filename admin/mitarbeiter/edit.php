<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
require_once(ADMINDIR."template/header.inc.php");   
// Objekt initialisieren
$benutzerfac=new Benutzer();
//Daten holen
$benutzerfac->getBenutzerById($_GET['id']);
$benutzer=$benutzerfac->getElement();
?>
<h1>Mitarbeiter <?php echo (isset($benutzer))?" - ".stripslashes(strip_tags($benutzer->name))." - bearbeiten":"neu erstellen"?></h1>
<br/>
<form action="<?php echo $l->makeFormLink(WEBDIR."admin/mitarbeiter/update.php?".htmlentities($_SERVER['QUERY_STRING']))?>" method="post" >
    <fieldset class="td1">
        <legend>persönl. Daten</legend>
        <?php echo ($benutzer->id)?'<label>ID</label>'.stripslashes(strip_tags($benutzer->id)).'<br class="clr"/><br/>':'';?>
        <label for="name">Vor- / Nachname</label>
        <input type="text" name="name" id="name" value="<?php echo stripslashes(strip_tags($benutzer->name))?>"/><br class="clr"/>
        <label for="username">Loginname</label>
        <input type="text" name="username" id="username" value="<?php echo stripslashes(strip_tags($benutzer->username))?>"/><br class="clr"/>
        <label for="email">eMail</label>
        <input type="text" name="email" id="email" value="<?php echo stripslashes(strip_tags($benutzer->email))?>"/><br class="clr"/>
    </fieldset>
    <fieldset class="td2">
        <legend>Einstellungen</legend>
        <label for="typ">Typ</label>
        <select name="typ" id="typ">
            <option value="mitarbeiter" <?php echo ($benutzer->usertype=="mitarbeiter")?$sel:"";?>>Mitarbeiter</option>
            <option value="admin" <?php echo ($benutzer->usertype=="admin")?$sel:"";?>>Admin</option>
        </select><br class="clr"/>
        <label for="status">Status</label>
        <input type="checkbox" name="status" id="status" value="0" <?php echo ($benutzer->block=="0")?$chk:"";?>/>
    </fieldset>
    <fieldset class="td1">
        <legend>neues Passwort</legend>
        <label for="passwort">neues Passwort</label>
        <input type="password" name="passwort" id="passwort"/>
    </fieldset>
    <fieldset class="none">
        <br/>
        <input type="submit" class="submit" value="Mitarbeiter speichern"/>
    </fieldset>
</form>
<?php
require_once(ADMINDIR."template/footer.inc.php");
?>