<?php
include("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
require_once(ADMINDIR."template/header.inc.php");   
// Objekt initialisieren
$benutzerfac=new Benutzer(20);
$benutzerfac->getBenutzer();
?>
<h1>Mitarbeiterverwaltung </h1>
<br/>
<?php echo $l->makeLink($icon_neu_small." Neuen Mitarbeiter anlegen",WEBDIR."admin/mitarbeiter/edit.php?mode=new","adminlink");?>
<br /><br/>
<?php if ($_SESSION['err']) echo '<div class="err">'.$_SESSION['err'].'</div><br/>';unset($_SESSION['err']) ?>
<?php if ($_SESSION['msg']) echo '<div class="ok">'.$_SESSION['msg'].'</div><br/>';unset($_SESSION['msg']) ?>
<table style="width:100%;float:left;">
    <tr>
        <?php
        echo $func->tableHeadSort("ID","id","","","width:35px;");
        echo $func->tableHeadSort("Benutzername","username","","");
        echo $func->tableHeadSort("Name","name");
        echo $func->tableHeadSort("Typ","typ","","","width:120px;");
        echo $func->tableHeadSort("Status","block","","","width:120px;");
        echo $func->tableHead("Admin","","","width:45px;");
        ?>                                     
    </tr>
    <?php
    $x=0;
    // Benutzerdaten abrufen
    while($benutzer=$benutzerfac->getElement())
    {
        // Ausgabe
        $x++;
        $class="1";
        if ($x%2) $class="2";
        ?>
        <tr>
            <td class="td<?php echo $class ?>">
                <?php echo $benutzer->id ?>
            </td>
            <td class="td<?php echo $class ?>">
                <?php echo stripslashes(strip_tags($benutzer->username)) ?>
            </td>
            <td class="td<?php echo $class ?>">
                <?php echo stripslashes(strip_tags($benutzer->name)) ?>
            </td>
            <td class="td<?php echo $class ?>">
                <?php echo $benutzer->usertype ?>
            </td>
            <td class="td<?php echo $class ?>">
                <?php echo (($benutzer->block == "0")?'<span >A</span> ':'<span style="color:#FF0000">I</span>').$l->makeLink($icon_refresh,WEBDIR."admin/mitarbeiter/update.php?mode=changestatus&amp;typ=".$_GET['typ']."&amp;status=".$_GET['status']."&amp;sort=".$_GET['sort']."&amp;rtg=".$_GET['rtg']."&amp;id=".$benutzer->id."&amp;offset=".$_GET['offset'],"adminlink");?>
            </td>
            <td class="td<?php echo $class ?>" style="text-align:center">
                <?php
                echo $l->makeLink($icon_edit_small,WEBDIR."admin/mitarbeiter/edit.php?mode=update&amp;id=".$benutzer->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");
                echo $l->makeLink($icon_delete_small,WEBDIR."admin/mitarbeiter/delete.php?id=".$benutzer->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");
                ?>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
<br class="clr"/>
<div id="htmlnavi">
    <?php $htmlnavi=$benutzerfac->getHtmlNavi("std","&amp;".htmlentities($_SERVER['QUERY_STRING']));
    echo ($htmlnavi=="Keine Eintr&auml;ge vorhanden!")?'<table style="width:100%"><tr><td class="td1">'.$htmlnavi.'</td></tr></table>':'<br/>&nbsp;<br/>'.$htmlnavi;?>
</div>
<br/>
<b>Legende: </b><?php echo $icon_edit_small?> = Eintrag editieren; <?php echo $icon_delete_small?> = Eintrag löschen
<?php
include(ADMINDIR."template/footer.inc.php");
?>