<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php"); 
// Überprüfen ob Aufgabe übergeben wurde
if (!isset($_GET['mode'])) $_GET['mode']="new";
// Newsobjekt erzeugen
$benutzerfac=new Benutzer();
// Datensatz suchen  
$benutzerfac->getBenutzerById($_GET['id']);
############################################
# Wenn nur der Status geändert werden soll #
############################################
if ($_GET['mode']=="changestatus")
{
    // Wenn Datensatz gefunden
    if ($benutzer=$benutzerfac->getElement())
    {
        //print_r($benutzer);
        $benutzertatus=($benutzer->block=="0")?"0":"1";
        $benutzerfac->update("block='".$benutzertatus."'","id",mysql_real_escape_string($_GET['id']));
        $_SESSION['msg']="Status erfolgreich geändert!";
    }
    // Wenn kein Datensatz gefunden
    else
    {
        $_SESSION['err']="Datensatz nicht gefunden!";
    }
}
#######################################
# Wenn Datensatz geändert werden soll #
#######################################
elseif ($_GET['mode']=="update")
{
    if ($benutzer=$benutzerfac->getElement())
    {
        // Setzen des Status
        (!isset($_POST['status']))?$_POST['status']="1":'';
        $salt=$func->generateRandomKey(32);
        $passwort=($_POST['passwort']!="")?md5($_POST['passwort'].$salt).":".$salt:$benutzer->password;
        // Datensatz updaten
        $benutzerfac->update("name='".mysql_real_escape_string($_POST['name'])."',
                          username='".mysql_real_escape_string($_POST['username'])."',
                             email='".mysql_real_escape_string($_POST['email'])."',
                             block='".mysql_real_escape_string($_POST['status'])."',
                          usertype='".mysql_real_escape_string($_POST['typ'])."',
                          password='".$passwort."'",
                                   "id",mysql_real_escape_string($_GET['id']));
        $_SESSION['msg']="Mitarbeiter erfolgreich geändert!";
    }
    else
    {
        $_SESSION['err']="Datensatz nicht gefunden!";
    }
}
#######################################
# Wenn Datensatz geändert werden soll #
#######################################
elseif ($_GET['mode']=="new")
{
    // Setzen des Status
    (!isset($_POST['status']))?$_POST['status']="1":'';
    $salt=$func->generateRandomKey(32);
    $passwort=md5((($_POST['passwort']!="")?$_POST['passwort']:$func->generateRandomKey(8)).$salt).":".$salt;
    // Datensatzarray
    $artikel[]="";
    $artikel[]=$_POST['name'];
    $artikel[]=$_POST['username'];
    $artikel[]=$_POST['email'];
    $artikel[]=$passwort;
    $artikel[]=$_POST['typ'];
    $artikel[]=$_POST['status'];

    // Datensatz updaten
    $benutzerfac->write($artikel);
    $_SESSION['msg']="Mitarbeiter erfolgreich hinzugefügt!";
}
header("Location:".$l->makeUrl(WEBDIR."admin/mitarbeiter/view.php?".$_SERVER['QUERY_STRING']));
?>