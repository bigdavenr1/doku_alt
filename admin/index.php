<?php
include("../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
include(ADMINDIR."template/header.inc.php");
?>
<h1>Administrationsbereich</h1><br/>
Sie sind jetzt als Administrator eingeloggt und können nun die freigeschalteten Module pflegen.<br/><br/>
Momentan können Sie folgende Module verwalten:<br/><br/>
<?php echo $l->makeLink("Menus pflegen",WEBDIR."admin/menu/view.php")?><br/>
<?php echo $l->makeLink("Menupunkte freigeben",WEBDIR."admin/menu/aktivierung.edit.php")?><br/>
<?php echo $l->makeLink("Inhaltsseiten pflegen",WEBDIR."admin/content/view.php")?><br/>
<?php echo $l->makeLink("Videos pflegen",WEBDIR."admin/videos/view.php")?><br/><br/>
<?php echo $l->makeLink("Kurse pflegen",WEBDIR."admin/kurse/view.php")?><br/><br/>
<?php echo $l->makeLink("Fitnesstudios pflegen",WEBDIR."admin/studios/view.php")?><br/><br/>                            
<?php
include(ADMINDIR."template/footer.inc.php");
?>