<?php 
if (!class_exists("Startseite"))
{
    class Startseite extends Basicdb
    { 
        // Konstruktor
        ////////////////////////////////////////////////////////////////////////
        
         function Startseite($anzahl = false)
        {
            global $sql;
            $this->anzahl = $anzahl;
            $this->table="jos_startseite";
            parent::__construct();
        }
        function getBildTopWithArtikel()
        {
            parent::createQuery("WHERE Typ='startbild'");
        }

        function getInhaltRight()
        {
            parent::createQuery("WHERE Typ='startrechts'");
        }
        
        function getStartseite($lang=false)
        {
           $this->felder=$this->table.".*";
           if($lang)
           {
               $this->felder="jos_startseite.*, a.text titel_uebersetzung,  b.text right_uebersetzung, c.text subtitel_uebersetzung, d.text text_uebersetzung" ;
               $query.=" LEFT JOIN uebersetzung a ON (a.typ='startseite_titel' AND a.sprache='".mysql_real_escape_string($lang)."')";
               $query.=" LEFT JOIN uebersetzung b ON (b.typ='startseite_right' AND b.sprache='".mysql_real_escape_string($lang)."')";
               $query.=" LEFT JOIN uebersetzung c ON (c.typ='startseite_subtitel' AND c.sprache='".mysql_real_escape_string($lang)."')";
               $query.=" LEFT JOIN uebersetzung d ON (d.typ='startseite_text' AND d.sprache='".mysql_real_escape_string($lang)."')";
           }
           parent::createQuery($query);
        }

        function getStartseiteForUebersetzung($sprachid)
        {
             $this->felder="jos_startseite.*, a.text titel_uebersetzung,  b.text right_uebersetzung, c.text subtitel_uebersetzung, d.text text_uebersetzung" ;
             $query.=" LEFT JOIN uebersetzung a ON (a.typ='startseite_titel' AND a.sprache='".mysql_real_escape_string($sprachid)."')";
             $query.=" LEFT JOIN uebersetzung b ON (b.typ='startseite_right' AND b.sprache='".mysql_real_escape_string($sprachid)."')";
             $query.=" LEFT JOIN uebersetzung c ON (c.typ='startseite_subtitel' AND c.sprache='".mysql_real_escape_string($sprachid)."')";
             $query.=" LEFT JOIN uebersetzung d ON (d.typ='startseite_text' AND d.sprache='".mysql_real_escape_string($sprachid)."')";
             parent::createQuery ($query);
        } 
    } 
} 
?>