<?php
if (!class_exists("Artikel"))
{
    class Artikel extends Basicdb
    {
        // Konstruktor
        //////////////////////////////////////////////////////////////////////////////////////////////

         function Artikel($anzahl = 0)
        {
            global $sql;
            $this->table = "jos_content";
            $this->anzahl = $anzahl;
            parent::__construct();
            $this->countvar=$this->table.".id";  
        }
        
        // User Funktionen
        //////////////////////////////////////////////////////////////////////////////////////////////
        
        // �berladen
         function getArtikelById($id,$langid=false)
        {
            $this->felder="jos_content.*";
            if( $langid!=false)
            {
                $this->felder.=", b.text text_uebersetzung";
                $query.=" LEFT JOIN uebersetzung b ON (b.artid=jos_content.id && b.typ='artikeltext' AND sprache='".mysql_real_escape_string($langid)."')";
            }
            parent::createQuery($query." WHERE jos_content.id='".mysql_real_escape_string($id)."'"," ", " ");
        }
        
        function  getAllArtTitle($langid=false)
        {
            $this->felder="jos_content.title,jos_content.id";
            if ($langid!=false)
            {
                $this->felder.=",uebersetzung.text, (SELECT DATE_FORMAT(date,GET_FORMAT(DATE,'EUR')) FROM log WHERE log.aid=uebersetzung.id ORDER BY log.id DESC LIMIT 0,1) zeit ";
                $query="LEFT JOIN uebersetzung ON (uebersetzung.artid=jos_content.id && uebersetzung.typ='artikeltitel')";
            }
            parent::createQuery("".$query);
        }
        
        function getbyIdWithTranslation($id,$sprachid)
        {
            $this->felder="jos_content.*, a.text title_uebersetzung, b.text text_uebersetzung";
            $query.=" LEFT JOIN uebersetzung a ON (a.artid=jos_content.id AND a.typ='artikeltitel' AND a.sprache='".mysql_real_escape_string($sprachid)."')";
            $query.=" LEFT JOIN uebersetzung b ON (b.artid=jos_content.id AND b.typ='artikeltext' AND b.sprache='".mysql_real_escape_string($sprachid)."')";
            parent::createQuery($query." WHERE jos_content.id='".mysql_real_escape_string($id)."'"," ", " ");
        }
        
    }
}
        
?>