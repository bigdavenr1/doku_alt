<?php
////////////////////////////////////////////////////////////////////////////////
// News-Klasse - Erbt von Basicdb
//             - verwaltet die Tabelle news 
////////////////////////////////////////////////////////////////////////////////

if (!class_exists("News"))
{
    class News extends Basicdb
    {   
    
        // Konstruktor
        ////////////////////////////////////////////////////////////////////////
        
         function News($anzahl = false)
        {
            global $sql;
            $this->table = $sql["table_news"];
            $this->anzahl = $anzahl;
            parent::__construct();
            $this->countvar="news.id";
        }
               
        // News Funktionen
        ////////////////////////////////////////////////////////////////////////
        
        function getOrderById($id)
        {
            parent::createQuery("WHERE id=".$id." ORDER BY id DESC");
        }
        
        function getByStatus($status)
        {
            parent::createQuery("WHERE status='".$status."' ORDER BY id DESC" );
        }
        
        function writePic($pic,$key,$type)// erstellt Thumbnailbild
        {
        // Gr��e auslesen (size[0]=breite, size[1]=h�he)
            $size = getimagesize($pic);

            if ($size[0] > $size[1])
            {
                $thumbBreite = 120;
                $thumbHoehe = (120*$size[1])/$size[0];
            }
            else
            {
                $thumbHoehe = 120;
                $thumbBreite = (120*$size[0])/$size[1];
            }
            
            if($type == '.jpg')
            { 
                $altesBild = ImageCreateFromJPEG($pic);
            }
            if($type == '.gif')
            { 
                $altesBild = ImageCreateFromGIF($pic);
            }            
            
            $neuesBild = imageCreateTrueColor($thumbBreite, $thumbHoehe);
        
            imageCopyResampled($neuesBild, $altesBild, 0, 0, 0, 0, $thumbBreite, $thumbHoehe, $size[0], $size[1]);
            
            
            if($type == '.jpg')
            { 
              ImageJPEG($neuesBild,  LOCALDIR."images/news/".$key.$type, 85);
            }
            if($type == '.gif')
            {             
               ImageGIF($neuesBild,  LOCALDIR."images/news/".$key.$type, 85);
            }
            
            chmod( LOCALDIR."images/news/".$key.$type, 0777);
        }
        
        function getNewsForUebersetzung($lang)
        {
            $this->felder="news.*, IF( a.text IS NOT NULL ,'ja','nein') uebersetzung, (SELECT DATE_FORMAT(date,GET_FORMAT(DATE,'EUR')) FROM log WHERE log.aid=a.id ORDER BY log.id DESC LIMIT 0,1) zeit ";
            $query="LEFT JOIN uebersetzung a ON (a.sprache='".$lang."' AND a.typ='newstitel' AND a.artid=news.id)";
            parent::createQuery($query);
        }

        function getByIdWithTranslation ($id,$sprachid)
        {
            $this->felder="news.*, a.text text_uebersetzung, b.text titel_uebersetzung";
            $query.=" LEFT JOIN uebersetzung a ON (a.artid=news.id AND a.typ='newstext' AND a.sprache='".mysql_real_escape_string($sprachid)."')";
            $query.=" LEFT JOIN uebersetzung b ON (b.artid=news.id AND b.typ='newstitel' AND b.sprache='".mysql_real_escape_string($sprachid)."')";
            parent::createQuery($query." WHERE news.id='".mysql_real_escape_string($id)."'");
        }
        
    }//end class
}//end if (!class_exists("News"))