<?php
if (!class_exists("Menu")):
    class Menu extends MenuData
    {
        ##############################################
        # Konstruktor (Parentkonstruktor übernehmen) #
        ##############################################
        protected function __construct()
        {
            parent::__construct();
        }
        ############################
        # Funktion zum Daten holen #
        ############################
        private function getMenuData($menutyp,$lang,$method)
        {
            // $menudataarray leeren
            $this->menudata=array();
            // geerbte Methode zum Datenholen aufrufen (je anchdem für welchen Zweck)
            parent::$method($menutyp,$lang);
            // Datenobejkt durchgehen und als array zur Verfügung stellen
            while($menudata=parent::getElement()):
                $this->menudata[]=$menudata;
            endwhile;
        }
        #######################################
        # Function zum Bauen eines MenuBaumes #
        #######################################
        private function buildNested()
        {
            $left='';
            $right='';
            // Array erzeugen
            $struct = array();
            // Verweis auf Array
            $this->menuarray =& $struct;
            // Menudatenarray durchlaufen
            foreach($this->menudata as $item):
                // Schlüssel des Menuarrays auslesen
                $keys = array_keys($this->menuarray);
                // Wenn Unterpunkt, als Children setzen
                if($left<$item->lft && $right> $item->rgt && ($lastitemright>$item->rgt || $lastitemright==0)):
                    $this->menuarray =& $this->menuarray[$keys[count($keys)-1]]['children'];
                    $lastitemright = $item->rgt;
                // Wenn Oberpunkt, neuen Knoten etablieren
                elseif($right< $item->lft && $right < $item->rgt):
                    $this->menuarray =& $struct;
                    $left = $item->lft;
                    $right= $item->rgt;
                    $lastitemright=0;
                endif;
                // Arrayeintrag schreiben an referenzierter Stelle
                $this->menuarray[$item->id] = array(
                'id'       => $item->id,
                'name'     => $item->name,
                'url'      => $item->link,
                'type'     => $item->type,
                'left'     => $item->lft,
                'right'    => $item->rgt,
                'level'    => $item->level,
                'menutype' => !empty($item->menutitle)?$item->menutitle:'',
                'alias'    => !empty($item->alias)?$item->alias:'artikel',
                'menuid'   => $item->menutype,
                'children' => array());
                // Wenn Link Active, Links und Rechts werte speichern
                if (($item->type=="modules" && $_GET['modul']==$item->alias) || ($item->type=="artikel" && $item->link==$_GET['artikelid'] )):  
                   $this->activelink['left']=$item->lft;
                   $this->activelink['right']=$item->rgt;
                endif;
            endforeach;
            $this->menuarray =& $struct;
        }
        ###############################################################################################
        # Funktion zum erstellen eines Arrays und der Filterung aktiver Menupunkte für ein PublicMenu #
        ###############################################################################################
        protected function buildPublicMenu($menutyp=false,$lang=false)
        {
            // Aufruf der Funktion zum Daten holen
            $this->getMenuData($menutyp,$lang,"getPublicMenuData");
            // Aufruf der Methode zum Bauen eines MenuBaumes
            $this->buildNested();
        }
        #########################################
        # Function zur Ausgabe eines Adminmenus #
        #########################################
        protected function buildAdminMenu($langid=false)
        {
            $this->getMenuData($menutyp,$langid,"getMenuDataAdminWithUebersetzung");
            $this->menuarray=$this->menudata;
        }
        #######################################
        # Funktion zur Ausgabe des Menuadmins #
        #######################################
        protected function buildMenuAdmin()
        {
            $this->getMenuData(false,false,"getMenuDataAdmin");
            // Aufruf der Methode zum Bauen eines MenuBaumes
            $this->buildNested();
        }
        ##########################################
        # Funktion zur Erzeugung eines Dropdowns #
        ##########################################
        protected function getMenuDropDown()
        {
            $this->getMenuData($menutyp,$lang,"getMenuForDropDown");
        }
        ################################################################################
        # Funktion zur Holen der MenuTypen denen noch keine Menupunkte zugeordnet sind #
        ################################################################################
        protected function getNullMenu()
        {
            $this->getMenuData(false,false,"getMenuTypes");
            $this->menuarray=$this->menudata;
        }
        protected function buildMenuActiveList()
        {
            $this->getMenuData(false,false,"getAllWithActivationsByLang");
            $this->menuarray=$this->menudata;
        }
    }
endif;
?>