<h1><?php echo ($this->_['menu'])?"Menupunkt: ".$this->_['menu']->name." - bearbeiten":"neuen Menupunkt erstellen"?></h1><br/>
<form id="menuadminbox" action="#" method="post">
    <fieldset class="td0">
        <legend>Menüdaten</legend>
        <label for="menutype">Menutyp</label><br/>
        <b><?php echo $this->_['menu']->menutypename?$this->_['menu']->menutypename:$this->_['menutypename']?></b><br/><br/>
        <label for="lang">Name</label><br/>
        <input type="text" id="name" name="name" value="<?php echo htmlspecialchars(strip_tags(stripslashes($this->_['menu']->name)))?>"/><br/>
        <br/>
        <label for="firma">Oberpunkt</label><br/>
        <?php
            echo $this->_['oberpunkte'];
        ?>
        <br/><br/>
        <label for="inhalt">Inhalt / Zuordnung</label><br/>
        <select name="inhalt" id="inhalt">
            <option value="">Nur als Punkt ohne Inhalt</option>
            <option value="-" id="el" <?php echo $this->_['menu']->modulcheck==10?'selected="selected"':''?>>Externer/Manueller Link</option>
            <option value="">-----------------------</option>
            <option value="">Inhaltsseiten</option>
            <option value="">-----------------------</option>
            <?php
                if (is_array($this->_['content']))
                    foreach ($this->_['content'] as $key => $content)
                    echo "<option ".(($this->_['menu']->modulcheck==01 && $this->_['menu']->link==$content->id)?'selected="selected"':'')
                    ." value='content---".strip_tags(htmlspecialchars(stripslashes($content->id)))."---".strip_tags(htmlspecialchars(stripslashes($content->title)))."'>"
                    .strip_tags(htmlspecialchars(stripslashes($content->title)))."</option>";
            ?>
            <option value="">-----------------------</option>
            <option value="">Module</option>
            <option value="">-----------------------</option>
            <?php
                if (is_array($this->_['modulpoints']))
                    foreach ($this->_['modulpoints'] as $key => $modul)
                        echo "<option  ".(($this->_['menu']->modulcheck==00 && $this->_['menu']->link==$modul->id)?'selected="selected"':'')
                        ." value='modules---".strip_tags(htmlspecialchars(stripslashes($modul->id)))."---"
                        .strip_tags(htmlspecialchars(stripslashes($modul->title)))."'>".strip_tags(htmlspecialchars(stripslashes($modul->title)))."</option>";
            ?>
        </select><br/><br/>
        <label for="elink" class="elink" style="<?php echo $this->_['menu']->modulcheck!=10?'display:none':''?>">manueller Link</label><br/>
        <input type="text" name="elink" class="elink" style="<?php echo $this->_['menu']->modulcheck!=10?'display:none':''?>"
               value="<?php echo $this->_['menu']->modulcheck==10?$this->func->sanitize($this->_['menu']->link):''?>"/>
    </fieldset>
    <br class="clr"/><br/>
    <input type="submit" name="submit" id="submit" value="Menü <?php echo empty($this->_['menu'])?'neu anlegen':'ändern';?>" />
</form>