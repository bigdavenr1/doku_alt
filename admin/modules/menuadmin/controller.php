<?php // Modulview und Modulmodel einbinden
namespace admin\modules\menuadmin;
require_once("model.php");
/**
 * Modulcontroller für die Menübearbeitung
 * @ TODO NEUER Link
 */
class Controller
{

    /**
     * Variable für übergebenen Request
     */
    private $request = null;

    /**
     * Variable für Template
     */
    private $template = 'list';

    /**
     * Variable für Viewobjekt
     */
    private $view = null;

    /**
     * Variable für Modelobjekt
     */
    private $model = null;

    /**
     * Variable für Menüobjekt
     */
    private $menu = null;

    /**
     * Konstruktor
     * @param $array $request Arrey mit allesn Requestdaten
     */
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->view = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->template =isset($this->request['view'])?$this->request['view']:'list';
        // neues Modelobjekt
        $this->model=new Model();
        // neue Menu erzeugen (zum auslesen der Menüpunkte -> kommt aus dem Menücontroller)
        $this->menu = new \inc\std\menu\Controller('menuadmin');
    }

    /**
     * Anzeigemethode
     * @return string Ausgabe des GUI
     */
    public function display()
    {
        // alle Module die holen
        $this->model->getModulpoints();
        $this->view->assign("modulpoints",$this->model->liste);

        // alle Contents auslesen
        $this->model->getContent();
        $this->view->assign("content",$this->model->liste);

        // alle Menütypen heraussuchen denen kein Menüpunkt zugeordnet ist.
        $this->model->getMenusWithoutPoints();
        $this->view->assign("emptyMenus",$this->model->liste);

        // prüfen ob eine Menüpunkt-ID übergeben wurde. ansonstenb auf 0 setzen
        $this->request['menuid']=empty($this->request['menuid'])?0:$this->request['menuid'];

        // Neues Dropdown für Unterpunktzuordnung erzeugen
        $this->dropdown=new \inc\std\menu\Controller('dropdown');

        /**
         * Anhand der übergebenen Menüpunkt-ID entscheiden
         */
        switch($this->request['menuid']):

            /**
             * Wenn Null (also Liste oder NEU)
             */
            case '0':

                // bei neuem Menüpunkt
                if ($this->template=='edit'):

                    // Prüfen ob Daten gesendet wurden und entsprechende Methode ausführ
                    $this->editAndDelete($id,'new');

                    // Menutype übergeben zum Heraussuchen der Unterpunkte
                    $this->dropdown->assign("menutypeid",$this->request['menu']);

                    // Werte für Dropdown übergeben
                    $this->dropdown->assign("submenu",$submenu = $this->model->getSubMenuWithMenuInformation( $this->request['menu']));
                    $this->dropdown->assign("menutypeid",intval($this->request['menu']));
                    $this->dropdown->assign('maxdeep', $submenu->maxdeep);

                    // Werte an View übergeben
                    $this->view->assign("menutypename",$submenu->title);
                    $this->view->assign('oberpunkte',$this->dropdown->getRequestetMenu());

                // Bei Anzeige der Liste
                else:

                    // Liste holen.
                    $this->view->assign('menus',$this->menu->getRequestetMenu());

                endif;
                break;

            /**
             * Wenn eine ID übergeben wurde (also update oder Ansicht)
             */
            default:

                // Menüpunktdaten auslesen
                $this->view->assign("menu",$this->menudata = $this->model->getMenupointsById(intval($this->request['menuid'])));

                // maximale Menütiefe, submneu, Menütyp, Elterm LFT und Elter RGT übergeben an Menüobjekt
                $this->dropdown->assign('maxdeep', $this->menudata->maxdeep);
                $this->dropdown->assign("submenu",$this->model->getById( $this->menudata->menutype,"kta_submenus"));
                $this->dropdown->assign("menutypeid",$this->menudata->menutype);
                $this->dropdown->assign('parentlft', $this->menudata->parentlft);
                $this->dropdown->assign('parentrgt', $this->menudata->parentrgt);

                // Dropdown übergeben (aus Menüklasse)
                $this->view->assign('oberpunkte', $this->dropdown->getRequestetMenu());

                // Submenü an View übergeben
                $this->view->assign("submenu",$this->model->getById( $this->menudata->menutype,"kta_submenus"));

                // Methode zum Speichern odre löschen aufrufen, wenn entzsprechendes Übergeben wurde
                // Entscheidet diese methode selbst
                $this->editAndDelete($this->menu->id);

        endswitch;

        // Template suchen und GUI zurück geben
        $this->view->setTemplate($this->template,dirname(__FILE__));
        return $this->view->loadTemplate();
    }

    /**
     * Methode zum Speichern der Daten
     * @param string $modus -> Modus der Änderung (Löschen, Ändern, Speichern)
     */
    private function changeData($modus)
    {

        // Daten holen
        //$this->model->getById( $this->request['menuid'], 'kta_menupoints' );

        // Übergabewerte aufbereiten
        $linkarray = preg_split( "~---~", $this->request['inhalt'] );
        $menupunktarray = preg_split( "~---~", $this->request['oberpunkt'] );

        // Bei update und neuen Menupunkt schauen ob Modul gewählt
        if ( $linkarray[0] == "modules" ):

            // Modulnamen auslesen
            $modulnamefac = new \Modul();
            $modulnamefac->getById( $linkarray[1], 'kta_modules' );
            $modul = $modulnamefac->getElement() ;

        endif;

        // Prüfung ob Link zum Modul, Content oder externen Link führt
        // 00(0) = Modul, 01(1)=Inhalt, 10(2)=manueller Link, 11(3)= Link ohne Zuordnung (javascript:void(0))
        if($this->func->convertUmlaut($linkarray[0]) == "modules"):
            $modulcheck=bindec(00);
        elseif($this->func->convertUmlaut($linkarray[0]) == "content"):
            $modulcheck=bindec(01);
        elseif($this->request['inhalt']=='-'):
            $modulcheck=bindec(10);
        else:
            $modulcheck=bindec(11);
        endif;


        /**
         * Wenn neuer Menupunkt erstellt werden soll.
         */
        if ($modus=="new"):

            // Menütypen auslesen
            $this->model->getMenuTypes($menupunktarray[0]);
            $submenu = $this->model->getElement();

            $this->model->updateSubemenuByNewPoint($submenu);

            $this->model->table="kta_menupoints";


            $this->model->updateMenupoint(
                    array("lft"=>array
                        ("plainSQL"=>"lft + (IF(lft>".$menupunktarray[1].",2,0))"),
                        "rgt"=>array
                        ("plainSQL"=>"rgt + (IF(rgt>".$menupunktarray[1].",2,0))")
                    ),$menupunktarray[1],"rgt",">");

             // Datensatzarray
            $queryVars->data['menutype']=$this->request['menu']; //menutype
            $queryVars->data['name']=$this->request['name']; //name
            $queryVars->data['link']=$this->request['inhalt']!='-'?$linkarray[1]:$this->request['elink'];  //link-ID:
            $queryVars->data['modulcheck']=array("plainSQL"=>$modulcheck); //Modulcheck: soll auf Module, Content oder externenLink verwiesen werden: 00=modul; 01=content; 10:externerLink
            $queryVars->data['lft']=$menupunktarray[1]+1; //left
            $queryVars->data['rgt']=$menupunktarray[1]+2; //right
            // Datensatz schreiben
            $queryVars->table="kta_menupoints";

            $this->model->write($queryVars);

            $_SESSION['msg']="Menupunkt erfolgreich angelegt!";

        #########################################
        # Wenn Menupunkt geupdatet werden soll. #
        #########################################

        elseif (empty($modus)):
            // Wenn Element gefunden
            if (!empty($this->menudata)):

                // Datensatz updaten
                $this->model->updateMenupoint(array("menutype"=>$this->menudata->menutype,
                                  "name"=>$this->request['name'],
                                  "link"=>($this->request['inhalt']!='-'?$linkarray[1]:$this->request['elink']),
                                  "modulcheck"=>array("plainSQL"=>$modulcheck)),$this->menudata->id,'id','=');

                // Wenn Zuordnung zu Hauptpunkten neu erfolgt ist
                if (($menupunktarray[1] != $this->menudata->parentlft && !empty($this->menudata->parentlft) ) || ($menupunktarray[1] != $this->menudata->submenulft) ):
                     if ($this->menudata->lft > $menupunktarray[1])
                        $this->model->updateMenupoint(array("lft" =>array("plainSQL"=>"
                            CASE
                                WHEN lft > ".$menupunktarray[1]." AND lft < ".$this->menudata->lft."
                                THEN lft +1 + (".($this->menudata->rgt-$this->menudata->lft).")
                                WHEN lft = ".$this->menudata->lft."
                                THEN ".$menupunktarray[1]." + 1
                                WHEN lft >".$this->menudata->lft." AND lft < ".$this->menudata->rgt."
                                THEN  (".$menupunktarray[1]."  + (lft-".$this->menudata->lft.")) +1
                                ELSE lft
                             END
                        "),
                        "rgt"=>array("plainSQL"=>"
                             CASE
                                WHEN rgt > ".$menupunktarray[1]." AND rgt < ".$this->menudata->lft."
                                THEN (rgt + ".($this->menudata->rgt-$this->menudata->lft+1).")
                                WHEN rgt = ".$this->menudata->rgt."
                                THEN ".($menupunktarray[1]+$this->menudata->rgt-$this->menudata->lft+1)."
                                WHEN rgt >".$this->menudata->lft." AND rgt < ".$this->menudata->rgt."
                                THEN  rgt - ".($this->menudata->lft-$menupunktarray[1]-1)."
                                ELSE rgt
                             END
                        ")),false,1,'=');
                    else
                         $this->model->updateMenupoint(array("lft" =>array("plainSQL"=>"
                            CASE
                                WHEN lft <= ".$menupunktarray[1]." AND lft > ".$this->menudata->rgt."
                                THEN lft - (".(($this->menudata->rgt-$this->menudata->lft)+1).")
                                WHEN lft >=".$this->menudata->lft." AND lft < ".$this->menudata->rgt."
                                THEN  (".$menupunktarray[1]." - ".($this->menudata->rgt-$this->menudata->lft+1).")
                                + (lft-".$this->menudata->lft.") +1
                                ELSE lft
                             END
                        "),
                        "rgt"=>array("plainSQL"=>"
                             CASE
                                WHEN rgt > ".$this->menudata->lft." AND rgt <= ".$this->menudata->rgt."
                                THEN rgt+ ".($menupunktarray[1]-$this->menudata->rgt)."
                                WHEN rgt >".$this->menudata->rgt." AND rgt < ".$menupunktarray[1]."
                                THEN rgt- ((".$this->menudata->rgt." - ".$this->menudata->lft." ) +1)
                                ELSE rgt
                             END
                        ")),false,1,'=');
                endif;
            endif;
            $_SESSION['msg']="Menupunkt erfolgreich geändert!";

        ############################################
        # Wenn Menupunkte verschoben werden sollen #
        ############################################
        elseif($modus=="positionup" || $modus=="positiondown"):
            $menupoint = $this->model->getPrevOrNextMenupointWithLastId($modus, intval($this->request['menuid']));
            if (!empty($menupoint->nrgt) && !empty ($menupoint->nlft))
                $this->model->moveMenuPoint($menupoint,$modus);
        endif;
    }

    /**
     * Methode um abgesendete Daten abzufangen
     * und weiteres Vorgehen zu bestimmen
     * @param Integer $id ID des Eintrages der geändert werden soll
     * @param String $modus Eintscheidungsvariable Bestimmung der nächsten Aktion
     */
    private function editAndDelete( $id , $modus = false )
    {

        /**
         * Wenn Ein Eintrag geändert oder neu angelegt werden soll und  Daten abgesendet wurden
         */
        if ( $this->template == 'edit' && !empty( $this->request['submit'] ) ) :

            // DAtenänderung anschieben
            $this->changeData( $modus ) ;

            // Erfolgsmeldung
            $_SESSION['msg'] = 'Menüeintrag erfolgreich' . ( empty( $modus ) ? 'geändert' : 'angelegt' ) ;
            // Weiterleitung

            header( 'Location:' . WEBDIR . 'admin/menuadmin.htm' ) ;
            exit ;

        /**
         * Wen Eintrag gelöscht werden soll und abgesendet wurde
         */
        elseif ( $this->template=='delete' && !empty( $this->request['submit'] ) ) :

           // Prüfen ob Unterpunkzt existiert, Wenn nicht
           if ( $this->menudata->rgt - $this->menudata->lft == 1 ) :
               // Menüpunkt lsöchen
               $this->model->deleteMenupoint($this->menudata->id);

               // Alle anderen Nested-Set-Menupunkte und Submenus anpassen
               $this->model->updateSubmenusByDelete(array("lft" =>array("plainSQL"=>"lft- (CASE
                                           WHEN lft > ".$this->menudata->lft." AND rgt>".$this->menudata->lft." AND lft!= ".$this->menudata->lft." AND rgt!=".$this->menudata->rgt."
                                           THEN 2
                                           ELSE 0
                                         END)"),
                                  "rgt" =>array("plainSQL"=> "CASE WHEN rgt != ".$this->menudata->rgt." AND rgt>".$this->menudata->lft." THEN rgt-2 ELSE rgt END")),
                                  false,1,'!=');
               // Alle Menüpunkte updaten im Nested Set
               $this->model->updateMenupoint(array("lft" =>array("plainSQL"=>"lft- (CASE
                           WHEN lft > ".$this->menudata->lft." AND  rgt>".$this->menudata->lft." AND lft!= ".$this->menudata->lft." AND rgt!=".$this->menudata->rgt."
                           THEN 2
                           ELSE 0
                         END)"),
                  "rgt" =>array("plainSQL" => "CASE WHEN rgt != ".$this->menudata->rgt." AND  rgt>".$this->menudata->lft." THEN rgt-2 ELSE rgt END ")),
                  false, 1, '!=');
                  $_SESSION['msg']="Menupunkt erfolgreich gelöscht";

            // Wenn Knoten nicht leer ist
            else :

              // Fehlertmeldung
              $_SESSION['err'] = "Eintrag nicht gefunden oder Knoten nicht leer!" ;

            endif ;

            // Weiterleitung
            header( 'Location:' . WEBDIR . 'admin/menuadmin.htm' ) ;
            exit ;

        /**
         * Wenn eine Position nach oben oder unten geschoben werden soll
         */
        elseif( $this->request['mode'] == "positionup" || $this->request['mode'] == "positiondown"  ) :

            // Methode zum Positionswechsel aufrufen
            $this->changedata($this->request['mode']) ;

            // Weiterleitung
            header( 'Location:' . WEBDIR . 'admin/menuadmin.htm' ) ;
            exit;

        endif;
    }
}