<?php
namespace admin\modules\menuadmin;
class Model extends \inc\Basicdb
{
     public function __construct($anzahl = false)
     {
          $this->table="kta_menupoints";
          $this->queryVars=(object) array('table' => $this->table, 'offsetname'=>false,'countVar'=>$this->table.".id");
          parent::__construct();
     }

        public function getSubMenuWithMenuInformation($submenuID)
        {
            $queryVars=clone($this->queryVars);
            $queryVars->table="kta_submenus";
            $queryVars->fields=array("kta_submenus.*", "a.maxdeep");
            $queryVars->joins=array(
                                        (object)
                                        array('type'=>'LEFT',
                                              'table'=>'kta_menus a',
                                              'clause'=>'kta_submenus.menutype=a.id'
                                        )
                                     );
            $queryVars->clause=' kta_submenus.id= :ID';
            $queryVars->preparedVars=array(':ID'=>intval($submenuID));
            unset($queryVars->countVar);
            parent::createQuery($queryVars);
            return parent::getElement();
        }

        public function getMenupointsById($id)
        {
            $queryVars=clone($this->queryVars);
            $queryVars->fields=array("kta_menupoints.*",
                                     "BIN(kta_menupoints.modulcheck) modulcheck",
                                     "a.title menutypename",
                                     "p.lft parentlft",
                                     "p.rgt parentrgt ",
                                     "a.lft submenulft",
                                     "a.rgt submenurgt",
									 "m.maxdeep");
            $queryVars->joins=array(
                                        (object)
                                        array('type'=>'LEFT',
                                              'table'=>'kta_submenus a',
                                              'clause'=>'a.id=kta_menupoints.menutype'
                                        ),
                                        (object)
                                        array('type'=>'LEFT',
                                              'table'=>'kta_menupoints p',
                                              'clause'=>'p.lft < kta_menupoints.lft AND p.rgt > kta_menupoints.rgt'
                                        ),
                                        (object)
                                        array('type'=>'LEFT',
                                              'table'=>'kta_menus m',
                                              'clause'=>'m.id = a.menutype'
                                        )
                                    );
            $queryVars->clause=' kta_menupoints.id= :ID';
            $queryVars->preparedVars=array(':ID'=>intval($id));
            $queryVars->order=(object) array('direction'=>'DESC', 'field'=>'p.lft');

            parent::createQuery($queryVars);
            return parent::getElement();
        }

		public function getPrevOrNextMenupointWithLastId($modus,$id)
		{
			$queryVars=clone($this->queryVars);
			 unset($queryVars->countVar);
			 $queryVars->fields=array("kta_menupoints.lft",
			 						  "kta_menupoints.rgt",
			 						  "kta_menupoints.menutype",
			 						  "a.lft nlft",
			 						  "a.rgt nrgt");
		     $queryVars->joins=array(
                                        (object)
                                        array('type'=>'LEFT',
                                              'table'=>'kta_menupoints a',
                                              'clause'=>($modus=='positionup'?'a.rgt':'a.lft')."= "
                                                        .($modus=='positionup'?'kta_menupoints.lft-1':'kta_menupoints.rgt+1')
                                        ));
			$queryVars->clause="kta_menupoints.id = :menuId";
			$queryVars->preparedVars=array('menuId'=>intval($id));
			parent::createQuery($queryVars);
			return parent::getElement();
		}

        public function getModulpoints()
        {
            $queryVars=clone($this->queryVars);
            $queryVars->table="kta_modules";
            unset($queryVars->countVar);
            $queryVars->fields=array('id',
                                     'title');
            parent::createQuery($queryVars);
            return $this->liste;
        }

		public function getContent()
        {
            $queryVars=clone($this->queryVars);
            $queryVars->table="kta_content";
            unset($queryVars->countVar);
            $queryVars->fields=array('id',
                                     'title');
            parent::createQuery($queryVars);
            return $this->liste;
        }

        public function getMenuType($typeId)
        {
            $queryVars=clone($this->queryVars);
            $queryVars->clause="id = :ID";
            $queryVars->preparedVars=array(':ID'=> intval($typeId));
            parent::createQuery($queryVars);
        }
        public function getMenusWithoutPoints()
        {
            $queryVars=clone($this->queryVars);
            $queryVars->table="kta_submenus";
            unset($queryVars->countVar);
            $queryVars->clause="( SELECT COUNT(*) FROM kta_menupoints WHERE menutype= ".$queryVars->table.".id) = 0";
            parent::createQuery($queryVars);
            return $this->liste;
        }

		public function moveMenuPoint($menupoint, $modus)
		{
			$queryVars=clone($this->queryVars);
            $queryVars->data=array("lft" =>array("plainSQL"=>" CASE
                                          WHEN lft <= ".($modus=='positionup'?$menupoint->nrgt:$menupoint->rgt)."
                                          THEN lft + ".($modus=='positionup'?$menupoint->rgt-$menupoint->lft+1:$menupoint->nrgt-$menupoint->nlft+1)."
                                          ELSE lft - ".($modus=='positionup'?$menupoint->nrgt-$menupoint->nlft+1:$menupoint->rgt-$menupoint->lft+1)."
                                        END"),
                                  "rgt" => array("plainSQL"=>"CASE
                                          WHEN rgt <= ".($modus=='positionup'?$menupoint->nrgt:$menupoint->rgt)."
                                          THEN rgt + ".($modus=='positionup'?$menupoint->rgt-$menupoint->lft+1:$menupoint->nrgt-$menupoint->nlft+1)."
                                          ELSE rgt - ".($modus=='positionup'?$menupoint->nrgt-$menupoint->nlft+1:$menupoint->rgt-$menupoint->lft+1)."
                                        END"));
            $queryVars->clause="lft BETWEEN
                                        ".($modus=='positionup'?$menupoint->nlft:$menupoint->lft)."
                                      AND
                                        ".($modus=='positionup'?$menupoint->rgt:$menupoint->nrgt);
            parent::update($queryVars);
		}

        public function updateMenupoint($data,$id,$field,$type)
        {
            $queryVars=clone($this->queryVars);
            $queryVars->data=$data;
			if ($id!==false):
            	$queryVars->clause=$field." ".$type." :ID";
            	$queryVars->preparedVars=array(':ID'=>$id);
			else:
				$queryVars->clause="1=1";
    		endif;
            parent::update($queryVars);
        }

		public function updateSubmenusByDelete($data,$id,$field,$type)
        {
            $queryVars=clone($this->queryVars);
			$queryVars->table="kta_submenus";
            $queryVars->data=$data;
			if ($id!==false):
            	$queryVars->clause=$field." ".$type." :ID";
            	$queryVars->preparedVars=array(':ID'=>$id);
			else:
				$queryVars->clause="1=1";
    		endif;
            parent::update($queryVars);
        }

        public function updateSubemenuByNewPoint($submenu)
        {
            $queryVars=clone($this->queryVars);
            $queryVars->data=array("lft"=>array
                        ("plainSQL"=>"lft + (IF(lft>".$submenu->lft.",2,0))"),
                        "rgt"=>array
                        ("plainSQL"=>"rgt + (IF(lft>=".$submenu->lft.",2,0))")
                    );
            $queryVars->table="kta_submenus";
            $queryVars->clause="lft >= :ID";
            $queryVars->preparedVars=array(':ID'=>$submenu->lft);
            parent::update($queryVars);


        }

        public function getMenuTypes($typeId)
        {
            $queryVars=clone($this->queryVars);
            $queryVars->table="kta_submenus";
            unset ($queryVars->countVar);
            $queryVars->clause="id = :ID";
            $queryVars->preparedVars=array(':ID'=> intval($typeId));
            parent::createQuery($queryVars);
        }

        public function deleteMenupoint($id)
        {
            $queryVars=clone($this->queryVars);
            $queryVars->table="kta_menupoints";
            $queryVars->clause="id= :ID";
            $queryVars->preparedVars=array(':ID'=>intval($id));

            // Menüpunkt löschen
            parent::deleteElement($queryVars) ;

        }
    }

?>