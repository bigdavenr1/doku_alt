<h1>Menü löschen - Bestätigung</h1><br/>
Wollen Sie den Menüeintrag <b>"<?php echo strip_tags(htmlspecialchars(stripslashes($this->_['menu']->name))) ?>" </b> wirklich löschen?
<form action="#" method="post">
    <?php echo $this->link->makeLink('<b>[ JA ]</b>',WEBDIR."admin/menuadmin.htm?submit=yes&amp;view=delete&amp;menuid=".$this->_['menu']->id,"adminlink");?>
    <?php echo $this->link->makeLink('<b>[ NEIN ]</b>',WEBDIR."admin/menuadmin.htm","adminlink");?>
</form>
