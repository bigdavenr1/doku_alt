<h1>Menu-Administration</h1><br/>
<?php echo $this->_['menus'];?>
<?php
global $icon_neu_small;
if(is_array($this->_['emptyMenus']))
    foreach($this->_['emptyMenus'] AS $value):
        echo "<div class=\"menutype\"><h2>".($value->description).'</h2><br class="clr"/>'.chr(10);
        echo '<div class="left" style="float:left;margin-right:20px;width:30%">'
             .$this->link->makeLink($icon_neu_small." neuen Menupunkt anlegen", WEBDIR."admin/menuadmin.htm?view=edit&amp;mode=newpoint&amp;menu=".$value->id)
             ."<br /></div></div>";
    endforeach;
