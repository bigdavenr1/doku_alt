<?php
namespace admin\modules\hilfeartadmin;
class Model extends \inc\Basicdb
{
    use ModulConfig;
    // Konstruktor
    ////////////////////////////////////////////////////////////////////////

    function __construct($anzahl = false)
    {
        global $sql;
        $this->anzahl = $anzahl;
        $this->queryVars=(object) array('table' => self::$table, 'countVar'=>$this->countvar, 'cps'=>15, 'offsetname'=>'usite');

        parent::__construct();
    }
	function getAllHilfearten($request=false)
    {



        $queryVars=clone $this->queryVars;
		$queryVars->fields=array(self::$table.".".self::$id,
		                          self::$table.".".self::$name
                                 );
        $queryVars->order=(object) array('field'=>$request['sort'],'direction'=>$request['rtg']);    
        
                                  
        parent::createQuery($queryVars);
		return $this->liste;
    }

	function getHilfeartById($id)
	{
	    $queryVars=clone $this->queryVars;
       
        $queryVars->fields=array( self::$table.".".self::$id,
         						  self::$table.".".self::$name);

        $queryVars->clause = self::$table.".".self::$id."= :hilfeartId";
        $queryVars->preparedVars=array(':hilfeartId'=>intval($id));
        unset($queryVars->cps);
        unset($queryVars->offsetname);

	    parent::createQuery($queryVars);
		return parent::getElement();


	    
	}

    public function updateOrNewHilfeart($request,$hilfeart=false)
	{
		$queryVars = clone $this->queryVars;
		$queryVars->data = array(self::$name=>$request['name']);


		if (is_object($hilfeart)):
			$queryVars->clause=self::$id."=:ID";
			$queryVars->preparedVars=array(':ID'=>$hilfeart->id);
			return parent::update($queryVars);
		else:
			parent::write($queryVars);
			return $this->lastid;
		endif;

	}
    
    public function deleteById($id)
    {
        $queryVars = clone $this->queryVars;
        $queryVars->clause="id = :ID";
        $queryVars->preparedVars=array(":ID"=>intval($id));
        return parent::deleteElement($queryVars);
    }

}

?>