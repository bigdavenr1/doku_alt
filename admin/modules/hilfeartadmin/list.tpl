<div class="products">
<div class="top-box">

    <h2>Hilfearten Liste</h2>
    <hr>
    <p class="desc">Übersicht über alle Hilfearten. Hilfearten können direkt bearbeitet werden</p>
    <br/><br/>
    <?php echo  $this->link->makeLink($icon_neu_small." Neuen Hilfearten einpflegen",WEBDIR."admin/hilfeartadmin.htm?view=edit","adminlink")?>
    <table style="width: 98%">
    <tr>
        <?php
          echo $this->func->tableHeadSort("Name","name");
          echo $this->func->tableHead("Admin","","","width:40px;");
        ?>
    </tr>
    <?php
    if (!empty($this->_['hilfearten'])):
        // Studiodaten abrufen
        foreach($this->_['hilfearten'] as $key => $hilfeart):
        // Ausgabe

            ?>
            <tr class="td<?php echo $x%2?>">
                
                <td>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($hilfeart->name)))?>
                </td>
                <td  style="text-align:center">

                    <?php echo  $this->link->makeLink('<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />',WEBDIR.'admin/hilfeartadmin.htm?view=delete&amp;id='.$hilfeart->id,"adminlink");?>
                    <?php echo  $this->link->makeLink('<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />',WEBDIR."admin/hilfeartadmin.htm?id=".$hilfeart->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$request['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
                </td>
            </tr>

         <?php
         endforeach;
         ?></table><br/><?php
         echo $this->_['htmlnavi'];
     else:
        ?> </table><br/>
        <?php echo $this->_['htmlnavi'];
         
     endif;
     ?>

<br/><br/>
<?php echo  $this->link->makeLink($icon_neu_small." Neuen hilfearten einpflegen",WEBDIR."admin/hilfeartadmin.htm?view=edit","adminlink")?>
</div>
</div>

