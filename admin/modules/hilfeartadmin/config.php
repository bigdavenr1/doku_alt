<?php
namespace admin\modules\hilfeartadmin;
trait ModulConfig
{
    /**
     * Name der SQL-Tabelle
     */
    protected static $table = "hilfeart";
    
    /**
     * Feld id
     */
    protected static $id = 'id';
    /**
     * Feld name
     */
    protected static $name = 'name';
        

    //protected static $queryVars =  array('table' => 'hilfeart');


}