<div id="artikel">
    <h1>Hilfeart löschen - Bestätigung</h1>
    Wollen Sie das Hilfeart   (<b>"<?php echo strip_tags(htmlspecialchars(stripslashes($this->_['hilfeart']->name))) ?>" </b>)
    wirklich löschen?
    <form action="#" method="post">
        <?php echo $this->link->makeLink('<b>[ JA ]</b>',WEBDIR."admin/hilfeartadmin.htm?confirmed=yes&amp;view=delete&amp;id=".$this->_['hilfeart']->id,"adminlink");?>
        <?php echo $this->link->makeLink('<b>[ NEIN ]</b>',WEBDIR."admin/hilfeartadmin.htm","adminlink"); ?>
    </form>
</div>


