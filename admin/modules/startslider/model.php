<?php
namespace admin\modules\startslider;
    class Model extends \inc\Basicdb
    {
        // Konstruktor
        ////////////////////////////////////////////////////////////////////////

        function __construct($anzahl = false)
        {
            global $sql;
            $this->anzahl = $anzahl;
            $this->table="kta_startslider";
            $this->countvar=$this->table.'.id';
            parent::__construct();
        }
        function getAllSliderPics($active=false)
        {
            $this->felder="*, BIN(active) active";
            parent::createQuery((!empty($active)?"WHERE BIN(active)=1":""));
        }
        function getSliderPicById($id)
        {
            parent::createQuery("WHERE id='".intval($this->con->quote($id))."'");
        }
    }

?>