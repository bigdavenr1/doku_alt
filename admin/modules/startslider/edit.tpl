<?php  $_SERVER['QUERY_STRING']=str_replace("&&", '&', str_replace("res=".$_GET['res'], '', str_replace("modul=".$_GET['modul'], '', $_SERVER['QUERY_STRING'])))?>
<h1>Startslider <?php echo empty($this->_['pic'])?'neu anlegen':'ändern';?></h1>
<b>Sie haben die Möglichkeit mit dem nachfolgenden Formularen die Startsliderbilder zu ändern.</b><br/><br/>
<div class="msg">
Bitte beachten Sie, dass die Bilder eine mindestbreite von 1920 Pixel und eine Mindesthöhe von 502 px aufweisen sollten. Dann sollte auch der Zuschnittsbereich nicht großartig verkleinert werden. Dies wären alles Qualitätsbremsen.
</div>
<form action="<?php echo WEBDIR?>admin/startslider.htm?<?php echo htmlentities(($_SERVER['QUERY_STRING'][strlen($_SERVER['QUERY_STRING'])-1]=='&'?substr($_SERVER['QUERY_STRING'],0,-1):$_SERVER['QUERY_STRING']))?>" method="post" enctype="multipart/form-data">
    <fieldset >
        <legend>Bildupload</legend>
        <label for="slider_val">Bilddatei</label><br/>
        <br class="clr"/>
        <div class="left" id="sliderbox">
        <?php 
        if (empty($this->_['pic'])):?>
            <a href="javascript:void(0)"  class="upload_slider once_slider_val">Neues Sliderbild hochladen</a> 
        <?php 
        else:
            $extension=file_exists(LOCALDIR."images/sliderimages/".$this->_['pic']->file.".png")?
                        'png'
                        :(file_exists(LOCALDIR."images/sliderimages/".$this->_['pic']->file.".gif")?
                            'gif'
                            :(file_exists(LOCALDIR."images/sliderimages/".$this->_['pic']->file.".jpg")?
                                'jpg'
                                :''));
                               
            if (!empty($extension)):?>
               <img style="width:95%" src="<?php echo WEBDIR?>images/sliderimages/<?php echo $this->_['pic']->file?>_thumb_big.<?php echo $extension?>"/>
            <?php    
            else:
                 echo "FEHLER: Dieses Bild ist nicht mehr vorhanden! Datenbankeintrag ".$this->_['link']->makeLink('löschen',WEBDIR.'admin/startslider.htm?view=delete&amp;picid='.$this->_['pic']->id,"adminlink")."?";
            endif;
        endif;?>  
        </div>
         <input type="hidden" id="slider_val" name="slider_val" value="<?php echo htmlspecialchars(strip_tags(stripslashes($this->_['pic']->file)))?>"/>   
    </fieldset>
    <br class="clr"/>
    <br/>
     <fieldset class="left">
        <legend>
            Sonstige Einstellungen
        </legend>
        
        <input type="checkbox" class="checkbox" value="1" id="active_val" name="active_val" <?php echo ord($this->_['pic']->active)?'checked="checked"':''?>/><label for="active_val">Startsliderbild aktiv</label><br class="clr"/>
    </fieldset>
    <br class="clr"/><br/>
    <input type="submit" name="submit" id="submit" value="Startslider <?php echo empty($this->_['pic'])?'neu anlegen':'ändern';?>" />
</form>
