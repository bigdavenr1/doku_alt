   <h1>Startslider verwalten</h1>
    <table style="width:100%" cellpadding="0" cellspacing="0" class="admintbl">
    <tr>
        <?php 
          echo $this->_['func']->tableHead("Bild","file","","width:150px;");
          echo $this->_['func']->tableHead("Link","description");
          echo $this->_['func']->tableHeadSort("Status","active","","","width:65px;");
          echo $this->_['func']->tableHead("Admin","","","width:40px;");
        ?>
    </tr>
    <?php
    if (!empty($this->_['pics'])):
        // Studiodaten abrufen
        foreach($this->_['pics'] as $key => $pic):
        // Ausgabe
            ?>
            <tr>
                <td>
                    <?php
                   $extension=file_exists(LOCALDIR."images/sliderimages/".$pic->file.".png")?
                        'png'
                        :(file_exists(LOCALDIR."images/sliderimages/".$pic->file.".gif")?
                            'gif'
                            :(file_exists(LOCALDIR."images/sliderimages/".$pic->file.".jpg")?
                                'jpg'
                                :''));
                               
                    if (!empty($extension)):?>
                        <a href="<?php echo WEBDIR?>images/sliderimages/<?php echo $pic->file.'.'.$extension?>" class="picture"><img src="<?php echo WEBDIR?>images/sliderimages/<?php echo $pic->file."_thumb_big.".$extension?>"/></a>
                    <?php endif;?>
                </td>
                <td>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($pic->description)))?>
                </td>
                <td>
                    <?php echo $pic->active==1?'<span class="msg">A</span>':'<span class="err">I</span>'?>
                </td>
                <td  style="text-align:center">
                    <?php echo  $this->_['link']->makeLink('<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />',WEBDIR.'admin/startslider.htm?view=delete&amp;picid='.$pic->id,"adminlink");?>
                    <?php echo  $this->_['link']->makeLink('<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />',WEBDIR."admin/startslider.htm?picid=".$pic->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&picid=".$request['picid'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
                </td>
            </tr>
         <?php
         endforeach;
     endif;
     ?></table><br/><?php
         echo $this->_['htmlnavi'];
     ?>
<br/><br/>
<?php echo  $this->_['link']->makeLink($icon_neu_small." Neues Sliderbild einpflegen",WEBDIR."admin/startslider.htm?view=edit","adminlink")?>
