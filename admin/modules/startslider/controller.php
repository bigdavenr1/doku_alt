<?php // Modulview und Modulmodel einbinden
namespace admin\modules\startslider;
require_once("model.php");
class Controller
{
    // private VAriablen setzen
    private $request = null;
    private $modultemplate = 'startslider';
    private $modulview = null;
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->modulview = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->modultemplate =isset($this->request['view'])?$this->request['view']:'startslider';
    }
    public function display()
    {
        $this->request['picid']=empty($this->request['picid'])?0:$this->request['picid'];
         // modultemplate setzen
        $this->assignvalues["keywords"] = "jhgklJ";
        // Codes übergeben
        $this->modulview->assign("link",$this->l);
        $this->modulview->assign('func', $this->func);
         // Template laden, füllen und ausgeben
        // Benutzerdaten aus Datenbank holen
        $this->sliderfac=new Model(25);
        switch($this->request['picid']):
            case '0':
                if ($this->modultemplate=='edit')
                    $this->editOrDelete($this->request['picid'],'new');
                $this->sliderfac->getAllSliderPics();
                $this->modulview->assign("htmlnavi",$this->sliderfac->getHtmlNavi());
                $this->modulview->assign("pics",$this->sliderfac->liste);
                break;
            default:
                $this->modultemplate =isset($this->request['view'])?$this->request['view']:'edit';
                $this->sliderfac->getSliderPicById(intval($this->request['picid']));
                // Benutzerdaten Global zu verfügung stellen
                $this->modulview->assign("pic",$this->pic=$this->sliderfac->getElement());
                // Prüfung ob gelöscht oder bearbeitet werden soll.
                $this->editOrDelete($this->slider->id); 
                break;  
        endswitch;
        $this->modulview->setTemplate($this->modultemplate,dirname(__FILE__));
        return $this->modulview->loadTemplate();
    }
    private function changeData($modus)
    {
        $dataarray=array('file'=>$this->request['slider_val'],
                  'description'=>$this->request['description'],
                       'active'=>$this->request['active_val']);
        if (empty($modus)):
            
            // Studiodaten in Datenbank speichern
            $this->sliderfac->update($dataarray,'id',$this->pic->id);
        else:
            // Datensatz schreibene
            $this->sliderfac->write($dataarray);
            $picture=$this->func->checkPictureAndGetInformations('temporary_files/',$this->request['slider_val']); 
            if ($picture->error==0):
                rename(LOCALDIR."temporary_files/".$this->request['slider_val'].".".$picture->extension, LOCALDIR."images/sliderimages/".$this->request['slider_val'].".".$picture->extension);
                rename(LOCALDIR."temporary_files/".$this->request['slider_val']."_thumb.".$picture->extension, LOCALDIR."images/sliderimages/".$this->request['slider_val']."_thumb.".$picture->extension);
                rename(LOCALDIR."temporary_files/".$this->request['slider_val']."_thumb_big.".$picture->extension, LOCALDIR."images/sliderimages/".$this->request['slider_val']."_thumb_big.".$picture->extension);
            endif;
        endif;
        $_SESSION['msg']="Sliderbild erfolgreich angelegt!";        
    }
    private function editOrDelete($id,$modus=false)
    {
        if ($this->modultemplate=='edit' && !empty($this->request['submit'])):
            $this->changeData($modus);
            $_SESSION['msg']='Sliderbild erfolgreich'.(empty($modus)?'geändert':'angelegt');
            header('Location:'.WEBDIR.'admin/startslider.htm');
        elseif ($this->modultemplate=='delete' && !empty($this->request['confirmed'])):
            $this->sliderfac->deleteElement('id',$this->request['picid']);
            $_SESSION['msg']='Sliderbild erfolgreich gelöscht!';
            header('Location:'.WEBDIR.'admin/startslider.htm');
        endif;
    }
}
?>