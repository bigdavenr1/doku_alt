<h1>Startsliderbild löschen - Bestätigung</h1><br/>
Wollen Sie das Bild<br/>
<?php
$extension=file_exists(LOCALDIR."images/sliderimages/".$this->_['pic']->file.".png")?
                        'png'
                        :(file_exists(LOCALDIR."images/sliderimages/".$this->_['pic']->file.".gif")?
                            'gif'
                            :(file_exists(LOCALDIR."images/sliderimages/".$this->_['pic']->file.".jpg")?
                                'jpg'
                                :''));

            if (!empty($extension)):?>
               <img  src="<?php echo WEBDIR?>images/sliderimages/<?php echo $this->_['pic']->file?>_thumb.<?php echo $extension?>"/> <br/>wirklich löschen?
               <?php endif;?>
<form action="#" method="post">
    <?php echo $this->_['link']->makeLink('<b>[ JA ]</b>',WEBDIR."admin/startslider.htm?confirmed=yes&amp;view=delete&amp;picid=".$this->request['picid'],"adminlink");?>
    <?php echo $this->_['link']->makeLink('<b>[ NEIN ]</b>',WEBDIR."admin/startslider.htm","adminlink");?>
</form>