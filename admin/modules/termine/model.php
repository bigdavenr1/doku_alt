<?php
namespace admin\modules\termine;
class Model extends \inc\Basicdb
{
    use ModulConfig;
    // Konstruktor
    ////////////////////////////////////////////////////////////////////////

    function __construct($anzahl = false)
    {
        global $sql;
        $this->anzahl = $anzahl;
        $this->queryVars=(object) array('table' => self::$table['t']);

        parent::__construct();
    }
	function getAllTermine($request=false)
    {
        $day=date('N');
        $von=date('Y-m-d',mktime(0,0,0,date('m'),date('d')-($day-1)+($_GET['week']*7),date('Y')));
        $bis=date('Y-m-d',mktime(0,0,0,date('m'),date('d')+(7-$day)+($_GET['week']*7),date('Y')));
        $queryVars=clone $this->queryVars;
		$queryVars->fields=array(
		                          self::$table['t'].".".self::$id,
		                          self::$table['t'].".".self::$titel,
		                          self::$table['t'].".".self::$beschreibung,
		                          'HOUR('.self::$table['t'].".".self::$beginn.') hb',
		                          'MINUTE('.self::$table['t'].".".self::$beginn.') mb',
		                          'DAYNAME('.self::$table['t'].".".self::$beginn.') db',
		                          "DATE_FORMAT(".self::$table['t'].".".self::$beginn.",GET_FORMAT(DATE,'EUR')) beginn",
		                          'HOUR('.self::$table['t'].".".self::$ende.') he',
		                          'MINUTE('.self::$table['t'].".".self::$ende.') me',
		                          'DAYNAME('.self::$table['t'].".".self::$ende.') de',
		                          "DATE_FORMAT(".self::$table['t'].".".self::$ende.",GET_FORMAT(DATE,'EUR')) ende",
		                          self::$table['k'].".".self::$klientVorname,
		                          self::$table['k'].".".self::$klientName
                                 );
         $queryVars->joins=array(
                                (object)
                                array('type'   => 'LEFT',
                                      'table'  => self::$table['k'],
                                      'clause' => self::$table['t'].'.'.self::$klient.' = '.self::$table['k'].'.'.self::$id

                                ));

        $queryVars->order = (object) array('field' => self::$table['t'].".".self::$beginn, 'direction' => 'ASC');
        $queryVars->clause="(terminbeginn >= '$von 00:00:00' AND terminbeginn < '$bis 00:00:00') OR (terminende <='$bis 00:00:00' AND terminende > '$von 00:00:00') OR (terminbeginn < '$von 00:00:00' AND terminende > '$bis 00:00:00')";
        parent::createQuery($queryVars);
		return($this->liste);
    }

	function getTerminById($id)
	{
	    $queryVars=clone $this->queryVars;

        $queryVars->fields=array(
                                  self::$table.".".self::$id,
                                  self::$table.".".self::$name,
                                  self::$table.".".self::$abteilung,
                                  self::$table.".".self::$stadt
                                      );

        $queryVars->clause = self::$table.".".self::$id."= :asdId";
        $queryVars->preparedVars=array(':asdId'=>intval($id));
        unset($queryVars->cps);
        unset($queryVars->offsetname);

	    parent::createQuery($queryVars);
		return parent::getElement();



	}

    public function updateOrNewAsd($request,$asd=false)
	{

		$queryVars = clone $this->queryVars;
		$queryVars->data = array(self::$stadt  => $request['stadt'],
	                          self::$abteilung => $request['abteilung'],
	                          self::$name      => $request['name']);


		if (is_object($asd)):
			$queryVars->clause=self::$id."=:ID";
			$queryVars->preparedVars=array(':ID'=>$asd->id);
			return parent::update($queryVars);
		else:
			parent::write($queryVars);
			return $this->lastid;
		endif;

	}

    public function deleteById($id)
    {
        $queryVars = clone $this->queryVars;
        $queryVars->clause = "id = :ID";
        $queryVars->preparedVars = [":ID" => intval($id)];
        return parent::deleteElement($queryVars);
    }

}

?>