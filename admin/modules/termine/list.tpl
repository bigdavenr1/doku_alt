<div class="products">
<div class="top-box">

    <h2>Termine</h2>
    <hr>
    <p class="desc">Übersicht über aller Termine. </p>
    <br/><br/>


    <?php
       $day=date('N');
       $von=date('d.m.Y',mktime(0,0,0,date('m'),date('d')-($day-1)+($_GET['week']*7),date('Y')));
       $bis=date('d.m.Y',mktime(0,0,0,date('m'),date('d')+(7-$day)+($_GET['week']*7),date('Y')));
    ?>


    <h5><a href="?week=<?php echo $_GET['week']-1?>"><<<</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Woche vom <span id="von"><?php echo $von?></span> bis <span id="bis"><?php echo $bis?></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="?week=<?php echo $_GET['week']+1?>">>>></a></h5><br/>

    <table style="width: 98%">
    <tr>
        <?php
          echo $this->func->tableHead("Beginn");
          echo $this->func->tableHead("Ende");
          echo $this->func->tableHead("Klient");
          echo $this->func->tableHead("Titel/Beschreibung");
        ?>
    </tr>
    <?php

    if (!empty($this->_['termine'])):
        // Studiodaten abrufen
        foreach($this->_['termine'] as $key => $asd):
        // Ausgabe

            ?>
            <tr class="td<?php echo $x%2?>">
                <td>
                    <i style="color:rgb(150,150,150)"><?php echo strip_tags(htmlspecialchars(stripslashes($asd->beginn)))?></i><br/>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($asd->db)))?>
                    <br/>
                    <?php echo $asd->hb<10?'0':''?><?php echo strip_tags(htmlspecialchars(stripslashes($asd->hb)))?>:<?php echo $asd->mb<10?'0':''?><?php echo strip_tags(htmlspecialchars(stripslashes($asd->mb)))?> Uhr
                </td>
                <td>
                <i style="color:rgb(150,150,150)"><small><?php echo strip_tags(htmlspecialchars(stripslashes($asd->ende)))?></small><br/></i>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($asd->de)))?>
                    <br/>
                    <?php echo $asd->he<10?'0':''?><?php echo strip_tags(htmlspecialchars(stripslashes($asd->he)))?>:<?php echo $asd->me<10?'0':''?><?php echo strip_tags(htmlspecialchars(stripslashes($asd->me)))?> Uhr
                </td>
                <td>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($asd->surname)))?>, <?php echo strip_tags(htmlspecialchars(stripslashes($asd->given_name)))?>

                </td>
                <td>
                   <b><?php echo strip_tags(htmlspecialchars(stripslashes($asd->titel)))?></b><br/><br/>
                   <hr/>
                    <br/>
                    <span style="color:rgb(100,100,100)"> <?php echo nl2br(strip_tags(htmlspecialchars(stripslashes($asd->beschreibung))))?></span>
                </td>


            </tr>

         <?php
         endforeach;
         ?></table><br/><?php
         echo $this->_['htmlnavi'];
     else:
        ?> </table><br/>
        <?php echo $this->_['htmlnavi'];

     endif;
     ?>


</div>
</div>