﻿
function initUI() {
        //init list
        var domEl = WinJS.Utilities.id("listView")[0];
        var list = new WinJS.UI.ListView(domEl);
        list.itemTemplate = WinJS.Utilities.id("calendarSelectTemplate")[0];

        var filterMask = {};
        var categories = null;

        //load items
        getCategories(function (data) {

            categories = data.map(function (item) {
                return { key: item.id, label: item.label, checked: false }
            });
            categories[0].checked = true;
            categories.forEach(function (c) {
                filterMask[c.key] = c.checked;//scheduler will show events only from selected categories
            });
            
            updateDetailsForm();//fill "Category" combo of details form with options

            list.itemDataSource = new WinJS.Binding.List(categories).dataSource;
            loadDataFor(categories[0].key);//load data for first(initially selected) category
        });

        function updateDetailsForm() {//fill "Category" combo of details form with options

            var selectedCategories = categories.filter(function (c) {
                return !!c.checked;
            });

            scheduler.updateCollection("categories", selectedCategories);
            scheduler.config.readonly = (selectedCategories.length === 0);
        }
      
        list.oniteminvoked= function (e) {
            
            var check = WinJS.Utilities.children(e.target).query("input")[0];
            check.checked = !check.checked;

            categories[list.indexOfElement(check)].checked = filterMask[check.value] = check.checked;
            
            scheduler.render_view_data();
            updateDetailsForm();

            if (check.checked)
                loadDataFor(check.value);

            e.preventDefault();

        };
        
      
        scheduler.filter_day = scheduler.filter_week = scheduler.filter_month = scheduler.filter_year = function (id, ev) {
            if (!ev.category_id)
                return true;
            return !!(filterMask[ev.category_id]);
        }
        
        function loadDataFor(name) {
            if (!scheduler.loadedCategories)
                scheduler.loadedCategories = {};
            if (scheduler.loadedCategories[name])
                return;

            if (!scheduler.loadedCategories[name]) {
                scheduler.loadedCategories[name] = true;
                getEvents(name, function (data) {//load data
                    scheduler.parse(data, "json");
                });
            }
        }

 


    (function initScheduler() {
        scheduler.config.multi_day = true;
        scheduler.config.details_on_create = true;
        scheduler.config.details_on_dblclick = true;
        scheduler.config.first_hour = 6;
        scheduler.init('scheduler_here', new Date(2012, 11, 1), "month");
        
        scheduler.attachEvent("onConfirmedBeforeEventDelete", function (id) {
            deleteEvent(id);
            return true;
        });
        scheduler.attachEvent("onEventChanged", function (id) {
            updateEvent(copyEvent(scheduler.getEvent(id)));
        });

        scheduler.attachEvent("onEventAdded", function (id) {
            var ev = copyEvent(scheduler.getEvent(id));
            delete ev.id;

            if (!ev.category_id)
                ev.category_id = scheduler.serverList("categories")[0].key;

            insertEvent(ev, function (newId) {          
                scheduler.changeEventId(id, newId);
            });
            return true;
        });

        scheduler.templates.event_class = function (start, end, ev) {
            return "event_" + ev.category_id;
        }

        scheduler.config.lightbox.sections = [
			{ name: "description", height: 200, map_to: "text", type: "textarea", focus: true },
            { name: "type", map_to: "category_id", type: "select", options: scheduler.serverList("categories") },
			{ name: "time", height: 72, type: "time", map_to: "auto" }
        ];
        scheduler.locale.labels.section_type = "Calendar";

    })()
}