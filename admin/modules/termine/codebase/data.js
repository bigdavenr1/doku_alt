﻿
    dateToStr = scheduler.date.date_to_str(scheduler.config.xml_date);

    function copyEvent(ev) {
        var res = {
            id:ev.id,
            text : ev.text,
            category_id : ev.category_id,
            start_date : dateToStr(ev.start_date),
            end_date : dateToStr(ev.end_date)
        };
        return res;
    }


    function deleteEvent(id) {
        getStore({
            store: "events",
            mode: "readwrite",
            success: function (store) {
                store['delete'](id);
            }
        });
        return true;
    }
    function updateEvent(hash) {
        getStore({
            store: "events",
            mode: "readwrite",
            success: function (store) {
                store.put(hash);
            }
        });

    }
    function insertEvent(hash, callback) {
        getStore({
            store: "events",
            mode: "readwrite",
            success: function (store) {
                var updated = store.add(hash);
                updated.onsuccess = function (res) {
                    callback(res.target.result);
                }
            }
        });
    }
    function getCategories(callback) {
        getStore({
            store: "categories",
            mode: "readonly",
            success: function (store) {
                var data = [];
                var cursor = store.openCursor();
                cursor.onsuccess = function (e) {
                    if (e.target.result) {
                        data.push(e.target.result.value);
                        e.target.result.continue();
                    } else {
                        callback(data);
                    }
                };
            }
        });
    }
    function getEvents(category, callback) {
        getStore({
            store: "events",
            mode: "readonly",
            success: function (store) {
                var data = [];
                var cursor = store.index("category_id").openCursor(category * 1);
                cursor.onsuccess = function (e) {
                    if (e.target.result) {
                        data.push(e.target.result.value);
                        e.target.result.continue();
                    } else {
                        callback(data);
                    }
                };

            }
        });
    }

    function getStore(config) {
        connect(function (db) {
            if (!config.store) config.store = "events";
            if(config.success)
                config.success(db.transaction(config.store, config.mode).objectStore(config.store));
        });
    }


    function connect(callback){

        try{
            var db = null;
            var dbName = "SchedulerApp";
            var version = 8;

            var req = window.indexedDB.open(dbName, version);
            req.onsuccess = function (ev) {
                db = ev.target.result;
                if(callback)
                    callback(db);
            }
            req.onupgradeneeded = function(e){
                return prepareDatabase(e, e.target.transaction);
            }
        }catch(e){
        }
    }

    function prepareDatabase(ev, transaction) {
        var storeItems = [
                { text: "MoDevEast", category_id: 1, start_date: "11.29.2012 14:00", end_date: "12.01.2012 17:00" },
                { text: "WebCongress", category_id: 1, start_date: "11.29.2012 16:00", end_date: "11.29.2012 20:00" },
                { text: "EnyoJS", category_id: 1, start_date: "12.01.2012 10:00", end_date: "12.01.2012 13:00" },
                { text: "Dallas Digital Summit 2012", category_id: 1, start_date: "12.04.2012 14:00", end_date: "12.04.2012 17:00" },

                { text: "The Rich Web Experience 2012", category_id: 2, start_date: "11.27.2012 8:00", end_date: "11.30.2012 17:00" },
                { text: "We Actually Build Stuff", category_id: 2, start_date: "11.30.2012 18:00", end_date: "11.30.2012 19:00" },
                { text: "Front In Londrina 2012", category_id: 2, start_date: "12.02.2012 8:00", end_date: "12.02.2012 14:00" },
                { text: "Frontenders DK Meetup Dec.", category_id: 2, start_date: "12.06.2012 14:00", end_date: "12.06.2012 17:00" },

                { text: "International Student Arrival and Orientation Institute", category_id: 3, start_date: "11.28.2012 00:00", end_date: "11.28.2012 17:00" },
                { text: "Study Abroad Assessment", category_id: 3, start_date: "12.03.2012 10:00", end_date: "12.03.2012 14:00" },
                { text: "Academic Advising in a Virtual World", category_id: 3, start_date: "12.10.2012 16:00", end_date: "12.10.2012 17:00" },
                { text: "Developing Institutional Naming Policies", category_id: 3, start_date: "12.13.2012 16:00", end_date: "12.13.2012 17:00" },
                { text: "Alumni Career Services: Developing an Online Programming Series", category_id: 3, start_date: "12.13.2012 16:00", end_date: "12.13.2012 17:00" },

                { text: "HelsinkiJS", category_id: 4, start_date: "12.04.2012 16:00", end_date: "12.04.2012 17:00" },
                { text: "London Lua", category_id: 4, start_date: "12.06.2012 10:00", end_date: "12.06.2012 12:00" },
                { text: "In The Brain of Greg Young", category_id: 4, start_date: "12.11.2012 15:00", end_date: "12.11.2012 16:00" },
                { text: "[Async]: Parslow and Elliott's Test Emporium", category_id: 4, start_date: "12.13.2012 14:00", end_date: "12.13.2012 17:00" }
            ]
        
        var categories = [
            "Mobile",
            "Web Development",
            "Higher Education",
            "JavaScript"
        ];

        var db = ev.target.result;

        if (!db.objectStoreNames.contains("categories")) {
            var cats = db.createObjectStore("categories", { keyPath: "id", autoIncrement: true });
            for (var i = 0; i < categories.length; i++)
                cats.add({ label: categories[i] });
        }
        
        if (!db.objectStoreNames.contains("events")) {
            var events = db.createObjectStore("events", { keyPath: "id", autoIncrement: true });
            events.createIndex("category_id", "category_id", {unique:false, multientry:false});

            for (var i = 0; i < storeItems.length; i++) {
                events.add(storeItems[i]);
            }
        }

        

    }