<div class="products">
<div class="top-box">
                <h3>Termine</h3>
                <hr>
                <p class="desc">Übersicht über alle Termine von <?php echo $this->_['termine']->klient_id?>. Termine können direkt bearbeitet werden</p>

               <br/>

                <?php echo  $this->link->makeLink($icon_neu_small." Neuen Termin einpflegen",WEBDIR."admin/testklintenadmin.htm?view=terminedit","adminlink")?>
                <br/>
                <table style="width:98%">
                <tr>
                    <?php
                      echo $this->func->tableHeadSort("Titel","titel","","","width:18%;");

                      echo $this->func->tableHead("Beschreibung","beschreibung","","width:50%");
                      echo $this->func->tableHeadSort("Terminbeginn","terminbeginn");
                      echo $this->func->tableHeadSort("Terminende","terminende");
                      echo $this->func->tableHead("Admin","");
                    ?>
                </tr>
                <?php
                if (!empty($this->_['termine'])):
                    // Studiodaten abrufen
                    foreach($this->_['termine'] as $key => $termin):
                    // Ausgabe
                        if(!empty($termin->titel)):
                        ?>
                        <tr class="td<?php echo $x%2?>">
                            
                            <td>
                                <?php echo $this->func->sanitize($termin->titel?></a>
                            </td>
                            <td>
                                <div style="max-height:100px;overflow:auto;">
                                    <?php echo nl2br($this->func->sanitize($termin->beschreibung))?>
                                </div>
                            </td>
                            <td>
                                <?php echo $this->func->sanitize($termin->terminbeginn)?>
                            </td>
                            <td>
                                <?php echo $this->func->sanitize($termin->terminende)?>
                            </td>

                            <td>
                                <?php echo  $this->link->makeLink('<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />',WEBDIR.'admin/testklientenadmin.htm?view=termindelete&amp;id='.$termin->id,"adminlink");?>
                                <?php echo  $this->link->makeLink('<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />',WEBDIR."admin/testklientenadmin.htm?view=terminedit&amp;id=".$termin->id,"adminlink");?>
                            </td>
                        </tr>

                     <?php
                        endif;
                     endforeach;
                     ?></table><br/><?php
                     echo $this->_['htmlnavi'];
                 else:
                    ?> </table><br/>
                    <?php echo $this->_['htmlnavi'];

                 endif;
                 ?>

            <br/><br/>
            <?php echo  $this->link->makeLink($icon_neu_small." Neuen Notiz einpflegen",WEBDIR."admin/testklientenadmin.htm?view=terminedit","adminlink")?>
 </div>
                </div>
              
            
