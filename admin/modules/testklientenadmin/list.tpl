<div class="products">
<div class="top-box">

    <h2>Klienten Liste</h2>
    <hr>
    <p class="desc">Übersicht über alle Klienten. Klienten können direkt bearbeitet werden</p>
    <br/><br/>
    <?php echo  $this->link->makeLink($icon_neu_small." Neuen Klienten einpflegen",WEBDIR."admin/testklientenadmin.htm?view=edit","adminlink")?>
    <table style="width: 98%">
    <tr>
        <?php
          echo $this->func->tableHeadSort("AZ","az","","","width:40px;");
          echo $this->func->tableHeadSort("Name","surname");

          echo $this->func->tableHeadSort("Geb.Datum","geb");
          echo $this->func->tableHeadSort("ASD","asd_name");

          echo $this->func->tableHeadSort("akt. Hilfeart","hilfeart_name");
          echo $this->func->tableHeadSort("Datum ab","datum_ab");
          echo $this->func->tableHeadSort("Datum bis","datum_bis");

          echo $this->func->tableHead("Admin","","","width:40px;");
        ?>
    </tr>
    <?php
    if (!empty($this->_['klienten'])):
        // Studiodaten abrufen
        foreach($this->_['klienten'] as $key => $klient):
        // Ausgabe



            ?>
            <tr class="td<?php echo $x%2?>">
                <td>
                    <?php echo $this->func->sanitize($klient->az)?>
                </td>
                <td>
                    <?php echo $this->func->sanitize($klient->surname.", ".$klient->given_name )?>
                </td>

                <td>
                    <?php echo $this->func->sanitize($klient->geb)?>
                </td>
                <td>
                    <?php echo (!empty($klient->asd_name)) ? $this->func->sanitize($klient->stadt).", ".$this->func->sanitize($klient->abteilung)."<br />" .$this->func->sanitize($klient->asd_name) : "--"?>
                </td>

                <td>
                    <?php echo (!empty($klient->hilfeart_name)) ? $this->func->sanitize($klient->hilfeart_name) : "--"?>
                </td>

                <td>
                    <?php echo (!empty($klient->datum_ab)) ? $this->func->sanitize($klient->datum_ab) : "--"?>
                </td>
                <td>
                    <?php echo (!empty($klient->datum_bis)) ? $this->func->sanitize($klient->datum_bis) : "--"?>
                </td>

                <td  style="text-align:center">

                    <?php echo  $this->link->makeLink('<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />',WEBDIR.'admin/testklientenadmin.htm?view=delete&amp;id='.$klient->id,"adminlink");?>
                    <?php echo  $this->link->makeLink('<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />',WEBDIR."admin/testklientenadmin.htm?id=".$klient->id,"adminlink");?>
                </td>
            </tr>

         <?php
         endforeach;
         ?></table><br/><?php
         echo $this->_['htmlnavi'];
     else:
        ?> </table><br/>
        <?php echo $this->_['htmlnavi'];

     endif;
     ?>

<br/><br/>
<?php echo  $this->link->makeLink($icon_neu_small." Neuen Klienten einpflegen",WEBDIR."admin/testklientenadmin.htm?view=edit","adminlink")?>
</div>
</div>