<div class="products">
<div class="top-box">
    <h3>Termine <?php echo empty($this->_['termin'])?>'neu anlegen':'ändern';?> von <?php echo $this->_['klient']->id?></h3>
    <hr>
    <p class="desc">Sie haben die Möglichkeit mit dem nachfolgenden Formularen die Termine zu ändern.<br/>* markierte Felder sind Pflichtfelder!</p>
    
    <div class="contact-form">
    <br/><br/>
    <form action="" method="post" style="padding-right:0px;margin-right:0px;">
    <fieldset>
        <label for="titel">Titel</label>
        <input type="text" placeholder="neuer Titel" id="titel" name="titel" class="<?php echo isset($this->_['valErrors']['titel'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['titel'])?$request['titel']:$this->_['termin']->titel); ?>"/><br/>

       <label>Beschreibung :</label>
    
        <textarea placeholder="Beschreibung" id="beschreibung" name="beschreibung" class="<?php echo isset($this->_['valErrors']['beschreibung'])?'validate_error':''?> validate_required" /><?php echo $this->func->sanitize(!empty($request['beschreibung'])?$request['beschreibung']:$this->_['termin']->beschreibung); ?></textarea>
        
        <label for="terminbeginn"> Terminbeginn : </label>
        <input type="text" placeholder="neuer Terminbeginn" id="terminbeginn" name="terminbeginn" class="<?php echo isset($this->_['valErrors']['terminbeginn'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['terminbeginn'])?$request['terminbeginn']:$this->_['termin'][0]->terminbeginn); ?>"/><br/>
        
        <label for=">terminende"> Terminende : </label>
        <input type="text" placeholder="neues Terminende" id="terminende" name="terminende" class="<?php echo isset($this->_['valErrors']['terminende'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['terminende'])?$request['terminende']:$this->_['termin'][0]->terminende); ?>"/><br/>                        
        
        <br/>
        <br class="clr"/><br/><br/>
    </fieldset>
    
    
    <br class="clear" />
    <br class="clr" style="clear:both;float:none;"/>
    
    <input type="submit" class="btn btn-primary btn1" name="submit" value="Daten speichern"/>
    
    </fieldset>
    
    </form>
    </div>
</div>
</div>