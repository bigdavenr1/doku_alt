<?php // view und Modulmodel einbinden
namespace admin\modules\testklientenadmin;
require_once("model.php");
class Controller
{
    // private VAriablen setzen
    private $request = null;
    private $template = 'list';
    private $view = null;
    /**
	 * Konstruktur
	 * bereitet Template und übergebene Entscheidungsvariablen auf
	 * @param Array $request Array mit übergebenen Variablen (GET POST FILE)
	 */
    public function __construct($request)
    {
        // neuen viewer instanziieren
        $this->view = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // template auslesen
        $this->template =isset($this->request['view'])?$this->request['view']:'list';
		// Modelobject
		$this->model = new Model();
    }
	public function display()
    {
    	// prüfen und aufbereiten ob User ID übergeben wurde
    	$this->request['id']=empty($this->request['id'])?0:$this->request['id'];
		// Anhand übergebener ID entscheiden
		switch($this->request['id']):
			// Wenn keine USerid übergeben wurde  (list oder new)
			case '0':

				// Wenn neuer DAtensatz
				if ($this->template=='edit'):
					$this->editOrDelete($id,'new');

                elseif($this->template=="kha_add" || $this->template=="kha_update"):

                   $this->model->inserOrUpdatetHilfeartAsd($this->request,$this->request['obj'],$this->template);
                   // wenn neues aktuell und Historie sind mit Ajax Query
                   if (!empty($_SESSION['err'])):
                       header("HTTP/1.0 400 Bad Request");
                       echo $_SESSION['err'];
                       unset($_SESSION['err']);
                       exit;
                   endif;
                   $this->view->assign("klient",$this->klient = $this->model->getClientById(intval($this->request['klient_id']), 'kha'));
                    $this->template="kha_add";
                //////
                elseif($this->template=="notiz_add" ||  $this->template=="notiz_update"):


                   $this->model->insertOrUpdateNotiz($this->request,$this->request['obj'],$this->template);

                   if (!empty($_SESSION['err'])):
                       header("HTTP/1.0 400 Bad Request");
                       echo $_SESSION['err'];
                       unset($_SESSION['err']);
                       exit;
                   endif;
                   $this->view->assign("klient",$this->klient = $this->model->getClientById(intval($this->request['klient_id']),'notiz'));
                   $this->template="notiz_update";
                //////
                elseif($this->template=="termin_add" ||  $this->template=="termin_update"):


                   $this->model->insertOrUpdateTermin($this->request,$this->request['obj'],$this->template);
                   
                   if (!empty($_SESSION['err'])):
                       header("HTTP/1.0 400 Bad Request");
                       echo $_SESSION['err'];
                       unset($_SESSION['err']);
                       exit;
                   endif;
                   $this->view->assign("klient",$this->klient = $this->model->getClientById(intval($this->request['klient_id']),'termin'));
                   $this->template="termin_add";
                ///////////
                //Liste der Termine des Klientes
                elseif($this->template=="terminList"):
                   
                   $this->view->assign("termine", $this->model->getAllTermineByKlient($this->request['id']));
                   $this->template="termine";
                //Ende der Liste der Termine des Klientes
                //Termine des Klientes hinzufügen oder updaten
                elseif($this->template=="terminedit"):


                   $this->model->insertOrUpdateTermin($this->request);

                   
                   $this->view->assign("termine", $this->model->getAllTermineByKlient($this->request['klient_id']));
                   $this->template="termine";
                //Ende der Liste der Termine des Klientes
				// Wenn Liste
				else:

					$this->view->assign("klienten",$this->model->getAllClientsMitAsdUndHilfearten($this->request));
                	$this->view->assign("htmlnavi",$this->model->getHtmlNavi());

				endif;
				break;
			// Wenn ID übergeben wurde (edit oder delete)
			default:
                
				// Template auf edit, wenn nicht gelöscht werden soll
				$this->template =isset($this->request['view'])?$this->request['view']:'edit';
                // Benutzerdaten Global zu verfügung stellen
				$this->view->assign("klient",$this->klient = $this->model->getClientById(intval($this->request['id'])));
				// Prüfung ob gelöscht oder bearbeitet werden soll.
				$this->editOrDelete($this->klient[0]->id);
				break;
		endswitch;


        $this->view->assign('hilfearten', $this->model->getAll('hilfeart',false,false));
        $this->view->assign('asds', $this->model->getAll('asd',false,false));
        $this->view->assign('users', $this->model->getAll('kta_users',false,false));
        $this->view->setTemplate($this->template,dirname(__FILE__));
        return $this->view->loadTemplate();
	}

	private function changeData($modus)
	{

        return $this->model->updateOrNewClient($this->request,$this->klient);
	}

	private function editOrDelete($id,$modus=false)
	{
		if ($this->template=='edit' && !empty($this->request['submit'])):
			$id = $this->changeData($modus);
			$_SESSION['msg']='Klienterfolgreich'.(empty($modus)?'geändert':'angelegt');
			if ( empty($modus))
				header('Location:'.WEBDIR.'admin/klientenadmin.htm');
			else
				header('Location:'.WEBDIR.'admin/klientenadmin.htm?id='.$id);
		    exit;
        elseif ($this->template=='delete' && !empty($this->request['confirmed'])):
        	$this->model->deleteById($id);
            if ($this->user->usertype!=1):
			     $_SESSION['msg']='User erfolgreich gelöscht!';
			     header('Location:'.WEBDIR.'admin/klientenadmin.htm');
				 exit;
            endif;
		endif;
	}


}
?>