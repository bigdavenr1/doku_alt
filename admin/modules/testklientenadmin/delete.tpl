<div id="artikel">
    <h1>User löschen - Bestätigung</h1>
    Wollen Sie den Klienten <?php echo strip_tags(htmlspecialchars(stripslashes($this->_['klient'][0]->surname))) ?> <b><?php echo strip_tags(htmlspecialchars(stripslashes($this->_['klient'][0]->given_name))) ?> <?php echo strip_tags(htmlspecialchars(stripslashes($this->_['user']->given_name))) ?> </b>
    wirklich löschen?
    <form action="#" method="post">
        <?php echo $this->link->makeLink('<b>[ JA ]</b>',WEBDIR."admin/klientenadmin.htm?confirmed=yes&amp;view=delete&amp;id=".$this->_['klient'][0]->id,"adminlink");?>
        <?php echo $this->link->makeLink('<b>[ NEIN ]</b>',WEBDIR."admin/klientenadmin.htm","adminlink"); ?>
    </form>
</div>


