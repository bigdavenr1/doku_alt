 <?php

            $tempn=array();
            $temph=array();
            foreach($this->_['klient'] as $key => $notiz):
                if(!in_array($notiz->id."--".$notiz->user_id."--".$notiz->datum,$tempn)):
                    $tempn[] = $notiz->id."--".$notiz->user_id."--".$notiz->datum;
                    $notizstr .="<ul id='$notiz->id--$notiz->datum--$notiz->user_id--$notiz->notiz'>
                                    <li>
                                        <span>". $this->func->sanitize($notiz->usurname.", ".$notiz->ugiven_name) ."<br />
                                        ". $this->func->sanitize($notiz->datum) ."</span>
                                    </li>
                                    <li>
                                        <p>". nl2br($this->func->sanitize($notiz->notiz)) ."
                                    </li>
                                    <div class='clear'> </div>
                                </ul>";

                endif;
                if(!in_array($notiz->datum_ab."--".$notiz->datum_bis,$temph) && $notiz->datum_ab."--".$notiz->datum_bis != $notiz->a_datum_ab."--".$notiz->a_datum_bis):
                    $temph[] = $notiz->datum_ab."--".$notiz->datum_bis;
                    $historiestr .= "- von : <time>".$this->func->sanitize($notiz->datum_ab)."</time> bis : <time>". $this->func->sanitize($notiz->datum_bis)."</time>
                                    <h5>".$this->func->sanitize($notiz->hilfeart_name)."</h5>
                                    <h6>".$this->func->sanitize($notiz->stadt).", ".$this->func->sanitize($notiz->abteilung)."<br />"
                                                .$this->func->sanitize($notiz->asd_name)."</h6><br />";
                endif;
                if($notiz->datum_ab."--".$notiz->datum_bis == $notiz->a_datum_ab."--".$notiz->a_datum_bis && !empty($notiz->a_datum_ab)):
                    $aktuellstr = "von : <time>".$this->func->sanitize($notiz->a_datum_ab)."</time> bis : <time>". $this->func->sanitize($notiz->a_datum_bis)."</time>
                                    <h5>".$this->func->sanitize($notiz->hilfeart_name)."</h5>
                                    <h6>".$this->func->sanitize($notiz->stadt).", ".$this->func->sanitize($notiz->abteilung)."<br />"
                                                .$this->func->sanitize($notiz->asd_name)."</h6>";

                endif;



            endforeach;
            if( empty( $notizstr ) )
               $notizstr= "Keine Notiz gefunden!";
            if( empty( $historiestr ) )
               $historiestr= "Keine Historie vorhanden!";
            if( empty( $aktuellstr ) )
               $aktuellstr= "Keine Aktuell vorhanden!";

            ?>

<link rel="stylesheet" type="text/css" href="<?php echo WEBDIR?>style/klientadmin.css" />
<div class="products">
<div class="top-box">
    <h2><?php echo $this->func->sanitize($this->_['klient'][0]->surname.", ". $this->_['klient'][0]->given_name)?></h2>
    <hr>
    <p class="desc">Übersicht über alle Klienten. Klienten können direkt bearbeitet werden</p>
    <br/><br/>
    <?php echo  $this->link->makeLink($icon_neu_small." Klient einpflegen",WEBDIR."admin/klientenadmin.htm?id=".$this->_['klient'][0]->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$request['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
    <br /><br />
    <div class="about-bottom" align="left">
        <div class="about-topgrids">
            <div class="about-topgrid1">
                <h3>Klients Informationen</h3>
                <label for="az"> AZ : </label>
                <label for="value_az">
                    <?php echo $this->func->sanitize($this->_['klient'][0]->az)?>
                </label><br />
                <label for="name"> Name : </label>
               <label for="value_name">
                    <?php echo $this->func->sanitize($this->_['klient'][0]->surname)?>
               </label><br />
               <label for="vorname"> Vorname : </label>
               <label for="value_vorname">
                    <?php echo $this->func->sanitize($this->_['klient'][0]->given_name)?>
               </label><br />
               <label for="geb"> Geburtsdatum : </label>
               <label for="value_az">
                    <?php echo $this->func->sanitize($this->_['klient'][0]->geb)?>
               </label>
            </div>
        </div>
        <!-- zweite Abschnitt -->
        <div class="about-histore">
            <h3>Klients Notizen</h3>
            <!-- Hier steht die Variable $notizstr -->
            <div class="historey-lines fixed_block_notizen">
                <?php echo $notizstr; ?>
            </div>
            <div class="clear"> </div>
        </div>
        <!-- dritte Abschnitt -->
        <div class="about-services">
            <h3>Klients Hilfearten</h3>
            <div class="questions">
                <h4><!-- Hier fehlt noch den Link des Bilds -->
                    <img alt="" src="images/marker.png">
                    <b><u>Aktuell</u></b> :
                </h4>
                <br />
                <!-- Hier steht die Variable $aktuellstr -->
                <?php echo $aktuellstr; ?>
            </div>
            <br />
            <div class="questions">
                <h4><!-- Hier fehlt noch den Link des Bilds -->
                    <img alt="" src="images/marker.png">
                    <b><u>Historie</u></b> :
                </h4>
                <br />
                <!-- Hier steht die Variable $historiestr -->
                <div class="fixed_block">
                    <ul id="historie_zone">
                        <?php echo $historiestr; ?>
                    </ul>    
                </div>
            </div>
        </div>
        <div class="clear"> </div>
    </div>


<br/><br/>
<?php echo  $this->link->makeLink($icon_neu_small." Klient einpflegen",WEBDIR."admin/klientenadmin.htm?id=".$this->_['klient'][0]->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$request['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
</div>
</div>