<?php
namespace admin\modules\klientenadmin;//******
class Model extends \inc\Basicdb
{
    use ModulConfig;
    // Konstruktor
    ////////////////////////////////////////////////////////////////////////

    function __construct($anzahl = false)
    {
        global $sql;
        $this->anzahl = $anzahl;
        $this->queryVars=(object) array('table' => self::$table['k'], 'countVar'=>$this->countvar, 'cps'=>15, 'offsetname'=>'usite');

        parent::__construct();
    }
	function getAllClients($request=false)
    {

        $queryVars=clone $this->queryVars;

		$queryVars->fields=array(self::$table['k'].'.'.self::$id,
		                         self::$table['k'].'.'.self::$surname,
                                 self::$table['k'].'.'.self::$given_name,
                                 self::$table['k'].'.'.self::$az,
                                 "DATE_FORMAT(".self::$table['k'].'.'.self::$geb.", '%d.%m.%Y') AS geb"
                                 );
        $queryVars->clause= 'GROUP BY '.self::$table['k'].".".self::$id;
        $queryVars->order = (object) array('field' => $request['sort'], 'direction' => $request['rtg']);

         parent::createQuery($queryVars,false,true);
		return $this->liste;
    }
	function getAllClientsMitAsdUndHilfearten($request=false)
    {
        $queryVars=clone $this->queryVars;
		$queryVars->fields=array(self::$table['k'].'.'.self::$id ,
		                         self::$table['k'].'.'.self::$surname ,
                                 self::$table['k'].'.'.self::$given_name,
                                 self::$table['k'].'.'.self::$az,
                                 "DATE_FORMAT(".self::$table['k'].'.'.self::$geb.", '%d.%m.%Y') AS geb",
                                 self::$table['h'].'.'.self::$name.' AS hilfeart_name',
                                 self::$table['a'].'.'.self::$name.' AS asd_name',
                                 self::$table['a'].'.'.self::$abteilung,
                                 self::$table['a'].'.'.self::$stadt,
                                 "DATE_FORMAT(".self::$table['kha'].'.'.self::$datum_ab.", '%d.%m.%Y') AS datum_ab",
                                 "DATE_FORMAT(".self::$table['kha'].'.'.self::$datum_bis.", '%d.%m.%Y') AS datum_bis"
                                 );

       $queryVars->joins=array(
                                (object)
                                array('type'   => 'LEFT',
                                      'table'  => self::$table['kha'],
                                      'clause' => self::$table['k'].'.'.self::$id.' = '.self::$table['kha'].'.'.self::$klient_id."
                                                  AND ( NOW() BETWEEN ".self::$table['kha'].".".self::$datum_ab. " AND ".self::$table['kha'].".".self::$datum_bis.")
                                                    AND
                                                    ( (SELECT COUNT(kha2.klient_id) FROM klient_hilfeart_asd kha2 WHERE kha2.klient_id=kta_klienten.id ) >=1
                                                    OR
                                                    NOT EXISTS (SELECT * FROM ".self::$table['kha']." kha1 WHERE kha1.".self::$datum_ab." > " .self::$table['kha']. ".". self::$datum_ab."
                                                    AND kha1.".self::$datum_bis." < ".self::$table['kha'].".".self::$datum_bis."  ))"
                                ),
                                (object)
                                array('type'   => 'LEFT',
                                      'table'  => self::$table['a'],
                                      'clause' => self::$table['a'].'.'.self::$id.' = '.self::$table['kha'].'.'.self::$asd_id
                                ),
                                (object)
                                array('type'   => 'LEFT',
                                      'table'  => self::$table['h'],
                                      'clause' => self::$table['h'].'.'.self::$id.' = '.self::$table['kha'].'.'.self::$hilfeart_id
                                )
                            );




        $queryVars->order = (object) array('field' => $request['sort'], 'direction' => $request['rtg']);
		$queryVars->groupBy=self::$table['k'].'.'.self::$id ;
        parent::createQuery($queryVars);
		return $this->liste;
    }

	function getClientById($id,$typ=false)
	{
	    $queryVars=clone $this->queryVars;
        $queryVars->fields=array(self::$table['k'].'.'.self::$id );
        $queryVars->joins=array();

        if($typ==false):
            array_push($queryVars->fields ,
                       self::$table['k'].'.'.self::$surname ,
                       self::$table['k'].'.'.self::$given_name,
                       self::$table['k'].'.'.self::$az,
                       "DATE_FORMAT(".self::$table['k'].'.'.self::$geb.", '%d.%m.%Y') AS geb");
        endif;
        if($typ=='kha' || $typ==false):
            array_push($queryVars->fields ,
                       self::$table['a'].'.'.self::$name ." AS asd_name",
                       self::$table['a'].'.'.self::$abteilung,
                       self::$table['a'].'.'.self::$stadt,
                       self::$table['h'].'.'.self::$name ." AS hilfeart_name",
                       self::$table['kha'].'.'.self::$asd_id ." AS asd_id",
                       self::$table['kha'].'.'.self::$hilfeart_id ." AS hilfeart_id",
                       "DATE_FORMAT(".self::$table['kha'].'.'.self::$datum_ab.", '%d.%m.%Y') AS datum_ab",
                       "DATE_FORMAT(".self::$table['kha'].'.'.self::$datum_bis.", '%d.%m.%Y') AS datum_bis",
                       'kha1.asd_id AS a_asd_id',
                       'kha1.hilfeart_id AS a_hilfeart_id',
                       "DATE_FORMAT(kha1.datum_ab, '%d.%m.%Y') AS a_datum_ab",
                       "DATE_FORMAT(kha1.datum_bis, '%d.%m.%Y') AS a_datum_bis");
            array_push($queryVars->joins,
                       (object)
                       array('type'   => 'LEFT',
                             'table'  => self::$table['kha'],
                             'clause' => self::$table['k'].'.'.self::$id.' = '.self::$table['kha'].'.'.self::$klient_id
                       ),
                       (object)
                       array('type'   => 'LEFT',
                             'table'  => self::$table['a'],
                             'clause' => self::$table['a'].'.'.self::$id.' = '.self::$table['kha'].'.'.self::$asd_id
                       ),
                       (object)
                       array('type'   => 'LEFT',
                             'table'  => self::$table['h'],
                             'clause' => self::$table['h'].'.'.self::$id.' = '.self::$table['kha'].'.'.self::$hilfeart_id
                       ),
                       (object)
                       array('type'   => 'LEFT',
                             'table'  => self::$table['kha']. " kha1",
                             'clause' => self::$table['k'].'.'.self::$id.' = kha1.'.self::$klient_id ."
                             AND (NOW() BETWEEN kha1.".self::$datum_ab." AND kha1.".self::$datum_bis.")
                             AND NOT EXISTS
                             (SELECT * FROM ".self::$table['kha']." kha2 WHERE NOW() BETWEEN kha2.datum_ab AND kha2.datum_bis
                             AND kha2.".self::$datum_ab." > kha1.".self::$datum_ab."  AND kha2.".self::$datum_bis." < kha1.".self::$datum_bis."
                             AND ". self::$table['k'].'.'.self::$id.' = kha2.'.self::$klient_id." )"
                            )
                      );
        endif;
        if($typ=='notiz' || $typ==false) :
            array_push($queryVars->joins,
                       (object)
                       array('type'   => 'LEFT',
                             'table'  => self::$table['n'],
                             'clause' => self::$table['k'].'.'.self::$id.' = '.self::$table['n'].'.'.self::$klient_id
                            ),
                       (object)
                       array('type'   => 'LEFT',
                             'table'  => self::$table['u'],
                             'clause' => self::$table['u'].'.'.self::$id.' = '.self::$table['n'].'.'.self::$user_id
                            )
                       );
            array_push($queryVars->fields,
                       "DATE_FORMAT(".self::$table['n'].'.'.self::$datum.", '%d.%m.%Y %H:%i:%S') AS datum",
                       self::$table['n'].'.'.self::$notiz,
                       self::$table['n'].'.'.self::$user_id,
                       self::$table['u'].'.'.self::$given_name ." ugiven_name",
                       self::$table['u'].'.'.self::$surname ." usurname");
            $queryVars->order=(object) array('field'=>'datum','direction'=>'DESC');
        endif;

        if($typ=='termin' || $typ==false) :
            array_push($queryVars->joins,
                       (object)
                       array('type'   => 'LEFT',
                             'table'  => self::$table['t'],
                             'clause' => self::$table['k'].'.'.self::$id.' = '.self::$table['t'].'.'.self::$klient_id
                            )
                       );
            array_push($queryVars->fields,
                       "DATE_FORMAT(".self::$table['t'].'.'.self::$terminbeginn.", '%d.%m.%Y %H:%i') AS terminbeginn",
                       "DATE_FORMAT(".self::$table['t'].'.'.self::$terminende.", '%d.%m.%Y %H:%i') AS terminende",
                       self::$table['t'].'.'.self::$id ." AS termin_id",
                       self::$table['t'].'.'.self::$titel,
                       self::$table['t'].'.'.self::$beschreibung);
            $queryVars->order=(object) array('field'=>'terminbeginn','direction'=>'ASC');
        endif;

        $queryVars->clause = self::$table['k'].".id= :klientId";
        $queryVars->preparedVars=array(':klientId'=>intval($id));
        unset($queryVars->cps);
        unset($queryVars->offsetname);

	    return( parent::createQuery($queryVars,false));
	}

    public function updateOrNewClient($request,$user=false)
	{

		$queryVars = clone $this->queryVars;
		$queryVars->data = array("given_name"=>$request['given_name'],
	                          "surname"=>$request['surname'],
	                          "az"=>$request['az'],
							  "geb"=>date('Y-m-d',strtotime($request['geb'])));


		if (is_array($user)):
			$queryVars->clause="id=:ID";
			$queryVars->preparedVars=array(':ID'=>$user[0]->id);
			return parent::update($queryVars);
		else:
			parent::write($queryVars);
			return $this->lastid;
		endif;

	}

    public function deleteById($id)
    {

        $queryVars = clone $this->queryVars;
        $queryVars->clause="id = :ID";
        $queryVars->preparedVars=array(":ID"=>intval($id));
        return parent::deleteElement($queryVars);
    }

    //HIlfeart, Asd und Datun Einfügen und updaten
    public function inserOrUpdatetHilfeartAsd($request,$kha=false,$action=false)
    {

        $queryVars = clone $this->queryVars;
        $queryVars->table = self::$table['kha'];
        $queryVars->data = array("klient_id"=>$request['klient_id'],
                                    "hilfeart_id"=>$request['hilfeart_id'],
                                    "asd_id"=>$request['asd_id'],
                                    "datum_ab"=>date("Y-m-d", strtotime($request['datum_ab'])),
                                    "datum_bis"=>date("Y-m-d", strtotime($request['datum_bis']))
                              );


        if ($action=="kha_update"):
            $queryVars->clause="klient_id=:KID AND hilfeart_id = :HID AND asd_id = :SID AND datum_ab = :DATUM_AB AND datum_bis = :DATUM_BIS";
            $queryVars->preparedVars=array(':KID' => $kha[0],
                                            ':HID' => $kha[1],
                                            ':SID' => $kha[2],
                                            ':DATUM_AB' => date("Y-m-d", strtotime($kha[3])),
                                            ':DATUM_BIS' => date("Y-m-d", strtotime($kha[4]))
                                            );
            return parent::update($queryVars);
        else:
            parent::write($queryVars);
            return $this->lastid;
        endif;

    }
    //Notiz updaten
    public function insertOrUpdateNotiz($request,$notiz=false,$action=FALSE)
    {

        $queryVars = clone $this->queryVars;
        $queryVars->table = self::$table['n'];
        $queryVars->data = array("klient_id"=>$request['klient_id'],
                                    "user_id"=>($action=="notiz_update")?$request['user_id']:$_SESSION['user']->id,
                                    "notiz"=>$request['notiz_text']
                              );

        if ($action=="notiz_update"):
            $queryVars->clause="klient_id=:KID AND user_id = :UID AND datum = :DATUM";
            $queryVars->preparedVars=array(':KID' => $notiz[0],
                                            ':UID' => $notiz[2],
                                            ':DATUM' => date("Y-m-d H:i:s", strtotime($notiz[1]))
                                            );
            return parent::update($queryVars);
        else:
            parent::write($queryVars);
            return $this->lastid;
        endif;
    }
    //Termine verwalden
    public function insertOrUpdateTermin($request,$termin=false,$action=FALSE)
    {

        $queryVars = clone $this->queryVars;
        $queryVars->table = self::$table['t'];
        $queryVars->data = array("klient_id"=>$request['klient_id'],
                                 "titel"         =>$request['titel'],
                                 "beschreibung"  =>$request['beschreibung'],
                                 "terminbeginn"  =>date("Y-m-d H:i", strtotime($request['terminbeginn'])),
                                 "terminende"    =>date("Y-m-d H:i", strtotime($request['terminende'])),
                              );

        if ($action=="termin_update"):
            $queryVars->clause="id = :ID ";
            $queryVars->preparedVars=array(':ID' => $termin[0]
                                            );
            return parent::update($queryVars);
        else:
            parent::write($queryVars);
            return $this->lastid;
        endif;
    }
    //Termine des Klientes
    function getAllTermineByKlient($id)
    {

        $queryVars=clone $this->queryVars;

        $queryVars->fields=array(self::$table['t'].'.'.self::$id,
                                 self::$table['t'].'.'.self::$titel,
                                 self::$table['t'].'.'.self::$beschreibung,
                                 "DATE_FORMAT(".self::$table['t'].'.'.self::$terminbeginn.", '%d.%m.%Y') AS terminbeginn",
                                 "DATE_FORMAT(".self::$table['t'].'.'.self::$terminende.", '%d.%m.%Y') AS terminende"
                                 );
        $queryVars->order=(object) array('field'=>'terminbeginn','direction'=>'DESC');

        $queryVars->clause = self::$table['t'].".klient_id= :klientId";
        $queryVars->preparedVars=array(':klientId'=>intval($id));

         parent::createQuery($queryVars,false,true);
        return $this->liste;
    }

}

?>
