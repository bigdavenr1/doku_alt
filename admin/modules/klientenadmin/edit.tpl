 <?php
    if(!empty($this->_['klient'])):
        $tempn=array();
        $temph=array();
        $tempt=array();

        foreach($this->_['klient'] as $key => $notiz):
            if(!in_array($notiz->id."--".$notiz->user_id."--".$notiz->datum,$tempn)):
                $tempn[] = $notiz->id."--".$notiz->user_id."--".$notiz->datum;
                $notizstr .=
                                "<ul id='$notiz->id--$notiz->datum--$notiz->user_id--$notiz->notiz' style='border-bottom:1px solid #000'>
                                    <li><b class='onclick_update_notiz'><img src='".WEBDIR."images/icons/page_edit.png'/></b>
                                        <b>". $this->func->sanitize($notiz->usurname.", ".$notiz->ugiven_name) ."
                                        ". $this->func->sanitize($notiz->datum) ."</b><br/><br/>

                                        ". nl2br($this->func->sanitize($notiz->notiz)) ."<br/><br/>
                                    </li>
                                    <div class='clear'> </div>
                                </ul><br/>"
                        ;
            endif;

            if(!in_array($notiz->datum_ab."--".$notiz->datum_bis,$temph) && $notiz->datum_ab."--".$notiz->datum_bis != $notiz->a_datum_ab."--".$notiz->a_datum_bis):
                $temph[] = $notiz->datum_ab."--".$notiz->datum_bis;
                $historiestr .= "<li id='$notiz->id--$notiz->hilfeart_id--$notiz->asd_id--$notiz->datum_ab--$notiz->datum_bis'>
                                    <ul>
                                        <li>- von : <time>".$this->func->sanitize($notiz->datum_ab)."</time> bis : <time>"
                                        . $this->func->sanitize($notiz->datum_bis)."</time> <b class=\"onclick_update\"><img src=\"".WEBDIR."images/icons/page_edit_small.gif\" alt=\"Bearbeiten\" title=\"Bearbeiten\" /></b></li>
                                        <li><h5>".$this->func->sanitize($notiz->hilfeart_name)."</h5></li>
                                        <li><h6>".$this->func->sanitize($notiz->stadt).", ".$this->func->sanitize($notiz->abteilung)."<br />"
                                                .$this->func->sanitize($notiz->asd_name)."</h6><br /></li>
                                    </ul>
                                </li>";
            endif;

            if($notiz->datum_ab."--".$notiz->datum_bis == $notiz->a_datum_ab."--".$notiz->a_datum_bis && !empty($notiz->a_datum_ab)):
                $aktuellstr = "<li id='$notiz->id--$notiz->hilfeart_id--$notiz->asd_id--$notiz->datum_ab--$notiz->datum_bis'>
                                   <ul>
                                       <li>von : <time>".$this->func->sanitize($notiz->a_datum_ab)."</time> bis : <time>". $this->func->sanitize($notiz->a_datum_bis)
                                       ."</time> <b class=\"onclick_update\"><img src=\"".WEBDIR."images/icons/page_edit_small.gif\" alt=\"Bearbeiten\" title=\"Bearbeiten\" /></b></li>
                                       <li><h5>".$this->func->sanitize($notiz->hilfeart_name)."</h5></li>
                                       <li><h6>".$this->func->sanitize($notiz->stadt).", ".$this->func->sanitize($notiz->abteilung)."<br />"
                                                .$this->func->sanitize($notiz->asd_name)."</h6></li>
                                   </ul>
                               </li>";

            endif;

            if(!in_array($notiz->termin_id."--".$notiz->titel."--".$notiz->beschreibung."--".$notiz->terminbeginn."--".$notiz->terminende,$tempt) && !empty($notiz->titel)):
                $tempt[] = $notiz->termin_id."--".$notiz->titel."--".$notiz->beschreibung."--".$notiz->terminbeginn."--".$notiz->terminende;
                $historietermine .= "<li id='$notiz->termin_id--$notiz->titel--$notiz->beschreibung--$notiz->terminbeginn--$notiz->terminende'>
                                    <ul>
                                        <li><b class=\"onclick_update_termin\" style=\"float:left \"><img src=\"".WEBDIR."images/icons/page_edit_small.gif\" alt=\"Bearbeiten\" title=\"Bearbeiten\" /></b>von <time>".$this->func->sanitize($notiz->terminbeginn)."</time> bis <time>"
                                        . $this->func->sanitize($notiz->terminende)."</time> </li>
                                        <li><h5>".$this->func->sanitize($notiz->titel)."</h5></li>
                                        <li><p>".nl2br($this->func->sanitize($notiz->beschreibung))."</p><br /></li>
                                    </ul>
                                </li>";
            endif;


        endforeach;
        if( empty( $notizstr ) )
           $notizstr= "Keine Notiz gefunden!";
        if( empty( $historiestr ) )
           $historiestr= "Keine Historie vorhanden!";
        if( empty( $aktuellstr ) )
           $aktuellstr= "Keine Aktuell vorhanden!";
        if( empty( $historietermine ) )
           $historietermine= "Keine Termine vorhanden!";


   endif;
?>

<?php  $_SERVER['QUERY_STRING']=str_replace("&&", '&', str_replace("res=".$_GET['res'], '', str_replace("modul=".$_GET['modul'], '', $_SERVER['QUERY_STRING'])))?>
<link href="<?php echo WEBDIR?>script/jquery-ui.min.css" rel="stylesheet">
 <script type="text/javascript" src="<?php echo WEBDIR?>script/jquery-ui.min.js"></script>
 <script type="text/javascript" src="<?php echo WEBDIR?>script/jquery-ui-timepicker-addon.js"></script>

  <script>
  $(function($){
        $.datepicker.regional['de'] = {clearText: 'löschen', clearStatus: 'aktuelles Datum löschen',
                closeText: 'schließen', closeStatus: 'ohne Änderungen schließen',
                prevText: '<zurück', prevStatus: 'letzten Monat zeigen',
                nextText: 'Vor>', nextStatus: 'nächsten Monat zeigen',
                currentText: 'heute', currentStatus: '',
                monthNames: ['Januar','Februar','März','April','Mai','Juni',
                'Juli','August','September','Oktober','November','Dezember'],
                monthNamesShort: ['Jan','Feb','Mär','Apr','Mai','Jun',
                'Jul','Aug','Sep','Okt','Nov','Dez'],
                monthStatus: 'anderen Monat anzeigen', yearStatus: 'anderes Jahr anzeigen',
                weekHeader: 'Wo', weekStatus: 'Woche des Monats',
                dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
                dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
                dayStatus: 'Setze DD als ersten Wochentag', dateStatus: 'Wähle D, M d',
                dateFormat: 'dd.mm.yy', firstDay: 1,
                initStatus: 'Wähle ein Datum', isRTL: false,

                };
        $.datepicker.setDefaults($.datepicker.regional['de']);
});
  $(function() {
    var date = new Date();
    date.setDate(date.getDate());
    $( ".timepicker" ).datetimepicker( {stepMinute: 15,defaultTime:'08:00'});
  });
  </script>
<div class="col span_2_of_3">
    <h3>Klienten <?php echo empty($this->_['klient'])?'neu anlegen':'ändern';?></h3>
    <hr>
    <p class="desc">Sie haben die Möglichkeit mit dem nachfolgenden Formularen die Klienten zu ändern.<br/>* markierte Felder sind Pflichtfelder!</p>
    </div>
    <div align="center" class="col span_1_of_4 about-services-termin" style="height:150px; overflow:auto;">
        <h3>HP-Termine &nbsp; &nbsp;<a href="javascript:void(0)" id="onclick_termin"><img style="display: inline" src="<?php echo WEBDIR?>images/icons/note_new.gif" alt="Neuen Datensatz anlegen" title="Neuen Datensatz anlegen" /></a><br style="clear:both;float:none;"/></h3>


        <?php echo $historietermine; ?>
    </div>
    <br/>
    <hr>
    <br/><br/>

        <div class="about-bottom" align="left">
            <div class="about-topgrids">
                <div class="about-topgrid1">
                    <h3>Klientendaten</h3>
                    <div class="contact-form">
                        <form action="" method="post" style="padding-right:0px;margin-right:0px;">
                            <fieldset>
                                <label for="az"> AZ : </label>
                                <input type="text" placeholder="neues Aktenzeichen" id="az" name="az" class="<?php echo isset($this->_['valErrors']['az'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['az'])?$request['az']:$this->_['klient'][0]->az); ?>"/><br/>
                                <label for="name"> Name : </label>
                                <input type="text" placeholder="neuer Name" id="surname" name="surname" class="<?php echo isset($this->_['valErrors']['surname'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['surname'])?$request['surname']:$this->_['klient'][0]->surname); ?>"/><br/>
                                <label for="vorname"> Vorname : </label>
                                <input type="text" placeholder="neuer Vorname" id="given_name" name="given_name" class="<?php echo isset($this->_['valErrors']['given_name'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['given_name'])?$request['given_name']:$this->_['klient'][0]->given_name); ?>"/><br/>
                                <label for="geb"> Geburtsdatum : </label>
                                <input type="text" placeholder="neues Geburtsdatum" id="geb" name="geb" class="<?php echo isset($this->_['valErrors']['geb'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['geb'])?$request['geb']:$this->_['klient'][0]->geb); ?>"/><br/>
                                <br class="clr"/><br/><br/>
                            </fieldset>
                            <br class="clear" />
                            <br class="clr" style="clear:both;float:none;"/>
                            <input type="submit" class="btn btn-primary btn1" name="submit" value="Daten speichern"/>
                        </form>
                    </div>

                </div>
            </div>
            <?php if(!empty($this->_['klient'])): ?>
                <!-- zweite Abschnitt -->
                <div class="about-histore">
                    <h3>Notizen</h3>
                    <p id="onclick_notiz">Notiz hinzufügen</p>
                    <br />
                    <div class="historey-lines fixed_block_notizen">
                    <!-- Hier steht die Variable $notizstr -->
                    <?php echo $notizstr; ?>
                    </div> <br />
                    <div class="clear"> </div>
                </div>
                <!-- dritte Abschnitt -->
                <div class="about-services">
                    <h3>Hilfen</h3>
                    <p id="onclick">Hilfeart und ASD hinzufügen</p>
                    <div class="questions">
                        <h4><!-- Hier fehlt noch den Link des Bilds -->
                            <img alt="" src="images/marker.png">
                            <b><u>Aktuell</u></b> :
                        </h4>
                        <br />
                        <!-- Hier steht die Variable $aktuellstr -->
                        <div>

                        </div>
                        <ul id="historie_zone">
                            <?php echo $aktuellstr; ?>
                        </ul>
                    </div>
                    <br />
                    <div class="questions">
                        <h4><!-- Hier fehlt noch den Link des Bilds -->
                            <img alt="" src="images/marker.png">
                            <b><u>Historie</u></b> :
                        </h4>
                        <br />
                        <!-- Hier steht die Variable $historiestr -->
                        <div class="fixed_block">
                            <ul id="historie_zone">
                                <?php echo $historiestr; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clear"> </div>
                <?php endif; ?>
            </div>

    <br/><br/>


<div class="clr"/></div>
<script src="<?php echo WEBDIR?>script/klientadmin.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo WEBDIR?>style/klientadmin.css" />


<!-- hilfeart Form -->
<div id="hilfeartdiv">
    <form class="form" action="javascript:void(0)" id="hilfeart">
        <img src="<?php echo WEBDIR?>images/closetransbtn.png" class="img" id="cancel"/>


        <label for="hilfeart">Hilfe hinzufügen</label>
        <select name="select_hilfeart_add" id="select_hilfeart_add">

            <?php
            // alle Hilfearten anzeigen
            if (!empty($this->_['hilfearten'])):
            foreach($this->_['hilfearten'] as $key => $hilfeart):
            ?>
                    <option value="<?php echo $this->func->sanitize($hilfeart->id)?>" ><?php echo $this->func->sanitize($hilfeart->name)?></option>
            <?php endforeach; endif; ?>
        </select>
        <label for="asd">ASD</label>
        <select name="select_asd_add" id="select_asd_add">

            <?php
            // alle ASDs anzeigen
            if(!empty($this->_['asds'])):
            foreach($this->_['asds'] as $key => $asd):
            ?>
                <option value="<?php echo $this->func->sanitize($asd->id)?>" ><?php echo $this->func->sanitize($asd->name)?></option>
            <?php endforeach; endif;?>
        </select>

        <label for="asd">Datum ab : </label>
        <input type="text" placeholder="Datum ab" id="datum_ab_add" />

        <label for="datum_bis">Datum bis : </label>
        <input type="text" id="datum_bis_add" placeholder="Datum bis" />

        <a href="javascript:void(0)" class="btn btn-primary btn1" id="send" />Hinzufügen</a>
        <input type="button"  class="btn btn-primary btn1" id="cancel" value="Abbrechen"/>
        <input type="hidden" id="klient_id" value="<?php echo $this->_['klient'][0]->id ?>" />
        <br/>
    </form>
</div>

<!-- termin Form -->
<div id="termindiv">
    <form class="form" action="javascript:void(0)" id="termin">
        <img src="<?php echo WEBDIR?>images/closetransbtn.png" class="img" id="cancel"/>


        <label for="asd">Titel : </label>
        <input type="text" placeholder="Titel" id="titel_add" />

        <label for="asd">Beschreibung : </label><br/>
        <textarea id="beschreibung_add" style="width:320px;height:75px;"></textarea><br/>

        <label for="asd">Termin Beginn : </label>
        <input type="text" placeholder="Terminbeginn" id="terminbeginn_add" class="timepicker" />

        <label for="datum_bis">Termin Ende : </label>
        <input type="text" id="terminende_add" placeholder="Terminende" class="timepicker"/>

        <a href="javascript:void(0)" class="btn btn-primary btn1" id="send" />Hinzufügen</a>
        <input type="button"  class="btn btn-primary btn1" id="cancel" value="Abbrechen"/>
        <input type="hidden" id="klient_id" value="<?php echo $this->_['klient'][0]->id ?>" />
        <br/>
    </form>
</div>



<!-- Notiz Form -->
<div id="notizdiv">
    <form class="form" action="#" id="notiz">
        <img src="<?php echo WEBDIR?>images/closetransbtn.png" class="img" id="cancel"/>

        <div id="wenn_update">
            <label for="mitarbeiter">Mitarbeiter</label>
            <select id="update_mitarbeiter">
                <?php
                // alle Mitarbeiter anzeigen
                if (!empty($this->_['users'])):
                    foreach($this->_['users'] as $key => $user):
                        ?>
                            <option value="<?php echo $this->func->sanitize($user->id)?>" ><?php echo $this->func->sanitize($user->username)?></option>
                        <?php
                    endforeach;
                endif; ?>
            </select>
        </div>
        <br />
        <label>Text :</label>

        <textarea id="update_notiz" name="notiz" class="<?php echo isset($this->_['valErrors']['notiz'])?'validate_error':''?> validate_required" /><?php echo $this->func->sanitize(!empty($request['notiz'])?$request['notiz']:$this->_['notiz']->notiz); ?></textarea>
        <br/>
        <br class="clr"/><br />

    <a href="javascript:void(0)" class="btn btn-primary btn1" id="send" />Hinzufügen</a>
            <input type="button"  class="btn btn-primary btn1" id="cancel" value="Abbrechen"/>
            <input type="hidden" id="klient_id" value="<?php echo $this->_['klient'][0]->id ?>" />
    <br/>
    </form>
</div>