<?php // Modulview und Modulmodel einbinden
namespace admin\modules\adminstart;
require_once("model.php");
class Controller
{
    // private VAriablen setzen
    private $request = null;
    private $modultemplate = 'adminstart';
    private $modulview = null;
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->modulview = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->modultemplate ='adminstart';
    }
    ############################################
    # Methode zum anzeigen des Moduöl-Contents.#
    #                                          #
    # @return String Content der Applikation.  #
    ############################################
    public function display()
    {
        $this->assignvalues['noslider']=true;
        // modultemplate setzen
        $this->modulview->setTemplate($this->modultemplate,dirname(__FILE__));
        // Modelklasse instanziieren
        $userstartfac=new Model();
        // methode zum Abruf des Artikel aufrufen
        $user=$userstartfac->getById($_SESSION['user']->id,'kta_users');
        // Videocodes auslesen
        if (!empty($user)):
            $this->modulview->assign('username',$user->vorname." ".$user->name);
        else:
            // SESSIONdaten wieder löschen und umleiten
            unset ($_SESSION['user']);
            header("Location: ".WEBDIR."status_msg/Session_destroyed");
        endif;
         // Template laden, füllen und ausgeben
        return $this->modulview->loadTemplate();
    }
}
?>