
<h1>Willkommen <?php echo $_SESSION['user']->username ?> im Adminbereich</h1>
Folgende Aktionen können Sie durchführen:
<br /><br />
<ul>
    
                <li>
                    <a href="<?php echo WEBDIR?>startseite.htm">Startseite</a>
                </li>
                <li>
                    <a href="<?php echo WEBDIR?>user/notizen.htm">Notizen</a>
                </li>
                <li>
                    <a href="<?php echo WEBDIR?>user/klienten.htm">Klienten</a>
                </li>
                <li>
                    <a href="<?php echo WEBDIR?>user/profile.htm">Profil ändern</a>
                </li>
                
                <li>
                    <br />
                </li>
                
                <li>
                    <a href="<?php echo WEBDIR?>admin/contentadmin.htm">Content verwalten</a>
                </li>
                <li>
                    <a href="<?php echo WEBDIR?>admin/menuadmin.htm">Menü verwalten</a>
                </li>
                  <li>
                    <a href="<?php echo WEBDIR?>admin/notizadmin.htm">Notiz verwalten</a>
                </li>
                
                 <li>
                    <a href="<?php echo WEBDIR?>admin/klientenadmin.htm">Klienten verwalten</a>
                </li>
                
                 <li>
                    <a href="<?php echo WEBDIR?>admin/hilfeartadmin.htm">Hilfeart verwalten</a>
                </li>
                  <li>
                    <a href="<?php echo WEBDIR?>admin/asdadmin.htm">ASD verwalten</a>
                </li> 
              
                 <li>
                    <a href="<?php echo WEBDIR?>admin/useradmin.htm">User verwalten</a>
                </li> 
                  
            <li>
                <br/>
            </li>
                <li>
                    <a href="<?php echo WEBDIR?>logout.htm">LOGOUT</a>
                </li>

</ul>
<br/>
Sollten Sie Fragen haben, kontaktieren Sie uns bitte.<br/><br/>
Vielen Dank
