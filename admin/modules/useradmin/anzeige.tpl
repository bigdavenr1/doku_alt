<div class="products">
<div class="top-box">

	<h2>Useradmin</h2>
	<hr>
	<p class="desc">Übersicht über alle User. User können direkt bearbeitet werden</p>
	<br/><br/>
	<table>
	<tr>
		<?php
		  echo $this->func->tableHeadSort("ID","id","","","width:40px;");
          echo $this->func->tableHeadSort("User","username");
          echo $this->func->tableHeadSort("Name","surname");
          echo $this->func->tableHeadSort("Vorname","given_name");
          echo $this->func->tableHeadSort("E-Mail","email");
          echo $this->func->tableHeadSort("Ort","city");
          echo $this->func->tableHeadSort("Typ","usertype","","","width:50px;");
          echo $this->func->tableHeadSort("Status","active","","","width:85px;");
          echo $this->func->tableHead("Admin","","","width:40px;");
		?>
	</tr>
    <?php
    if (!empty($this->_['users'])):
        // Studiodaten abrufen
        foreach($this->_['users'] as $key => $user):
        // Ausgabe

            ?>
            <tr class="td<?php echo $x%2?>">
                <td>
                	<?php echo strip_tags(htmlspecialchars(stripslashes($user->id)))?>
                </td>
                <td>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($user->username)))?>
                </td>
                <td>
                	<?php echo strip_tags(htmlspecialchars(stripslashes($user->surname)))?>
                </td>
                <td>
                	<?php echo strip_tags(htmlspecialchars(stripslashes($user->given_name)))?>
                </td>
                <td>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($user->email)))?>
                </td>
                <td>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($user->city)))?>
                </td>
                <td>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($user->usertypename)))?>
                </td>
                <td>
                    <?php echo $user->active==1?'<span class="msg">A</span>':'<span class="err">I</span>'?>
                </td>
                <td  style="text-align:center">

                    <?php echo  $this->link->makeLink('<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />',WEBDIR.'admin/useradmin.htm?view=delete&amp;userid='.$user->id,"adminlink");?>
                    <?php echo  $this->link->makeLink('<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />',WEBDIR."admin/useradmin.htm?userid=".$user->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$request['userid'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
                </td>
            </tr>

         <?php
         endforeach;
         ?></table><br/><?php
         echo $this->_['htmlnavi'];
     else:
     	echo $this->_['htmlnavi'];
     endif;
     ?>

<br/><br/>
<?php echo  $this->link->makeLink($icon_neu_small." Neuen User einpflegen",WEBDIR."admin/useradmin.htm?view=edit","adminlink")?>
</div>
</div>