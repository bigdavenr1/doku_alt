<?php // view und Modulmodel einbinden
namespace admin\modules\useradmin;
require_once("model.php");
class Controller
{
    // private VAriablen setzen
    private $request = null;
    private $template = 'anzeige';
    private $view = null;
    /**
	 * Konstruktur
	 * bereitet Template und übergebene Entscheidungsvariablen auf
	 * @param Array $request Array mit übergebenen Variablen (GET POST FILE)
	 */
    public function __construct($request)
    {
        // neuen viewer instanziieren
        $this->view = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // template auslesen
        $this->template =isset($this->request['view'])?$this->request['view']:'anzeige';
		// Modelobject
		$this->model = new Model();
    }
	public function display()
    {
    	// prüfen und aufbereiten ob User ID übergeben wurde
    	$this->request['userid']=empty($this->request['userid'])?0:$this->request['userid'];
		// Anhand übergebener ID entscheiden
		switch($this->request['userid']):
			// Wenn keine USerid übergeben wurde  (list oder new)
			case '0':
				// Wenn neuer DAtensatz
				if ($this->template=='edit'):
					$this->editOrDelete($id,'new');
					// Usergroups auslesen
					$this->model->getAll('kta_usergroups',false,false);
					$this->view->assign('usergroups', $this->model->liste);
		            // Wenn Liste
				else:					
					$this->model->getAll('kta_users',false,15,$this->request['sort'],$this->request['rtg']);
                	$this->view->assign("htmlnavi",$this->model->getHtmlNavi());
					$this->view->assign("users",$this->model->liste);
				endif;
				break;
			// Wenn ID übergeben wurde (edit oder delete)
			default:
				// Usergroups auslesen
				$this->model->getAll('kta_usergroups',false,false);
			    $this->view->assign('usergroups', $this->model->liste);
				// Template auf edit, wenn nicht gelöscht werden soll
				$this->template =isset($this->request['view'])?$this->request['view']:'edit';
				$this->model->getUserById(intval($this->request['userid']));
                // Benutzerdaten Global zu verfügung stellen
				$this->view->assign("user",$this->user=$this->model->getElement());
				// Prüfung ob gelöscht oder bearbeitet werden soll.
				$this->editOrDelete($this->user->id);
				break;
		endswitch;
		// usergruppen auslesen
        $this->model->getAll('kta_usergroups');
        $this->view->assign("usergroups",$this->model->liste);
        $this->view->setTemplate($this->template,dirname(__FILE__));
        return $this->view->loadTemplate();
	}

	private function changeData($modus)
	{
        if ($this->request['email'] != $this->user->email  &&  empty($this->request['pass'])):            
            $_SESSION['err']="zu einer neuen E-Mail-Adresse muss ein neues Passwort gesetzt werden, da diese beiden Werte zusammenhängen!";
            header('Location'.WEBDIR.'admin/useradmin.htm?id='.$this->user->id);
            exit;
        endif;
        $this->model->updateOrNewUser($this->request,$this->user);
	}
	
	private function editOrDelete($id,$modus=false)
	{
		if ($this->template=='edit' && !empty($this->request['submit'])):
			$this->changeData($modus);
			$_SESSION['msg']='User erfolgreich'.(empty($modus)?'geändert':'angelegt');
			header('Location:'.WEBDIR.'admin/useradmin.htm');
        elseif ($this->template=='delete' && !empty($this->request['confirmed'])):
        	$this->model->deleteElement('id',$id);
            if ($this->user->usertype!=1):
			     $_SESSION['msg']='User erfolgreich gelöscht!';
			     header('Location:'.WEBDIR.'admin/useradmin.htm');
            endif;
        elseif ($this->template=='delete' && $this->user->usertype==1):
            $_SESSION['err']='Der Admin-Zugang kann nicht gelöscht werden';
            header('Location:'.WEBDIR.'admin/useradmin.htm');
        endif;
	}
}
?>