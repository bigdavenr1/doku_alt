<?php  $_SERVER['QUERY_STRING']=str_replace("&&", '&', str_replace("res=".$_GET['res'], '', str_replace("modul=".$_GET['modul'], '', $_SERVER['QUERY_STRING'])))?>
<div class="col span_2_of_3">
<h3>User <?php echo empty($this->_['user'])?'neu anlegen':'ändern';?></h3>
<hr>
<p class="desc">Sie haben die Möglichkeit mit dem nachfolgenden Formularen die Userdaten zu ändern.<br/>* markierte Felder sind Pflichtfelder!</p>

<div class="contact-form">
<br/><br/>
<form action="" method="post" style="padding-right:0px;margin-right:0px;">
<fieldset style="">
    <label> Benutzername</label>
    <input type="text" id="username" name="username" placeholder="Username" class="validate_username <?php echo isset($this->_['valErrors']['username'])?'validate_error':''?>" value="<?php echo $this->func->sanitize(!empty($request['username'])?$request['username']:$this->_['user']->username) ?>" autocomplete="off"/><br/>
    
    <label>Userrolle</label><br/>
    <select name="usertype">
                <?php
                foreach($this->_['usergroups'] as  $group)
                    if($group->id!=1)
                        echo '<option '.($group->id==$this->_['user']->usertype?'selected="selected"':'').' value="'.$group->id.'">'.$group->name.'</option>';
                ?>
            </select>
	<br/>

	<input value="1" type="checkbox" <?php echo $this->_['user']->active==1?'checked="checked"':''?> name="active"> </label>Account aktiviert?</label>
    <br/><br/><br/>
    <label>E-Mail</label>
    <input type="text" id="email" name="email" placeholder="E-Mail" class="validate_email <?php echo isset($this->_['valErrors']['email'])?'validate_error':''?>" value="<?php echo $this->func->sanitize(!empty($request['email'])?$request['email']:$this->_['user']->email) ?>" autocomplete="off"/><br/>
	<label>E-Mail wiederholen</label>
    <input type="text" id="email_repeat" name="email_repeat" placeholder="neue E-Mail wiederholen" class="validate_emailrepeat <?php echo isset($this->_['valErrors']['email_repeat'])?'validate_error':''?>" value="<?php echo $this->func->sanitize(!empty($request['email_repeat'])?$request['email_repeat']:'') ?>" autocomplete="off"/><br/>
    <br/>
    <label>Neues Passwort</label>
    <input type="password" id="pass" placeholder="Neues Passwort" name="pass" class="<?php echo isset($this->_['valErrors']['pass'])?'validate_error':''?> validate_required" autocomplete="off"/><br/>
	<label>Passwort wiederholen</label>
    <input type="password" placeholder="Passwort wiederholen" id="pass_repeat" name="pass_repeat" class="validate_required" autocomplete="off"/>
    <br/><br/>
</fieldset>
<fieldset>
    <label>Telefon</label>
    <input type="text" id="tel" placeholder="Telefon"  name="tel" value="<?php echo $this->func->sanitize(!empty($request['tel'])?$request['tel']:$this->_['user']->tel); ?>"/><br/>
    <label>Anschrift</label>
    <input type="text" id="street1" placeholder="Anschrift 1" name="street1" class="<?php echo isset($this->_['valErrors']['street1'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['street1'])?$request['street1']:$this->_['user']->street1); ?>"/><br/>

    

    <br class="clr"/>
    <label>PLZ / Ort</label> 
    <br class="clr"/>
    <input style="width:45px;margin-right:4px;float:left"  type="text" id="zipuserdaten" placeholder="PLZ" name="zip" value="<?php echo $this->func->sanitize(!empty($this->request['zip'])?$this->request['zip']:$this->_['user']->zip); ?>"/>
    <input type="text" id="cityuserdaten" placeholder="Stadt" name="city" style="max-width:500px;margin-top:0px;" value="<?php echo $this->func->sanitize(!empty($this->request['city'])?$this->request['city']:$this->_['user']->city); ?>"/><br/>
    <br class="clr"/><br/><br/>
</fieldset>
<fieldset>
   <label>Vorname</label>

    <input type="text" placeholder="Vorname" id="given_name" name="given_name" class="<?php echo isset($this->_['valErrors']['given_name'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['given_name'])?$request['given_name']:$this->_['user']->given_name); ?>"/><br/>
	<label>Nachname</label>
    <input type="text" placeholder="Nachname" id="surname" name="surname" class="<?php echo isset($this->_['valErrors']['surname'])?'validate_error':''?> validate_required" value="<?php echo $this->func->sanitize(!empty($request['surname'])?$request['surname']:$this->_['user']->surname); ?>"/><br/>
</fieldset>
<br class="clear" />
<br class="clr" style="clear:both;float:none;"/>
<input type="submit" class="btn btn-primary btn1" name="submit" value="Daten speichern"/>



</form>
</div>
</div>
<div class="col span_1_of_3">
<h3>Hinweis</h3>
Wenn die E-Mail-Adresse geändert werden soll, ist unbedingt die EIngabe eines Passwortes erforderlich, da aus Sicherheitsgründen das Passwort mit der E-Mail-Adresse zusammenhängend und nicht als Klartext gespeichert wird. Das Passwort kann der User jederzeit in seinem internen Bereich ändern.
</div>
<div class="clr"/></div>
