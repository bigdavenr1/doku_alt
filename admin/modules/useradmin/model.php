<?php
namespace admin\modules\useradmin;
class Model extends \inc\Basicdb
{
    // Konstruktor
    ////////////////////////////////////////////////////////////////////////

    function __construct($anzahl = false)
    {
        global $sql;
        $this->anzahl = $anzahl;
        $this->table="kta_users";
        $this->countvar=$this->table.'.id';
        $this->queryVars=(object) array('table' => $this->table, 'countVar'=>$this->countvar, 'cps'=>15, 'offsetname'=>'usite');

        parent::__construct();
    }
	function getUserById($id)
	{
	    $queryVars= clone $this->queryVars;
        $queryVars->joins=array(
            (object)
            array('type'=>'LEFT',
                  'table'=>'kta_usergroups a',
                  'clause'=>'a.id='.$this->table.'.usertype'
            )
        );

        $queryVars->fields=array($this->table.".*",
                                 'a.name usertypename', 'BIN('.$this->table.".active) active");

        $queryVars->clause = $this->table.".id= :userId";
        $queryVars->preparedVars=array(':userId'=>intval($id));
        unset($queryVars->cps);
        unset($queryVars->offsetname);

	    parent::createQuery($queryVars);

	}

    public function updateOrNewUser($request,$user=false)
	{
		$queryVars = clone $this->queryVars;
		$queryVars->data = array("given_name"=>$request['given_name'],
	                          "surname"=>$request['surname'],
	                          "street1"=>$request['street1'],
	                          "street2"=>$request['street2'],
	                              "zip"=>$request['zip'],
	                             "city"=>$request['city'],
	                             "username"=>$request['username'],
	                             "usertype"=>$request['usertype'],
	                             "active"=>array('plainSQL'=>"b'".$request['active']."'"),
	                              "tel"=>$request['tel']);
        if (!empty($request['pass']))
            $queryVars->data['pass']=$this->func->bcrypt_encode($request['email'],$request['pass']);
		if ($request['email'] != $user->email):
		    $queryVars->data["email"]=$request['email'];
            $queryVars->data["pass"]=$this->func->bcrypt_encode($request['email'],$request['pass']);
	    endif;

		if (is_object($user)):
			$queryVars->clause="id=:ID";
			$queryVars->preparedVars=array(':ID'=>$user->id);
			return parent::update($queryVars);
		else:
			return parent::write($queryVars);
		endif;

	}

}

?>