<div id="artikel">
    <h1>User löschen - Bestätigung</h1>
    Wollen Sie den User (<?php echo strip_tags(htmlspecialchars(stripslashes($this->_['user']->username))) ?>) <b>"<?php echo strip_tags(htmlspecialchars(stripslashes($this->_['user']->surname))) ?> <?php echo strip_tags(htmlspecialchars(stripslashes($this->_['user']->given_name))) ?>" </b>
    aus <?php echo strip_tags(htmlspecialchars(stripslashes($this->_['user']->zip))). " " .strip_tags(htmlspecialchars(stripslashes($this->_['user']->city)))?>
    wirklich löschen?
    <form action="#" method="post">
        <?php echo $this->link->makeLink('<b>[ JA ]</b>',WEBDIR."admin/useradmin.htm?confirmed=yes&amp;view=delete&amp;userid=".$this->_['user']->id,"adminlink");?>
        <?php echo $this->link->makeLink('<b>[ NEIN ]</b>',WEBDIR."admin/useradmin.htm","adminlink"); ?>
    </form>
</div>


