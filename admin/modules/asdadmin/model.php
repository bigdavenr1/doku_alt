<?php
namespace admin\modules\asdadmin;
class Model extends \inc\Basicdb
{
    use ModulConfig;
    // Konstruktor
    ////////////////////////////////////////////////////////////////////////

    function __construct($anzahl = false)
    {
        global $sql;
        $this->anzahl = $anzahl;
        $this->queryVars=(object) array('table' => self::$table, 'countVar'=>$this->countvar, 'cps'=>15, 'offsetname'=>'usite');

        parent::__construct();
    }
	function getAllASDs($request=false)
    {



        $queryVars=clone $this->queryVars;
		$queryVars->fields=array(
		                          self::$table.".".self::$id,
		                          self::$table.".".self::$name,
		                          self::$table.".".self::$abteilung,
		                          self::$table.".".self::$stadt
                                 );
                                    
        $queryVars->order = (object) array('field' => $request['sort'], 'direction' => $request['rtg']);
            
        parent::createQuery($queryVars);
		return $this->liste;
    }

	function getAsdById($id)
	{
	    $queryVars=clone $this->queryVars;
       
        $queryVars->fields=array(
                                  self::$table.".".self::$id,
                                  self::$table.".".self::$name,
                                  self::$table.".".self::$abteilung,
                                  self::$table.".".self::$stadt
                                      );

        $queryVars->clause = self::$table.".".self::$id."= :asdId";
        $queryVars->preparedVars=array(':asdId'=>intval($id));
        unset($queryVars->cps);
        unset($queryVars->offsetname);

	    parent::createQuery($queryVars);
		return parent::getElement();


	    
	}

    public function updateOrNewAsd($request,$asd=false)
	{
	    
		$queryVars = clone $this->queryVars;
		$queryVars->data = array(self::$stadt  => $request['stadt'],
	                          self::$abteilung => $request['abteilung'],
	                          self::$name      => $request['name']);


		if (is_object($asd)):
			$queryVars->clause=self::$id."=:ID";
			$queryVars->preparedVars=array(':ID'=>$asd->id);
			return parent::update($queryVars);
		else:
			parent::write($queryVars);
			return $this->lastid;
		endif;

	}
    
    public function deleteById($id)
    {
        $queryVars = clone $this->queryVars;    
        $queryVars->clause = "id = :ID";
        $queryVars->preparedVars = [":ID" => intval($id)];
        return parent::deleteElement($queryVars);
    }

}

?>