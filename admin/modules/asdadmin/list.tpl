<div class="products">
<div class="top-box">

    <h2>ASDs Liste</h2>
    <hr>
    <p class="desc">Übersicht über alle Asd. Asd können direkt bearbeitet werden</p>
    <br/><br/>
    <?php echo  $this->link->makeLink($icon_neu_small." Neuen Asd einpflegen",WEBDIR."admin/asdadmin.htm?view=edit","adminlink")?>
    <table style="width: 98%">
    <tr>
        <?php
          echo $this->func->tableHeadSort("Name","name");
          echo $this->func->tableHeadSort("Abteilung","abteilung");
          echo $this->func->tableHeadSort("Stadt","stadt");
          
          echo $this->func->tableHead("Admin","","","width:40px;");
        ?>
    </tr>
    <?php
    if (!empty($this->_['asds'])):
        // Studiodaten abrufen
        foreach($this->_['asds'] as $key => $asd):
        // Ausgabe

            ?>
            <tr class="td<?php echo $x%2?>">
                <td>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($asd->name)))?>
                </td>
                <td>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($asd->abteilung)))?>
                </td>
                <td>
                    <?php echo strip_tags(htmlspecialchars(stripslashes($asd->stadt)))?>
                </td>
                
                <td  style="text-align:center">

                    <?php echo  $this->link->makeLink('<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />',WEBDIR.'admin/asdadmin.htm?view=delete&amp;id='.$asd->id,"adminlink");?>
                    <?php echo  $this->link->makeLink('<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />',WEBDIR."admin/asdadmin.htm?id=".$asd->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$request['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
                </td>
            </tr>

         <?php
         endforeach;
         ?></table><br/><?php
         echo $this->_['htmlnavi'];
     else:
        ?> </table><br/>
        <?php echo $this->_['htmlnavi'];
         
     endif;
     ?>

<br/><br/>
<?php echo  $this->link->makeLink($icon_neu_small." Neuen Asd einpflegen",WEBDIR."admin/asdadmin.htm?view=edit","adminlink")?>
</div>
</div>