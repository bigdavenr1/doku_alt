<?php // Modulview und Modulmodel einbinden
include("view.php");
require_once("model.php");
class Modulcontroller extends Modulview
{
    // private VAriablen setzen
    private $request = null;
    private $modultemplate = 'anzeige';
    private $modulview = null;
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->modulview = new Modulview();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->modultemplate =isset($request['view'])?$this->request['view']:'anzeige';
    }
	public function display()
    {
    	$this->request['regionid']=empty($this->request['regionid'])?0:$this->request['regionid'];
    	// Benutzerdaten aus Datenbank holen
		$this->regionfac=new region(25);
		switch($this->request['regionid']):
			case '0':
                if ($this->modultemplate=='edit')
					$this->editAndDelete($this->request['id'],'new');
				$this->modulview->setTemplate($this->modultemplate);
				$this->regionfac->getAll();
				$this->modulview->assign("regions",$this->regionfac->liste);
				$this->modulview->assign("link",$this->l);
                $this->modulview->assign("htmlNav",$this->regionfac->getHtmlNavi('std',$_SERVER['QUERY_STRING']=""));
				break;
			default:
				$this->modultemplate =isset($this->request['view'])?$this->request['view']:'edit';
				$this->modulview->setTemplate($this->modultemplate);
				$this->modulview->assign("link",$this->l);
				$this->regionfac->getRegionById(intval($this->request['regionid']));
				$this->region=$this->regionfac->getElement();
				// Prüfung ob gelöscht oder bearbeitet werden soll.
				$this->editAndDelete($this->region->id);		
			break;	
		endswitch;
        // modultemplate setzen
		$this->assignvalues["keywords"] = "jhgklJ";
		// Codes übergeben
        $this->modulview->assign("region",$this->region);
		$this->modulview->assign('func', $this->func);
         // Template laden, füllen und ausgeben
        return $this->modulview->loadTemplate();
	}
	private function changeData($modus)
	{
        if (empty($modus)):
			// Studiodaten in Datenbank speichern
	    	$this->regionfac->update("name='".mysql_quote($this->request['name'])."',
	                                 state='".mysql_quote($this->request['state_val'])."'",
	                                 'id',$this->region->id);
		else:
			$artikel[]="";
            $artikel[]=$this->request['state_val'];
			$artikel[]=$this->request['name'];
			// Datensatz schreibene
			$this->regionfac->write($artikel);
		endif;
		$_SESSION['msg']="Region erfolgreich angelegt!";	
	}
	private function editAndDelete($id,$modus=false)
	{
		if ($this->modultemplate=='edit' && !empty($this->request['submit'])):
			$this->changeData($modus);
			$_SESSION['msg']='Region erfolgreich'.(empty($modus)?'geändert':'angelegt');
			header('Location:'.WEBDIR.'admin/regionadmin.htm');
        elseif ($this->modultemplate=='delete' && !empty($this->request['submit'])):
        	$this->regionfac->deleteElement('id',$id);
			$_SESSION['msg']='Region erfolgreich gelöscht!';
			header('Location:'.WEBDIR.'admin/regionadmin.htm');
		endif;
	}
}
?>