<h1>Region <?php echo empty($this->_['region'])?'neu anlegen':'ändern';?></h1>
<b>Sie haben die Möglichkeit mit dem nachfolgenden Formularen die Regiondaten zu ändern.</b><br/><br/>
<form action="#" method="post" enctype="multipart/form-data">
    <fieldset class="td0">
        <legend>
            Regiondatendaten
        </legend>
        <label for="name">Name (englisch)</label><br/>
        <input type="text" name="name" id="name" value="<?php echo $this->_['region']->name;?>" /><br class="clr"/>
        <label for="state_val">Land</label><br/>
        <input type="text" id="country" name="country" class="ajax request_getcountry" value="<?php echo htmlspecialchars(strip_tags(stripslashes($this->_['region']->countryname)))?>"/><br/>
        <input type="hidden" id="country_val" name="country_val" class="" value="<?php echo intval($this->_['region']->country)?>"/><br class="clr"/>
        <label for="state_val">Bundesland / State</label><br/>
        <input type="text" id="state" name="state" class="ajax request_getstate require_country" value="<?php echo htmlspecialchars(strip_tags(stripslashes($this->_['region']->statename)))?>"/><br/>
        <input type="hidden" id="state_val" name="state_val" class="" value="<?php echo intval($this->_['region']->state)?>"/><br class="clr"/>
        <br class="clr"/>
    </fieldset>
    <input type="submit" name="submit" id="submit" value="Region <?php echo empty($this->_['region'])?'neu anlegen':'ändern';?>" />
</form>
