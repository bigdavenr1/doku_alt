<h1>Regionen pflegen</h1>
<table style="width:800px;">
<tr>
	<?php echo $this->_['func']->tableHeadSort('id','id',"","","width:40px;");
	echo $this->_['func']->tableHeadSort('Name (engl.)','name',"","","");
	echo $this->_['func']->tableHeadSort('Bundesland','state',"","","width:70px;");
	echo $this->_['func']->tableHead('Admin',"","","width:60px;")?>
</tr>

<?php
if (!empty($this->_['regions'])):
    // Studiodaten abrufen
    foreach($this->_['regions'] as $key => $region):
    // Ausgabe
        ?>
        <tr class="td<?php echo $x%2?>">
            <td>
                <?php echo strip_tags(htmlspecialchars(stripslashes($region->id)))?>
            </td>
            <td>
            	<?php echo strip_tags(htmlspecialchars(stripslashes($region->name)))?>
            </td>
            <td>
            	<?php echo strip_tags(htmlspecialchars(stripslashes($region->statename)))?>
            </td>
            <td  style="text-align:center">              
                <?php echo  $this->_['link']->makeLink('<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />',WEBDIR.'admin/regionadmin.htm?view=delete&amp;regionid='.$region->id,"adminlink");?>
                <?php echo  $this->_['link']->makeLink('<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />',WEBDIR."admin/regionadmin.htm?regionid=".$region->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&regionid=".$_GET['regionid'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
            </td>
        </tr>
    <?php
    endforeach;
endif;
 ?>
</table><br/>
 <?php
 echo $this->_['htmlNav']; 
 ?>	
<br/><br/>
<?php echo  $this->_['link']->makeLink($icon_neu_small." Neue Region einpflegen",WEBDIR."admin/regionadmin.htm?view=edit","adminlink")?>