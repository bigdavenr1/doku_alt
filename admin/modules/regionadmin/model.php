<?php
if (!class_exists("Region"))
{
    class Region extends Basicdb
    {
        // Konstruktor
        ////////////////////////////////////////////////////////////////////////

        function __construct($anzahl = false)
        {
            global $sql;
            $this->anzahl = $anzahl;
            
            $this->table="kta_regions";
            parent::__construct();
            $this->countvar=$this->table.".id";
        }
		
		function getRegionById($id)
		{
			$this->felder=$this->table.".*, a.name statename, b.name countryname, b.id country";
             
            $query="LEFT JOIN kta_states a ON (a.id=".$this->table.".state)";
            $query.="LEFT JOIN kta_countries b ON (b.id=a.country)";
            parent::createQuery($query." WHERE ".$this->table.".id='".mysql_quote($id)."'");
        }
        function getAll()
        {
            $this->felder=$this->table.".*, a.name statename";
            $query="LEFT JOIN kta_states a ON (a.id=".$this->table.".state)";
            parent::createQuery($query);
        }
    }
}
?>