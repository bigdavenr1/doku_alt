<?php // Modulview und Modulmodel einbinden
include("view.php");
require_once("model.php");
class Modulcontroller extends Modulview
{
    // private VAriablen setzen
    private $request = null;
    private $modultemplate = 'anzeige';
    private $modulview = null;
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->modulview = new Modulview();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->modultemplate =isset($request['view'])?$this->request['view']:'anzeige';
    }
	public function display()
    {
    	$this->request['languageid']=empty($this->request['languageid'])?0:$this->request['languageid'];
    	// Benutzerdaten aus Datenbank holen
		$this->languagefac=new Language(25);
		switch($this->request['languageid']):
			case '0':
                if ($this->modultemplate=='edit')
					$this->editAndDelete($this->request['id'],'new');
				$this->modulview->setTemplate($this->modultemplate);
				$this->languagefac->getAll();
				$this->modulview->assign("languages",$this->languagefac->liste);
				$this->modulview->assign("link",$this->l);
                $this->modulview->assign("htmlNav",$this->languagefac->getHtmlNavi('std',$_SERVER['QUERY_STRING']=""));
				break;
			default:
				$this->modultemplate =isset($this->request['view'])?$this->request['view']:'edit';
				$this->modulview->setTemplate($this->modultemplate);
				$this->modulview->assign("link",$this->l);
				$this->languagefac->getById(intval($this->request['languageid']));
				$this->language=$this->languagefac->getElement();
				// Prüfung ob gelöscht oder bearbeitet werden soll.
				$this->editAndDelete($this->language->id);		
			break;	
		endswitch;
        // modultemplate setzen
		$this->assignvalues["keywords"] = "jhgklJ";
		// Codes übergeben
        $this->modulview->assign("language",$this->language);
		$this->modulview->assign('func', $this->func);
         // Template laden, füllen und ausgeben
        return $this->modulview->loadTemplate();
	}
	private function changeData($modus)
	{
        if (empty($modus)):
			// Studiodaten in Datenbank speichern
	    	$this->languagefac->update("language='".mysql_quote($this->request['name'])."',
	                                 code='".mysql_quote($this->request['code'])."'",
	                                 'id',$this->language->id);
		else:
			$artikel[]="";
			$artikel[]=$this->request['name'];
			$artikel[]=$this->request['code'];
			// Datensatz schreibene
			$this->languagefac->write($artikel);
		endif;
		$_SESSION['msg']="Sprache erfolgreich angelegt!";	
	}
	private function editAndDelete($id,$modus=false)
	{
		if ($this->modultemplate=='edit' && !empty($this->request['submit'])):
			$this->changeData($modus);
			$_SESSION['msg']='Sprache erfolgreich'.(empty($modus)?'geändert':'angelegt');
			header('Location:'.WEBDIR.'admin/languageadmin.htm');
        elseif ($this->modultemplate=='delete' && !empty($this->request['submit'])):
        	$this->languagefac->deleteElement('id',$id);
			$_SESSION['msg']='Sprache erfolgreich gelöscht!';
			header('Location:'.WEBDIR.'admin/languageadmin.htm');
		endif;
	}
}
?>