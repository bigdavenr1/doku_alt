<h1>Sprache <?php echo empty($this->_['language'])?'neu anlegen':'ändern';?></h1>
<b>Sie haben die Möglichkeit mit dem nachfolgenden Formularen die Sprachdaten zu ändern.</b><br/><br/>
<form action="#" method="post" enctype="multipart/form-data">
    <fieldset class="td0">
        <legend>
            Sprachendaten
        </legend>
        <label for="name">Name (englisch)</label>
        <input type="text" name="name" id="name" value="<?php echo $this->_['language']->language;?>" /><br class="clr"/>
        <label for="code">Code</label>
        <input type="text" name="code" id="code" value="<?php echo $this->_['language']->code;?>" /><br class="clr"/>
        <br class="clr"/>
    </fieldset>
    <input type="submit" name="submit" id="submit" value="Sprache <?php echo empty($this->_['language'])?'neu anlegen':'ändern';?>" />
</form>
