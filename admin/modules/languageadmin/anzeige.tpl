<h1>Sprachen pflegen</h1>
<table style="width:800px;">
<tr>
	<?php echo $this->_['func']->tableHeadSort('id','id',"","","width:40px;");
	echo $this->_['func']->tableHeadSort('Name (engl.)','name',"","","");
	echo $this->_['func']->tableHeadSort('Code','code',"","","width:70px;");
	echo $this->_['func']->tableHead('Admin',"","","width:60px;")?>
</tr>

<?php
// Studiodaten abrufen
foreach($this->_['languages'] as $key => $language):
// Ausgabe

    ?>
    <tr class="td<?php echo $x%2?>">
        <td>
            <?php echo strip_tags(htmlspecialchars(stripslashes($language->id)))?>
        </td>
        <td>
        	<?php echo strip_tags(htmlspecialchars(stripslashes($language->language)))?>
        </td>
        <td>
        	<?php echo strip_tags(htmlspecialchars(stripslashes($language->code)))?>
        </td>
        <td  style="text-align:center">
          
            <?php echo  $this->_['link']->makeLink('<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />',WEBDIR.'admin/languageadmin.htm?view=delete&amp;languageid='.$language->id,"adminlink");?>
            <?php echo  $this->_['link']->makeLink('<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />',WEBDIR."admin/languageadmin.htm?languageid=".$language->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&languageid=".$_GET['languageid'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
        </td>
    </tr>
 <?php
 endforeach;
 ?>
</table><br/>
 <?php
 echo $this->_['htmlNav']; 
 ?>
		
<br/><br/>
<?php echo  $this->_['link']->makeLink($icon_neu_small." Neue Sprache einpflegen",WEBDIR."admin/languageadmin.htm?view=edit","adminlink")?>