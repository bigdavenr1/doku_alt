<h1>Newsartikel pflegen</h1>
<table style="width:610px">
<tr>
    <?php echo $this->func->tableHead('bild',"width:60px;");
    echo $this->func->tableHeadSort('Titel','title',"","","");
    echo $this->func->tableHeadSort('Datum','date',"","","width:70px;");
    echo $this->func->tableHead('Admin',"","","width:60px;")?>
</tr>
<?php
foreach($this->_['recordlist'] as $key => $news):
    ?>
    <tr class="td<?php echo $x%2?>">
        <td>
            <?php
            $picture=$this->func->checkPictureAndGetInformations('images/news/',$news->image);         
            if ($picture->error==0): ?>
                <a href="<?php echo $picture->thumbbig?>" class="picture"><img src="<?php echo $picture->thumb?>"/></a>
            <?php 
            else: 
                echo "keins";
            endif;?>
        </td>
        <td>
            <?php echo strip_tags(htmlspecialchars(stripslashes($news->title)))?>
        </td>
        <td>
            <?php echo strip_tags(htmlspecialchars(stripslashes($news->ger_date)))?>
        </td>
        <td  style="text-align:center">
            <?php echo $this->link->makeLink('<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />',WEBDIR.'admin/newsadmin.htm?view=delete&amp;recordid='.$news->id,"adminlink");?>
            <?php echo $this->link->makeLink('<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />',WEBDIR."admin/newsadmin.htm?recordid=".$news->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&recordid=".$_GET['recordid'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
        </td>
    </tr>
    <?php
endforeach;
?>
</table><br/>
<?php echo $this->_['htmlNav'];?>      
<br/><br/>
<?php echo  $this->link->makeLink($icon_neu_small." Neuen Newsartikel einpflegen",WEBDIR."admin/newsadmin.htm?view=edit","adminlink")?>