<?php
if (!class_exists("Newsadmin"))
{
    class Newsadmin extends Basicdb
    {
        // Konstruktor
        ////////////////////////////////////////////////////////////////////////

        function __construct($anzahl = false)
        {
            global $sql;
            $this->anzahl = $anzahl;
            
            $this->table="kta_news";
            parent::__construct();
            $this->countvar=$this->table.".id";
        }
        
    }
}
?>