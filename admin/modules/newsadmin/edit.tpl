<h1>Newsartikel <?php echo !empty($this->_['record'])?'bearbeiten':'erstellen'?></h1>
<form action="" method="post">
    <fieldset>
        <legend>Text</legend>
        <?php echo $this->func->createInput("Titel *", "titel", $this->_["record"]->title) ?>
        <br/>
        <label for="text">Newsartikeltext *</label>
        <br/>
        <?php //echo $this->_['spaw']->show()?>
        <script src="/inc/ckeditor/ckeditor.js"></script>
        <textarea id="text" name="text"><?php echo stripslashes($this->_['record']->text);?></textarea>
        <script type="text/javascript">
             CKEDITOR.replace( 'text' );
        </script>
    </fieldset>
    <fieldset>
        <legend>Vorschaubilddaten</legend>
        <div id="newsimgbox">
        <?php
            $picture=$this->func->checkPictureAndGetInformations('images/news/',$this->_["record"]->image);      
            if ($picture->error==0): ?>
                <a href="<?php echo $picture->thumbbig?>" class="picture"><img src="<?php echo $picture->thumbbig?>"/></a>
            <?php 
            else: 
                echo "kein Vorschaubild";
            endif;
        ?>
        </div>
        <br/>
        <input type="checkbox" name="delimg" class="delimg"/><label for="newsimg_val" >Vorschaubild komplett löschen</label>
            <br class="clr"/>
            <a href="javascript:void(0)" id="newsimg" class="upload_newsimg once_newsimg_val">Neues Vorschaubild hochladen</a>
        <input type="hidden" id="newsimg_old" name="newsimg_old" value="<?php echo htmlspecialchars(strip_tags(stripslashes($this->_['record']->image)))?>"/>     
        <input type="hidden" id="newsimg_new" name="newsimg_new" />   
        <input type="hidden" id="newsimg_val" name="newsimg_val" value="<?php echo htmlspecialchars(strip_tags(stripslashes($this->_['record']->image)))?>"/>
    </fieldset>
    <fieldset>
        <legend>sonstige Einstelllungen</legend>
        <?php $a=ord($this->_['record']->active); $p=ord($this->_['record']->previewpic);?>
         <input type="checkbox" name="active" <?php echo !empty($a)?'checked="checked"':''?>/>
        <label for="active">Artikel freischalten</label><br/>
         <input type="checkbox" name="previewpic" <?php echo !empty($p)?'checked="checked"':''?>/>
        <label for="previewpic">Vorschaubild im Artikel anzeigen?</label>
    </fieldset>
    <br/>
    <input type="submit" name="submit" value="Datensatz speichern"/>
</form>