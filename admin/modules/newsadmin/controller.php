<?php // Modulview und Modulmodel einbinden
require_once("model.php");
require_once(INCLUDEDIR."spaw2/spaw.inc.php");
class Modulcontroller extends View
{
    // private VAriablen setzen
    private $request = null;
    private $modultemplate = 'list';
    private $modulview = null;
    private $modulname = "news";
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->modulview = new View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->modultemplate =isset($request['view'])?$this->request['view']:'list';
        // Modulfactory erstellen 
        $this->modulclassname=ucfirst($this->modulname)."admin";
        $this->modelfac = new $this->modulclassname(25);
        $this->modelfac->felder="*, DATE_FORMAT(date,GET_FORMAT(DATE,'EUR')) ger_date";
        // Datensatzarray und oBject initialisieren
        $this->modelfac->liste = array();
        $this->record=(object) array();
    }
    public function display()
    {
        $this->request['recordid']=empty($this->request['recordid'])?0:$this->request['recordid'];
        switch($this->request['recordid']):
            case '0':
                if ($this->modultemplate=='edit'):
                    $this->editAndDelete($this->request['recordid'],'new');
                    $this->spaw=new SpawEditor("text","");
                    $this->spaw->width="100%";
                    $this->spaw->height="400px";
                    $this->modulview->assign('spaw', $this->spaw);
                else:
                    $this->modelfac->getAll();
                    $this->modulview->assign("recordlist",$this->modelfac->liste);
                    $this->modulview->assign("htmlNav",$this->modelfac->getHtmlNavi());
                endif;
                break;
            default:
                $this->modultemplate =isset($this->request['view'])?$this->request['view']:'edit';
                $this->modelfac->getById(intval($this->request['recordid']));
                $this->record=$this->modelfac->getElement();
                $this->modulview->assign("record",$this->record);
                $this->spaw=new SpawEditor("text",stripslashes($this->record->text));
                $this->spaw->width="100%";
                $this->spaw->height="400px";
                $this->modulview->assign('spaw', $this->spaw);
                // Prüfung ob gelöscht oder bearbeitet werden soll.
                $this->editAndDelete($this->record->id);       
                break;  
        endswitch;
        $this->modulview->setTemplate($this->modultemplate,dirname(__FILE__));
        // modultemplate setzen
        $this->assignvalues["keywords"] = "";
        // Template laden, füllen und ausgeben
        return $this->modulview->loadTemplate();
    }
    private function changeData($modus)
    {
        if (empty($modus)):
            // Studiodaten in Datenbank speichern
            $this->modelfac->update(array("title"=>$this->request['titel'],
                                            "alias"=>$this->func->convertUmlaut($this->request['titel']),
                                            "text"=>$this->request['text'],
                                            "image"=>$this->request['newsimg_val'],
                                            "active"=>!empty($this->request['active'])?chr(1):chr(0),
                                            "previewpic"=>!empty($this->request['previewpic'])?chr(1):chr(0)),
                                      'id',$this->record->id);
                                      $this->moveImage($this->request['newsimg_val']);
                                      if($this->request['newsimg_val']!=$this->record->image  && !empty($this->record->image) ):
                                            $this->deleteImage($this->record->image);
                                      endif;
        else:
            $artikel=array("title"=>$this->request['titel'],
                           "alias"=>$this->func->convertUmlaut($this->request['titel']),
                           "text"=>$this->request['text'],
                           "image"=>$this->request['newsimg_val'],
                           "active"=>!empty($this->request['active'])?chr(1):chr(0),
                           "previewpic"=>!empty($this->request['previewpic'])?chr(1):chr(0));
                           $this->moveImage($this->request['newsimg_val']);
            // Datensatz schreibene
            $this->modelfac->write($artikel);
        endif;
        $_SESSION['msg']="Datensatz erfolgreich angelegt!";  
    }
    private function editAndDelete($id,$modus=false)
    {
        if ($this->modultemplate=='edit' && !empty($this->request['submit'])):             
            $this->changeData($modus);
            $_SESSION['msg']='Datensatz erfolgreich'.(empty($modus)?'geändert':'angelegt');
            header('Location:'.WEBDIR.'admin/'.$this->modulname.'admin.htm');
        elseif ($this->modultemplate=='delete' && !empty($this->request['submit'])):
            $this->deleteImage($this->record->image);
            $this->modelfac->deleteElement('id',$this->record->id);
            $_SESSION['msg']='Datensatz erfolgreich gelöscht!';
            header('Location:'.WEBDIR.'admin/'.$this->modulname.'admin.htm');
        endif;
    }
    //TODO in Func auslagern 
    private function deleteImage($img)
    {
        if (empty($img)) return false;
        $r = glob(LOCALDIR.'images/news/'.$img.'*.*');
        if ($r !== false) foreach ($r as $file):
            unlink($file);
        endforeach;
    }
    
    private function moveImage($img)
    {
        if (empty($img)) return false;
        $r = glob(LOCALDIR.'temporary_files/'.$img.'*.*');
        if ($r !== false) foreach ($r as $file):
            rename($file, LOCALDIR.'images/news/'.pathinfo($file, PATHINFO_BASENAME));
        endforeach;
    }
}
?>