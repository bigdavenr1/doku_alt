
<div class="col span_2_of_3">
<h3>Notizen <?php echo empty($this->_['notiz'])?'neu anlegen':'ändern';?></h3>
<hr>
<p class="desc">Sie haben die Möglichkeit mit dem nachfolgenden Formularen die Notizen zu ändern.<br/>* markierte Felder sind Pflichtfelder!</p>

<div class="contact-form">
<br/><br/>
<form action="" method="post" style="padding-right:0px;margin-right:0px;">
<fieldset>
    <label for="klient">Klienten</label>
        <select name="klient_id">
            <option value="0">Alle</option>
    <?php
    // alle Klienten zu auswählen
    foreach($this->_['klienten'] as $key => $klient):
        if($klient->id == $this->_['notiz']->klient_id):

    ?>
              <option value="<?php echo $this->func->sanitize($klient->id)?>" selected ><?php echo $this->func->sanitize($klient->surname).", ".$this->func->sanitize($klient->given_name)?></option>
    <?php
        else:
            echo "klient-id ist : ".$this->_['notiz']->klient_id;
    ?>
              <option value="<?php echo $this->func->sanitize($klient->id)?>"><?php echo $this->func->sanitize($klient->surname).", ".$this->func->sanitize($klient->given_name)?></option>
    <?php endif; endforeach; ?>
    </select>
   <label>Text :</label>

    <textarea placeholder="Notiz" id="notiz" name="notiz" class="<?php echo isset($this->_['valErrors']['notiz'])?'validate_error':''?> validate_required" /><?php echo $this->func->sanitize(!empty($request['notiz'])?$request['notiz']:$this->_['notiz']->notiz); ?></textarea>
    <br/>
    <br class="clr"/><br/><br/>
</fieldset>
<?php if (!empty($this->request['id'])):?>
<fieldset>



</fieldset>
<?php endif;?>

<br class="clear" />
<br class="clr" style="clear:both;float:none;"/>

<input type="submit" class="btn btn-primary btn1" name="submit" value="Daten speichern"/>



</form>
</div>
</div>
<div class="col span_1_of_3">
<h3>Hinweis</h3>

Ein Hinweis zu schreiben später!
<br/><br/>

</div>
<div class="clr"/></div>
