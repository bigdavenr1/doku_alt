<?php
namespace admin\modules\notizadmin;
trait ModulConfig
{
    /**
     * Name der SQL-Tabelle
     */
    protected static $table = ['k' => "kta_klienten",
                                'u' => "kta_users",
                               'n' => "notizen",
                               'kha' => "klient_hilfeart_asd",
                               'h' => "hilfeart",
                               'a' => "asd"
                               ];
    
    
    /**
     * Feld id
     */
    protected static $id = 'id';
    /**
     * Feld given_name
     */
    protected static $given_name = 'given_name';
    /**
     * Feld surname
     */
    protected static $surname = 'surname';
    /**
     * Feld name
     */
    protected static $name = 'name';
    /**
     * Feld klient-id
     */
    protected static $klient_id = 'klient_id';
    
    
    //die Felder der Tabelle 'kta_klienten'------------------------------------------
    
    
    /**
     * Feld az
     */
    protected static $az = 'az';
    /**
     * Feld geb
     */
    protected static $geb = 'geb';
    
    
    
    
    // die Felder der Tabelle 'kta_users'------------------------------------------
    
    
    
    /**
     * Feld username
     */
    protected static $username = 'username';
    
    
    //die Felder der Tabelle 'notizen'------------------------------------------
    
    
    /**
     * Feld notiz
     */
    protected static $notiz = 'notiz';
    /**
     * Feld datum
     */
    protected static $datum = 'datum';
    /**
     * Feld user-id
     */
    protected static $user_id = 'user_id';
    
    
    //die Felder der Tabelle 'hilfeart'------------------------------------------
    
    //die Felder der Tabelle 'klient-hilfeart-asd'------------------------------------------
    
    
    /**
     * Feld hilfeart-id
     */
    protected static $hilfeart_id = 'hilfeart_id';
    /**
     * Feld asd-id
     */
    protected static $asd_id = 'asd_id';
    
    
    
    //die Felder der Tabelle 'hilfeart'------------------------------------------
        
    
        
    //die Felder der Tabelle 'asd'------------------------------------------


}