<?php // view und Modulmodel einbinden
namespace admin\modules\notizadmin;
require_once("model.php");
class Controller
{
    // private VAriablen setzen
    private $request = null;
    private $template = 'list';
    private $view = null;
    /**
	 * Konstruktur
	 * bereitet Template und übergebene Entscheidungsvariablen auf
	 * @param Array $request Array mit übergebenen Variablen (GET POST FILE)
	 */
    public function __construct($request)
    {
        // neuen viewer instanziieren
        $this->view = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // template auslesen
        $this->template =isset($this->request['view'])?$this->request['view']:'list';
		// Modelobject
		$this->model = new Model();
        if(isset($this->request['submit1'])):
            // Klientfilterwert speichern oder löschen
            if(!empty($this->request['klient']))
                $_SESSION['nsearch']['k']=$this->request['klient'];
            else
                unset($_SESSION['nsearch']['k']);
            // Hilfeartfilterwert speichern oder löschen
            if(!empty($this->request['hilfeart']))
                $_SESSION['nsearch']['h']=$this->request['hilfeart'];
            else
                unset($_SESSION['nsearch']['h']);
            // ASDfilterwert speichern oder löschen
            if(!empty($this->request['asd']))
                $_SESSION['nsearch']['a']=$this->request['asd'];
            else
                unset($_SESSION['nsearch']['a']);
            // Userfilterwert speichern oder löschen
            if(!empty($this->request['user']))
                $_SESSION['nsearch']['u']=$this->request['user'];
            else
                unset($_SESSION['nsearch']['u']);
        endif;
        if(!isset($this->request['usite']) && !isset($this->request['submit1']))
            unset($_SESSION['nsearch']);
    }
	public function display()
    {
    	// prüfen und aufbereiten ob Asd ID übergeben wurde
    	$this->request['id'] = empty( $this->request['id'] ) ? 0 : $this->request['id'];
        //explodierte String aus request['id']
        $expl = explode("--", $this->request['id']);
		// Anhand übergebener ID entscheiden
		switch($this->request['id']):
			// Wenn keine Asd id übergeben wurde  (list oder new)
			case 0:
				// Wenn neuer DAtensatz
				if ($this->template=='edit'):
					$this->editOrDelete($id,'new');

		            // Wenn Liste
				else:
                    //Achtung : diese Linie ist doppel**************************************

					$this->view->assign("notizen",$this->model->getAllNotizen($this->request));
                	$this->view->assign("htmlnavi",$this->model->getHtmlNavi('std',preg_replace('~(\&{0,1})res='.$this->request['res'].'~','',preg_replace("~(\&{0,1})modul=".$this->request['modul']."~","",$_SERVER['QUERY_STRING']))));

				endif;
				break;
			// Wenn ID übergeben wurde (edit oder delete)
			default:
				// Template auf edit, wenn nicht gelöscht werden soll
				$this->template =isset($this->request['view'])?$this->request['view']:'edit';
                // notiz daten Global zu verfügung stellen

				$this->view->assign("notiz",$this->notiz = $this->model->getNotizById($expl));

				// Prüfung ob gelöscht oder bearbeitet werden soll.
				$this->editOrDelete($this->notiz->id);
				break;
		endswitch;
        $this->view->assign('klienten', $test=$this->model->getAll('kta_klienten',false,false));
        $this->view->assign('hilfearten', $this->model->getAll('hilfeart',false,false));
        $this->view->assign('asds', $this->model->getAll('asd',false,false));
        $this->view->assign('users', $this->model->getAll('kta_users',false,false));

        $this->view->setTemplate($this->template,dirname(__FILE__));
        return $this->view->loadTemplate();
	}

	private function changeData($modus)
	{
        return $this->model->updateOrNewNotiz($this->request,$this->notiz);
	}

	private function editOrDelete($id,$modus=false)
	{
		if ($this->template=='edit' && !empty($this->request['submit'])):
			$id = $this->changeData($modus);
			$_SESSION['msg']='Notiz erfolgreich'.(empty($modus)?'geändert':'angelegt');
			if ( empty($modus))
				header('Location:'.WEBDIR.'admin/notizadmin.htm');
			else
				header('Location:'.WEBDIR.'admin/notizadmin.htm?id='.$id);
		    exit;
        elseif ($this->template=='delete' && !empty($this->request['confirmed'])):
        	$this->model->deleteElement('id',$id);
            if ($this->user->usertype!=1):
			     $_SESSION['msg']='Notiz erfolgreich gelöscht!';
			     header('Location:'.WEBDIR.'admin/notizadmin.htm');
				 exit;
            endif;
		endif;
	}
}
?>