<?php
namespace admin\modules\notizadmin;
class Model extends \inc\Basicdb
{
    use ModulConfig;
    // Konstruktor
    ////////////////////////////////////////////////////////////////////////

    function __construct($anzahl = false)
    {
        global $sql;
        $this->anzahl = $anzahl;
        //$this->table = self::$table['n'];

        $this->queryVars=(object) array('table' => self::$table['n'], 'cps'=>15, 'offsetname'=>'usite');

        parent::__construct();
    }
    //$filter ist gesetzt um ein Ergebnis zu filtern
	function getAllNotizen($request=false)
    {
        $queryVars=clone $this->queryVars;

        $queryVars->preparedVars=array();
        if(!empty($_SESSION['nsearch']['k'])):
            $queryVars->clause .= (!empty($queryVars->clause)?" AND ":""). "kta_klienten.id = :KID ";
            $queryVars->preparedVars["KID"]= $_SESSION['nsearch']['k'];
        endif;
        if(!empty($_SESSION['nsearch']['h'])):

            $queryVars->clause .= (!empty($queryVars->clause)?" AND ":"")." hilfeart.id = :HID ";
            $queryVars->preparedVars["HID"]= $_SESSION['nsearch']['h'];
        endif;
        if(!empty($_SESSION['nsearch']['a'])):

            $queryVars->clause .= (!empty($queryVars->clause)?" AND ":"")." asd.id = :AID ";
            $queryVars->preparedVars["AID"] = $_SESSION['nsearch']['a'];
        endif;
        if(!empty($_SESSION['nsearch']['u'])):

            $queryVars->clause .= (!empty($queryVars->clause)?" AND ":"")." kta_users.id = :UID ";
            $queryVars->preparedVars["UID"] = $_SESSION['nsearch']['u'];
        endif;

        $queryVars->fields=array(self::$table['k'].'.'.self::$surname .' AS klient_surname',
                                 self::$table['k'].'.'.self::$given_name.' AS klient_given_name',
                                 self::$table['k'].'.'.self::$az,
                                 self::$table['n'].'.'.self::$klient_id,
                                 self::$table['n'].'.'.self::$user_id,
                                 self::$table['n'].'.'.self::$datum,
                                 "DATE_FORMAT(".self::$table['n'].'.'.self::$datum.", '%d.%m.%Y %H:%i:%S') AS datum_formated",
                                 self::$table['n'].'.'.self::$notiz,
                                 self::$table['u'].'.'.self::$given_name,
                                 self::$table['u'].'.'.self::$surname,

                                 );

        $queryVars->joins=array(
                                        (object)
                                        array('type'   => 'RIGHT',
                                              'table'  => self::$table['k'],
                                              'clause' => self::$table['k'].'.'.self::$id.' = '.self::$table['n'].'.'.self::$klient_id
                                        ),
                                        (object)
                                        array('type'   => 'LEFT',
                                              'table'  => self::$table['u'],
                                              'clause' => self::$table['u'].'.'.self::$id.' = '.self::$table['n'].'.'.self::$user_id
                                        ),
                                        (object)
							            array('type'   => 'LEFT',
							                  'table'  => self::$table['kha'],
							                  'clause' => self::$table['k'].'.'.self::$id.' = '.self::$table['kha'].'.'.self::$klient_id
							            ),
                                        (object)
							            array('type'   => 'LEFT',
							                  'table'  => self::$table['a'],
							                  'clause' => self::$table['a'].'.'.self::$id.' = '.self::$table['kha'].'.'.self::$asd_id
							            ),
							            (object)
							            array('type'   => 'LEFT',
							                  'table'  => self::$table['h'],
							                  'clause' => self::$table['h'].'.'.self::$id.' = '.self::$table['kha'].'.'.self::$hilfeart_id
							            ),
                                    );

        $queryVars->groupBy = self::$table['n'].".".self::$klient_id.",".self::$table['n'].".".self::$user_id.",".self::$table['n'].".".self::$datum;
        $queryVars->order = (object) array('field' => $request['sort'], 'direction' =>$request['rtg']);

        parent::createQuery($queryVars);
		return $this->liste;
    }
    /**
     * @param array $data Array mit Klient-ID [0], User-ID [1], Datum [2]
     */
	function getNotizById($data)//$kid, $uid, $datum)
	{
	    $queryVars=clone $this->queryVars;

        $queryVars->fields=array(self::$table['k'].'.'.self::$id,
                                 self::$table['k'].'.'.self::$surname,
                                 self::$table['k'].'.'.self::$given_name,
                                 self::$table['n'].'.'.self::$klient_id,
                                 self::$table['n'].'.'.self::$user_id,
                                 self::$table['n'].'.'.self::$notiz,
                                 self::$table['n'].'.'.self::$datum,
                                 "DATE_FORMAT(".self::$table['n'].'.'.self::$datum.", '%d.%m.%Y %H:%i:%S') AS datum_formated"

                                 );

        $queryVars->joins=array(
                                    (object)
                                    array('type'   => 'RIGHT',
                                          'table'  => self::$table['k'],
                                          'clause' => self::$table['k'].'.'.self::$id.' = '.self::$table['n'].'.'.self::$klient_id
                                    ),
                                    (object)
                                    array('type'   => 'LEFT',
                                          'table'  => self::$table['u'],
                                          'clause' => self::$table['u'].'.'.self::$id.' = '.self::$table['n'].'.'.self::$user_id
                                    )
                                );

  $queryVars->clause=self::$klient_id."=:KID AND ".self::$user_id ."= :UID AND ".self::$datum ."= :DATUM";
        $queryVars->preparedVars=array(':KID'=>$data[0], "UID"=>$data[1], ":DATUM"=>$data[2]);
        unset($queryVars->cps);
        unset($queryVars->offsetname);

	    parent::createQuery($queryVars);
		return parent::getElement();

	}

    public function updateOrNewNotiz($request,$notiz=false)
	{
        $data=explode("--",$request['id']);
		$queryVars = clone $this->queryVars;
		$queryVars->data = array(self::$klient_id=>(!empty($request['klient_id'])?$request['klient_id']:$data[0]),
                                 self::$notiz=>$request[self::$notiz],
                                 self::$user_id=>$_SESSION['user']->id
                                 );

        //wenn vorhandene Notiz ist => updaten
		if (is_object($notiz)):
			$queryVars->clause=self::$klient_id."=:KID AND ".self::$user_id ."= :UID AND ".self::$datum ."= :DATUM";
			$queryVars->preparedVars=array(':KID'=>$notiz->klient_id, "UID"=>$notiz->user_id, ":DATUM"=>$notiz->datum);
			return parent::update($queryVars);
		//wenn new Notiz ist => einfügen
		else:
			parent::write($queryVars);
			return $this->lastid;
		endif;

	}
}
