<?php // Modulview und Modulmodel einbinden
namespace admin\modules\contentadmin;
require_once("model.php");
class Controller
{
    // private VAriablen setzen
    private $request = null;
    private $modultemplate = 'anzeige';
    private $modulview = null;
    ##################################################
    # Konstruktor, erstellet den Controller          #
    #                                                #
    # @param Array $request Array aus $_GET & $_POST.#
    ##################################################
    public function __construct($request)
    {
        // neuen Modulviewer instanziieren
        $this->modulview = new \inc\View();
        // Requestvariablen für dieses Objekt zur Verfügung stellen
        $this->request = $request;
        // Modultemplate auslesen
        $this->modultemplate =isset($request['view'])?$this->request['view']:'anzeige';
    }
    public function display()
    {
        $this->assignvalues['noslider']=true;
        $this->modulview->assign('func', $this->func); //registrieren der Funktionsseite, z.B. Sanitize
        $this->request['contentid']=empty($this->request['contentid'])?0:$this->request['contentid'];
        // Inhaltsdaten aus Datenbank holen
        $this->contentfac=new Model(25);
        $this->contentfac->getContent(); //Abfrage mit fertigen Datum
        switch($this->request['contentid']):
            case '0':
                if ($this->modultemplate=='edit')
                    $this->editAndDelete($this->request['contentid'],'new');
                $this->modulview->setTemplate($this->modultemplate,dirname(__FILE__));
                $this->contentfac->getAll();
                $this->modulview->assign("contenttypes",$this->contentfac->liste);
                $this->modulview->assign("link",$this->l);
                $this->modulview->assign("htmlNav",$this->contentfac->getHtmlNavi('std',$_SERVER['QUERY_STRING']=""));
                break;
            default:
                $this->modultemplate =isset($this->request['view'])?$this->request['view']:'edit';
                $this->modulview->setTemplate($this->modultemplate,dirname(__FILE__));
                $this->modulview->assign("link",$this->l);
                $this->contentfac->getById(intval($this->request['contentid']));
                $this->content=$this->contentfac->getElement();
                // Prüfung ob gelöscht oder bearbeitet werden soll.
                $this->editAndDelete($this->request['contentid']);
            break;
        endswitch;
        // modultemplate setzen
        $this->assignvalues["keywords"] = "jhgklJ";
        // Codes übergeben
        $this->modulview->assign("content",$this->content);
        $this->modulview->assign('func', $this->func);
         // Template laden, füllen und ausgeben
        return $this->modulview->loadTemplate();
    }
    private function changeData($modus)
    {
        $dataarray=array('title'=>$this->request['title'],
                         'alias'=>$this->func->convertUmlaut($this->func->sanitize($this->request['title'])),
                         'title_alias'=>$this->request['title'],
                         'introtext'=>$this->request['title'],
                         'fulltext'=>$this->request['fulltext'],
                         'created'=>array('plainSQL'=>'NOW()'),
                         'modified'=>array('plainSQL'=>'NOW()'),
                         'headerueberschrift'=>$this->request['title'],
                         'headertext'=>$this->request['title'],
                         'metakey'=>$this->request['metakey'],
                         'metadesc'=>$this->request['metadesc']);

        if (empty($modus)):
            // Inhaltsdaten in der Datenbank editieren
            unset($dataarray['created']);
            $this->contentfac->update($dataarray,'id',$this->request['contentid']);
        else:
            $this->contentfac->write($dataarray); // Datensatz schreiben -> basicDb
        endif;
    }
    private function editAndDelete($id,$modus=false)
    {
        if ($this->modultemplate=='edit' && !empty($this->request['submit'])):
            $this->changeData($modus);
            $_SESSION['msg']='Inhaltsseite erfolgreich '.(empty($modus)?'geändert':'angelegt');
            header('Location:'.WEBDIR.'admin/contentadmin.htm');
        elseif ($this->modultemplate=='delete' && !empty($this->request['submit'])):
            $this->contentfac->deleteElement('id',$id);
            $_SESSION['msg']='Inhaltsseite erfolgreich gelöscht!';
            header('Location:'.WEBDIR.'admin/contentadmin.htm');
        endif;
    }
}
?>