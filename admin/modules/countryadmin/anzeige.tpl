<h1>Länder pflegen</h1>
<table style="width:800px;">
<tr>
	<?php echo $this->_['func']->tableHeadSort('id','id',"","","width:40px;");
	echo $this->_['func']->tableHeadSort('Name (engl.)','name',"","","");
	echo $this->_['func']->tableHeadSort('ISO3166','ISO3166',"","","width:70px;");
	echo $this->_['func']->tableHead('Admin',"","","width:60px;")?>
</tr>

<?php
// Studiodaten abrufen
foreach($this->_['countries'] as $key => $land):
// Ausgabe

    ?>
    <tr class="td<?php echo $x%2?>">
        <td>
            <?php echo strip_tags(htmlspecialchars(stripslashes($land->id)))?>
        </td>
        <td>
        	<?php echo strip_tags(htmlspecialchars(stripslashes($land->name)))?>
        </td>
        <td>
        	<?php echo strip_tags(htmlspecialchars(stripslashes($land->ISO3166)))?>
        </td>
        <td  style="text-align:center">
          
            <?php echo  $this->_['link']->makeLink('<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />',WEBDIR.'admin/countryadmin.htm?view=delete&amp;countryid='.$land->id,"adminlink");?>
            <?php echo  $this->_['link']->makeLink('<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />',WEBDIR."admin/countryadmin.htm?countryid=".$land->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&countryid=".$_GET['countryid'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
        </td>
    </tr>
 <?php
 endforeach;
 ?>
</table><br/>
 <?php
 echo $this->_['htmlNav']; 
 ?>
		
<br/><br/>
<?php echo  $this->_['link']->makeLink($icon_neu_small." Neues Land einpflegen",WEBDIR."admin/countryadmin.htm?view=edit","adminlink")?>