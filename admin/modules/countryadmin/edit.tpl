  

    <h1>Land <?php echo empty($this->_['country'])?'neu anlegen':'ändern';?></h1>
    <b>Sie haben die Möglichkeit mit dem nachfolgenden Formularen die Landdaten zu ändern.</b><br/><br/>

<form action="#" method="post" enctype="multipart/form-data">
    <fieldset class="td0">
        <legend>
            Länderdaten
        </legend>
        <label for="name">Name (englisch)</label>
        <input type="text" name="name" id="name" value="<?php echo $this->_['country']->name;?>" /><br class="clr"/>
        <label for="ISO3166">ISO3166-Code</label>
        <input type="text" name="ISO3166" id="ISO3166" value="<?php echo $this->_['country']->ISO3166;?>" /><br class="clr"/>
        <br class="clr"/>
    </fieldset>
 
        <input type="submit" name="submit" id="submit" value="Land <?php echo empty($this->_['country'])?'neu anlegen':'ändern';?>" />

</form>
