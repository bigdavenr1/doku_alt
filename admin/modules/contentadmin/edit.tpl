
<h1>Inhaltsseite <?php echo empty($this->_['content'])?'neu anlegen':'ändern';?></h1>
<br />
<form action="#" method="post" enctype="multipart/form-data">
    <fieldset class="none">
        <legend>Inhaltsseitendatendaten</legend>
        <label for="titel">Titel</label>
        <input type="text" name="title" id="title" value="<?php echo $this->_['func']->sanitize($this->_['content']->title);?>"/><br class="clr"/><br/>
        <label for="beschreibung">Inhaltsseite</label><br class="clr"/>
        <script src="/inc/ckeditor/ckeditor.js"></script>
        <textarea id="fulltext" name="fulltext"><?php echo stripslashes($this->_['content']->fulltext);?></textarea>
        <script type="text/javascript">
             CKEDITOR.replace( 'fulltext' );
        </script>
    </fieldset>
    <br class="clr"/><br/>
    <fieldset class="none">
        <legend>Meta-Daten</legend>
        <label for="metakey">Meta-Keywords (mit Komma trennen)</label>
        <input type="text" name="metakey" id="metakey" value="<?php echo $this->_['func']->sanitize($this->_['content']->metakey)?>"/><br class="clr"/><br/>
        <label for="metadesc">Meta-Description (max 120 Zeichen)</label>
        <input type="text" name="metadesc" id="metadesc" value="<?php echo $this->_['func']->sanitize($this->_['content']->metadesc)?>"/><br class="clr"/><br/>
    </fieldset>
   
    <br class="clr"/><br/>
    <fieldset class="none">
        <input type="submit" name="submit" id="submit" value="Inhalt <?php echo empty($this->_['content'])?'neu anlegen':'ändern';?>" />
    </fieldset>
</form>
<br/><br class="clr"/>
