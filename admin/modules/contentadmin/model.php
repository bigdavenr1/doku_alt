<?php
namespace admin\modules\contentadmin;

    class Model extends \inc\Basicdb
    {
        use ModulConfig;
        
        // Konstruktor
        ////////////////////////////////////////////////////////////////////////

        function __construct($anzahl = false)
        {
            global $sql;
            $this->queryVars=(object) array('table' => self::$table, 'offsetname'=>false, 'cps'=>25, 'countVar'=>self::$table.".id");
            parent::__construct();

        }

		public function getContent()
        {
            $queryVars=clone($this->queryVars);
            $queryVars->fields=array(self::$id,
                                   self::$title,
                                   self::$alias,
                                   self::$title_alias,
                                   self::$introtext,
                                   self::$fulltext,
                                   "DATE_FORMAT(".self::$created.", '%d.%m.%Y') as created",
                                   "DATE_FORMAT(".self::$modified.", '%d.%m.%Y') as modified",
                                   "DATE_FORMAT(".self::$publish_up.", '%d.%m.%Y') as publish_up",
                                   "DATE_FORMAT(".self::$publish_down.", '%d.%m.%Y') as publish_down",
                                   self::$headerueberschrift,
                                   self::$headertext,
                                   self::$metakey,
                                   self::$metadesc,
                                   self::$access,
                                   self::$hits);
            parent::createQuery($queryVars);
        }

        public function writeContent(array $dataarray)
        {
            $queryVars=clone($this->queryVars);
            $queryVars->data=$dataarray;
            parent::write($queryVars);
        }

        public function updateContent(array $dataarray, $id)
        {
            $queryVars=clone($this->queryVars);
            $queryVars->data=$dataarray;
            $queryVars->clause="id=:ID";
            $queryVars->preparedVars=array(':ID'=>intval($id));
            unset($queryVars->data['created']);
            parent::update($queryVars);
        }
    }

?>