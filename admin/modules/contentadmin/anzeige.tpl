<h1>Content pflegen</h1>
<table style="width:100%;" cellspacing="0" cellpadding="0" class="admintbl">
<tr>
	<?php echo $this->_['func']->tableHeadSort('id','id',"","","width:40px;");
	echo $this->_['func']->tableHeadSort('Titel','title');
	echo $this->_['func']->tableHeadSort('Create','create',"","","width:80px;");
	echo $this->_['func']->tableHeadSort('Modified','modified',"","","width:80px;");
	//echo $this->_['func']->tableHeadSort('Menupoint','menupoint',"","","width:60px;");
	echo $this->_['func']->tableHead('Admin',"","","width:30px;")?>
</tr>

<?php
if (is_array($this->_['contenttypes'])):

// Studiodaten abrufen
foreach($this->_['contenttypes'] as $key => $content):
// Ausgabe

    ?>
    <tr class="td<?php echo $x%2?>">
        <td>
            <?php echo $this->_['func']->sanitize($content->id)?>
        </td>
        <td>
        	<?php echo $this->_['func']->sanitize($content->title)?>
        </td>
        <td>
        	<?php echo $this->_['func']->sanitize($content->created)?>
        </td>
        <td>
        	<?php echo $this->_['func']->sanitize($content->modified)?>
        </td>
        <td  style="text-align:center">
            <?php echo  $this->_['link']->makeLink('<img src="'.WEBDIR.'images/icons/page_edit_small.gif" alt="Bearbeiten" title="Bearbeiten" />',WEBDIR."admin/contentadmin.htm?contentid=".$content->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&contentid=".$_GET['contentid'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
            <?php echo  $this->_['link']->makeLink('<img src="'.WEBDIR.'images/icons/cross_small.gif" alt="Löschen" title="Löschen" />',WEBDIR.'admin/contentadmin.htm?view=delete&amp;contentid='.$content->id,"adminlink");?>
        </td>
    </tr>
 <?php
 endforeach;
 endif;
 ?>
</table><br/>
 <?php
 echo $this->_['htmlNav']; 
 ?>
		
<br/><br/>
<?php echo  $this->_['link']->makeLink($icon_neu_small."Neuen Inhalt einpflegen",WEBDIR."admin/contentadmin.htm?view=edit","adminlink")?>