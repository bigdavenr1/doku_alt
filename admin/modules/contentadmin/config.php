<?php
namespace admin\modules\contentadmin;
trait ModulConfig
{
    /**
     * Name der SQL-Tabelle
     */
    protected static $table = "kta_content";
    
     /**
     * Feld id
     */
     protected static $id = 'id';
     /**
     * Feld Titel
     */
    protected static $title = 'title';
    /**
     * Feld alias
     */
    protected static $alias = 'alias';
    /**
     * Feld title_alias
     */
    protected static $title_alias = 'title_alias';
    /**
     * Feld introtext
     */
    protected static $introtext = 'introtext';
    /**
     * Feld fulltext
     */
    protected static $fulltext = '`fulltext`';
    /**
     * Feld created
     */
    protected static $created = 'created';
    /**
     * Feld modified
     */
    protected static $modified = 'modified';
    /**
     * Feld publish_up
     */
    protected static $publish_up = 'publish_up';
    /**
     * Feld publish_down
     */
    protected static $publish_down = 'publish_down';
    /**
     * Feld headerueberschrift
     */
    protected static $headerueberschrift = 'headerueberschrift';
    /**
     * Feld headertext
     */
    protected static $headertext = 'headertext';
    /**
     * Feld metakey
     */
    protected static $metakey = 'metakey';
    /**
     * Feld metadesc
     */
    protected static $metadesc = 'metadesc';
    /**
     * Feld access
     */
    protected static $access = 'access';
    /**
     * Feld hits
     */
    protected static $hits = 'hits';
    

    


}