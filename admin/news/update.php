<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
// Überprüfen ob Aufgabe übergeben wurde
if (!isset($_GET['mode'])) $_GET['mode']="new";
// Newsobjekt erzeugen
$newsfac=new News();
// Wenn nur der Status geändert werden soll
if ($_GET['mode']=="changestatus")
{
    // Datensatz suchen
    $newsfac->getById($_GET['id']);
    // Wenn Datensatz gefunden
    if ($news=$newsfac->getElement())
    {
        $newstatus=($news->status=="A")?"I":"A";
        $newsfac->update("status='".$newstatus."'","id",$_GET['id']);
        $_SESSION['msg']="Status erfolgreich geändert!";
        header("Location:".$l->makeUrl(WEBDIR."admin/news/view.php?".$_SERVER['QUERY_STRING']));
    }
    // Wenn kein Datensatz gefunden
    else
    {
        $_SESSION['err']="Datensatz nicht gefunden!";
        header("Location:".$l->makeUrl(WEBDIR."admin/news/view.php?".$_SERVER['QUERY_STRING']));
    }
}
// Wenn Datensatz geändert werden soll
elseif ($_GET['mode']=="update")
{
    $newsfac->getById($_GET['id']);
    if ($news=$newsfac->getElement())
    {
        if (file_exists(LOCALDIR."images/news/".$news->bild))
        {
            // Bildtyp auslesen
            $typ=getimagesize(LOCALDIR."images/news/".$news->bild);
            // Endung setzen
            if ($typ[2]==2) $type = '.jpg';
            elseif ($typ[2]==1) $type = '.gif';
            // Alten NAmen als Standard setzen udn nur bei Bedarf überschreiben
            $bild=$news->bild;
            // Wenn Bild gelöscht werden soll
            if (isset($_POST['delbild']) || $_FILES['bild']['name']!="")
            {
                if (file_exists(LOCALDIR."images/news/".$news->bild) && trim($news->bild)!="") unlink(LOCALDIR."images/news/".$news->bild);
                if (file_exists(LOCALDIR."images/news/".basename($news->bild,$type)."_thumb".$type)) unlink(LOCALDIR."images/news/".basename($news->bild,$type)."_thumb".$type);
                if (file_exists(LOCALDIR."images/news/".basename($news->bild,$type)."_thumb_big".$type)) unlink(LOCALDIR."images/news/".basename($news->bild,$type)."_thumb_big".$type);
                if (file_exists(LOCALDIR."images/news/".basename($news->bild,$type)."_original".$type)) unlink(LOCALDIR."images/news/".basename($news->bild,$type)."_original".$type);
                $bild="";
            }
        }
        // Wnen neues Bild hochgeladen werden soll
        if ($_FILES['bild']['name']!="")
        {
            // Bildtyp auslesen
            $typ2=getimagesize($_FILES['bild']['tmp_name']);
            if ($typ2[2] != 2 && $typ2[2] != 1) $_SESSION['err']="Es sind nur JPG oder GIF-Dateien erlaubt! Datensatz wurde gespeichert. Das Bild jedoch wurde noch nicht geupdatet!";
            else
            {
                // Endung setzen
                if ($typ2[2]==2) $type2 = '.jpg';
                elseif ($typ2[2]==1) $type2 = '.gif';
                // neuen Namen setzen
                $key=$func->generateRandomKey(12);
                // Bild hochladen //Angabe des Temp-Namen, des neuen Namen, der Endung, des imagesize-Arrays und des Unterordners innerhalb des image-Ordners
                $func->bildUpload($_FILES['bild']['tmp_name'],$key,$type2,$typ2,"images/news/");
                // namen für die Datenbank setzen
                $bild=$key.$type2;
            }
        }
        // Setzen des Status
        $status=($_POST['status']=="on")?"A":"I";
        // Setzen ob Vorschaubild im Artikel angezeigt werden soll
        $vorschaubild=($_POST['vorschaubild']=="on")?"Y":"N";
        // Datensatz updaten
        $newsfac->update("text='".mysql_real_escape_string($_POST['text'])."',
                         titel='".mysql_real_escape_string($_POST['titel'])."',
                         bild='".$bild."',
                         status='".$status."',
                         vorschaubild='".$vorschaubild."'","id",mysql_real_escape_string($_GET['id']));
        $_SESSION['msg']="Newsartikel erfolgreich geändert!";
        header("Location:".$l->makeUrl(WEBDIR."admin/news/view.php?".$_SERVER['QUERY_STRING']));
    }
}

// Wenn Datensatz geändert werden soll
elseif ($_GET['mode']=="new")
{
    // Wnen neues Bild hochgeladen werden soll
    if ($_FILES['bild']['name']!="")
    {
        // Bildtyp auslesen
        $typ2=getimagesize($_FILES['bild']['tmp_name']);
        if ($typ2[2] != 2 && $typ2[2] != 1) $_SESSION['err']="Es sind nur JPG oder GIF-Dateien erlaubt! Datensatz wurde gespeichert. Das Bild jedoch wurde noch nicht geupdatet!";
        else
        {
            // Endung setzen
            if ($typ2[2]==2) $type2 = '.jpg';
            elseif ($typ2[2]==1) $type2 = '.gif';
            // neuen Namen setzen
            $key=$func->generateRandomKey(12);
            // Bild hochladen //Angabe des Temp-Namen, des neuen Namen, der Endung, des imagesize-Arrays und des Unterordners innerhalb des image-Ordners
            $func->bildUpload($_FILES['bild']['tmp_name'],$key,$type2,$typ2,"images/news/");
            // namen für die Datenbank setzen
            $bild=$key.$type2;
        }
    }
    // Setzen des Status
    $status=($_POST['status']=="on")?"A":"I";
    // Setzen ob Vorschaubild im Artikel angezeigt werden soll
    $vorschaubild=($_POST['vorschaubild']=="on")?"Y":"N";
    // Datensatzarray
    $artikel[]="";
    $artikel[]="";
    $artikel[]=$_POST['titel'];
    $artikel[]=$_POST['text'];
    $artikel[]=$bild;
    $artikel[]=$status;
    $artikel[]=$vorschaubild;

    // Datensatz updaten
    $newsfac->write($artikel);
    $_SESSION['msg']="Newsartikel erfolgreich hinzugefügt!";
    header("Location:".$l->makeUrl(WEBDIR."admin/news/view.php?".$_SERVER['QUERY_STRING']));
}

?>