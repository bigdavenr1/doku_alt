<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
require_once(ADMINDIR."template/header.inc.php");   
require_once("../../inc/spaw2/spaw.inc.php");
// Objekt initialisieren
$newsfac= new News();
// Abfragefelder für formatiertes Datum
$newsfac->felder=" *,DATE_FORMAT(datum,GET_FORMAT(DATE,'EUR')) zeit";
// Daten holen
$newsfac->getById($_GET['id']);
$news=$newsfac->getElement();
// Inhalt für Spawtool setzen
$spaw = new SpawEditor("text",stripslashes($news->text));
?>
<h1>Newsartikel <?php echo (isset($news))?"- ".stripslashes(strip_tags($news->titel))." - bearbeiten":"neu erstellen"?></h1><br/>
<form action="<?php echo $l->makeFormLink(WEBDIR."admin/news/update.php?".htmlentities($_SERVER['QUERY_STRING']))?>" method="post" enctype="multipart/form-data">
    <fieldset class="td1">
        <legend>Artikeltext</legend>
        <?php echo(isset($news))?'<label>Erstellungsdatum</label>'.$news->zeit.'<br class="clr"/>':''?>
        <label for="titel">Titel</label>
        <input type="text" name="titel" id="titel" value="<?php echo stripslashes(strip_tags($news->titel))?>"/><br class="clr"/>
        <label>Artikeltext</label>
        <?php $spaw->show();?>
        <br class="clr"/>
    </fieldset>
    <fieldset class="td2">
        <legend>Artikelbild</legend>
        <?php
        if (file_exists(LOCALDIR."images/news/".$news->bild) && $news->bild != "")
        {
            // Bildtyp auslesen
            $typ=getimagesize(LOCALDIR."images/news/".$news->bild);
            // Endung setzen
            if ($typ[2]==2) $type = '.jpg';
            elseif ($typ[2]==1) $type = '.gif';
            // Wenn Bild gefunden und Endung gesetzt wurde
            if (isset($type))
            {
                // Wenn Thumbnail existiert
                if (file_exists(LOCALDIR."images/news/".basename($news->bild,$type)."_thumb".$type))
                {
                    // Größe des Thumbnails auslesen
                    $size=getimagesize(LOCALDIR."images/news/".basename($news->bild,$type)."_thumb".$type);
                    // Ausgabe
                    ?>
                    <label>vorhandenes Bild</label>
                    <img src="<?php echo WEBDIR."images/news/".basename($news->bild,$type)."_thumb".$type?>" <?php echo $size[3]?> alt="<?php echo stripslashes(strip_tags($news->titel))?>" title="<?php echo stripslashes(strip_tags($news->titel))?>"/>
                    <br class="clr"/>
                    <input type="checkbox" name="delbild"/>Bild löschen<br class="clr"/>
                    <hr/>
                    (bei upload eines neuen Bildes wird das alte automatisch gelöscht)
                    <?php
                }
            }
        }
        ?>
        <label>neues Bild</label><input type="file" name="bild" id="bild"/><br class="clr"/>
    </fieldset>
    <fieldset class="td1">
        <legend>Einstellungen</legend>
        <input type="checkbox" <?php echo ($news->status=="A")?$chk:""?> name="status"/>Newsartikel aktivieren / deaktivieren<br class="clr"/>
        <input type="checkbox" <?php echo ($news->vorschaubild=="Y")?$chk:""?> name="vorschaubild"/>Vorschaubild im Artikel anzeigen<br class="clr"/>
    </fieldset>
    <br/>
    <input type="submit" class="submit" value="Newsartikel speichern"/>
</form>
<?php
require_once(ADMINDIR."template/footer.inc.php");
?>