<?php
include("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
require_once(ADMINDIR."template/header.inc.php");   
// Objekt initialisieren
$newsfac=new News(10);
?>
<h1>Newswaltung</h2>
<?php echo $l->makeLink($icon_neu_small." Neuen Newsartikel anlegen",WEBDIR."admin/news/edit.php?mode=new","adminlink")?>
<br /><br/>
<?php if ($_SESSION['err']) echo '<div class="err">'.$_SESSION['err'].'</div><br/>';unset($_SESSION['err']) ?>
<?php if ($_SESSION['msg']) echo '<div class="ok">'.$_SESSION['msg'].'</div><br/>';unset($_SESSION['msg']) ?>
<table style="width:100%;float:left;">
    <tr>
        <?php
        echo $func->tableHeadSort("ID","id","","","width:30px;");
        echo $func->tableHeadSort("Datum","datum","","","width:60px;");
        echo $func->tableHeadSort("Titel","titel");
        echo $func->tableHeadSort("Status","status","","","width:55px;");
        echo $func->tableHead("Admin","","","width:30px;");
        ?>                                     
    </tr>
    <?php
    // Aufruf der Datenbankverbindung
    $newsfac->felder=" id, titel, datum, status, DATE_FORMAT(datum,GET_FORMAT(DATE,'EUR')) zeit";
    $newsfac->getAll();   
    $x=0;
    // Benutzerdaten abrufen
    while($news=$newsfac->getElement())
    {
        // Ausgabe
        $x++;
        $class="1";
        if ($x%2) $class="2";
        ?>
        <tr>
            <td class="td<?php echo $class ?>">
                <?php echo $news->id ?>
            </td>
            <td class="td<?php echo $class ?>">
                <?php echo $news->zeit ?>
            </td>
            <td class="td<?php echo $class ?>">
                <?php echo $news->titel ?>
            </td>
            <td class="td<?php echo $class ?>">
                <?php if ($news->status == "A") echo '<span >A</span> '; else echo '<span style="color:#FF0000">I</span> '; echo $l->makeLink($icon_refresh,WEBDIR."admin/news/update.php?mode=changestatus&amp;id=".$news->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
            </td>
            <td class="td<?php echo $class ?>" style="text-align:center">
                <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/news/edit.php?mode=update&amp;id=".$news->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
                <?php echo $l->makeLink($icon_delete_small,WEBDIR."admin/news/delete.php?id=".$news->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
<br/>
<div id="htmlnavi">
    <?php $htmlnavi=$newsfac->getHtmlNavi("std","&amp;".htmlentities($_SERVER['QUERY_STRING']));
    echo ($htmlnavi=="Keine Eintr&auml;ge vorhanden!")?'<table style="width:100%"><tr><td class="td1">'.$htmlnavi.'</td></tr></table>':'<br/>&nbsp;<br/>'.$htmlnavi;?>
</div>
<br/>
<?php echo $l->makeLink($icon_neu_small." Neuen Newsartikel anlegen",WEBDIR."admin/news/edit.php?mode=new","adminlink")?>
<br/>
<br/>
<b>Legende:</b><?php echo $icon_refresh;?> = Status ändern; <?php echo $icon_edit_small?> = Eintrag editieren; <?php echo $icon_delete_small?> = Eintrag löschen
<?php
include(ADMINDIR."template/footer.inc.php");
?>