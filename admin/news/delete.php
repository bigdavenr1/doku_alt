<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
// Objekt initialisieren
$newsfac = new News();
$newsfac->getById($_GET['id']); 
// prüfen ob Element vorhanden ist
if ($news = $newsfac->getElement())
{
//     print_r($news);
    // Sicherheitsabfrage
    if (!$_GET['bestaetigt'])
    {
        require_once(ADMINDIR."template/header.inc.php");
        ?>
        <h1>Newsartikel löschen - Bitte bestätigen</h1>
        Wollen Sie den Newsartikel:<br/><br/><b><?php echo stripslashes(strip_tags($news->titel))?></b><br/><br/>wirklich löschen?<br/><br/>
        <?php echo $l->makeLink('<b>[ JA ]</b>',$_SERVER['PHP_SELF']."?bestaetigt=yes&amp;".htmlentities($_SERVER['QUERY_STRING']),"adminlink").' / '.$l->makeLink('<b>[ NEIN ]</b>',WEBDIR."admin/news/view.php?mode=back&amp;".htmlentities($_SERVER['QUERY_STRING']),"adminlink");
        require_once(ADMINDIR."template/footer.inc.php");
    }     
    // Wenn Löschen bestätigt
    else
    {
        $newsfac->deleteElement("id",$_GET['id']);
        $_SESSION['msg']="Newsartikel erfolgreich gelöscht";
        header ("Location:".$l->makeUrl(WEBDIR."admin/news/view.php?".str_replace("bestaetigt=yes","",$_SERVER['QUERY_STRING'])));
    }
}     
// Wenn Beitrag nicht gefunden wurde
else 
{
    $_SESSION['err']="Eintrag nicht gefunden !";
    header ("Location:".$l->makeUrl(WEBDIR."admin/news/view.php?".$_SERVER['QUERY_STRING']));
}
?>