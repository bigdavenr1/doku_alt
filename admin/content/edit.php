<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
require_once(ADMINDIR."template/header.inc.php");    
require_once(INCLUDEDIR."spaw2/spaw.inc.php");
// Objekt initialisieren
$artikelfac=new Artikel();
// Wenn Datensatz geändert werden soll
$artikelfac->getArtikelById($_GET['id']," "," ");
// prüfen ob Element vorhanden ist
$artikel = $artikelfac->getElement();
// Neue WYSIWYG-Area mit Inhalt füllen
$spaw=new SpawEditor("inhalt",stripslashes($artikel->introtext));
// Größe der Bearbeitungsarea festlegen
$spaw->width="750px";
$spaw->height="600px";
?>
<script src="<?php echo WEBDIR?>script/helper.js" type="text/javascript"></script>
<h1>Inhaltsseite <?php echo (!empty($artikel))?"- ".stripslashes(strip_tags($artikel->title))." - bearbeiten":"neu erstellen"?></h1>
<br />
<form action="<?php echo $l->makeFormLink(WEBDIR."admin/content/update.php?".htmlentities($_SERVER['QUERY_STRING']))?>" method="post">
    <fieldset class="td0">
        <legend>Inhaltsseitendatendaten</legend>
        <label for="titel">Titel</label>
        <input type="text" name="titel" id="titel" value="<?php echo htmlspecialchars(stripslashes(strip_tags($artikel->title)))?>"/><br class="clr"/><br/>
        <label for="beschreibung">Inhaltsseite</label><br class="clr"/>
        <?php $spaw->show();?>
    </fieldset>
    <fieldset class="td1">
        <legend>Meta-Daten</legend>
        <label for="metakey">Meta-Keywords (mit Komma trennen)</label>
        <input type="text" name="metakey" id="metakey" value="<?php echo htmlspecialchars(stripslashes(strip_tags($artikel->metakey)))?>"/><br class="clr"/><br/>
        <label for="metadesc">Meta-Description (max 120 Zeichen)</label>
        <input type="text" name="metadesc" id="metadesc" value="<?php echo htmlspecialchars(stripslashes(strip_tags($artikel->metadesc)))?>"/><br class="clr"/><br/>
    </fieldset>
    <?php 
    // Bilderbearbeitung nur bei update ausführen (Speicherkosnsitenz)
    if (!empty($artikel)): 
        ?>
        <fieldset class="td0">
            <legend>Headergrafik</legend>
            <a href="javascript:void(0)" onclick="replace_href(/(&bild=)+([^&]*)/,'&bild=standard')">Standardgrafik wählen</a><br/>
            <a href="javascript:void(0)" onclick="javascript:window.open('/admin/headerimage/edit.php','Popupfenster', 'width=800,height=500,resizable=yes');return false">vorhandenes Headerbild zuordnen</a><br/><br/>
            <?php
            // Wenn ein Bild gewählt wurde, das nicht mit dem gespeicherten Übereinstimmt und auch nicht das Standardbild ist
            if (!empty($_GET['bild']) && $_GET['bild']!=$artikel->images && $_GET['bild']!="standard" ):
                // Typ-Infos auslesen
                $typ=getimagesize(LOCALDIR.$_GET['bild']);
                // Prüfen ob Typ richtig
                if ($typ[2]==1 || $typ[2]==2 || $typ[2]==3):
                    // Bild ausgeben
                    echo '<div class="err">ACHTUNG Bildzuordnung noch nicht gespeichert!</div>';
                    echo '<input type="hidden" name="img" value="'.$_GET['bild'].'"/>';
                    echo '<img src="'.WEBDIR.$_GET['bild'].'" width="775" style="vertical-align:bottom">';
                endif;
            // Wenn kein Bild übergeben wurde oder das Übergebene Bild mit dem gespeicherten Bild übereinstimm UND Das gespeicherte NICHT leer ist
            elseif((empty($_GET['bild']) || $_GET['bild']==$artikel->images) && !empty($artikel->images)):
                // Typ-Infos auslesen
                $typ=getimagesize(LOCALDIR.$artikel->images);
                // Prüfen ob Typ richtig ist
                if ($typ[2]==1 || $typ[2]==2 || $typ[2]==3):
                    // Bild ausgeben
                    echo '<input type="hidden" name="img" value="'.$artikel->images.'"/>';
                    echo '<img src="'.WEBDIR.$artikel->images.'" width="775" style="vertical-align:bottom">';
                endif;
            // Ansonsten
            else:
                // Standardbild ausgeben
                echo '<input type="hidden" name="img" value=""/>';
                // Wenn gewähltes BIld standard und gespeichertes Bild nicht standard ist -> Meldung ausgeben
                if ($_GET['bild']=="standard" && !empty($artikel->images)): 
                    echo '<div class="err">ACHTUNG Bildzuordnung noch nicht gespeichert!</div>';
                endif;
                echo '<div class="msg">Kein Bild zugeordnet. Es wird dieses Standardbild verwendet</div>';
                echo '<img src="'.WEBDIR.'images/header/default.jpg" width="775" style="vertical-align:bottom">';
            endif;
            ?>
        </fieldset>
        <fieldset class="td1">
            <legend>Headertext</legend>
            <label>Überschrift</label><input type="text" name="hueberschrift" value="<?php echo htmlspecialchars(strip_tags(stripslashes($artikel->headerueberschrift)))?>"/><br class="clr"/>
            <label>Headertext</label><textarea name="headertext" style="margin-left:0px;width:350px;height:90px"><?php echo htmlspecialchars(strip_tags(stripslashes($artikel->headertext)))?></textarea>
        </fieldset>
        <fieldset class="td0">
            <legend>Bilder rechts</legend>
            <b>oberes Bild</b><br/>
            <?php
            // Wenn kein Bild gewählt wurde und kein Bild gespeichert ist ODER das übergebene Bild den WERT "zufall" hat
            if((empty($artikel->bild1) && empty($_GET['bildright1'])) || $_GET['bildright1']=="zufall" || (!file_exists(LOCALDIR.$_GET['bildright1']) && !file_exists(LOCALDIR.$artikel->bild1))):
                // Meldung ausgeben
                echo '<input type="hidden" name="imgrgt1" value=""/>';
                // Wenn gewähltes BIld zufällig und gespeichertes Bild nicht zufällig ist -> Meldung ausgeben
                if ($_GET['bildright1']=="zufall" && !empty($artikel->bild1) ): 
                    echo '<div class="err">ACHTUNG Bildzuordnung noch nicht gespeichert!</div>';
                endif;
                echo '<div class="msg">Zufallsbild gewählt! Es wird ein Zufallsbild angezeigt!</div>';
            // Wenn Übergebenes Bild nicht mit gespeicherten Übereinstimmt und übergebenes Bild nicht zufällig und leer ist
            elseif ($_GET['bildright1']!=$artikel->bild1 && $_GET['bildright1']!="zufall" && !empty($_GET['bildright1'])):
                // Bildtyp-Infos holen
                $typ=getimagesize(LOCALDIR.$_GET['bildright1']);
                // Prüfen of Typ stimmt
                if ($typ[2]==1 || $typ[2]==2 || $typ[2]==3):
                    // Meldung und Bilder ausgeben
                    echo '<div class="err" style="width:138px">ACHTUNG Bildzuordnung noch nicht gespeichert!</div>';
                    echo '<input type="hidden" name="imgrgt1" value="'.$_GET['bildright1'].'"/>';
                    echo '<img src="'.WEBDIR.$_GET['bildright1'].'" width="154" height="154" style="vertical-align:bottom"><br/>';
                    echo '<a href="javascript:void(0)" onclick="replace_href(/(&bildright1=)+([^&]*)/,\'&bildright1=zufall\')">Zufallsbild wählen</a><br/>';
                endif;
            // Ansonsten
            else:
                // Bildtyp-Infos holen
                $typ=getimagesize(LOCALDIR.$artikel->bild1);
                // Prüfen ob Typ richtig ist
                if ($typ[2]==1 || $typ[2]==2 || $typ[2]==3):
                    // Bild ausgeben
                    echo '<input type="hidden" name="imgrgt1" value="'.$artikel->bild1.'"/>';  
                    echo '<img src="'.WEBDIR.$artikel->bild1.'" width="154" height="154" style="vertical-align:bottom"><br/>';
                    echo '<a href="'.$_SERVER['PHP_SELF'].'?'.preg_replace("~&bildright1\=([^\&]*)~","",$_SERVER['QUERY_STRING']).'&bildright1=zufall">Zufallsbild wählen</a><br/>';
                endif;
            endif;
            ?>
            <a href="javascript:void(0)" onclick="javascript:window.open('/admin/rightimage/edit.php?bildnr=1','Popupfenster', 'width=800,height=500,resizable=yes');return false">1. Bild zuordnen</a><br/><br/>
            <b>unteres Bild</b><br/>
            <?php
            // Wenn kein Bild gewählt wurde und kein Bild gespeichert ist ODER das übergebene Bild den WERT "zufall" hat
            if((empty($artikel->bild2) && empty($_GET['bildright2'])) || $_GET['bildright2']=="zufall" || (!file_exists(LOCALDIR.$_GET['bildright2']) && !file_exists(LOCALDIR.$artikel->bild2))):
                echo '<input type="hidden" name="imgrgt2" value=""/>';
                // Wenn gewähltes BIld zufällig und gespeichertes Bild nicht zufällig ist -> Meldung ausgeben
                if ($_GET['bildright2']=="zufall" && !empty($artikel->bild2)): 
                    echo '<div class="err">ACHTUNG Bildzuordnung noch nicht gespeichert!</div>';
                endif;
                echo '<div class="msg">Zufallsbild gewählt! Es wird ein Zufallsbild angezeigt!</div>';
            // Wenn Übergebenes Bild nicht mit gespeicherten Übereinstimmt und übergebenes Bild nicht zufällig und leer ist
            elseif ($_GET['bildright2']!=$artikel->bild1 && $_GET['bildright2']!="zufall" && !empty($_GET['bildright2'])):
                // Bildtyp-Infos holen
                $typ=getimagesize(LOCALDIR.$_GET['bildright2']);
                // Prüfen of Typ stimmt
                if ($typ[2]==1 || $typ[2]==2 || $typ[2]==3):
                    // Meldung und Bilder ausgeben
                    echo '<div class="err" style="width:138px">ACHTUNG Bildzuordnung noch nicht gespeichert!</div>';
                    echo '<input type="hidden" name="imgrgt2" value="'.$_GET['bildright2'].'"/>';
                    echo '<img src="'.WEBDIR.$_GET['bildright2'].'" width="154" height="154" style="vertical-align:bottom"><br/>';
                    echo '<a href="javascript:void(0)" onclick="replace_href(/(&bildright2=)+([^&]*)/,\'&bildright2=zufall\')">Zufallsbild wählen</a><br/>';
                endif;
            // Ansonsten
            else:
                // Bildtypinfos holen
                $typ=getimagesize(LOCALDIR.$artikel->bild2);
                // Prüfen ob Typ stimmt
                if ($typ[2]==1 || $typ[2]==2 || $typ[2]==3):
                    // Bild ausgeben
                    echo '<input type="hidden" name="imgrgt2" value="'.$artikel->bild2.'"/>';
                    echo '<img src="'.WEBDIR.$artikel->bild2.'" width="154" height="154" style="vertical-align:bottom"><br/>';
                    echo '<a href="'.$_SERVER['PHP_SELF'].'?'.preg_replace("~&bildright2\=([^\&]*)~","",$_SERVER['QUERY_STRING']).'&bildright2=zufall">Zufallsbild wählen</a><br/>';
                endif;
            endif;
            ?>
            <a href="javascript:void(0)" onclick="javascript:window.open('/admin/rightimage/edit.php?bildnr=2','Popupfenster', 'width=800,height=500,resizable=yes');return false">2. Bild zuordnen</a><br/><br/>
        </fieldset>
    <?php
    endif;
    ?>
    <fieldset class="none">
        <input type="submit" value="Inhaltsseite speichern"/>
    </fieldset>
</form>
<?php
require_once(ADMINDIR."template/footer.inc.php");
?>