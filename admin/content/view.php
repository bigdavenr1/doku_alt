<?php
include("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
include(ADMINDIR."template/header.inc.php");
// Objekt initialisieren
$artfac=new Artikel(10);
$artfac->felder="jos_content.*, DATE_FORMAT(jos_content.created,GET_FORMAT(DATE,'EUR')) createddatum, DATE_FORMAT(jos_content.modified,GET_FORMAT(DATE,'EUR')) modifieddatum, jos_menu.name zuordnung";
$artfac->getAll("LEFT JOIN jos_menu ON (jos_menu.type='artikel' AND jos_menu.link=jos_content.id)" );
?>
<h1>Verwaltung statischer Seiten</h2><br/>
<?php echo $l->makeLink($icon_neu_small." Neuen Seite anlegen",WEBDIR."admin/content/edit.php?mode=new","adminlink")?>
<br /><br/>
<?php if ($_SESSION['err']) echo '<div class="err">'.$_SESSION['err'].'</div><br/>';unset($_SESSION['err']) ?>
<?php if ($_SESSION['msg']) echo '<div class="ok">'.$_SESSION['msg'].'</div><br/>';unset($_SESSION['msg']) ?>
<table style="width:100%;float:left;">
    <tr>
        <?php
        echo $func->tableHeadSort("ID","id","","","width:40px;");
        echo $func->tableHeadSort("Titel","title");
        echo $func->tableHeadSort("erstellt","created","","","width:70px;");
        echo $func->tableHeadSort("geändert","modified","","","width:85px;");
        echo $func->tableHeadSort("Menupunkt","zuordnung","","","width:95px;");
        echo $func->tableHead("Admin","","","width:30px;");
        ?>
    </tr>
    <?php
    // Aufruf der Datenbankverbindung
    $x=0;
    // Benutzerdaten abrufen
    while($artikel=$artfac->getElement())
    {
        // Ausgabe
        $x++;
        ?>
        <tr class="td<?php echo $x%2?>">
            <td>
                <?php echo $artikel->id ?>
            </td>
            <td>
                <?php echo $artikel->title ?>
            </td>
                        <td >
                <?php echo $artikel->createddatum ?>
            </td>
            <td >
                <?php echo $artikel->modifieddatum ?>
            </td>
            <td >
                <?php echo $artikel->zuordnung ?>
            </td>
            <td  style="text-align:center">
                <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/content/edit.php?mode=update&amp;id=".$artikel->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
                <?php echo $l->makeLink($icon_delete_small,WEBDIR."admin/content/delete.php?id=".$artikel->id."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");?>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
<br class="clr"/>
<div id="htmlnavi">
    <?php $htmlnavi=$artfac->getHtmlNavi("std","&amp;".htmlentities($_SERVER['QUERY_STRING']));
    echo ($htmlnavi=="Keine Eintr&auml;ge vorhanden!")?'<table style="width:100%"><tr><td class="td1">'.$htmlnavi.'</td></tr></table>':'<br/>&nbsp;<br/>'.$htmlnavi;?>
</div>
<br/>
<?php echo $l->makeLink($icon_neu_small." Neuen Seite anlegen",WEBDIR."admin/content/edit.php?mode=new","adminlink")?>
<br/>
<br/>
<b>Legende:</b><?php echo $icon_refresh;?> = Status ändern; <?php echo $icon_edit_small?> = Eintrag editieren; <?php echo $icon_delete_small?> = Eintrag löschen
<?php
include(ADMINDIR."template/footer.inc.php");
?>