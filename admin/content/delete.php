<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
// Objekt initialisieren
$artikelfac=new Artikel();
// Wenn Datensatz geändert werden soll
$artikelfac->getArtikelById($_GET['id']," "," ");
// prüfen ob Element vorhanden ist
if ($artikel = $artikelfac->getElement())
{
    // Sicherheitsabfrage
    if (!$_GET['bestaetigt'])
    {
        require_once(ADMINDIR."template/header.inc.php");
        ?>
        <h1>Inhaltsseite löschen - Bitte bestätigen</h1><br/>
        Wollen Sie die Inhaltsseite:<br/><br/><b><?php echo stripslashes(strip_tags($artikel->title))?></b><br/><br/>wirklich löschen?<br/><br/>
        <?php echo $l->makeLink('<b>[ JA ]</b>',$_SERVER['PHP_SELF']."?bestaetigt=yes&amp;".htmlentities($_SERVER['QUERY_STRING']),"adminlink").' / '.$l->makeLink('<b>[ NEIN ]</b>',WEBDIR."admin/content/view.php?mode=back&amp;".htmlentities($_SERVER['QUERY_STRING']),"adminlink");
        require_once(ADMINDIR."template/footer.inc.php");
    }
    // Wenn Löschen bestätigt
    else
    {
        $artikelfac->deleteElement("id",$_GET['id']);
        $_SESSION['msg']="Inhaltsseite erfolgreich gelöscht";
        header ("Location:".$l->makeUrl(WEBDIR."admin/content/view.php?".ereg_replace("mode=".$_GET['mode']."&","",ereg_replace("id=".$_GET['id']."&","",ereg_replace("typ=".$_GET['typ']."&","",str_replace("bestaetigt=yes","",$_SERVER['QUERY_STRING']))))));
    }
}     
// Wenn Beitrag nicht gefunden wurde
else 
{
    $_SESSION['err']="Eintrag nicht gefunden !";
    header ("Location:".$l->makeUrl(WEBDIR."admin/content/view.php?".ereg_replace("mode=".$_GET['mode']."&","",ereg_replace("id=".$_GET['id']."&","",ereg_replace("typ=".$_GET['typ']."&","",$_SERVER['QUERY_STRING'])))));
}
?>