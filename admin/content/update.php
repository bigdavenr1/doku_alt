<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
// Überprüfen ob Aufgabe übergeben wurde
if (!isset($_GET['mode'])) $_GET['mode']="new";
// Newsobjekt erzeugen
$artikelfac=new Artikel();
// Wenn Datensatz geändert werden soll
if ($_GET['mode']=="update"):
    $artikelfac->getArtikelById($_GET['id']);
    if ($artikel=$artikelfac->getElement()):
        // Datensatz updaten
        $artikelfac->update("title='".mysql_real_escape_string($_POST['titel'])."',
                         introtext='".mysql_real_escape_string($_POST['inhalt'])."',
                           metakey='".mysql_real_escape_string($_POST['metakey'])."',
                          metadesc='".mysql_real_escape_string($_POST['metadesc'])."',
                            images='".mysql_real_escape_string($_POST['img'])."',
                headerueberschrift='".mysql_real_escape_string($_POST['hueberschrift'])."',
                             bild1='".mysql_real_escape_string($_POST['imgrgt1'])."',
                             bild2='".mysql_real_escape_string($_POST['imgrgt2'])."',
                        headertext='".mysql_real_escape_string($_POST['headertext'])."'",
                        "id",mysql_real_escape_string($_GET['id']));
        $_SESSION['msg']="Inhaltsseite erfolgreich geändert!";
        header("Location:".$l->makeUrl(WEBDIR."admin/content/view.php?".ereg_replace("&bildright2=".$_GET['bildright2'],"",ereg_replace("&bild=".$_GET['bild'],"",ereg_replace("&bildright1=".$_GET['bildright1'],"",ereg_replace("mode=".$_GET['mode'],"",ereg_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING'])))))));
    endif;
// Wenn Datensatz geändert werden soll
elseif ($_GET['mode']=="new"):
    // Datensatzarray
    $artikel[]="";
    $artikel[]=$_POST['titel'];
    $artikel[]=$func->convertUmlaut($_POST['titel']);
    $artikel[]="";
    $artikel[]=$_POST['inhalt'];
    $artikel[]="";
    $artikel[]=$_POST['imgrgt1'];
    $artikel[]=$_POST['imgrgt2'];
    $artikel[]="";
    $artikel[]="";
    $artikel[]="NOW()";
    $artikel[]="";
    $artikel[]="";
    $artikel[]="";
    $artikel[]="";
    $artikel[]="";
    $artikel[]="";
    $artikel[]="";
    $artikel[]="";
    $artikel[]=$_POST['img'];
    $artikel[]=$_POST['hueberschrift'];
    $artikel[]=$_POST['headertext'];
    $artikel[]="";
    $artikel[]="";
    $artikel[]="";
    $artikel[]=$_POST['metakey'];
    $artikel[]=$_POST['metadesc'];
    // Datensatz schreibene
    $artikelfac->write($artikel);
    $_SESSION['msg']="Inhaltsseite erfolgreich angelegt!";
    header("Location:".$l->makeUrl(WEBDIR."admin/content/view.php?".ereg_replace("&bildright2=".$_GET['bildright2'],"",ereg_replace("&bild=".$_GET['bild'],"",ereg_replace("&bildright1=".$_GET['bildright1'],"",ereg_replace("mode=".$_GET['mode'],"",ereg_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING'])))))));
endif;