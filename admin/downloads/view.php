<?php
include("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
require_once(ADMINDIR."template/header.inc.php");   
// Objekt initialisieren
$downloadfac=new Download(20);
// Alle Datensätze holen
$downloadfac->felder="jos_downloads.*, jos_sprachen.name Sprachname";
$query=" LEFT JOIN jos_sprachen ON (jos_downloads.sprache=jos_sprachen.ID) ";
$downloadfac->getAll($query);
?>
<h1>Verwaltung der Downloads </h1>
<br/>
<?php echo $l->makeLink($icon_neu_small." Neuen Download anlegen",WEBDIR."admin/downloads/edit.php?mode=new","adminlink");?>
<br /><br/>
<?php if ($_SESSION['err']) echo '<div class="err">'.$_SESSION['err'].'</div><br/>';unset($_SESSION['err']) ?>
<?php if ($_SESSION['msg']) echo '<div class="ok">'.$_SESSION['msg'].'</div><br/>';unset($_SESSION['msg']) ?>
<table style="width:100%;float:left;">
    <tr>
        <?php
        // Tabellenköpfe setzen
        echo $func->tableHeadSort("ID","ID","","","width:30px;");
        echo $func->tableHeadSort("Bild","Preview","","","width:60px;");
        echo $func->tableHeadSort("Titel","Titel","","","width:360px;");
        echo $func->tableHeadSort("Sprache","sprache","","","width:45px;");
        echo $func->tableHeadSort("Datei","Datei");
        echo $func->tableHeadSort("Status","aktiv","","","width:45px;");
        echo $func->tableHead("Admin","","","width:45px;");
        ?>                                     
    </tr>
    <?php
    $x=0;
    // Jeden Datensatz abrufen
    while($download=$downloadfac->getElement()):
        // Ausgabe
        $x++;
        $class="1";
        if ($x%2) $class="2";
        ?>
        <tr>
            <td class="td<?php echo $class ?>">
                <?php echo $download->ID ?>
            </td>
            <td class="td<?php echo $class ?>">
                <?php
                // Wenn Vorschaubild eingetragen udn vorhanden 
                if ($download->Preview && file_exists(LOCALDIR."documents/downloads/preview/".$download->Preview)):
                    ?>
                    <img src="<?php echo WEBDIR.'documents/downloads/preview/'.$download->Preview?>" title="<?php echo stripslashes(strip_tags($download->Titel))?>" alt="<?php echo stripslashes(strip_tags($download->Titel))?>"/>
                    <?php
                // Wenn nicht
                else: echo "Kein Bild vorhanden";
                endif;
                ?>
            </td>
            <td class="td<?php echo $class ?>">
                <?php echo stripslashes(strip_tags($download->Titel)) ?>
            </td>
            <td class="td<?php echo $class ?>">
                <?php echo stripslashes(strip_tags($download->Sprachname)) ?>
            </td>
            <td class="td<?php echo $class ?>">
                <a href="<?php echo WEBDIR?>documents/downloads/<?php echo stripslashes(strip_tags($download->Datei)) ?>" target="_blank"><?php echo stripslashes(strip_tags($download->Datei)) ?></a>
            </td>
            <td class="td<?php echo $class ?>" style="text-align:center">
                <?php echo ($download->aktiv==1)?'<span style="color:#0F0">A</span>':'<span style="color:#F00">I</span>';?>
            </td>
            <td class="td<?php echo $class ?>" style="text-align:center">
                <?php
                echo $l->makeLink($icon_refresh,WEBDIR."admin/downloads/update.php?mode=status&amp;id=".$download->ID."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");
                echo $l->makeLink($icon_edit_small,WEBDIR."admin/downloads/edit.php?mode=update&amp;id=".$download->ID."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");
                echo $l->makeLink($icon_delete_small,WEBDIR."admin/downloads/delete.php?id=".$download->ID."&amp;".htmlentities(str_replace("mode=".$_GET['mode'],"",str_replace("&id=".$_GET['id'],"",$_SERVER['QUERY_STRING']))),"adminlink");
                ?>
            </td>
        </tr>
        <?php
    endwhile;
    ?>
</table>
<br class="clr"/>
<div id="htmlnavi">
    <?php $htmlnavi=$downloadfac->getHtmlNavi("std","&amp;".htmlentities($_SERVER['QUERY_STRING']));
    echo ($htmlnavi=="Keine Eintr&auml;ge vorhanden!")?'<table style="width:100%"><tr><td class="td1">'.$htmlnavi.'</td></tr></table>':'<br/>&nbsp;<br/>'.$htmlnavi;?>
</div>
<br/>
<b>Legende: </b><?php echo $icon_refresh?> = Status ändern; <?php echo $icon_delete_small?> = Eintrag löschen
<?php
include(ADMINDIR."template/footer.inc.php");
?>