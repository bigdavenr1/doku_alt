<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
   
// Objekt initialisieren
$downloadfac = new Download();
$downloadfac->getDownloadById($_GET['id']);
// prüfen ob Element vorhanden ist
if ($download = $downloadfac->getElement())
{
    // Sicherheitsabfrage
    if (!$_GET['bestaetigt'])
    {
        require_once(ADMINDIR."template/header.inc.php");
        ?>
        <h1>Download löschen - Bitte bestätigen</h1><br/>
        Wollen Sie den Download:<br/><br/><b><?php echo stripslashes(strip_tags($download->Titel))?></b><br/>
        <?php
        // Wenn Vorschaubild eingetragen udn vorhanden 
        if ($download->Preview && file_exists(LOCALDIR."documents/downloads/preview/".$download->Preview)):
            ?>
            <img src="<?php echo WEBDIR.'documents/downloads/preview/'.$download->Preview?>" title="<?php echo stripslashes(strip_tags($download->Titel))?>" alt="<?php echo stripslashes(strip_tags($download->Titel))?>"/>
            <?php
            // Wenn nicht
        else: echo "Kein Bild vorhanden";
        endif;
        ?>
        <br/><br/>wirklich löschen?<br/><br/>
        <?php echo $l->makeLink('<b>[ JA ]</b>',$_SERVER['PHP_SELF']."?bestaetigt=yes&amp;".htmlentities($_SERVER['QUERY_STRING']),"adminlink").' / '.$l->makeLink('<b>[ NEIN ]</b>',WEBDIR."admin/downloads/view.php?mode=back&amp;".htmlentities($_SERVER['QUERY_STRING']),"adminlink");
        require_once(ADMINDIR."template/footer.inc.php");
    }
    // Wenn Löschen bestätigt
    else
    {
        $downloadfac->deleteElement("ID",$_GET['id']);
        $_SESSION['msg']="Download erfolgreich gelöscht";
        if (file_exists(LOCALDIR."documents/downloads/".$download->Datei)) unlink(LOCALDIR."documents/downloads/".$download->Datei);
        if (file_exists(LOCALDIR."documents/downloads/preview/".$download->Preview)) unlink(LOCALDIR."documents/downloads/preview/".$download->Preview);
        header ("Location:".$l->makeUrl(WEBDIR."admin/downloads/view.php?".ereg_replace("mode=".$_GET['mode']."&","",ereg_replace("id=".$_GET['id']."&","",ereg_replace("typ=".$_GET['typ']."&","",str_replace("bestaetigt=yes","",$_SERVER['QUERY_STRING']))))));
        
    }
}     
// Wenn Beitrag nicht gefunden wurde
else 
{
    $_SESSION['err']="Eintrag nicht gefunden !";
    header ("Location:".$l->makeUrl(WEBDIR."admin/downloads/view.php?".ereg_replace("mode=".$_GET['mode']."&","",ereg_replace("id=".$_GET['id']."&","",ereg_replace("typ=".$_GET['typ']."&","",$_SERVER['QUERY_STRING'])))));
}
?>