<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
require_once(ADMINDIR."template/header.inc.php");   
require_once("../../inc/spaw2/spaw.inc.php");
// Objekt initialisieren
$downloadfac=new Download(20);
// Daten holen
$downloadfac->getDownloadById($_GET['id']);
$download=$downloadfac->getElement();
?>
<h1>Download <?php echo (isset($download))?" - ".stripslashes(strip_tags($download->Titel))." - bearbeiten":"neu erstellen"?></h1>
<br/>
<?php if ($_SESSION['err']) echo '<span class="errbox">'.$_SESSION['err'].'</span><br/>'; unset($_SESSION['err']);?>
<form action="<?php echo $l->makeFormLink(WEBDIR."admin/downloads/update.php?".htmlentities($_SERVER['QUERY_STRING']))?>" method="post" enctype="multipart/form-data">
    <fieldset class="td2">
        <legend>Downloaddaten</legend>
        <label for="titel">Titel</label>
        <input type="text" name="titel" id="titel" value="<?php echo stripslashes(strip_tags($download->Titel))?>"/><br class="clr"/>
        <label for="beschreibung">Beschreibung</label><br class="clr"/>
        <textarea name="beschreibung" style="background:#fff" id="beschreibung" cols="15" rows="20"><?php echo stripslashes(strip_tags($download->Beschreibung))?></textarea><br class="clr"/>
    </fieldset>
    </br><br/>
    
    <fieldset class="td1">
        <legend>Datei</legend>
        <?php if (!$download->Datei):?>
            !!ACHTUNG!! Dateien (insbesondere Bilddateien) werden dem Kunden ohne Bearbeitung 1:1 angezeigt. Bitte achten Sie auf die Abmaße der Dateien<br/><br/>
            <label for="datei">Datei</label>
            <input type="file" name="datei" id="datei" /><br class="clr"/>
        <?php else:?>
            <a href="<?php echo WEBDIR?>documents/downloads/<?php echo stripslashes(strip_tags($download->Datei)) ?>" target="_blank"><?php echo stripslashes(strip_tags($download->Datei)) ?></a>
        <?php endif;?>
    </fieldset>
    </br><br/>
    <fieldset class="td2">
        <legend>Sprache</legend>
        <label for="sprache">Sprache</label>
        <select name="sprache" id="sprache">
            <?php
            $langfac=new Sprache();
            $langfac->getAll(" "," "," ");
            while($sprache=$langfac->getElement()):   
                ?><option <?php echo ($sprache->id==$download->sprache)?$sel:''?> value="<?php echo stripslashes(strip_tags($sprache->id))?>"><?php echo stripslashes(strip_tags($sprache->name))?></option><?php
            endwhile;
            ?>
        </select>
    </fieldset>
    </br><br/>
    <fieldset class="td1">
        <legend>Vorschau</legend>
        <?php if (!$download->Preview):?>
            Muss nur bei PDF-Dateien hochgeladen werden, oder wenn ein anderes Vorschaubild angezeigt werden soll als das hiochgeladene große Bild.<br/><br/>
            <label for="vorschau">Datei</label>
            <input type="file" name="vorschau" id="vorschau" /><br class="clr"/>
        <?php else:
            if ($download->Preview && file_exists(LOCALDIR."documents/downloads/preview/".$download->Preview)):
                ?>
                <img src="<?php echo WEBDIR.'documents/downloads/preview/'.$download->Preview?>" title="<?php echo stripslashes(strip_tags($download->Titel))?>" alt="<?php echo stripslashes(strip_tags($download->Titel))?>"/>
                <?php
                // Wenn nicht
            else: echo "Kein Bild vorhanden";
            endif;
        endif;?>
    </fieldset>
    </br><br/>
    
    <input type="submit" class="submit" value="Download speichern"/>
</form>
<?php
require_once(ADMINDIR."template/footer.inc.php");
?>