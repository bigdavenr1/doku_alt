<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php"); 
// Überprüfen ob Aufgabe übergeben wurde
if (!isset($_GET['mode'])) $_GET['mode']="new";
// Newsobjekt erzeugen
$downloadfac=new Download();
####################################
# Wenn Status geändert werden soll #
####################################
if ($_GET['mode']=="status"):
    $downloadfac->getDownloadById($_GET['id']);
    if ($download=$downloadfac->getElement()):
        $aktiv=($download->aktiv==1)?'0':'1';
        // Datensatz updaten
        $downloadfac->update("aktiv='".mysql_real_escape_string($aktiv)."'","ID",mysql_real_escape_string($download->ID));
        $_SESSION['msg']="Status geändert!";
    else:
        $_SESSION['err']="Datensatz existiert nicht!";
    endif;
    header("Location:".$l->makeUrl(WEBDIR."admin/downloads/view.php?".ereg_replace("mode=".$_GET['mode']."&","",ereg_replace("id=".$_GET['id']."&","",$_SERVER['QUERY_STRING']))));
#######################################
# Wenn Datensatz geändert werden soll #
#######################################
elseif($_GET['mode']=="update"):
    $downloadfac->getDownloadById($_GET['id']);
    if ($download=$downloadfac->getElement()):
        // Datensatz updaten
        $downloadfac->update("sprache='".mysql_real_escape_string($_POST['sprache'])."',Titel='".mysql_real_escape_string($_POST['titel'])."', Beschreibung='".mysql_real_escape_string($_POST['beschreibung'])."'","ID",mysql_real_escape_string($download->ID));
        $_SESSION['msg']="Datensatz geändert!";
    else:
        $_SESSION['err']="Datensatz existiert nicht!";
    endif;
    header("Location:".$l->makeUrl(WEBDIR."admin/downloads/view.php?".ereg_replace("mode=".$_GET['mode']."&","",ereg_replace("id=".$_GET['id']."&","",$_SERVER['QUERY_STRING']))));
##############################################
# Wenn neuer Datensatz erstellt werdenb soll #
##############################################
elseif ($_GET['mode']=="new"):
    // Wenn ein File übergeben wurde
    if ($_FILES['datei']['name']!="" && $_FILES['datei']['error']==0):
        // Schauen ob PDF-Format
        if ($_FILES['datei']['type']=="application/pdf"):
            // Schauen ob Vorschaubild hochgeladen wurde
            if ($_FILES['vorschau']['name']!=""):
                // Typ des Vorschaubildes bestimmen
                $vorschautyp=getimagesize($_FILES['vorschau']['tmp_name']);
                // Wenn JPG oder GIF
                if($vorschautyp[2]==1 || $vorschautyp[2]==2):
                    // Zufalsskey erzeugen
                    $key=$func->generateRandomKey(10);
                    // Filename setzen
                    $filename=$key.".pdf";
                    // Datei verschieben
                    move_uploaded_file($_FILES['datei']['tmp_name'],LOCALDIR."documents/downloads/".$key.".pdf");
                    // Vorschaubild bearbeiten
                    // Breite und Höhe ermitteln
                    // Setzen der Hoehe und Breite
                    $breite = $vorschautyp[0];
                    $hoehe  = $vorschautyp[1];
                    // Wenn Breite größer als Hoehe und Größer als maximaler Wert
                    if ($breite > $hoehe ):
                        // Festsetzen der Breite und Höhe
                        if ($breite > 70):
                            $breitethumb= 70;
                            $hoehethumb=$breitethumb * $vorschautyp[1] / $vorschautyp[0];
                        else:
                            $breitethumb= $breite;
                            $hoehethumb= $hoehe;
                        endif;    
                    // Ansonsten
                    else:
                        // Festsetzen der Breite und Höhe
                        if ($hoehe >70):
                            $hoehethumb= 70;
                            $breitethumb=$hoehethumb * $vorschautyp[0] / $vorschautyp[1];
                        else:
                            $breitethumb= $breite;
                            $hoehethumb= $hoehe;
                        endif;
                    endif;
                    // Wenn JPG-Datei
                    if ($vorschautyp[2]==2):
                        // Thumb erzeugen
                        $altesBild = ImageCreateFromJPEG($_FILES['vorschau']['tmp_name']);
                        $neuesBild = imageCreateTrueColor($breitethumb, $hoehethumb);
                        imageCopyResampled($neuesBild, $altesBild, 0, 0, 0, 0, $breitethumb, $hoehethumb, $vorschautyp[0], $vorschautyp[1]);
                        ImageJPEG($neuesBild,  LOCALDIR."documents/downloads/preview/".$key.".jpg", 85);
                        chmod( LOCALDIR."documents/downloads/preview/".$key.".jpg", 0777);
                        // Previewname setzen
                        $previewname=$key.".jpg";
                    // Wenn GIF-Datei
                    elseif($vorschautyp[2]==1):
                        // Thumb erzeugen
                        $altesBild = ImageCreateFromGIF($_FILES['vorschau']['tmp_name']);
                        $neuesBild = imageCreateTrueColor($breitethumb, $hoehethumb);
                        imageCopyResampled($neuesBild, $altesBild, 0, 0, 0, 0, $breitethumb, $hoehethumb, $vorschautyp[0], $vorschautyp[1]);
                        ImageGIF($neuesBild,  LOCALDIR."documents/downloads/preview/".$key.".gif");
                        chmod( LOCALDIR."documents/downloads/preview/".$key.".gif", 0777);
                        // Previewname setzen
                        $previewname=$key.".gif";
                    endif;
                // Wenn kein JPG oder GIF
                else: 
                    $_SESSION['err']="Es sind nur JPG oder GIF als Vorschaubilder gestattet.<br/>";
                endif;
            // Wenn kein Vorschaubild hochgeladen wurde
            else:
                $_SESSION['err']="Zu PDF-Dateien muss ein Vorschau-Bild hochgeladen werden.<br/>";
            endif;
        // Wenn kein PDF-Format
        else:
            // Zufalsskey erzeugen
            $key=$func->generateRandomKey(10);
            // Dateiformat auslesen
            $typ=getimagesize($_FILES['datei']['tmp_name']);
            // Wenn JPG
            if ($typ[2]==2):
                // Prüfen ob Vorschaubild ausgewählt wurde
                if ($_FILES['vorschau']['name']!=''):
                    $typ=getimagesize($_FILES['vorschau']['tmp_name']);
                    if ($typ[2]==2):$altesBild = ImageCreateFromJPEG($_FILES['vorschau']['tmp_name']);
                    elseif ($typ[2]==1):$altesBild = ImageCreateFromGIF($_FILES['vorschau']['tmp_name']);
                    else: $_SESSION['err']="Es sind nur JPG oder GIF als Vorschaubilder gestattet.<br/>";
                    endif;
                else:
                    $altesBild = ImageCreateFromJPEG($_FILES['datei']['tmp_name']);
                endif;
                if (!isset($_SESSION['err'])):
                    // Vorschaubild bearbeiten
                    // Breite und Höhe ermitteln
                    // Setzen der Hoehe und Breite
                    $breite = $typ[0];
                    $hoehe  = $typ[1];
                    // Wenn Breite größer als Hoehe und Größer als maximaler Wert
                    if ($breite > $hoehe ):
                        // Festsetzen der Breite und Höhe
                        if ($breite > 70):
                            $breitethumb= 70;
                            $hoehethumb=$breitethumb * $typ[1] / $typ[0];
                        else:
                            $breitethumb= $breite;
                            $hoehethumb= $hoehe;
                        endif;
                    // Ansonsten
                    else:
                        // Festsetzen der Breite und Höhe
                        if ($hoehe >70):
                            $hoehethumb= 70;
                            $breitethumb=$hoehethumb * $typ[0] / $typ[1];
                        else:
                            $breitethumb= $breite;
                            $hoehethumb= $hoehe;
                        endif;
                    endif;
                    // Thumb erzeugen
                    $neuesBild = imageCreateTrueColor($breitethumb, $hoehethumb);
                    imageCopyResampled($neuesBild, $altesBild, 0, 0, 0, 0, $breitethumb, $hoehethumb, $typ[0], $typ[1]);
                    // Wenn Vorschaubild ein JPG
                    if ($typ[2]==2):
                        ImageJPEG($neuesBild,  LOCALDIR."documents/downloads/preview/".$key.".jpg", 85);
                        // Previewname setzen
                        chmod( LOCALDIR."documents/downloads/preview/".$key.".jpg", 0777);
                        $previewname=$key.".jpg";
                    // Wenn Vorschaubild ein GIF
                    else:
                        ImageGIF($neuesBild,  LOCALDIR."documents/downloads/preview/".$key.".gif");
                        chmod( LOCALDIR."documents/downloads/preview/".$key.".gif", 0777);
                        // Previewname setzen
                        $previewname=$key.".gif";
                    endif;
                    // Filename setzen
                    $filename=$key.".jpg";
                    // Datei verschieben
                    move_uploaded_file($_FILES['datei']['tmp_name'],LOCALDIR."documents/downloads/".$key.".jpg");
                endif;
            // Wenn GIF
            elseif ($typ[2]==1):
                // Prüfen ob Vorschaubild ausgewählt wurde
                if ($_FILES['vorschau']['name']!=''):
                    $typ=getimagesize($_FILES['vorschau']['tmp_name']);
                    if ($typ[2]==2):$altesBild = ImageCreateFromJPEG($_FILES['vorschau']['tmp_name']);
                    elseif ($typ[2]==1):$altesBild = ImageCreateFromGIF($_FILES['vorschau']['tmp_name']);
                    else: $_SESSION['err']="Es sind nur JPG oder GIF als Vorschaubilder gestattet.<br/>";
                    endif;
                else:
                    $altesBild = ImageCreateFromGIF($_FILES['datei']['tmp_name']);
                endif;
                if (!isset($_SESSION['err'])):
                    // Vorschaubild bearbeiten
                    // Breite und Höhe ermitteln
                    // Setzen der Hoehe und Breite
                    $breite = $typ[0];
                    $hoehe  = $typ[1];
                    // Wenn Breite größer als Hoehe und Größer als maximaler Wert
                    if ($breite > $hoehe ):
                        // Festsetzen der Breite und Höhe
                        if ($breite > 70):
                            $breitethumb= 70;
                            $hoehethumb=$breitethumb * $typ[1] / $typ[0];
                        else:
                            $breitethumb= $breite;
                            $hoehethumb= $hoehe;
                        endif;
                    // Ansonsten
                    else:
                        // Festsetzen der Breite und Höhe
                        if ($hoehe >70):
                            $hoehethumb= 70;
                            $breitethumb=$hoehethumb * $typ[0] / $typ[1];
                        else:
                            $breitethumb= $breite;
                            $hoehethumb= $hoehe;
                        endif;
                    endif;
                    // Thumb erzeugen
                    $neuesBild = imageCreateTrueColor($breitethumb, $hoehethumb);
                    imageCopyResampled($neuesBild, $altesBild, 0, 0, 0, 0, $breitethumb, $hoehethumb, $typ[0], $typ[1]);
                    // Wenn Vorschaubild ein JPG
                    if ($typ[2]==2):
                        ImageJPEG($neuesBild,  LOCALDIR."documents/downloads/preview/".$key.".jpg", 85);
                        chmod( LOCALDIR."documents/downloads/preview/".$key.".jpg", 0777);
                        // Previewname setzen
                        $previewname=$key.".jpg";
                    // Wenn Vorschaubild ein GIF
                    else:
                        ImageGIF($neuesBild,  LOCALDIR."documents/downloads/preview/".$key.".gif");
                        chmod( LOCALDIR."documents/downloads/preview/".$key.".gif", 0777);
                        // Previewname setzen
                        $previewname=$key.".gif";
                    endif;
                    // Filename setzen
                    $filename=$key.".gif";
                    // Datei verschieben
                    move_uploaded_file($_FILES['datei']['tmp_name'],LOCALDIR."documents/downloads/".$key.".gif");
                endif;
            // Wenn kein JPG oder GIF
            else: $_SESSION['err']="Es sind nur JPG/GIF und PDF-Dateien erlaubt!";
            endif;
        endif;
    // wenn keine Datei ausgewählt wurde
    else: $_SESSION['err']="Sie müssen eine Datei hochladen!";
    endif;
    // Wenn Fehler aufgetreten sind
    if (isset($_SESSION['err'])):
        header("Location:".$l->makeUrl(WEBDIR."admin/downloads/edit.php?".ereg_replace("mode=".$_GET['mode']."&","",ereg_replace("id=".$_GET['id']."&","",$_SERVER['QUERY_STRING']))));
    // Wenn keine Fehler aufgetreten sind
    else:
    // Datensatzarray
    $artikel[]="";
    $artikel[]=$_POST['titel'];
    $artikel[]=$_POST['beschreibung'];
    $artikel[]=$filename;
    $artikel[]=$previewname;
    $artikel[]='NOW()';
    $artikel[]="1";
    $artikel[]=$_POST['sprache'];
    // Datensatz schreiben
    $downloadfac->write($artikel);
    $_SESSION['msg']="Download erfolgreich hinzugefügt!";
    header("Location:".$l->makeUrl(WEBDIR."admin/downloads/view.php?".ereg_replace("mode=".$_GET['mode']."&","",ereg_replace("id=".$_GET['id']."&","",$_SERVER['QUERY_STRING']))));
    endif;
endif;
?>