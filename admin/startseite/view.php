<?php
include("../../inc/config.php");
include(ADMINDIR."inc/adminconfig.php");
include(ADMINDIR."template/header.inc.php");
require_once("../../inc/spaw2/spaw.inc.php");
// Objekt initialisieren
$startseitefac=new Startseite();
$startseitefac->getAll();
while($startseite=$startseitefac->getElement()):
    if ($startseite->typ=="text"):
        $spaw = new SpawEditor("text",stripslashes($startseite->wert));
    endif;
endwhile;
?>
<h1>Startseitenwaltung </h1>
<br/>
<?php if ($_SESSION['err']) echo '<div class="err">'.$_SESSION['err'].'</div><br/>';unset($_SESSION['err']) ?>
<?php if ($_SESSION['msg']) echo '<div class="ok">'.$_SESSION['msg'].'</div><br/>';unset($_SESSION['msg']) ?>
<form action="<?php echo $l->makeFormLink(WEBDIR."admin/startseite/update.php?".htmlentities($_SERVER['QUERY_STRING']))?>" method="post" enctype="multipart/form-data">
    <fieldset class="td1">
        <legend>Startseitentext ändern</legend>
        <?php
        $spaw->width="750px";
        $spaw->height="500px";
        $spaw->show()
        ?>
        <br />
        <input type="submit" value="Startseitentext ändern"/>
   </fieldset>
</form>
    <fieldset class="td2">
        <legend>Widgets</legend>
        diese folgen demnächst
   </fieldset>
</form> 
<br class="clr"/>
<?php
include(ADMINDIR."template/footer.inc.php");
?>