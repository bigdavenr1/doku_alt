<?php
include("../../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
// Adminsprache in Session schreiben
($_GET['adminlang'])?$_SESSION['adminlang']=$_GET['adminlang']:'';
?>
<h1> Übersetzung der Modultemplates verwalten</h1><br/>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" >
    <fieldset class="td2"><legend>Sprache wählen</legend>
        <select name="adminlang">
            <option value="">bitte Sprache wählen</option>
            <?php
            $adminsprachefac=new Sprache();
            $adminsprachefac->getAll("WHERE ISOCode != 'de'"," "," ");
            while($adminsprache=$adminsprachefac->getElement()):
                ?>
                <option value="<?php echo $adminsprache->id?>" <?php echo ($_SESSION['adminlang']==$adminsprache->id)?$sel:''; ?> ><?php echo $adminsprache->name?></option>
                <?php
            endwhile;
            ?>
        </select><br/><input type="submit" value="Sprache wählen"/>
    </fieldset>
</form>
<?php
// Wenn eine Adminsprache gesetzt wurde
if (isset($_SESSION['adminlang']) && $_SESSION['adminlang']!=""):
    ?>
    <table width="100%">
        <tr>
            <?php
            echo $func->tableHeadSort("Modul","title");
            echo $func->tableHeadSort("Teil","count");
            echo $func->tableHeadSort("Übers.","uebersetzung","","","width:100px;");
            echo $func->tableHead("Admin","","","width:30px;");
            ?>
        </tr>
        <?php
        $tplfac=new tpl(20);
        $tplfac->getAllModultemplatesByLang($_SESSION['adminlang']);
        while($tpl=$tplfac->getElement()):    
            $x++;                 
            ?>
            <tr>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo stripslashes(strip_tags($tpl->title))?>
                </td>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo stripslashes(strip_tags($tpl->count))?>
                </td>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo stripslashes(strip_tags($tpl->uebersetzung))?>
                </td>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/uebersetzung/modultemplates/edit.php?id=".$tpl->id."&amp;".htmlentities($_SERVER['QUERY_STRING']));?>
                </td>
            </tr>
            <?php
        endwhile;
        ?> 
    </table>
    <br class="clr"/>
    <div id="htmlnavi">
        <?php echo $tplfac->getHtmlNavi("std","&amp;".htmlentities($_SERVER['QUERY_STRING']));?>
    </div>
<?php
else: echo "<br/>Bitte zuerst Sprache wählen!";
endif;
include(INCLUDEDIR."footer.inc.php");
?>