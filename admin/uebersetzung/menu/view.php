<?php
include("../../../inc/config.php");
include(ADMINDIR."inc/adminconfig.php");
include(ADMINDIR."template/header.inc.php");
// Adminsprache in Session schreiben
$_SESSION['adminlang']=($_GET['adminlang']?$_GET['adminlang']:$_SESSION['adminlang']);
?>
<h1> Übersetzung der Menus verwalten</h1><br/>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" >
    <fieldset class="td2"><legend>Sprache wählen</legend>
        <select name="adminlang">
            <option value="">bitte Sprache wählen</option>
            <?php
            $adminsprachefac=new Sprache();
            $adminsprachefac->getAll("WHERE ISOCode != 'de'"," "," ");
            while($adminsprache=$adminsprachefac->getElement()):
                ?>
                <option value="<?php echo $adminsprache->id?>" <?php echo ($_SESSION['adminlang']==$adminsprache->id)?$sel:''; ?> ><?php echo $adminsprache->name?></option>
                <?php
            endwhile
            ?>
        </select><br/><input type="submit" value="Sprache wählen"/>
    </fieldset>
</form>
<?php
// Wenn eine Adminsprache gesetzt wurde
if (isset($_SESSION['adminlang']) && $_SESSION['adminlang']!=""):
    // menudaten holen
    $menudatafac=new MenuTemplate();
    $menudata=$menudatafac->getMenuDataForTranslation($_SESSION['adminlang']);
    // leere Arrays setzen
    $menupoints=array();
    $menutypen=array();
    // Datenliste durchgehen
    foreach ($menudata AS $key => $value):
        // menutyparray füllen
        if (!$menupoints[stripslashes(strip_tags($value->id))] && trim($value->name)!="") :
            $menupoints[stripslashes(strip_tags($value->id))]=array(stripslashes(strip_tags($value->name)),stripslashes(strip_tags($value->uebersetzung)));
        endif;
        // Menupunktearray füllen
        if (!$menutypen[stripslashes(strip_tags($value->menuid))] && trim($value->menutitle)!=""):
            $menutypen[stripslashes(strip_tags($value->menuid))]=array(stripslashes(strip_tags($value->menutitle)),stripslashes(strip_tags($value->menutypubersetzung)));
        endif;
    endforeach;
    ?>
    <form action="<?php echo $l->makeFormLink(WEBDIR."admin/uebersetzung/menu/update.php");?>" method="post" >
        <fieldset class="td1">
            <legend>Menutypen übersetzen</legend>
            <table style="width:100%;">
                <tr>
                    <th>
                        original
                    </th>
                    <th>
                        Übersetzung
                    </th>
                </tr>
                <?php
                // Menutypenarray auslesen und ausgeben
                foreach($menutypen AS $key => $value):
                    echo '<tr><td>'.$value[0].'</td>';
                    echo '<td><input type="text" name="menutype['.$key.']" value="'.$value[1].'" /></td>';
                endforeach;
                ?>
            </table>
        </fieldset >
        <fieldset class="td2">
            <legend>Menupunkte übersetzen</legend>
            <table style="width:100%;">
                <tr>
                    <th>
                        original
                    </th>
                    <th>
                        Übersetzung
                    </th>
                </tr>
                <?php
                // Menupunktearray auslesen und durchgehen
                foreach($menupoints AS $key => $value):
                    echo '<tr><td>'.$value[0].'</td>';
                    echo '<td><input type="text" name="menupoints['.$key.']" value="'.$value[1].'" /></td>';
                endforeach;
                ?>
            </table>
        </fieldset>
        <fieldset>
            <input type="submit" value="Übersetzungen speichern"/>
        </fieldset>
    </form>
    <?php
else: echo "<br/>Bitte zuerst Sprache wählen!";
endif;
include(ADMINDIR."template/footer.inc.php");
?>