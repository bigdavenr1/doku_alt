<?php
include("../../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
require_once(INCLUDEDIR."spaw2/spaw.inc.php");
// Adminsprache in Session schreiben
($_GET['adminlang'])?$_SESSION['adminlang']=$_GET['adminlang']:'';
?>
<h1> Übersetzung eines Newsartikels verwalten</h1><br/>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" >
    <fieldset class="td2"><legend>Sprache wählen</legend>
        <select name="adminlang">
            <option value="">bitte Sprache wählen</option>
            <?php
            $adminsprachefac=new Sprache();
            $adminsprachefac->getAll("WHERE ISOCode != 'de'"," "," ");
            while($adminsprache=$adminsprachefac->getElement())
            {
                ?>
                <option value="<?php echo $adminsprache->id?>" <?php echo ($_SESSION['adminlang']==$adminsprache->id)?$sel:''; ?> ><?php echo $adminsprache->name?></option>
                <?php
            }
            ?>
        </select><br/><input type="submit" value="Sprache wählen"/>
    </fieldset>
</form>
<?php
// Wenn eine Adminsprache gesetzt wurde
if (isset($_SESSION['adminlang']) && $_SESSION['adminlang']!="")
{
    $newsfac=new News();
    $newsfac->getByIdWithTranslation($_GET['id'],$_SESSION['adminlang']);
    $news=$newsfac->getElement();
    $spaw=new SpawEditor("text",stripslashes($news->text_uebersetzung));
    $spaw->width="100%";
    $spaw->height="400px";
    ?>
    <form action="<?php echo $l->makeFormLink(WEBDIR."admin/uebersetzung/news/update.php?id=".$_GET['id']);?>" method="post" >
        <fieldset class="td1">
           <legend>Newstitel</legend>
           <div class="td2" style="padding:5px;">
               <label><b>Original:</b></label><br class="clr"/>
               <?php echo strip_tags(stripslashes($news->titel))?>
           </div>                                                  <br/>
           <div class="td2" style="padding:5px;">
               <label><b>Übersetzung:</b></label><br class="clr"/>
               <input type="text" name="titel" value="<?php echo strip_tags(stripslashes($news->titel_uebersetzung))?>"/>
           </div>         
        </fieldset>
        <fieldset class="td2">
           <legend>Newsinhalt</legend>
           <div class="td1" style="padding:5px;">
               <label><b>Original:</b></label><br class="clr"/>
               <?php echo stripslashes($news->text)?>
           </div>                                                  <br/>
           <div class="td1" style="padding:5px;">
               <label><b>Übersetzung:</b></label><br class="clr"/>
                    <?php $spaw->show();?>
           </div>            
        </fieldset>
        <fieldset>
            <input type="submit" value="Übersetzung speichern">
        </fieldset>
    </form>
    <?php
}    
else echo "<br/>Bitte zuerst Sprache wählen!";
include(INCLUDEDIR."footer.inc.php");
?>