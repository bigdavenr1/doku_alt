<?php
include("../../../inc/config.php");
include(INCLUDEDIR."header.inc.php");
// Adminsprache in Session schreiben
($_GET['adminlang'])?$_SESSION['adminlang']=$_GET['adminlang']:'';
?>

<h1> Übersetzung der News verwalten</h1><br/>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" >
    <fieldset class="td2"><legend>Sprache wählen</legend>
        <select name="adminlang">
            <option value="">bitte Sprache wählen</option>
            <?php
            $adminsprachefac=new Sprache();
            $adminsprachefac->getAll("WHERE ISOCode != 'de'"," "," ");
            while($adminsprache=$adminsprachefac->getElement())
            {
                ?>
                <option value="<?php echo $adminsprache->id?>" <?php echo ($_SESSION['adminlang']==$adminsprache->id)?$sel:''; ?> ><?php echo $adminsprache->name?></option>
                <?php
            }
            ?>
        </select><br/><input type="submit" value="Sprache wählen"/>
    </fieldset>
</form>
<?php
// Wenn eine Adminsprache gesetzt wurde
if (isset($_SESSION['adminlang']) && $_SESSION['adminlang']!="")
{
    ?>
    <span class="errbox">!HINWEIS! Alle nicht in einer Sprache übersetzten Newsartikel werden auch in der jeweiligen Sprachversion <b>NICHT</b> angezeigt!</span><br/>
    <table width="100%">
        <tr>
            <?php
            echo $func->tableHeadSort("Newstitel","titel","","");
            echo $func->tableHeadSort("Übersetzung vorh.","uebersetzung");
            echo $func->tableHeadSort("Änderungsdatum","zeit");
            echo $func->tableHead("Admin","","","width:30px;");
            ?>
        </tr>
        <?php 
        $newsfac=new News(20);
        $newsfac->getNewsForUebersetzung($_SESSION['adminlang']);
        while($news=$newsfac->getElement())
        {
            $x++;                 
            ?>
            <tr>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo stripslashes(strip_tags($news->titel))?>
                </td>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo stripslashes(strip_tags($news->uebersetzung))?>
                </td>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo stripslashes(strip_tags($news->zeit))?>
                </td>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/uebersetzung/news/edit.php?id=".$news->id."&amp;".htmlentities($_SERVER['QUERY_STRING']));?>
                </td>
            </tr>
            <?php
        }
        ?> 
    </table>
    <br class="clr"/>
    <div id="htmlnavi">
        <?php echo $newsfac->getHtmlNavi("std","&amp;".htmlentities($_SERVER['QUERY_STRING']));?>
    </div>
<?php
}
else echo "<br/>Bitte zuerst Sprache wählen!";
include(INCLUDEDIR."footer.inc.php");
?>