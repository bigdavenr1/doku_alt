<?php
include("../../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
require_once(ADMINDIR."template/header.inc.php");   
// Adminsprache in Session schreiben
($_GET['adminlang'])?$_SESSION['adminlang']=$_GET['adminlang']:'';
?>
<h1> Übersetzung der Startseite verwalten</h1><br/>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" >
    <fieldset class="td2"><legend>Sprache wählen</legend>
        <select name="adminlang">
            <option value="">bitte Sprache wählen</option>
            <?php
            $adminsprachefac=new Sprache();
            $adminsprachefac->getAll("WHERE ISOCode != 'de'");
            while($adminsprache=$adminsprachefac->getElement())
            {
                ?>
                <option value="<?php echo $adminsprache->id?>" <?php echo ($_SESSION['adminlang']==$adminsprache->id)?$sel:''; ?> ><?php echo $adminsprache->name?></option>
                <?php
            }
            ?>
        </select><br/><input type="submit" value="Sprache wählen"/>
    </fieldset>
</form>
<?php
// Wenn eine Adminsprache gesetzt wurde
if (isset($_SESSION['adminlang']) && $_SESSION['adminlang']!="")
{
    require_once("../../../inc/spaw2/spaw.inc.php");
    // Objekt initialisieren
    $startseitefac=new Startseite();
    $startseitefac->getStartseiteForUebersetzung($_SESSION['adminlang']);
    $startseite=$startseitefac->getElement();
    $spaw = new SpawEditor("rechts",stripslashes($startseite->right_uebersetzung));
    
    ?>
    <h1>Startseitenwaltung </h1>
    <br/>
    <?php if ($_SESSION['err']) echo '<div class="err">'.$_SESSION['err'].'</div><br/>';unset($_SESSION['err']) ?>
    <?php if ($_SESSION['msg']) echo '<div class="ok">'.$_SESSION['msg'].'</div><br/>';unset($_SESSION['msg']) ?>
    <form action="<?php echo $l->makeFormLink(WEBDIR."admin/uebersetzung/startseite/update.php?".htmlentities($_SERVER['QUERY_STRING']))?>" method="post">
        <fieldset class="td1">
            <legend>Übersetzung der Bildinfo der Startseite ändern</legend>
            <br/>
            <b>Original</b> 
            <div class="td2"> 
                <label>Überschrift</label><div style="float:left;padding-top:7px"><?php echo stripslashes(strip_tags($startseite->titel))?></div><br class="clr"/>
                <label>2. Zeile</label><div style="float:left;padding-top:7px"><?php echo stripslashes(strip_tags($startseite->subtitel))?></div><br class="clr"/>
                <label>Text</label><div style="float:left;padding-top:7px"><?php echo stripslashes(strip_tags($startseite->text))?></div><br class="clr"/>
            </div>    
            <br/><br/>
            <b>Übersetzung</b>
            <div class="td2">
                <label>Überschrift übers.</label><input type="text" name="starttitel" value="<?php echo stripslashes(strip_tags($startseite->titel_uebersetzung))?>"><br class="clr"/><br/>
                <label>2. Zeile übers</label><input type="text" name="subtitel" value="<?php echo stripslashes(strip_tags($startseite->subtitel_uebersetzung))?>"><br class="clr"/><br/>
                <label>Text übersetzen</label><textarea style="margin:0px;" name="starttext"><?php echo stripslashes(strip_tags($startseite->text_uebersetzung))?></textarea><br/><br/>
            </div> <br/>
            <input type="submit" value="Übersetzung Bildinfo ändern">
       </fieldset>
       <fieldset class="td2">
            <legend>Übersetzung der rechten Startseite ändern</legend>
            <b>Original</b>
            <div class="td1">
               <?php echo stripslashes($startseite->rechts)?>
            </div>    
            <br/><br/>
            <b>Übersetzung</b>
            <div class="td1">
                ACHTUNG Bilder dürfen maximal 280 Pixel breit sein!<br/>Es setehen für diesen Bereich auch für Texte maximal 280 Pixel in der Breite zur Verfügung! <br/><br/>
                <?php
                $spaw->width="240px";
                $spaw->show()
                ?>
            </div>
            <br/>
            <input type="submit" value="Übersetzung rechter Startseitenbereich ändern">
       </fieldset>
    </form>           
    <br class="clr"/>
<?php
}
else echo "<br/>Bitte zuerst Sprache wählen!";
include(ADMINDIR."template/footer.inc.php");
?>