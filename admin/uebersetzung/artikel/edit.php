<?php
include("../../../inc/config.php");
include(ADMINDIR."inc/adminconfig.php");
include(ADMINDIR."template/header.inc.php");
require_once(INCLUDEDIR."spaw2/spaw.inc.php");
// Adminsprache in Session schreiben
($_GET['adminlang'])?$_SESSION['adminlang']=$_GET['adminlang']:'';
?>
<h1> Übersetzung einer Inhaltsseite verwalten</h1><br/>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" >
    <fieldset class="td2"><legend>Sprache wählen</legend>
        <select name="adminlang">
            <option value="">bitte Sprache wählen</option>
            <?php
            $adminsprachefac=new Sprache();
            $adminsprachefac->getAll("WHERE ISOCode != 'de'"," "," ");
            while($adminsprache=$adminsprachefac->getElement())
            {
                ?>
                <option value="<?php echo $adminsprache->id?>" <?php echo ($_SESSION['adminlang']==$adminsprache->id)?$sel:''; ?> ><?php echo $adminsprache->name?></option>
                <?php
            }
            ?>
        </select><br/><input type="submit" value="Sprache wählen"/>
    </fieldset>
</form>
<?php
// Wenn eine Adminsprache gesetzt wurde
if (isset($_SESSION['adminlang']) && $_SESSION['adminlang']!=""):
    $artikelfac= new Artikel();
    $artikelfac->getbyIdWithTranslation($_GET['id'],$_SESSION['adminlang']);
    $artikel=$artikelfac->getElement();
    $spaw=new SpawEditor("inhalt_uebersetzung",stripslashes($artikel->text_uebersetzung));
    $spaw->width="100%";
    $spaw->height="400px";
    ?>
    <form action="<?php echo $l->makeFormLink(WEBDIR."admin/uebersetzung/artikel/update.php?id=".$_GET['id']);?>" method="post" >
        <fieldset class="td1">
           <legend>Titel</legend>
           <div class="td2" style="padding:5px;">
               <label><b>Original:</b></label><br class="clr"/>
               <?php echo stripslashes(strip_tags($artikel->title))?>
           </div>   
           <div class="td2" style="padding:5px;">
               <label><b>Übersetzung:</b></label><br class="clr"/>
               <input typ="text" name="title_uebersetzung" value="<?php echo stripslashes(strip_tags($artikel->title_uebersetzung))?>" />
           </div>            
        </fieldset>
        <fieldset class="td2">
           <legend>Inhalt</legend>
           <div class="td1" style="padding:5px;">
               <label><b>Original:</b></label><br class="clr"/> <br/>
               <div style="height:200px;overflow:auto">
                    <?php echo stripslashes($artikel->introtext)?>
              </div>
           </div>   
           <br/>
           <div class="td2" style="padding:5px;">
               <label><b>Übersetzung:</b></label><br class="clr"/>
              <?php $spaw->show();?>
           </div>            
        </fieldset>
        <fieldset>
            <input type="submit" value="Übersetzung speichern">
        </fieldset>
    </form>
    <?php 
else: echo "<br/>Bitte zuerst Sprache wählen!";
endif;
include(ADMINDIR."template/footer.inc.php");
?>