<?php
include("../../../inc/config.php");
include(ADMINDIR."inc/adminconfig.php");
include(ADMINDIR."template/header.inc.php");
// Adminsprache in Session schreiben
$_SESSION['adminlang']=($_GET['adminlang']?$_GET['adminlang']:$_SESSION['adminlang']);
?>
<h1> Übersetzung der Artikel verwalten</h1><br/>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method="get" >
    <fieldset class="td2"><legend>Sprache wählen</legend>
        <select name="adminlang">
            <option value="">bitte Sprache wählen</option>
            <?php
            $adminsprachefac=new Sprache();
            $adminsprachefac->getAll("WHERE ISOCode != 'de'"," "," ");
            while($adminsprache=$adminsprachefac->getElement()):
               ?>
                <option value="<?php echo $adminsprache->id?>" <?php echo ($_SESSION['adminlang']==$adminsprache->id)?$sel:''; ?> ><?php echo $adminsprache->name?></option>
                <?php
            endwhile;
            ?>
        </select><br/><input type="submit" value="Sprache wählen"/>
    </fieldset>
</form>
<?php
// Wenn eine Adminsprache gesetzt wurde
if (isset($_SESSION['adminlang']) && $_SESSION['adminlang']!=""):
    ?>
    <table width="100%">
        <tr>
            <?php
            echo $func->tableHeadSort("Titel","title");
            echo $func->tableHeadSort("Übersetzung","text");
            echo $func->tableHeadSort("Änderungsdatum","zeit");
            echo $func->tableHead("Admin","","","width:30px;");
            ?>
        </tr>
        <?php
        $artikelfac=new Artikel(20);
        $artikelfac->getAllArtTitle($_SESSION['adminlang']);
        while($artikel=$artikelfac->getElement()):
            $x++;                 
            ?>
            <tr>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo stripslashes(strip_tags($artikel->title))?>
                </td>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo stripslashes(strip_tags($artikel->text))?>
                </td>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo stripslashes(strip_tags($artikel->zeit))?>
                </td>
                <td class="td<?php echo ($x%2)?'1':'2'?>">
                     <?php echo $l->makeLink($icon_edit_small,WEBDIR."admin/uebersetzung/artikel/edit.php?id=".$artikel->id."&amp;".htmlentities($_SERVER['QUERY_STRING']));?>
                </td>
            </tr>
            <?php
        endwhile;
        ?> 
    </table>
    <br class="clr"/>
    <div id="htmlnavi">
        <?php echo $artikelfac->getHtmlNavi("std","&amp;".htmlentities($_SERVER['QUERY_STRING']));?>
    </div>
<?php
else: echo "<br/>Bitte zuerst Sprache wählen!";
endif;
include(ADMINDIR."template/footer.inc.php");
?>