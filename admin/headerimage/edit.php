<?php #############################################
# admin/haederimage/edit.php                      #
# Zeigt Bildauswahl, Bildupload und cropper für   #
# haedergrafik an                                 #
# Gehörend zu nbCMS V1.01 27.09.2010              #
#                                                 #
# benötigte Komponenten:                          #
# - nbCMS-Kerneldateien                           #
###################################################
include("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
// Wenn kein File hochgeladen wurde oder gerade in Bearbeitung ist
if ($_FILES['bild']['tmp_name']=="" && $_POST['tmp_bildname']==""):
    // artikelnummer speichern
    $_SESSION['artnr']=$_GET['art'];
    ?>
    <html>
        <head>
            <title>Headergrafik hochladen</title>
            <link href="<?php echo WEBDIR;?>admin/template/style/admin.css" type="text/css" rel="stylesheet" />
            <script src="<?php echo WEBDIR?>script/helper.js" type="text/javascript"></script>
        </head>
        <body style="background:none;background-color:#FFF">
            <div style="float:left;width:370px;border-right:1px solid black;padding:10px;height:100%">
                <h1>Headergrafik auswählen</h1>
                <div style="height:450px;overflow:auto">
                    <?php
                    // Bilder aus einem Verzeichnis auslesen
                    // und sortiert in einer Tabelle anzeigen
                    $bilderliste = array();
                    $verzeichnis = "images/header/";
                    $handle = openDir(LOCALDIR.$verzeichnis);
                    while ($datei = readDir($handle)) :
                        $info=getimagesize(LOCALDIR.$verzeichnis.$datei);
                        if ($info[2]==1 || $info[2]==2 || $info[2]==3) :
                            array_push($bilderliste, array(filemtime(LOCALDIR.$verzeichnis.$datei) , $verzeichnis.$datei , $info[0] , $info[1]));
                        endif;
                    endwhile;
                    closeDir($handle);
                    rsort($bilderliste);
                    foreach ($bilderliste as $zaehler => $element) :
                        echo '<a href="javascript:void(0)" onclick="replace_op(/(&bild=)+([^&]*)/,\'&bild='.$element[1].'\')"><img style="margin:2px;" src="'.WEBDIR.$element[1].'" width="345px;"></a>';
                    endforeach;?>
                </div>
            </div>
            <div style="float:left;width:370px;padding:10px;">
                <h1>neue Headergrafik hochladen</h1>
                <?php if ($_SESSION['err']):echo '<div class="err">'.$_SESSION['err'].'</div>';unset($_SESSION['err']);endif;?>
                <form action="<?php echo $_SERVER['PHP_SELF']?>" method="post" enctype="multipart/form-data">
                    Bilddatei auswählen<br/><input type="file" name="bild"><br class="clr"/><br/>
                    <input type="submit" value="Datei hochladen"/>
                </form>
            </div>
            <br class="clr"/>
        </body>
    </html>
<?php
// Wenn ein Bild hochgeladen aber noch nicht gecroppt wurde
elseif ($_FILES['bild']['tmp_name']!="" && $_POST['tmp_bildname']=="" ):
    // Bilddaten auslesen
    $typ=getimagesize($_FILES['bild']['tmp_name']);
    // neuen zufälligen Dateinamen erstellen
    $filename=$func->generateRandomKey();
    // Wenn Typ stimmt
    if($typ[2]==(1||2||3) && $typ[0]>=837 && $typ[1]>=190):
        $ext=($typ[2]==1?'gif':($typ[2]==2?'jpg':'png'));
        // Originalbild temporär abspeichern
        move_uploaded_file($_FILES['bild']['tmp_name'],"pic_tmp/".$filename.".".$ext);
        // Aufruf des ImageCreate Befehls je nach DAtentyp
        switch ($typ[2]):
            case 2: $altesBild=ImageCreateFromJPEG("pic_tmp/".$filename.".".$ext);
            break;
            case 1: $altesBild=ImageCreateFromGIF ("pic_tmp/".$filename.".".$ext);
            break;
            default: $altesBild=ImageCreateFromPNG ("pic_tmp/".$filename.".".$ext);
            break;
        endswitch;
        // neues Bild zeichen
        $neuesBild = imageCreateTrueColor(500, floor($typ[1]*500/$typ[0]));
        // Altes Bild in Neues gesampelt laden
        imageCopyResampled($neuesBild, $altesBild, 0, 0, 0, 0, 500, floor($typ[1]*500/$typ[0]), $typ[0], $typ[1]);
        // Thumbdate erzeugenBildes
        switch ($typ[2]):
            case 2: ImageJPEG($neuesBild,"pic_tmp/".$filename."_thumb.".$ext, 85);
            break;
            case 1: ImageGIF($neuesBild,"pic_tmp/".$filename."_thumb.".$ext);
            break;
            default: ImagePNG($neuesBild,"pic_tmp/".$filename."_thumb.".$ext, 85);
            break;
        endswitch;
        chmod("pic_tmp/".$filename.".".$ext,0777);
        ?>
        <html>
            <head>
                <title>Headergrafik hochladen</title>
                <link href="<?php echo WEBDIR;?>admin/template/style/admin.css" type="text/css" rel="stylesheet" />
                <link rel="stylesheet" href="<?php echo WEBDIR;?>style/lightbox.css" type="text/css" media="screen" />
                <link href="<?php echo WEBDIR;?>style/cropper.css" type="text/css" rel="stylesheet" />
                <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
                <meta http-equiv="Content-Language" content="en-us" />
                <script src="<?php echo WEBDIR;?>script/prototype.js" type="text/javascript"></script>
                <script src="<?php echo WEBDIR;?>script/scriptaculous.js" type="text/javascript"></script>
                <script src="<?php echo WEBDIR;?>script/cropper.js" type="text/javascript"></script>
                <script src="<?php echo WEBDIR?>script/helper.js" type="text/javascript"></script>
                <script type="text/javascript" charset="utf-8">
                    // setup the callback function
                    function onEndCrop( coords, dimensions ) {
                        $( 'x1' ).value = coords.x1;
                        $( 'y1' ).value = coords.y1;
                        $( 'x2' ).value = coords.x2;
                        $( 'y2' ).value = coords.y2;
                        $( 'width' ).value = dimensions.width;
                        $( 'height' ).value = dimensions.height;
                    }
                    // basic example
                    Event.observe(
                        window,
                        'load',
                        function() {
                            new Cropper.Img(
                                'testImage',
                                {
                                    onEndCrop: onEndCrop,
                                    displayOnInit: true,
                                    onloadCoords: { x1: 0, y1: 0, x2: 837, y2: 190},
                                    ratioDim: { x: 837, y: 190}
                                }
                            )
                        }
                    );
                </script>
                <style type="text/css">
                    #testWrap {
                        margin-left:10px; /* Just while testing, to make sure we return the correct positions for the image & not the window */
                    }
                </style>
            </head>
            <body style="background:none;background-color:#FFF">
                <h1>neues Bild hochladen</h1>
                <b>Vorschaubild - Ausschnitt wählen</b><br/><br/>
                <div id="testWrap">
                    <img src="pic_tmp/<?php echo $filename?>_thumb.<?php echo $ext?>" alt="test image" id="testImage" />
                </div>
                <form action="<?php $_SERVER[PHP_SELF]?>" method="post">
                    <input type="hidden" name="tmp_bildname" id="tmp_bildname" value="<?php echo $filename.".".$ext?>"/>
                    <input type="hidden" name="thumbname" id="thumbname" value="<?php echo $filename."_thumb.".$ext?>"/>
                    <input type="hidden" name="x1" id="x1" />
                    <input type="hidden" name="y1" id="y1" />
                    <input type="hidden" name="x2" id="x2" />
                    <input type="hidden" name="y2" id="y2" />
                    <input type="hidden" name="width" id="width" />
                    <input type="hidden" name="height" id="height" /><br/><br/>
                    <input type="submit" value="Bildausschnitt speichern"/><br/>
                </form>
            </body>
        </html>
    <?php
    // Wenn kein GIF oder JPG hochgeladen wurde-> Zurückleitung
    else:
        $_SESSION['err']="Falscher Dateityp oder falsche Bildgröße.<br/> Es sind nur JPG , GIF oder PNG Bilder erlaubt!<br/>Mindestgröße des Bildes: 837 x 190 Pixel!";
        header("LOCATION:".$_SERVER['PHP_SELF']);
    endif;
else:
    // Bildinfos auslesen
    $typ=getimagesize("pic_tmp/".$_POST['tmp_bildname']);
    $ext=($typ[2]==1?'gif':($typ[2]==2?'jpg':'png'));
    // Aufruf des ImageCreate Befehls je nach Datentyp
    switch ($typ[2]):
        case 2: $altesBild=ImageCreateFromJPEG("pic_tmp/".$_POST['tmp_bildname']);
        break;
        case 1: $altesBild=ImageCreateFromGIF ("pic_tmp/".$_POST['tmp_bildname']);
        break;
        default: $altesBild=ImageCreateFromPNG ("pic_tmp/".$_POST['tmp_bildname']);
        break;
    endswitch;
    #################
    # Bild erzeugen #
    #################
    // neues Bild zeichen
    $neuesBild = imageCreateTrueColor(837, 190);
    // Größenberechnungen
    $thumbheight=floor($typ[1]*500/$typ[0]);
    $srcx=floor($typ[0]*$_POST['x1']/500);
    $srcy=floor($typ[1]*$_POST['y1']/$thumbheight);
    $srcwidth=floor($typ[0]*$_POST['width']/500);
    $srcheight=floor($typ[1]*$_POST['height']/$thumbheight);
    // Altes Bild in Neues gesampelt laden
    imageCopyResampled($neuesBild, $altesBild, 0, 0, $srcx , $srcy, 837, 190, $srcwidth, $srcheight);
    // Bild speichern
    switch ($typ[2]):
        case 2: ImageJPEG($neuesBild,LOCALDIR."images/header/".$_POST['tmp_bildname'], 85);
        break;
        case 1: ImageGIF($neuesBild,LOCALDIR."images/header/".$_POST['tmp_bildname']);
        break;
        default: ImagePNG($neuesBild,LOCALDIR."images/header/".$_POST['tmp_bildname'], 85);
        break;
    endswitch;
    // Bild freigeben
    chmod(LOCALDIR."images/header/".$_POST['tmp_bildname'],0777);
    unlink("pic_tmp/".$_POST['tmp_bildname']);
    unlink("pic_tmp/".$_POST['thumbname']);
    // Fenster schließen und Artikelfenster aktualisieren
    ?>
    <html>
    <head>
        <script src="<?php echo WEBDIR?>script/helper.js" type="text/javascript"></script>
    </head>
        <body onload="replace_op(/(&bild=)+([^&]*)/,'&bild=images/header/<?php echo $_POST['tmp_bildname']?>')">
        </body>
    </html>
    <?php
endif;