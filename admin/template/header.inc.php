<!DOCTYPE HTML>
<html lang="de">
    <head>
        <title>Adminbereich nbCMS</title>
        <link rel="stylesheet" href="<?php echo WEBDIR?>admin/template/style/admin.css" type="text/css" />
    </head>
    <body>
        <div id="container">
            <div id="top">
                <img src="<?php echo WEBDIR?>admin/template/images/nbCMS-logo.png"/><h1> Administrationsbereich</h1>
            </div>            
            <?php
                $menutyp="1";
                require(ADMINDIR."template/menu.inc.php");
                // Adminmenu inkludieren
            ?>
            <div id="content">
            