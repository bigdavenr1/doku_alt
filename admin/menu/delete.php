<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");  
// Objekt initialisieren
$menufac= new MenuTemplate();
$menufac->getById($_GET['id']); 
$menu=$menufac->getElement();
if ( $menu->rgt-$menu->lft==1):
    // Sicherheitsabfrage
    if (!$_GET['bestaetigt']):
        require_once(ADMINDIR."template/header.inc.php");
        ?>
        <h1>Menupunkt löschen - Bitte bestätigen</h1><br/>
        Wollen Sie den Menupunkt:<br/><br/><b><?php echo stripslashes(strip_tags($menu->name))?></b><br/><br/>wirklich löschen?<br/><br/>
        <?php echo $l->makeLink('<b>[ JA ]</b>',$_SERVER['PHP_SELF']."?bestaetigt=yes&amp;".htmlentities($_SERVER['QUERY_STRING']),"adminlink").' / '.$l->makeLink('<b>[ NEIN ]</b>',WEBDIR."admin/menu/view.php?mode=back&amp;".htmlentities($_SERVER['QUERY_STRING']),"adminlink");
        require_once(ADMINDIR."template/footer.inc.php");
    // Wenn Löschen bestätigt
    else:      
        $menufac->deleteElement("id",$menu->id); 
        $menufac->update("lft =lft- (CASE 
                                   WHEN lft > ".$menu->lft." 
                                   THEN 2 
                                   ELSE 0 
                                 END), 
                          rgt = CASE WHEN rgt != ".$menu->rgt." THEN rgt-2 ELSE rgt END 
                          WHERE rgt>".$menu->lft." AND lft!= ".$menu->lft." AND rgt!=".$menu->rgt); $_SESSION['msg']="Menupunkt erfolgreich gelöscht";       
    endif;
// Wenn Beitrag nicht gefunden wurde
else :
  $_SESSION['err']="Eintrag nicht gefunden oder Knoten nicht leer!";
endif;
header ("Location:".$l->makeUrl(WEBDIR."admin/menu/view.php?".ereg_replace("mode=".$_GET['mode']."&","",ereg_replace("id=".$_GET['id']."&","",ereg_replace("typ=".$_GET['typ']."&","",$_SERVER['QUERY_STRING'])))));
?>