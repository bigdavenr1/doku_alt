<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
require_once(ADMINDIR."template/header.inc.php");   
// Objekt initialisieren
$menufac= new MenuTemplate();
// Daten holen
$menufac->getById($_GET['id']);
$menu=$menufac->getElement();
?>
<h1><?php echo ($menu)?"Menupunkt: ".$menu->name." - bearbeiten":"neuen Menupunkt erstellen"?></h1>
<br/>
<form action="<?php echo $l->makeFormLink(WEBDIR."admin/menu/update.php?".htmlentities($_SERVER['QUERY_STRING']))?>" method="post" >
    <fieldset class="td1">
        <legend>Menupunktdaten</legend>
        <?php echo ($menu->id)?'<label for="id">ID</label>'.stripslashes(strip_tags($menu->id)).'<br class="clr"/>':'';?>
        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="<?php echo stripslashes(strip_tags($menu->name))?>"/><br class="clr"/>
        <label for="firma">Oberpunkt</label>
            <?php
            if ($menu->rgt-$menu->lft==1 || empty($menu->lft)):
                // Menudaten holen
                $menudata1fac = new MenuTemplate();
                $menudata1fac->getMenuAsDropbox("oberpunkt",$menu->lft,$menu->rgt);
            else:
                echo "Dieser Punkt ist ein Oberpunkt und enthält Unterpunkte! <br/>Somit bei der gewählten Menutiefe keinem anderen Punkt unterordbar!";
                echo '<input type="hidden" name="oberpunkt" value="'.$menu->menutype.'---'.$menu->lft.'---'.$menu->rgt.'">';
            endif;
            ?>
        <br class="clr"/>
        <label for="inhalt">Inhalt / Zuordnung</label>
        <select name="inhalt" id="inhalt">
            <option value="">Nur als Punkt ohne Inhalt</option>
            <option value="">-----------------------</option>
            <?php
            $artikelfac=new Artikel();
            $artikelfac->getAll();
            while($artikel=$artikelfac->getElement()):
                echo '<option value="artikel---'.$artikel->id.'" '.(($menu->type=="artikel" && $menu->link==$artikel->id)?$sel:'').'> - '.$artikel->title.'</option>';
            endwhile;
            ?>
            <option value=""> </option>
            <option value="">Module</option>
            <option value="">-----------------------</option>
            <?php
            $modulfac=new Modul();
            $modulfac->getAll();
            while($modul=$modulfac->getElement()):
                echo '<option value="modules---'.$modul->id.'" '.(($menu->type=="modules" && $menu->link==$modul->id)?$sel:'').'> - '.$modul->title.'</option>';
            endwhile;
            ?>
        </select>
    </fieldset>
    <fieldset class="none"/>
        <input type="submit" class="submit" value="Menupunkt speichern"/>
    </fieldset>
</form>
<?php
require_once(ADMINDIR."template/footer.inc.php");
?>