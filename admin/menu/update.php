<?php
require_once("../../inc/config.php");
require_once(ADMINDIR."inc/adminconfig.php");
//require_once(ADMINDIR."inc/menu.class.php");
// Newsobjekt erzeugen
$menufac= new MenuTemplate();
// Daten holen
$menufac->getById($_GET['id']);
// Übergabewerte aufbereiten
$linkarray=split("---",$_POST['inhalt']);
$menupunktarray=split("---",$_POST['oberpunkt']);
// Bei update und neuen Menupunkt schauen ob Modul gewählt
if ($linkarray[0]=="modules"):
    // Modulnamen auslesen
    $modulnamefac=new Modul();
    $modulnamefac->getById($linkarray[1]);
    $modul=$modulnamefac->getElement();
endif;
##############################################
# Wenn neuer Menupunkt erstellt werden soll. #
##############################################
if ($_GET['mode']=="newpoint"):
    // Übergabewerte aufbereiten
    $linkarray=split("---",$_POST['inhalt']);
    $menupunktarray=split("---",$_POST['oberpunkt']);   
    // Alle Einträge dir Größer als neu einzutragender Sind, müssen lft und rgt angepasst bekommen
    $menufac->update ("lft =lft+ (CASE 
                                    WHEN lft > ".$menupunktarray[1]." 
                                    THEN 2 
                                    ELSE 0 
                                  END), 
                       rgt = rgt+2 
                       WHERE lft >=".$menupunktarray[1]);
    // Datensatzarray
    $artikel[]="";
    $artikel[]=$menupunktarray[0];
    $artikel[]=$_POST['name'];
    $artikel[]=($linkarray[0]=="modules")?$modul->module:$func->convertUmlaut($_POST['name']);
    $artikel[]=$linkarray[1];
    $artikel[]=$linkarray[0];
    $artikel[]=$menupunktarray[1]+1;
    $artikel[]=$menupunktarray[1]+2;
    // Datensatz schreibene
    $menufac->table="jos_menu";
    $menufac->write($artikel);
    $_SESSION['msg']="Menupunkt erfolgreich angelegt!";    
#########################################
# Wenn Menupunkt geupdatet werden soll. #
#########################################
elseif ($_GET['mode']=="update"):
    // Wenn Element gefunden
    if ($menu=$menufac->getElement()):
        // Wenn Zuordnung zu Hauptpunkten neu erfolgt ist
        if (($menupunktarray[1]<$menu->lft && $menupunktarray[2]<$menu->rgt)||($menupunktarray[1]>$menu->lft && $menupunktarray[2]>$menu->rgt)):
            // Die Reihenfolge neu setzen
            $menufac->update("lft = CASE 
                                      WHEN lft = ".$menu->lft." AND ".$menu->lft." < ".$menupunktarray[1]."
                                      THEN ".$menupunktarray[1]."-1
                                      WHEN lft = ".$menu->lft." AND ".$menu->lft." > ".$menupunktarray[1]."
                                      THEN ".$menupunktarray[1]."+1  
                                      WHEN lft > ".$menu->lft." AND lft <= ".$menupunktarray[1]."
                                      THEN lft-2
                                      WHEN lft > ".$menupunktarray[1]." AND lft < ".$menu->lft." +2
                                      THEN lft+2 
                                      ELSE lft
                                    END,
                              rgt = CASE 
                                      WHEN rgt = ".$menu->rgt." AND ".$menu->rgt." < ".$menupunktarray[2]."
                                      THEN ".$menupunktarray[1]."
                                      WHEN rgt = ".$menu->rgt." AND ".$menu->rgt." > ".$menupunktarray[2]."
                                      THEN ".$menupunktarray[1]."+2  
                                      WHEN rgt >= ".$menu->rgt." AND rgt < ".$menupunktarray[1]."
                                      THEN rgt-2
                                      WHEN rgt < ".$menu->rgt." AND rgt > ".$menupunktarray[1]."
                                      THEN rgt+2                  
                                      ELSE rgt
                                    END");       
        endif;
        // Datensatz updaten
        $menufac->update("menutype='12',
                           name='".mysql_real_escape_string($_POST['name'])."',
                          alias='".mysql_real_escape_string((($linkarray[0]=="modules")?$modul->module:$func->convertUmlaut($_POST['name'])))."',
                           link='".mysql_real_escape_string($linkarray[1])."',
                           type='".mysql_real_escape_string($linkarray[0])."'","id",$_GET['id']);
    endif;
    $_SESSION['msg']="Menupunkt erfolgreich geändert!";    
############################################
# Wenn Menupunkte verschoben werden sollen #
############################################
elseif($_GET['mode']=="positionup" || $_GET['mode']=="positiondown"):
    // Element mit Nebenstehenden Element und letztewr vergebener Orderzahl holen
    $menufac->table="jos_menu";
    $menufac->felder="jos_menu.lft, jos_menu.rgt, jos_menu.menutype, a.lft nlft, a.rgt nrgt";
    // SQL-Query zum Abfragen der beiden zu tauschenden Elemente erstellen und ausführen
    $menufac->getAll(" LEFT JOIN jos_menu a on ( ".($_GET['mode']=='positionup'?'a.rgt':'a.lft')."= ".($_GET['mode']=='positionup'?'jos_menu.lft-1':'jos_menu.rgt+1').") WHERE jos_menu.id = '".intval(mysql_real_escape_string($_GET['id']))."'");
    $menupoint=$menufac->getElement();
    if (!empty($menupoint->nrgt) && !empty ($menupoint->nlft)):
        // Datensätze updaten (Positionsverschiebung) 
        $menufac->update("lft = CASE
                                  WHEN lft <= ".($_GET['mode']=='positionup'?$menupoint->nrgt:$menupoint->rgt)."
                                  THEN lft + ".($_GET['mode']=='positionup'?$menupoint->rgt-$menupoint->lft+1:$menupoint->nrgt-$menupoint->nlft+1)." 
                                  ELSE lft - ".($_GET['mode']=='positionup'?$menupoint->nrgt-$menupoint->nlft+1:$menupoint->rgt-$menupoint->lft+1)."
                                END,
                          rgt = CASE
                                  WHEN rgt <= ".($_GET['mode']=='positionup'?$menupoint->nrgt:$menupoint->rgt)."
                                  THEN rgt + ".($_GET['mode']=='positionup'?$menupoint->rgt-$menupoint->lft+1:$menupoint->nrgt-$menupoint->nlft+1)."
                                  ELSE rgt - ".($_GET['mode']=='positionup'?$menupoint->nrgt-$menupoint->nlft+1:$menupoint->rgt-$menupoint->lft+1)."
                                END
                          WHERE
                          lft BETWEEN 
                                ".($_GET['mode']=='positionup'?$menupoint->nlft:$menupoint->lft)." 
                              AND 
                                ".($_GET['mode']=='positionup'?$menupoint->rgt:$menupoint->nrgt));
    endif;
    $_SESSION['msg']="Sortierung erfolgreich geändert!";  
endif;
header("Location:".$l->makeUrl(WEBDIR."admin/menu/view.php?".ereg_replace("mode=".$_GET['mode']."&","",ereg_replace("id=".$_GET['id']."&","",$_SERVER['QUERY_STRING']))));
?>